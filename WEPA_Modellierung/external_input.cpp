#include "external_input.h"

void set_map(const vector<string> & members, map<string, unsigned int> & member_map)
{
	member_map.clear();
	int i;
	for (i = 0; i < members.size(); ++i)
	{
		member_map.insert(pair<string, int>(members.at(i), i));
	}
}

int get_index(const string & key, const map<string, unsigned int> & keymap)
{
	map<string, unsigned int>::const_iterator it = keymap.find(key);
	if (it == keymap.end())
	{
		return -1;
	}
	return (*it).second;
}

void adapt_plant_map(map<string, unsigned int> & plant_map, map<string,unsigned int> & new_plant_map)
{
	int index;
	string plant;
	for (map<string, unsigned int>::iterator it = plant_map.begin(); it != plant_map.end(); ++it)
	{
		index = (*it).second;
		plant = (*it).first;
		//plant_map.erase(it);
		if (plant == "PL58573")
		{
			new_plant_map.insert(pair<string, unsigned int>("Poland", index));
		}
		else if (plant == "DE59757"/*"M�schede"*/)
		{
			new_plant_map.insert(pair<string, unsigned int>("Mueschede", index));
		}
		else if (plant == "DE34431"/*"Giershagen"*/)
		{
			new_plant_map.insert(pair<string, unsigned int>("Giershagen", index));
		}
		else if (plant == "DE06237"/*"Leuna"*/)
		{
			new_plant_map.insert(pair<string, unsigned int>("Leuna", index));
		}
		else if (plant == "DE55120"/*"Mainz"*/)
		{
			new_plant_map.insert(pair<string, unsigned int>("Mainz", index));
		}
		else if (plant == "IT55100" /*Lucca bzw. Salanetti*/)
		{
			new_plant_map.insert(pair<string, unsigned int>("Lucca", index));
		}
		else if (plant == "IT03043" /*Cassino*/)
		{
			new_plant_map.insert(pair<string, unsigned int>("Cassino", index));
		}
		else if (plant == "DE09648"/*"Kriebstein"*/)
		{
			new_plant_map.insert(pair<string, unsigned int>("Kriebstein", index));
		}
		else if (plant == "FR59166"/*"Lille"*/)
		{
			new_plant_map.insert(pair<string, unsigned int>("Lille", index));
		}
		else if (plant == "ES50600"/*"Ejea"*/)
		{
			new_plant_map.insert(pair<string, unsigned int>("GC", index));
		}
		else if (plant == "FR10000")
		{
			
		}
		else
		{
			cout << "Fuer das Werk " << (*it).first << " ist kein Mapping enthalten!" << endl;
			exit(1);
		}
	}

	/*for (map<string, unsigned int>::iterator it = new_plant_map.begin(); it != new_plant_map.end(); ++it)
	{
		cout << (*it).first << " " << (*it).second << endl;
	}

	char uio;
	cin >> uio;*/
}

void get_start_inventories(const string & filename, const vector<string> & artnrs, const vector<string> & plants, vector<vector<int> > start_inventory)
{
	
	start_inventory.resize(plants.size(), vector<int>(artnrs.size(), 0));

	string artnr;
	string plant;
	string plant1;
	string plant2;
	int artnr_index;
	int plant_index;

	map<string, unsigned int> artnr_map;
	map<string, unsigned int> plant_map_old;
	map<string, unsigned int> plant_map_new;
	set_map(artnrs, artnr_map);
	set_map(plants, plant_map_old);
	adapt_plant_map(plant_map_old, plant_map_new);

	ifstream data(filename.c_str());
	if (data.fail())
	{
		cout << "Kann Datei " << filename << " nicht oeffnen!" << endl;
		exit(1);
	}

	string line;
	while (!data.eof())
	{
		getline(data, line);
		if (line.size() == 0)
		{
			continue;
		}
		vector<string> daten;
		readin_explode(line, daten, ';');
		if (daten.size() < 2)
		{
			continue;
		}

		artnr = daten.at(1);
		if (artnr.size() < 5)
		{
			artnr = "0" + artnr;
		}
		plant = daten.at(2);

		/*cout << artnr << " - - " << plant << endl;

		char op;
		cin >> op;*/

		artnr_index = get_index(artnr, artnr_map);

		if (artnr_index == -1)
		{
			continue;
		}

		istringstream is(plant);
		is >> plant1;

		if (plant1 != "Grp1")
		{
			continue;
		}

		is >> plant2;

		plant_index = get_index(plant2, plant_map_new);

		/*cout << "plant: " << plant << endl;
		cout << "plant2: " << plant2 << endl;
		cout << "index: " << plant_index << endl;*/

		if (plant_index == -1)
		{
			continue;
		}

		/*cout << "plant: " << plant2 << endl;
		cout << "plant_index: " << plant_index << endl;
		char op;
		cin >> op;*/

		start_inventory.at(plant_index).at(artnr_index) = atoi(daten.at(3).c_str());

	
		
	}

	/*for (i = 0; i < plants.size(); ++i)
	{
		int sum = 0;
		for (int j = 0; j < artnrs.size(); ++j)
		{
			sum+= start_inventory.at(i).at(j);

		}
		cout << plants.at(i) << ": " << sum << endl;
	}

	char oi;
	cin >> oi;*/


	data.close();
}


void get_real_production(const string & filename, const vector<string> & artnrs, const vector<string> & resources, const vector<production> & processes, vector<vector<double> > & fixations, const vector<double> & nPallets_per_ton)
{
	fixations.clear();
	fixations.resize(processes.size(), vector<double>(3, -1.0));

	int i,j;

	map<string, unsigned int> artnr_map;
	map<string, unsigned int> resource_map;
	set_map(artnrs, artnr_map);
	set_map(resources, resource_map);

	string line;

	ifstream data(filename.c_str());
	if (data.fail())
	{
		cout << "Kann Datei " << filename << " nicht oeffnen!" << endl;
		exit(1);
	}
	getline(data, line);
	getline(data, line);
	getline(data, line);

	string resource;
	string artnr;
	int resourceindex;
	int artnrindex;
	vector<double> tons(3, 0.0);

	while (!data.eof())
	{
		getline(data, line);
		if (line.size() == 0)
		{
			continue;
		}
		vector<string> daten;
		readin_explode(line, daten, ';');
		if (daten.size() < 4)
		{
			continue;
		}
		resource = daten.at(1);
		artnr = daten.at(2);

		resourceindex = get_index(resource, resource_map);
		artnrindex = get_index(artnr, artnr_map);

		/*if (artnrindex == 0)
		{
			cout << line << endl;
			cout << artnrindex << endl;
			cout << resourceindex << endl;
			cout << daten.size() << endl;
			char p0;
			cin >> p0;
		}*/


		/*if (artnrindex == 1112)
		{
			cout << line << endl;
			char op;
			cin >> op;
		}*/

		if (resourceindex == -1 || artnrindex == -1)
		{
			continue;
		}

		tons.at(0) = 0.0;
		tons.at(1) = 0.0;
		tons.at(2) = 0.0;

		if (daten.size() > 3)
		{
			if (daten.at(3).size() > 0)
			{
				tons.at(0) = atof(daten.at(3).c_str());
			}
		}
		if (daten.size() > 4)
		{
			if (daten.at(4).size() > 0)
			{
				tons.at(1) = atof(daten.at(4).c_str());
			}
		}

		if (daten.size() > 5)
		{
			if (daten.at(5).size() > 0)
			{
				tons.at(2) = atof(daten.at(5).c_str());
			}
		}

		/*if (artnrindex == 0)
		{
			cout << "test" << endl;
			cout << tons.at(0) << " " << tons.at(1) << " " << tons.at(2) << endl;
			char op;
			cin >> op;
		}*/
		
		/*if (artnrindex == 1112)
		{
			cout << tons.at(0) << " " << tons.at(1) << " " << tons.at(2) << endl;
			char pop;
			cin >> pop;
		}*/
		

		for (i = 0; i < processes.size(); ++i)
		{
			if (processes.at(i).machine == resourceindex && processes.at(i).product == artnrindex)
			{
				/*if (artnrindex == 867)
				{
					cout << line << endl;
					cout << "Prozessindex " << i << endl;

					cout << processes.at(i).tonspershift << endl;
					cout << nPallets_per_ton.at(artnrindex) << endl;

					
					char op;
					cin >> op;
				}*/
				for (j = 0; j < tons.size(); ++j)
				{
					if (tons.at(j) > 0.0)
					{
						fixations.at(i).at(j) = tons.at(j) / processes.at(i).tonspershift;
					}
					else
					{
						fixations.at(i).at(j) = -1.0;
					}
				}
				/*if (artnrindex == 1112)
				{
					cout << fixations.at(i).at(0) << endl;
					cout << fixations.at(i).at(1) << endl;
					cout << fixations.at(i).at(2) << endl;

					char op;
					cin >> op;
				}*/
				break;
			}
		}

		/*if (artnrindex == 1112)
		{
			cout << tons.at(0) << " " << tons.at(1) << " " << tons.at(2) << endl;
			char pop;
			cin >> pop;
		}*/
	}

	/*for (i = 0; i < resources.size(); ++i)
	{
		double value = 0.0;
		for (j = 0; j < fixations.size(); ++j)
		{
			if (processes.at(j).machine == i)
			{
				if (fixations.at(j).at(2) > 0.0)
				{
					value += fixations.at(j).at(2) * processes.at(j).tonspershift;
				}
			}
		}
		cout << "Resource: " << resources.at(i) << ": " << value << endl;
	}*/

	data.close();
/*
	for (i = 0; i < fixations.size(); ++i)
	{
		for (j = 0; j < fixations.at(i).size(); ++j)
		{
			cout << fixations.at(i).at(j) << " ";
		}
		cout << endl;
	}
	char oi;
	cin >> oi;*/

	//char oi;
	//cin >> oi;
}

void get_external_endinventories2(const vector<vector<double> > & fixations, const vector<vector<double> > & initial_inventories, vector<vector<double> > & minimum_inventories, const int & nProducts, const vector<production> & processes, const int & nPlants, const vector<double> & nPallets_per_ton, const vector<demand> & demands, const vector<string> & products, const int & nPeriods)
{
	int i, j;
	int productindex;
	int periodindex;

	minimum_inventories.clear();
	minimum_inventories.resize(nProducts,vector<double>(nPeriods,0.0));

	vector<vector<double> > product_demands(nProducts,vector<double>(nPeriods-1,0.0));


	for (i = 0; i < demands.size(); ++i)
	{
		productindex = demands.at(i).product;
		periodindex = demands.at(i).period;

		product_demands.at(productindex).at(periodindex) += demands.at(i).amount;
	}


	vector<vector<double> > real_production(nProducts,vector<double>(nPeriods-1,0.0));
	for (i = 0; i < processes.size(); ++i)
	{
		productindex = processes.at(i).product;
		
		for (j = 0; j < nPeriods-1; ++j)
		{
			if (fixations.at(i).at(j) > 0.0)
			{
				real_production.at(productindex).at(j) += fixations.at(i).at(j) * processes.at(i).tonspershift * nPallets_per_ton.at(processes.at(i).product);
			}
		}
	}

	for (i = 0; i < nProducts; ++i)
	{
		for (j = 0; j < nPlants; ++j)
		{
			minimum_inventories.at(i).at(0) += initial_inventories.at(j).at(i);
		}
	}

	for (i = 0; i < nProducts; ++i)
	{
		for (j = 1; j < nPeriods; ++j)
		{
			/*if (products.at(i) == "112123")
			{
				cout << j << endl;
			}*/

			minimum_inventories.at(i).at(j) = minimum_inventories.at(i).at(j-1);
			/*if (i == 732)
			{
				cout << "products.at(i)" << endl;
				cout << minimum_inventories.at(i).at(j) << endl;

				char oi;
				cin >> oi;
			}*/

			minimum_inventories.at(i).at(j) += real_production.at(i).at(j-1);
			/*if (products.at(i) == "112123")
			{
				cout << minimum_inventories.at(i).at(j) << endl;
			}*/

			minimum_inventories.at(i).at(j) -= product_demands.at(i).at(j-1);
			/*if (products.at(i) == "112123")
			{
				cout << minimum_inventories.at(i).at(j) << endl;
			}*/
			/*if (products.at(i) == "112123")
			{
				char oi;
				cin >> oi;
			}*/

			//falls mehr transportiert wird als Bestand und Produktion verf�gbar
			if (minimum_inventories.at(i).at(j) < 0.0)
			{
				minimum_inventories.at(i).at(j) = 0.0;
			}
		}
	}

	/*for (j = 0; j < nPeriods; ++j)
	{
		cout << "mininv " << minimum_inventories.at(733).at(j) << endl;
		cout << "prod " << real_production.at(733).at(j) << endl;
		cout << "dem " << product_demands.at(733).at(j) << endl;
		//cout << "fix " << fixations.at(126).at(j) << endl;
		//cout << "t/shift " << processes.at(126).tonspershift << endl;
		cout << "pallet " << nPallets_per_ton.at(733) << endl;
		cout << endl;
	}
	char op;
	cin >> op;*/

	/*for (i = 0; i < nProducts; ++i)
	{
		if (products.at(i) == "112123")
		{
			cout << products.at(i) << " ";
		
			for (j = 0; j < nPeriods; ++j)
			{
		
				cout << "Bestand: " << minimum_inventories.at(i).at(j);
				if (j > 0)
				{
					cout << " hist.: " << real_production.at(i).at(j-1) << " " << "Bedarf: " << product_demands.at(i).at(j-1);
				}
				cout << endl;
			}
			char op;
			cin >> op;
		}
	}*/
}

void get_external_endinventories(const vector<vector<double> > & fixations, const vector<vector<double> > & initial_inventories, vector<double> & minimum_inventories, const int & nProducts, const vector<production> & processes, const int & nPlants, const vector<double> & nPallets_per_ton, const vector<demand> & demands, const vector<string> & products)
{
	int i;
	int j;
	int productindex;

	minimum_inventories.clear();
	minimum_inventories.resize(nProducts, 0.0);

	vector<int> product_demands(nProducts, 0);
	for (i = 0; i < demands.size(); ++i)
	{
		productindex = demands.at(i).product;
		product_demands.at(productindex) += demands.at(i).amount;
	}

	vector<double> amount_production(nProducts, 0.0);
	for (i = 0; i < nProducts; ++i)
	{
		for (j = 0; j < nPlants; ++j)
		{
			//cout << j << " " << initial_inventories.size() << endl;
			amount_production.at(i) += initial_inventories.at(j).at(i);
		}
	}

	for (i = 0; i < processes.size(); ++i)
	{
		productindex = processes.at(i).product;
		for (j = 0; j < fixations.at(i).size(); ++j)
		{
			if (fixations.at(i).at(j) > 0.0)
			{
				amount_production.at(productindex) += fixations.at(i).at(j) * processes.at(i).tonspershift * nPallets_per_ton.at(processes.at(i).product);
			}
		}
	}
	
	for (i = 0; i < nProducts; ++i)
	{
		minimum_inventories.at(i) = amount_production.at(i) - product_demands.at(i);
		if (minimum_inventories.at(i) < 0.0)
		{
			minimum_inventories.at(i) = 0.0;
		}
	}
}