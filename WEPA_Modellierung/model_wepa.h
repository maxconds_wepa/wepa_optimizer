/*! \file
* class for modelling and solving
*/

#ifndef MODEL_WEPA_H
#define MODEL_WEPA_H

#include <vector>
#include <ilcplex/cplex.h>
#include <sstream>
#include <fstream>
#include <map>

#include "cplex_model.h"
#include "data.h"
#include "bib.h"
#include "timemeasurement.h"
#include<unordered_set>
using namespace std;

/*!
* \brief class for modelling and solving
*/


class Wepa
{
public:
	unordered_set<int> m_additionalAllowedProcessesPlantFixations;
	/*!
	* constructor
	* \param nPlants number of plants
	* \param plants ordered names of plants
	* \param nCustomers number of customers
	* \param customers ordered names of customers
	* \param nResources number of machines
	* \param resources ordered names of machines
	* \param nProducts number of prodcuts
	* \param products ordered article numbers
	* \param nPeriods number of periods (=number of considered periods in sales plan+1)
	* \param periods ordered names of periods
	* \param nProcesses number of processes
	* \param processes ordered processes consisting of product index, machine index, tons per shift, cost per shift, plant index
	* \param capacity capacities per machine and period
	* \param transport_plant ordered ic-transport informations consisting of index of start plant, index of end plant, transport cost per pallet
	* \param transport_customer ordered customer transport informations consisting of index of plant, index of customer, transport cost per pallet
	* \param demands demand list consisting of customer index, product index, period index and amount in pallets
	* \param nPallets_per_ton amount of pallets per ton for ordered products
	* \param min_runtime minimum lotsize
	* \param min_for_split minimium lotsize for which splitting to more than one plant is allowed
	* \param lkw_capacity lkw capacity in pallets
	* \param baskets baskets
	* \param basket_coherence basket indices per demand index
	* \param customer_for_basket customer index per basket index
	* \param start_inventory start inventory per plant and product
	* \param min_inventory_v2 minimum inventory per product and period
	* \param min_inventory_v1 minimum inventory per plant, product and period
	* \param plant_capacities internal warehouse capacity per plant
	* \param fixations runtime in shifts per process and period according to the sales plan
	* \param set_fixations fix production according to sales plan?
	* \param external_warehouse_cost external warehouse cost
	* \param fixed_transports fixed transports
	*/
	Wepa(const int & nPlants, 
		const vector<string> & plants, 
		const int & nCustomers, 
		const vector<string> & customers, 
		const int & nResources, 
		const vector<string> & resources, 
		const int & nProducts, 
		const vector<string> & products, 
		const int & nPeriods, 
		const vector<string> & periods, 
		const int & nProcesses, 
		const vector<production> & processes, 
		const vector<vector<double> > & capacity, 
		const vector<transports> & transport_plant, 
		const vector<transports> & transport_customer, 
		const vector<demand> & demands, 
		const vector<demand> & finalPeriodMinInventoryUsagePerCustomer,
		const vector<double> & nPallets_per_ton, 
		const double & min_runtime, 
		const double & min_for_split, 
		const int & lkw_capacity, 
		const vector<vector<int> > & baskets, 
		const vector<vector<int> > & basket_coherence, 
		const vector<int> & customer_for_basket, 
		const vector<vector<double> > & start_inventory, 
		const vector<vector<double> > & min_inventory_v2, 
		const vector<vector<vector<double> > > & min_inventory_v1, 
		const vector<int> & plant_capacities, 
		const vector<vector<double> > & fixations, 
		const bool & set_fixations, 
		const double & external_warehouse_cost, 
		const vector<fixed_transport> & fixed_transports, 
		const string & logfilename, 
		const string & expansion_stage,
		const double fixationThreshold,
		//const vector<article_customer_fixations> & customer_fixations
		Article_customer_fixation_collection & custFixations,
		const vector<int> & resourcesWithFixations,
		Customer_product_related_transports_collection & cpTransports
		);

	/*!
	* \brief destructor
	*/
	~Wepa();

	//data mining
	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	/*! 
	*
	* \brief preprocessing, additional information: can products be produced in plants, is there a demand for a product, connect edges for transportation with transport information, construct relevant edges for optimization
	*/
	void preprocessing();
	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	/*!
	* adapt the start inventory, so that the fixations together with the start inventory can fulfill all demands
	* \param fixations fixations per process and period
	*/
	void adapt_start_inventory();


	//generation of basis model
	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	/*! 
	* \brief init all variables 
	*/
	void init_vars();
	/*!
	* \brief init all constraints
	*/
	void init_cons();
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	//adaption of optimization model according to fixations
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	/*!
	* \brief adapt the split constraint according to the fixation
	*/
	void adapt_split_injuries();
	/*!
	* adapt the minimum runtimes for all processes and all products according to the fixations
	*/
	void adapt_minimum_production();
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	//fixations
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	/*!
	* fix the production accordingly to the fixations
	* \param fixations fixations per process and period
	*/
	void set_production_fixations(const vector<vector<double> > & fixations);

	void set_production_fixations_for_resources_with_fixations (const vector<vector<double>> & fixations);

	void set_production_fixations_on_threshold(const vector<vector<double> > & fixations);

	void set_production_fixations_for_period(const vector<vector<double> > & fixations,int t);

	void set_production_fixations_on_threshold_for_period(const vector<vector<double> > & fixations,int t);

	void set_production_fixations_for_resources_with_fixations_for_period (const vector<vector<double>> & fixations,int t);

	/*!
	* \fix production to plants from the fixation
	*/
	void fix_plant_production();
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	void fix_plant_production_for_period(int t);

	//basket separation
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	/*!
	* prepare to separate baskets in the current solution, first step: save the current transport solutions
	* \param solutions amount to be transported per period, plant, customer and product
	*/
	void separate_baskets_part1(vector<vector<bool> > & solutions);

	void separate_baskets_part1_for_period(vector<vector<bool> > & solutions,int t);

	/*!
	* add the constraints and variables for basket separation
	* \param solutions is there a transport for plant index and demand index?
	*/
	bool separate_baskets_part2(const vector<vector<bool> > & solutions);

	bool separate_baskets_part2_for_period(const vector<vector<bool> > & solutions,int t);
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	bool isValidConnectionWithRespectToBaskets (int plant,int demandIndex);
	//solving
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	/*!
	* solve current cplex model with given epgap
	* \param epgap gap for cplex
	* \param polish use(1) or don't use(0) solution polishing
	* \param gap realized gap after solving
	*/
	double solve_model(const double & epgap, 
					   const bool & polish, 
					   double & gap,
					   bool is_fix_plant_production,
					   bool use_baskets,
					   bool set_fixations,
					   bool just_production_cost,
					   TM &tm,
					   vector<production_result> & solution_production, 
					   vector<customer_group_production_result> & customer_group_solution_production,
					   vector<single_transport_result_plant> & solution_single_transport_plant, 
					   vector<single_customer_group_transport_result_plant> & customer_group_solution_single_transport_plant,
					   vector<single_transport_result_customer> & solution_single_transport_customer, 
					   vector<dummy_result> & solution_dummy, 
					   vector<inventory_result> & solution_inventory, 
					   vector<customer_group_inventory_result> & customer_group_solution_inventory,
					   vector<vector<pair<double, double> > > & solution_external_warehouse_info,
					   list<int> & solution_additionalAllowedProcessesPlantFixations,
					   bool considerCustomerProductionPriorities,
					   bool simultaneousConsiderationCapacityDummiesAndProductionProcessPriorities
					   );
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	//results
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	/*!
	* save solution for current period
	* \param solution_production production results consisting of product index, machine index, period index, runtime in shifts and process index
	* \param solution_single_transport_plant ic-transport results consisting of product index, start plant index, end plant index, period index, transported amount in pallets
	* \param solution_single_transport_customer customer transport results consisting of product index, plant index, customer index, period index, transported amount in pallets
	* \param solution_dummy results for usage of dummy capacities consisting of machine index, period index, amount of dummy usage
	* \param solution_inventory inventory results
	* \param period current period (beginning with 1)
	* \param external_warehouse_info results for the inventory of the external warehouses
	*/
	void write_results_for_period(vector<production_result> & solution_production,
								  vector<customer_group_production_result> & solution_customer_group_production, 
								  vector<single_transport_result_plant> & solution_single_transport_plant,
								  vector<single_customer_group_transport_result_plant> & solution_customer_group_single_transport_plant,
								  vector<single_transport_result_customer> & solution_single_transport_customer, 
								  vector<dummy_result> & solution_dummy, 
								  vector<inventory_result> & solution_inventory,
								  vector<customer_group_inventory_result> & solution_customer_group_inventory,
								  const int & period, 
								  vector<vector<pair<double, double> > > & external_warehouse_info,
								  bool isDummies
								  );
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	

	//cleaning
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	/*!
	* \brief clean current cplex model
	*/
	void clean_model();
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

private:
	void resetVarsForZeroPeriod();
	void resetVarsForPeriod(int period);
	vector<double>m_customerProductionProcessDummyCost;
	vector<bool>m_isAllowedProcessesWithPrioritizations;
	bool m_isConsiderCustomerProductionPriorities;
	bool m_isSimultaneousConsiderationCapacityDummiesAndProductionProcessPriorities;
	const double TOLERANCE_BASKET_BINARIES_TRANSPORT_VARIABLE_CONNECTION;//BasketBinariesTransportVariableConnection=0.01;
	const double & m_fixationThreshold;
	//input
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	/*!
	* \brief number of plants
	*/
	const int & m_nPlants;
	/*!
	* \brief ordered names of plants
	*/
	const vector<string> & m_plants;
	/*!
	* \brief number of customers
	*/
	const int & m_nCustomers;
	/*!
	* \brief ordered names of customers
	*/
	const vector<string> & m_customers;
	/*!
	* \brief number of machines
	*/
	const int & m_nResources;
	/*!
	* \brief ordered names of machines
	*/
	const vector<string> & m_resources;
	/*!
	* \brief number of products
	*/
	const int & m_nProducts;
	/*!
	* \brief ordered article numbers 
	*/
	const vector<string> & m_products;
	/*!
	* \brief number of periods
	*/
	const int & m_nPeriods;
	/*!
	* \brief ordered period names
	*/
	const vector<string> & m_periods;
	/*!
	* \brief number of processes 
	*/
	const int & m_nProcesses;
	/*!
	* \param ordered processes consisting of product index, machine index, tons per shift, cost per shift, plant index
	*/
	const vector<production> & m_processes;
	/*!
	* \brief capacity per machine and period
	*/
	const vector<vector<double> > & m_capacity;
	/*!
	* \brief ordered ic-transport informations consisting of index of start plant, index of end plant, transport cost per pallet
	*/
	const vector<transports> & m_transport_plant;
	/*!
	* \brief ordered customer transport informations consisting of index of plant, index of customer, transport cost per pallet
	*/
	const vector<transports> & m_transport_customer;
	/*!
	* \brief demand list consisting of customer index, product index, period index and amount in pallets
	*/
	const vector<demand> & m_demands;
	/*!
	* \brief amount of pallets per ton for ordered products
	*/
	const vector<double> & m_nPallets_per_ton;
	/*!
	* \brief minimum lotsize
	*/
	const double & m_min_runtime;
	/*!
	* \brief (adapted) amount of minimum shifts for which splitting of a product to plants is allowed
	*/
	vector<vector<double> > m_minsplits;
	/*!
	* \brief minimium lotsize for which splitting to more than one plant is allowed
	*/
	const double & m_min_for_split;
	/*!
	* \brief lkw capacity in pallets
	*/
	const int & m_lkw_capacity;
	/*!
	* \brief external warehouse cost
	*/
	const double & m_external_warehouse_cost;
	/*!
	* \brief cost for dummy machines
	*/
	double m_dummy_cost;
	/*!
	* \brief set of baskets given by the corresponding demand indices
	*/
	const vector<vector<int> > & m_baskets;
	/*!
	* \brief per product index indices of relevant baskets
	*/
	const vector<vector<int> > & m_basket_coherence;
	/*!
	* \brief per basket index customer index
	*/
	const vector<int> & m_customer_for_basket;
	/*!
	* \brief minimum lotsize per process and period
	*/
	vector<vector<double> > m_minruns;	
	/*!
	* \brief big M or maximum production in shifts per product
	*/
	vector<double> m_bigM;
	/*!
	* \brief per product and period, if true constraint that product cant be splitted can be injured
	*/
	vector<vector<bool> > m_splittable_product;
	/*!
	* \brief per product and plant: is production possible?
	*/
	vector<vector<bool> > m_product_in_plant;
	/*!
	* \brief per customer and product: does a demand exists?
	*/
	vector<vector<bool> > m_is_demand_for_customer_and_product;
	/*!
	* \brief per customer, product and period: does a demand exist?
	*/
	vector<vector<vector<bool> > > m_is_demand_for_customer_and_product_in_period;
	/*!
	* \brief per start plant and end plant, the index of the corresponding entry in m_transport_plant
	*/
	vector<vector<int> > m_transport_indices_plant;
	/*!
	* \brief per plant and customer, the index of the corresponding entry in m_transport_customer
	*/
	vector<vector<int> > m_transport_indices_customer;
	/*!
	* \brief per start plant and end plant and product: is there a edge for ic-transportation
	*/
	vector<vector<vector<bool> > > m_transport_necessary_for_product;
	/*!
	* \brief per plant and customer: exists a connection
	*/
	vector<vector<bool> > m_plant_for_customer;
	/*!
	* \brief maximum demand in m_demands
	*/
	double m_max_demand;
	/*!
	* \brief start inventory per plant and product
	*/
	vector<vector<double> > m_start_inventory;
	/*!
	* \brief minimum inventory per plant, product and period
	*/
	const vector<vector<vector<double> > > & m_min_inventory_v1;
	/*!
	* \brief resources where fixations have to be applied as given by the historical productions
	*/
	const vector<int> & m_resourcesWithFixations;
	bool *resourcesWithFixationsBool;
	/*
	* \brief Minimum inventory usage per customer
	*/
	const vector <demand> m_finalPeriodMinInventoryPerCustomer;
	/*!
	* \brief minimum inventory per product and period
	*/
	vector<vector<double> >  m_min_inventory_v2;
	/*!
	* \brief internal inventory capacity per plant
	*/
	const vector<int> & m_plant_capacities;
	/*!
	* \brief processes for product
	*/
	vector<vector<int> > m_processes_for_product;
	/*!
	* \brief can the given process produce the given product?
	*/
	vector<vector<bool> > m_is_process_for_product;
	/*!
	* \brief can the given machine produce the given product?
	*/
	vector<vector<bool> > m_is_resource_for_product;
	/*!
	* \brief is there a demand for the given product?
	*/
	vector<bool> m_is_demand_for_product;
	/*!
	* \brief is there a demand for the given product in the given period=
	*/
	vector<vector<bool> > m_is_demand_for_product_and_period;
	/*!
	* \brief is there a demand for the customer
	*/
	vector<bool> m_is_demand_for_customer;
	/*!
	* \brief inventory differences for feasibility
	*/
	vector<vector<double> > m_inventory_difference;
	/*!
	* \brief production fixations for customer products
	*/
	//vector<article_customer_fixations> m_customer_fixations;
	Article_customer_fixation_collection & m_custFixations;
	/*!
	* \brief customer product related transports
	*/
	Customer_product_related_transports_collection & m_cpTransports;
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	

	//preparation
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	/*!
	* \brief determining all needed ic transports for the optimization problem neglecting baskets
	* if a direct customer transport exists, try to find a cheaper transport with ic plant, and add the cheapest, if no direct transport exists, add the cheapest
	*/
	void get_relevant_edges_without_baskets();
	/*!
	\brief get all for the customer period-independent relevant plants (inventory or production)
	*/
	void get_relevant_plants_for_customer();
	/*!
	* mark the necessarity to add an ic transport variable for the product from start to end plant
	* \param product product index
	* \param plant1 index of start plant
	* \param plant2 index of end plant
	*/
	void set_transport_possibility(const int & product, const int & plant1, const int & plant2);
	/*!
	* set the necessarity to add a transport variable from the plant to the customer
	* \param plant plant index
	* \param customer customer index
	*/
	void set_plant_relevant_for_customer(const int & plant, const int & customer);
	/*!
	* \brief determining if there is production possible at a location
	*/
	void get_if_production_possible();
	/*!
	* \brief computation of suitable dummy machine costs according to input data
	*/
	void get_dummy_cost();
	/*!
	* \brief computation of suitable maximum runtimes per process according to input data
	*/
	void get_bigM();
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	//details
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	/*!
	* can the product be produced by the plant?
	* \param plant plant index
	* \param product product index
	*/
	bool produces_plant_product(const int & plant, const int & product);
	/*!
	* exists a transport possibility from the start plant to the end plant?
	* \param plant1 index of start plant
	* \param plant2 index of end plant
	*/
	bool exists_transport_possibility_plant(const int & plant1, const int & plant2);
	/*!
	* exists a transport possibility from the plant to the customer?
	* \param plant plant index
	* \param customer customer index
	*/
	bool exists_transport_possibility_customer(const int & plant, const int & customer);
	/*!
	* has the customer a demand for the product?
	* \param product product index
	* \param customer customer index
	*/
	bool is_product_relevant_for_customer(const int & product, const int & customer);
	/*!
	* does the plant produce a product product relevant for the customer or does there exist a start inventory for a customer relevant product?
	* \param plant plant index
	* \param customer customer index
	*/
	bool is_plant_relevant_for_customer(const int & plant, const int & customer);

	/*!
	* Do exist constraints for the given product and customer w.r.t. production facilities?
	*/
	bool is_in_customer_fixation_group (const int & product,const int & customer);


	/*!
	* returns the intercompany transport costs from the start to the end plant
	* \param plant1 index of start plant
	* \param plant2 index of end plant
	*/
	double get_ic_cost(const int & plant1, const int & plant2);
	/*!
	* returns the transport costs from plant to customer
	* \param plant index of plant
	* \param customer index of customer
	*/
	double get_customer_cost(const int & plant, const int & customer);
	/*!
	* get the index in the details for a transport from start to end plant
	* \param plant1 index of start plant
	* \param plant2 index of end plant
	*/
	int get_transport_index_plant(const int & plant1, const int & plant2);
	/*!
	* get the index in the details for a transport from plant to customer
	* \param plant index of plant
	* \param customer index of customer
	*/
	int get_transport_index_customer(const int & plant, const int & customer);
	/*!
	* get the overall start inventory for a product
	* \param product product index
	*/
	double get_overall_start_inventory_for_product(const int & product);
	/*!
	* get fixed production in pallets
	* \param product product index
	* \param period period index
	*/
	double get_production(const int & product, const int & period);
	/*!
	* get demand
	* \param product product index
	* \param period period index
	*/
	double get_demand(const int & product, const int & period);
	/*!
	* get the maximum for the output in tons per shift among all corresponding processes
	* \param plant plant index
	* \param product product index
	*/
	double get_max_tonpershift(const int & plant, const int & product);
	/*!
	* get the overall demand for the given product until the given period
	* \param product product index
	* \param period period index
	*/
	double get_overall_demand_for_product_until_period(const int & product, const int & period);
	/*!
	* \brief get the minimum runtime for the given process and period
	* \param process process index
	* \param period period index
	*/
	double get_minimum_runtime(const int & process, const int & period);
	/*!
	* is there inventory at the beginning?
	* \param product product index
	* \param plant plant index
	*/
	bool is_start_inventory_for_product_in_plant(const int & product, const int & plant);
	/*!
	* has the customer demand for the given product?
	* \param customer customer index
	* \param product product index
	*/
	bool is_demand_for_customer_and_product(const int & customer, const int & product);
	/*!
	* has the customer demand for the given product in the given period?
	* \param customer customer index
	* \param product product index
	* \param period period index
	*/
	bool is_demand_for_customer_and_product_in_period(const int & customer, const int & product, const int & period);
	/*! should a transport variable for the given product from plant1 to plant2 should be created
	* \param product product index
	* \param plant1 start plant
	* \param plant2 end plant
	*/
	bool is_transport_necessary(const int & product, const int & plant1, const int & plant2);
	/*!
	* \brief determining if there are demands per product
	*/
	void get_if_demand_for_product();
	/*!
	* \brief get the transport indices from start to end
	*/
	void get_transport_indices();
	/*!
	* \brief providing production processes per product
	*/
	void get_processes_for_product();
	/*!
	* get the maximum demand of all demands
	*/
	void get_maximum_demand();
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	//model generation
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	/*!
	* \brief dummy variable for naming of variables and constraints
	*/
	stringstream namebuf;
	/*!
	* \brief pointer to cplex instance
	*/
	CPLEX* cplex;
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//feasibility checks
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	/*!
	* \brief path for the error logfile
	*/
	string m_logfilename;
	/*!
	\brief providing inconsistencies in data concerning production
	*/
	bool get_infeasibilities_for_production();
	/*!
	\brief providing inconsistencies in data concerning transport and setting intercompany transports to guarantee feasibility to relevant
	*/
	bool get_infeasibilities_for_transport();
	/*!
	* depth first search to get cheapest transport from producing plant to customer
	* \param last last plant index
	* \param current current plant index
	* \param customer customer index
	* \param visited_plants indices for already visited plants are set to true
	* \param cost current cost
	* \param best_cost best cost
	* \param way current transport way
	* \param best_way best transport way
	* \param demandindex demand index
	*/
	void dfs(int last, int current, const int & customer, vector<bool> visited_plants, double cost, double & best_cost, vector<int> way, vector<int> & best_way, const int & demandindex);
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	//basis model
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	/*!
	* \brief indices for production variables per process and period
	*/
	vector<vector<int> > m_var_run;
	/*!
	* \brief indices for production setup variables per process and period
	*/
	vector<vector<int> > m_var_run_setup;
	/*!
	* \brief indices for transport variables per plant, customer, product and period
	*/
	vector<vector<int> > m_var_transport_customer;
	/*!
	* \brief indices for the variables of the fictitious transports per plant, customer, product in the last periods
	*/
	vector<vector<int>> m_var_fictitious_transport_customer;
	/*!
	* \brief indices for transport variables per start plant, end plant, product and period
	*/
	vector<vector<vector<vector<int> > > > m_var_transport_plant;
	/*!
	* \brief indices for inventory variables per plant, product and period
	*/
	vector<vector<vector<int> > > m_var_inventory_plant;
	/*!
	* \brief indices for inventory variables per customer, product and period
	*/
	vector<vector<vector<int> > > m_var_inventory_customer;
	/*!
	* \brief indices for external inventory variables per plant and period
	*/
	vector<vector<int> > m_var_external_inventory_plant;
	/*!
	* \brief indices for dummy variables per machine and period
	*/
	vector<vector<int> > m_var_dummy;
	/*!
	* \brief indices for setup variables for production in plant per product, plant and period
	*/
	vector<vector<vector<int> > > m_var_plant_run;
	/*!
	* \brief indices for binary variable for information if a product is splitted to more plants per product and period
	*/
	vector<vector<int> > m_var_split;
	/*!
	* \brief variable for inventory differences
	*/
	vector<vector<vector<int> > > m_var_inventory_difference;
	/*!
	\brief indices for variables for customer group related production
	*/
	vector<vector<vector<int> > > m_var_customer_group_run;
	/*!
	\brief indices for variables for customer group related inventory
	*/
	vector<vector<vector<int> > > m_var_customer_group_inventory;
	/*!
	\brief indices for variables for customer group related interplant transports
	*/
	vector<vector<vector<vector<int> > > > m_var_customer_group_transport_plant;
	/*!
	* \brief indices for capacity constraints per machine and period
	*/
	vector<vector<int> > m_cons_capacity;
	/*!
	* \brief indices for inventory constraints per customer, product and period
	*/
	vector<vector<vector<int> > > m_cons_inventory_customer;
	/*!
	* \brief indices for inventory constraints per plant, product and period
	*/
	vector<vector<vector<int> > > m_cons_inventory_plant;
	/*!
	* \brief indices for inventory constraints per plant, customer fixation group and period
	*/
	vector<vector<vector<int> > > m_cons_customer_group_inventory_plant;
	/*!
	* \brief indices for external inventory constraints per plant and period
	*/
	vector<vector<int> > m_cons_external_inventory_plant;

	/*!
	* \brief indices for inventory constraints per plant, product and period
	*/
	vector<vector<vector<int> > > m_cons_product_plant_quantity_fixation;
	/*!
	* \brief indices for minimum production constraints per process and period
	*/
	vector<vector<int> > m_cons_minrun;
	/*!
	* \brief indices for maximum production constraints or big M constraints per process and period
	*/
	vector<vector<int> > m_cons_maxrun;
	/*!
	* \brief per product and period: constraint which guarantees that product can just be produced in more plants if it is splittable
	*/
	vector<vector<int> > m_cons_ns;
	/*!
	* \brief per machine, product and period: constraint which guarantees that plant production setup variable is set to 1 if at least on production setup variable for a corresponding process is set to 1 in the plant
	*/
	vector<vector<vector<int> > > m_cons_ns_connector;
	/*!
	* \brief per product and period: constraint which guarantees that minimum runtime for splitting is adhered to
	*/
	vector<vector<int> > m_cons_ns_connector2;
	/*!
	* \brief per product and period: constraint which guarantees that minimum inventory is adhered to
	*/
	vector<vector<int> > m_cons_min_inventory_v2;
	/*!
	* \brief per product and period: constrain which guarantees that the maximum inventory difference is adhered to
	*/
	vector<vector<int> > m_cons_inventory_difference;
	/*!
	*\brief contraints that guarantee that the total sum of fictitious transports correspond to the minimum inventory level of the customer
	*/
	vector<vector<int>> m_cons_fictitious_transports_quantity_per_customer_meet_minlevel_customer;
	/*!
	*\brief contraints that guarantee that the total sum of fictitious transports from a plant to the customers for a given product do not exceed the inventory level at the plant
	*/
	vector<vector<int>> m_cons_fictitious_transports_quantity_per_customer_meet_inventorylevel_plant;
	/*!
	*\brief contraints that guarantee that the initial inventory for each product at each plant is taken into account
	*/
	vector<vector<int>> m_cons_start_inventory;


	/*!
	*\brief Initializes variables that are independent from periods
	*/
	void init_general_vars ();
	/*!
	* \brief init the initial inventory variables for artificial period 0
	*/
	void init_vars_for_zero_period();

	bool checkDummies(int period);

	bool solve_period_model(int period,
						const double & epgap, 
						 const bool & polish, 
						 double & gap,
						 bool is_fix_plant_production,
						 bool use_baskets,
						 bool set_fixations,
						 bool just_production_cost,
						 TM &tm,
						 bool isUseCapacityDummies,
						 bool isConsiderPrioritization,
						 std::string probSuffix
					);
	/*!
	* init variables for current period, without baskets
	* \param period current period (beginning with 1)
	*/
	void init_vars_for_period(const int & period,bool isUseCapacityDummies,bool isConsiderPrioritization);
	/*!
	* add the possibility for interplant transports from plant1 to plant2 for the given product in the given period
	\param plant1 index of the start plant
	\param plant2 index of the end plant
	\param product product index
	\param period period index
	*/
	bool add_ic_variable(const int & plant1, const int & plant2, const int & product, const int & period);

	bool add_customer_group_ic_variable(const int & plant1, const int & plant2, const int & cGroup, const int & period);
	/*!
	* add the possibility for customer transports from the given plant to the given customer for the given product in the given period and set a lower bound for the transport
	\param plant plant index
	\param customer customer index
	\param product product index
	\param period period index
	\param fixation value to be fixed
	*/
	void add_customer_transport_variable(const int & plant, const int & customer, const int & product, const int & period, const double & fixation);

	/*!
	*Adds the constraint that the remaining inventory at the end of the last period is assigned to fictitious transports to the customer locations in order to obtain an allocation of the production
	*to the plant locations that takes into account transport costs
	*/
	void init_minInv_LastPeriod_Delivery_Constraint();
	void init_startInv_Constraint();
	/*!
	* init constraints independent from a single period
	*/
	void init_general_cons();
	/*!
	* init constraints for current period, without baskets
	* \param period current period (beginning with 1)
	*/
	void init_cons_for_period(const int & period,bool isUseCapacityDummies);
	/*!
	* init a run variable for the given process index and period index
	* \param process process index
	* \param period period index
	*/
	bool init_run_variable(const int & process, const int & period);
	/*! init a run setup variable for the given process index and period index
	* \param process process index
	* \param period period index
	*/
	bool init_run_setup_variable(const int & process, const int & period);
	/*! init a ic variable for the given product and the given plants for the given period
	* \param product product index
	* \param plant1 index of the start plant
	* \param plant2 index of the end plant
	* \param period period index
	*/
	bool init_ic_variable(const int & product, const int & plant1, const int & plant2, const int & period);

	
	/*! init a customer transport variable for the given demand from the given plant 
	* \param plant plant index
	* \param demandindex index of the demand information
	*/
	bool init_customer_transport_variable(const int & plant, const int & demandindex);
	/*! init a plant inventory variable
	* \param plant plant index
	* \param product product index
	* \param period period index
	*/
	bool init_plant_inventory_variable(const int & plant, const int & product, const int & period);
	/*! init an external inventory variable for the given plant in the given period
	* \param plant plant index
	*/
	bool init_external_plant_inventory_variable(const int & plant, const int & period);
	/*! init a customer inventory variable 
	* \param customer customer index
	* \param product product index
	* \param period period index
	*/
	bool init_customer_inventory_variable(const int & customer, const int & product, const int & period);
	/*! init dummy variable for the given resource
	* \param resource resource index
	* \param period period index
	*/
	bool init_dummy_variable(const int & resource, const int & period);
	/*! init variable for the product beeing produced by the plant in the given product
	* \param plant plant index
	* \param product product index
	* \param period period index
	*/
	bool init_plant_production_variable(const int & plant, const int & product, const int & period);
	/*! init variable for the splitablitiy of the given product
	* \param product product index
	* \param period period index
	*/
	bool init_split_variable(const int & product, const int & period);
	/*! init inventory difference variable
	*/
	bool init_inventory_diff_variable(const int & product, const int & plant, const int & period);
	/*! init customer group production variable
	*/

	bool init_customer_group_production_variable(CustFixationProcessInfo & processInfo, const int & cProdGroup,const int & period,bool isConsiderPrioritization);
	//bool init_customer_group_production_variable(const int & process, const int & cProdGroup,const int & period);
	/*! init customer group inventory variable
	*/
	bool init_customer_group_inventory_variable(const int & plant, const int & cProdGroup,const int & period);
	/*! init customer group ic transport variable
	*/
	bool init_customer_group_ic_variable(const int & sourcePlant, const int & destPlant, const int & cProdGroup,const int & period);
	/*! init resource constraint
	* \param resource resource index
	* \param period period index
	*/
	bool init_resource_constraint(const int & resource, const int & period,bool isUseCapacityDummies);
	/*! init plant inventory constraint
	* \param plant plant index
	* \param product product index
	* \param period period index
	*/
	bool init_plant_inventory_constraint(const int & plant, const int & product, const int & period);
	/*! init plant inventory constraint
	* \param plant plant index
	* \param product product index
	* \param period period index
	*/
	bool init_customer_group_plant_inventory_constraint(const int & plant, const int & cProdGroup, const int & period);
	/*! init external plant inventory constraint
	* \param plant plant index
	* \param period period index
	*/
	bool init_external_plant_inventory_constraint(const int & plant, const int & period);
	/*! init customer inventory constraint
	* \param customer customer index
	* \param product product index
	* \param period period index
	*/
	bool init_customer_inventory_constraint(const int & customer, const int & product, const int & period);
	/*! add the demands to the right side of the customer inventory constraints
	* \param period period index
	*/
	/*! init inventory difference constraint per product and period*/
	bool init_inventory_diff_constraint(const int & product, const int & period);
	void add_demands(const int & period);
	/*! init minimum runtime inventory constraints
	* \param process process index
	* \param period period index
	*/
	bool init_minrun_constraint(const int & process, const int & period);
	/*! init big M or maximum runtime inventory constraints
	* \param process process index
	* \param period period index
	*/
	bool init_maxrun_constraint(const int & process, const int & period);
	/*! init split inventory constraint1
	* \param product product index
	* \param period period index
	*/
	bool init_split_nr_of_plants_constraint(const int & product, const int & period);
	/*! init split inventory constraint2
	* \param plant plant index
	* \param process process index
	* \param period period index
	*/
	bool init_split_production_in_plant_constraint(const int & plant, const int & process, const int & period);
	/*! init split inventory constraint3
	* \param product product index
	* \param period period index
	*/
	bool init_split_allowed_constraint(const int & product, const int & period);
	/*! init minimum inventory constraint
	* \param product product index
	* \param period period index
	*/
	bool init_minimum_inventory_constraint(const int & product, const int & period);

	bool init_product_plant_quantity_fixation_contraint(const int & product,const int &plant, const int & period,double quantity);
	/*! get the inventory differences per period
	*/
	void get_inventory_differences();
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	//model information
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	/*!
	* exists a variable for the given index in the cplex model? (under the assumption that all variable indices are initialized by 1)
	* \param index variable index
	*/
	bool exists_variable(const int & index);
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	//model adaptions
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	/*!
	* \brief adapt the minimum runtime for the given process and period to the given value
	* \param process process index
	* \param period period index
	* \param value value to adapt
	*/
	void adapt_minimum_runtime(const int & process, const int & period, const double & value);
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	//basket separation
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	/*!
	* init basket constraints and corresponding ic-variables
	* \param relevant_baskets indices of relevant baskets
	* \param plant plant index
	* \param demandindex demand index
	*/
	void init_basket_variables_and_constraints(const vector<int> & relevant_baskets, const int & plant, const int & demandindex);

	/*! 
	* get all plants which produce products in the considered basket or with start inventory
	* \param basket basket index
	* \param relevant_plants output, true if the plant is relevant for the basket
	*/
	void get_relevant_plants_for_basket(const int & basket, vector<bool> & relevant_plants);
	/*!
	* add all interplant transport variables which can be necessary for the considered basket
	* \param plant plant index
	* \param basket basket index
	*/
	void add_ic_transports_for_basket(const int & plant, const int & basket,const int & customer);
	/*!
	* \brief returns if a product is in a basket
	* \param product product index
	* \param customer customer index
	* \param basketindex index of the basket in the array of customer baskets
	*/
	bool is_product_in_basket(const int & product, const int & customer, const int & basketindex, const int & period);
	/*!
	* get the by the current solution injured baskets, returns the number of injured baskets
	* \param plant plant index
	* \param demandindex demand index
	* \param solutions exists a transport per plant and demandindex?
	* \param relevant_baskets indices of relevant baskets
	*/
	int get_relevant_baskets_for_plant_and_demand(const int & plant, const int & demandindex, const vector<vector<bool> > & solutions, vector<int> & relevant_baskets);
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	//fixations
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	const bool & m_set_fixations;
	
	/*! 
	* \brief for fixation: allowed deviation of optimized product inventory
	*/
	double m_accuracy_inventory;
	/*!
	* \brief for fixation: allowed deviation of optimized production
	*/
	double m_accuracy_machine_production;
	/*!
	* \brief for fixation: allowed deviation of optimized plant production
	*/
	double m_accuracy_plant_production;

	/*!
	* fix transports or adapt the necessarity of transport possibilities
	* \param set_fixations transport fixation yes or no
	*/
	void adapt_and_set_transport_fixations(const bool & set_fixations);
	/*!
	* fix the output per plant and per product
	* \param production_per_plant_and_product values per plant, product and period to be fixed 
	*/
	void fix_production_per_plant_and_product(const vector<vector<vector<double> > > & production_per_plant_and_product);


	/*!
	* get for the fixations the production per plant and product
	* \param production_per_plant_and_product output per period
	* \param fixations fixations per process and period
	*/
	void get_production_per_plant_and_product(vector<vector<vector<double> > > & production_per_plant_and_product, const vector<vector<double> > & fixations);
	/*!
	* \brief are there transports which must be fixed?
	*/
	bool exist_fixed_transports();
	/*! 
	* \brief get the fixation in shifts for the process and the period
	* \param process process index
	* \param period period index
	*/
	double get_fixation_shifts(const int & process, const int & period);
	/*!
	* \brief get if there is a fixation of the process in the period
	* \param process process index
	* \param period period index
	*/
	bool is_fixation(const int & process, const int & period);
	/*!
	* \brief get if the historical quantity of the process at this period is below the fixation threshold
	* \param process process index
	* \param period period index
	*/
	bool is_threshold(const int & process, const int & period);
	/*!
	* is production fixed to zero for a process and a period
	* \param process process index
	* \param period period index
	*/
	bool is_fixed_to_zero(const int & process, const int & period);
	/*!
	* fix the production to zero for the given process and the given period
	* \param process process index
	* \param period period index
	*/
	void fix_production_to_zero(const int & process, const int & period,bool isSoftFixation);

	void fix_customer_group_production_to_zero(const int & cGroup,const int & process, const int & period,bool isSoftFixation);

	/*!
	* set the minimum for the production according to the sales plan
	* \param process process index
	* \param period period index
	*/
	void fix_production_to_sales_plan(const int & process, const int & period,bool isSoftFixation);

	void fix_production_to_plant_production(const int & process, const int & period);

	/*!
	* \brief production fixations
	*/
	const vector<vector<double> > & m_fixations;
	/*!
	* \brief transport fixations
	*/
	const vector<fixed_transport> & m_fixed_transports;

	const string & m_expansion_stage;


	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

};

#endif