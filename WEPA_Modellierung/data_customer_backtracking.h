#ifndef DCBT_H
#define DCBT_H

#include <vector>
#include <map>
#include <iostream>

#include "data.h"

using namespace std;

class DCBT
{
public:
	DCBT(const int & nPlants, const int & nPeriods);
	~DCBT();

	const int & m_nPlants;
	const int & m_nPeriods;

	void map_production_information(const int & nMachines, const vector<map<int,double> > & pallet_production, const vector<int> & machine_location);
	void map_customer_information(const int & nCustomers, const vector<map<int, double> > & transport_per_customer, const vector<vector<transport_information2> > & detailled_transports);
	//void map_customer_information_customer_group(const int & nCustomers, const vector<map<int, double> > & transport_per_customer, const vector<vector<transport_information2> > & detailled_transports);
	void map_ic_information(const vector<vector<transport_information2 > > & detailled_transports);
	void map_inventory_information(const vector<vector<pair<double, double> > > & inventory);
	void map_solution(const vector<vector<vector<vector<vector<double> > > > > & solution, const int & nMachines);
	
	map<int, int> old_to_new_machine;
	vector<int> new_to_old_machine;
	vector<vector<double> > machine_production;
	int new_nMachines;

	map<int, int> old_to_new_customer;
	vector<int> new_to_old_customer;
	int new_nCustomers;

	vector<vector<vector<double> > > customer_transports;
	vector<vector<vector<double> > > ic_transports;
	vector<double> start_inventory;

	vector<vector<map<int,double> > > backtracking;
	
	vector<int> new_machine_location;
};

#endif