#include "data_customer_backtracking.h"

DCBT::DCBT(const int & nPlants, const int & nPeriods):
m_nPlants(nPlants),
m_nPeriods(nPeriods)
{
	
}

void DCBT::map_production_information(const int & nMachines, const vector<map<int, double> > & pallet_production, const vector<int> & machine_location)
{
	map<int, int>::iterator it2;

	vector<bool> used_machines;
	used_machines.resize(nMachines, false);

	int nPeriods = pallet_production.size();
	
	int i;
	for (i = 0; i < nPeriods; ++i)
	{
		for (map<int, double>::const_iterator it = pallet_production.at(i).begin(); it != pallet_production.at(i).end(); ++it)
		{
			used_machines.at((*it).first) = true;
		}
	}

	new_nMachines = 0;
	for (i = 0; i < used_machines.size(); ++i)
	{
		if (used_machines.at(i))
		{
			it2 = old_to_new_machine.find(i);
			old_to_new_machine.insert(pair<int, int>(i, new_to_old_machine.size()));
			new_to_old_machine.push_back(i);
			new_nMachines++;
		}
	}

	machine_production.resize(new_to_old_machine.size(), vector<double>(nPeriods, 0.0));

	for (i = 0; i < nPeriods; ++i)
	{
		for (map<int, double>::const_iterator it = pallet_production.at(i).begin(); it != pallet_production.at(i).end(); ++it)
		{
			it2 = old_to_new_machine.find((*it).first);
			machine_production.at((*it2).second).at(i) += (*it).second;
		}
	}

	new_machine_location.resize(new_nMachines);
	for (i = 0; i < machine_location.size(); ++i)
	{
		if (used_machines.at(i))
		{
			it2 = old_to_new_machine.find(i);
			new_machine_location.at((*it2).second) = machine_location.at(i);
		}
	}
}

void DCBT::map_customer_information(const int & nCustomers, 
											const vector<map<int, double> > & transport_per_customer, 
											const vector<vector<transport_information2> > & detailled_transports
											)
{
	map<int, int>::iterator it2;

	vector<bool> used_customers(nCustomers, false);

	int nPeriods = transport_per_customer.size();
	
	int i;
	for (i = 0; i < nPeriods; ++i)
	{
		for (map<int, double>::const_iterator it = transport_per_customer.at(i).begin(); it != transport_per_customer.at(i).end(); ++it)
		{	
			used_customers.at((*it).first) = true;
		}
	}
	new_nCustomers = 0;
	for (i = 0; i < used_customers.size(); ++i)
	{
		if (used_customers.at(i))
		{
			it2 = old_to_new_customer.find(i);
			old_to_new_customer.insert(pair<int, int>(i, new_to_old_customer.size()));
			new_to_old_customer.push_back(i);
			new_nCustomers++;
		}
	}

	customer_transports.resize(m_nPlants, vector<vector<double>> (new_nCustomers, vector<double>(m_nPeriods, 0.0)));
	int n;
	for (i = 0; i < nPeriods; ++i)
	{
		for (n = 0; n < detailled_transports.at(i).size(); ++n)
		{	
			it2 = old_to_new_customer.find(detailled_transports.at(i).at(n).end_index);
			customer_transports.at(detailled_transports.at(i).at(n).start_index).at((*it2).second).at(i) += detailled_transports.at(i).at(n).amount;	
		}
	}
	
}
//
//void DCBT::map_customer_information_customer_group(const int & nCustomers, 
//											const vector<map<int, double> > & transport_per_customer, 
//											const vector<vector<transport_information2> > & detailled_transports
//											)
//{
//	map<int, int>::iterator it2;
//
//	vector<bool> used_customers(nCustomers, false);
//
//	int nPeriods = transport_per_customer.size();
//	
//	int i;
//	for (i = 0; i < nPeriods; ++i)
//	{
//		for (map<int, double>::const_iterator it = transport_per_customer.at(i).begin(); it != transport_per_customer.at(i).end(); ++it)
//		{
//			used_customers.at((*it).first) = true;
//		}
//	}
//	new_nCustomers = 0;
//	for (i = 0; i < used_customers.size(); ++i)
//	{
//		if (used_customers.at(i))
//		{
//			it2 = old_to_new_customer.find(i);
//			old_to_new_customer.insert(pair<int, int>(i, new_to_old_customer.size()));
//			new_to_old_customer.push_back(i);
//			new_nCustomers++;
//		}
//	}
//
//	customer_transports.resize(m_nPlants, vector<vector<double>> (new_nCustomers, vector<double>(m_nPeriods, 0.0)));
//	int n;
//	for (i = 0; i < nPeriods; ++i)
//	{
//		for (n = 0; n < detailled_transports.at(i).size(); ++n)
//		{	
//			it2 = old_to_new_customer.find(detailled_transports.at(i).at(n).end_index);
//			customer_transports.at(detailled_transports.at(i).at(n).start_index).at((*it2).second).at(i) += detailled_transports.at(i).at(n).amount;	
//		}
//	}
//	
//}


void DCBT::map_ic_information(const vector<vector<transport_information2> > & detailled_transports)
{
	ic_transports.resize(m_nPlants, vector<vector<double> >(m_nPlants, vector<double>(m_nPeriods, 0.0)));

	int i, n;
	for (i = 0; i < m_nPeriods; ++i)
	{
		for (n = 0; n < detailled_transports.at(i).size(); ++n)
		{
			ic_transports.at(detailled_transports.at(i).at(n).start_index).at(detailled_transports.at(i).at(n).end_index).at(i) += detailled_transports.at(i).at(n).amount;
		}
	}
}

void DCBT::map_inventory_information(const vector<vector<pair<double, double> > > & inventory)
{
	start_inventory.resize(m_nPlants, 0.0);
	int i;
	for (i = 0; i < m_nPlants; ++i)
	{
		start_inventory.at(i) = inventory.at(i).at(0).first;
	}
}



void DCBT::map_solution(const vector<vector<vector<vector<vector<double> > > > > & solution, const int & nMachines)
{
	int m, pk, p, p2, k;
	map<int,double>::iterator it;

	backtracking.resize(nMachines,vector<map<int,double> >(m_nPeriods));

	for (m = 0; m < new_nMachines; ++m)
	{
		for (pk = 0; pk < m_nPeriods; ++pk)
		{
			for (p = 0; p < m_nPlants; ++p)
			{
				for (p2 = 0; p2 < new_nCustomers; ++p2)
				{
					for (k = 0; k < m_nPeriods; ++k)
					{
						backtracking_information dummy;

					
						dummy.machine_index = new_to_old_machine.at(m);
						dummy.customer_index = new_to_old_customer.at(p2);
						dummy.period_index = k;

						/*if (solution.at(m).at(pk).at(p).at(p2).at(k)>0)
						{
							std::cout<<"KZO:"<<"Maschine:"<<m<<" ProdP:"<<pk<<" Werk:"<<p<<" Kunde:"<<p2<<" Periode:"<<k<<" Menge:"<<solution.at(m).at(pk).at(p).at(p2).at(k)<<std::endl;
						}*/

						dummy.amount = solution.at(m).at(pk).at(p).at(p2).at(k);

						

						it = backtracking.at(dummy.machine_index).at(pk).find(dummy.customer_index);

						
						if (it == backtracking.at(dummy.machine_index).at(pk).end())
						{
							backtracking.at(dummy.machine_index).at(pk).insert(pair<int,double>(dummy.customer_index,dummy.amount));

							
						}
						else
						{
							(*it).second += dummy.amount;
							
						}

						
					}
				}
			}
		}
	}

}

DCBT::~DCBT()
{

}