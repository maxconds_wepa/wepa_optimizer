#ifndef EXTERNAL_INPUT_H
#define EXTERNAL_INPUT_H



#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <map>
#include <sstream>

#include "bib.h"
#include "data.h"

using namespace std;

void set_map(const vector<string> & members, map<string, unsigned int> & member_map);
int get_index(const string & key, const map<string, unsigned int> & keymap);
void adapt_plant_map(map<string, unsigned int> & plant_map);
void get_start_inventories(const string & filename, const vector<string> & artnrs, const vector<string> & plants, vector<vector<int> > start_inventory);
void get_real_production(const string & filename, const vector<string> & artnrs, const vector<string> & resources, const vector<production> & processes, vector<vector<double> > & fixations, const vector<double> & nPallets_per_ton);
void get_external_endinventories(const vector<vector<double> > & fixations, const vector<vector<double> > & initial_inventories, vector<double> & minimum_inventories, const int & nProducts, const vector<production> & processes, const int & nPlants, const vector<double> & nPallets_per_ton, const vector<demand> & demands, const vector<string> & products);
void get_external_endinventories2(const vector<vector<double> > & fixations, const vector<vector<double> > & initial_inventories, vector<vector<double> > & minimum_inventories, const int & nProducts, const vector<production> & processes, const int & nPlants, const vector<double> & nPallets_per_ton, const vector<demand> & demands, const vector<string> & products, const int & nPeriods);

#endif