/*! \file
* cplex routines
*/

#ifndef cplex_model_h
#define cplex_model_h

#include <ilcplex/cplex.h>
#include <string>
#include <sstream>
#include <iostream>
#include <vector>

using namespace std;

/*!
* \brief cplex wrapper class for modelling and solving
*/
class CPLEX
{
	private:
		/*! 
		* \brief CPLEX environment
		*/
		CPXENVptr env;
		/*!
		* \brief CPLEX status
		*/
		int status;
		/*!
		* \brief CPLEX mixed-integer-programming object
		*/
		CPXLPptr lp; 
		/*!
		* \brief dummy variable for naming
		*/
		char *_buffer;
		/*!
		* \brief dummy variable for naming
		*/
		int _bufsize;
		/*!
		* \brief dummy variable for naming
		*/
		stringstream namebuf;
		
		//const static char *probName="OptProblem";
	public:
		bool isSucessFul;
		int numContV;
		int numBinaryV;
		int numIntV;
		/*!
		* \brief constructor
		*/
		CPLEX(bool cpx);
		/*!
		* \brief constraint counter
		*/
		int conscounter;
		/*!
		* \brief variable counter
		*/
		int varcounter;
		/*!
		* binary variable generator
		* \param name 
		* \param obj objective coefficient
		* \param varindex place to store variable index
		*/
		int add_var_binary(const char* name,  double obj, int & varindex);
		/*!
		* integer variable generator
		* \param name name 
		* \param lb lower bound 
		* \param ub upper bound 
		* \param obj objective coefficient
		* \param varindex place to store the variable index 
		*/
		int add_var_integer(const char* name, double lb, double ub, double obj, int & varindex);
		/*!
		* integer variable generator
		* \param name name 
		* \param lb lower bound 
		* \param ub upper bound
		* \param obj objective coefficient
		* \param varindex place to store the variable index
		*/
		int add_var_continuous(const char* name, double lb, double ub, double obj, int & varindex);
		/*!
		* constraint generator
		* \param name name
		* \param sense sense: 'L' lower or equal, 'E' equal, 'G' greater or equal
		* \param rhs right hand side
		* \param consindex place to store the variable index
		*/
		int add_cons(const char* name, char sense, double rhs, int & consindex);
		/*!
		* changing objective coeffizient
		* \param var place where variable index is stored
		* \param koeff new coefficient
		*/
		int change_obj_coeff(const int & var, const double & koeff);
		/*!
		*	changing bound(s) of a variable
		*	\param var variable index
		*	\param bound bound
		*	\param what bound (L)ower, (U)pper, (B)oth
		*/
		int change_bound(int var, double bound, char what);
		/*!
		* changing coefficient in a constraint
		* \param var place where variable index is stored
		* \param cons place where index of constraint is stored
		* \param koeff new coefficient
		*/
		int change_coeff(const int & var, const int & cons, const double & koeff);
		/*!
		* adding variable to a constraint
		* \param cons place where constraint index is stored
		* \param var place where variable index is stored
		* \param koeff coefficient for new variable
		*/
		int add_coeff_linear(int & cons, int var, double koeff);
		/*!
		* change the right hand side of a constraint
		* \param cons index of the constraint
		* \param rhs new right hand side
		*/
		int change_cons_rhs(int cons, double rhs);
		/*! 
		* \brief solve the current linear programm
		*/ 
		int solve_lp();
		/*!
		* solve the current mixed integer program
		* \param epgap maximum optimality gap
		* \param polish use polishing?
		*/
		int solve_mip(const double & epgap, const bool & polish);
		/*!
		* \brief get objective value of solved program
		*/
		double get_objval();
		/*!
		* write name to buffer
		* \param name name 
		*/
		void _writeToBuffer(const char * name);
		/*!
		* set the objective sense of the problem "min" or "max"
		* \param sense objective sense: "min" or "max"
		*/
		void setObjSense(const char* sense);
		/*!
		* write lp file of current problem
		* name filename
		*/
		void write_problem(string name);
		/*!
		* get solution value of variable
		* \param j variable index
		* \param ret solution value
		*/
		int get_varsol(int j, double & ret);
		/*!
		* get solution value of variable
		* \param j variable index
		*/
		double get_varsol(int j);
		/*! 
		* \brief get the relative gap of the solution
		*/
		double get_gap();
		/*! \brief intern CPLEX status*/
		int solve_status;
		/*!
		* initialize problem
		*/
		void createprob();
		/*! 
		* deinitialize problem
		*/
		void freeprob();
		/*!
		* destructor
		*/
		~CPLEX();

		//int add_mipstart(const vector<int> & indices, const vector<double> & values, bool first);
};

#endif