
#include "data.h"


Article_customer_fixation_collection::Article_customer_fixation_collection()
{
	//acFixations.resize ();
	//acFixations =new vector<article_customer_fixations>();
	//customersOfProduct=new unordered_map<int,hash_set<int>>();

}
int Article_customer_fixation_collection::numberACFixations()
{
	return acFixations.size ();
}
bool Article_customer_fixation_collection::isCustomerLocationInFixationsForArticle (int customer,int product)
{
	//if (customersOfProduct.find(product)==customersOfProduct .end())
		//return false;
	if (customerFixationIndices.find (product)==customerFixationIndices .end())
		return false;

	if (customerFixationIndices.at (product).find(customer)==customerFixationIndices.at(product).end())
		return false;

	//if (customersOfProduct.at (product).find (customer)==customersOfProduct.at(product).end())
		//return false;
	return true;
}

int Article_customer_fixation_collection::getCustomerFixationIndex(int customer,int product)
{
	if (customerFixationIndices.find (product)==customerFixationIndices .end())
		return -1;

	if (customerFixationIndices.at (product).find(customer)==customerFixationIndices.at(product).end())
		return -1;

	return customerFixationIndices.at (product).at(customer);
}

article_customer_fixations & Article_customer_fixation_collection::get(int i)
{
	return acFixations .at(i);
}
void Article_customer_fixation_collection::add(article_customer_fixations acf)
{
	int i;
	unordered_set<int> dummy;
	unordered_map<int,int>dummy_intintMap;

	/*if (customersOfProduct.find (acf.article )==customersOfProduct .end ())
	{	
		customersOfProduct.insert (pair<int,unordered_set<int>>(acf.article,dummy));
	}*/

	if (customerFixationIndices.find(acf.article )==customerFixationIndices.end ())
	{
		customerFixationIndices.insert (pair<int,unordered_map <int,int>>(acf.article,dummy_intintMap));
	}

	for (i=0;i<acf.customerLocations.size ();i++)
	{	
		if (customerFixationIndices.at (acf.article).find (acf.customerLocations.at (i))==customerFixationIndices.at (acf.article).end ())
		{
			customerFixationIndices.at(acf.article).insert (pair<int,int>(acf.customerLocations .at (i),acFixations .size ()));
		}
		/*if (!isCustomerLocationInFixationsForArticle (acf.customerLocations .at(i),acf.article ))
		{
			customersOfProduct .at (acf.article).insert(acf.customerLocations .at(i));
		}*/
	}

	acFixations .push_back (acf);
}

void article_customer_fixations::addCustomerLocation(int cLoc)
{
	int i;

	for (i=0;i<customerLocations.size ();i++)
	{
		if (customerLocations[i]==cLoc)
			break;
	}

	if (i==customerLocations.size())
		customerLocations .push_back (cLoc);
}

void article_customer_fixations::addAllowedProcess(CustFixationProcessInfo procInfo)//(int proc)
{
	int i;

	for (i=0;i<allowedProcesses.size ();i++)
	{
		if (allowedProcesses[i].processNumber==procInfo.processNumber)
			break;
	}

	if (i==allowedProcesses.size())
		allowedProcesses.push_back (procInfo);
}

Customer_product_related_transports_collection::Customer_product_related_transports_collection()
{
	//this->crTranspProcesses =new unordered_map<int,unordered_map<int,unordered_set<int>>>();
}
void Customer_product_related_transports_collection::add(int customer,int product,int transportation_proces)
{
	unordered_map<int,unordered_set<int>> dummy_1;
	unordered_set<int> dummy_2;

	if (crTranspProcesses.find(customer)==crTranspProcesses.end ())
		crTranspProcesses.insert(pair<int,unordered_map<int,unordered_set<int>>>(customer,dummy_1));//(pair<int,pair<int,unordered_set <int>>>(customer,dummy_1));


	if(crTranspProcesses.at(customer).find (product)==crTranspProcesses.at(customer).end ())
	{
		crTranspProcesses.at(customer).insert (pair<int,unordered_set<int>>(product,dummy_2));
	}

	if (crTranspProcesses .at(customer).at(product).find(transportation_proces)==crTranspProcesses .at(customer).at(product).end())
		crTranspProcesses .at(customer).at(product).insert (transportation_proces);
}

bool Customer_product_related_transports_collection::isPlantValidForCustomerArticle(int customer,int product,int transportation_proces)
{
	//Check whether there are entries for customer and product related transports
	if (crTranspProcesses.find(customer)==crTranspProcesses.end ())
		return true;


	if(crTranspProcesses.at(customer).find (product)==crTranspProcesses.at(customer).end ())
	{
		return true;
	}

	if (crTranspProcesses .at(customer).at(product).find(transportation_proces)==crTranspProcesses .at(customer).at(product).end())
		return false;

	return true;
}

list<int>Customer_product_related_transports_collection:: getPlantsForCustomerProduct (int customer,int product)
{
	list<int> p;
	
	if (crTranspProcesses.find(customer)!=crTranspProcesses.end () && crTranspProcesses.at(customer).find (product)!=crTranspProcesses.at(customer).end ())
	{
		//int k;
		//for (k=0;k<crTranspProcesses.at(customer).at(product).size();k++)
		//{
			auto it=crTranspProcesses.at(customer).at(product).begin();
			auto itEnd=crTranspProcesses.at(customer).at(product).end();

			while(it!=itEnd)
			{
				p.push_back(*it);
				it++;
			}

		//}
	}
		
	return p;
}