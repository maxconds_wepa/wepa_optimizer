#include<iostream>
#include <fstream>
#include<string>
#include<list>
#include <Windows.h>
#include <sql.h>
#include <sqltypes.h>
#include <sqlext.h>
#ifndef DATA_T

#define DATA_T

void storeListToArray(std::list<std::string> l,std::string *a,int start,int end);
class SimpleDataTable
{
protected:
	std::string *base;
	int numCols,numRows;
	bool isHeadLine;
	
public:
	SimpleDataTable (int nRows,int nCols);
	int NumCols();
	int NumRows();
	std::string *columns;
	std::string **tab;
	bool IsHeadline_g();
	void IsHeadline_s(bool isHL);
};

class General_Data_Source 
{
public:
	virtual SimpleDataTable *getData()=0;
	virtual void writeData(SimpleDataTable & source)=0;
	virtual void writeColumNames(std::string *line,int length)=0;
	virtual void writeData(std::string *line,int length)=0;
};
class CSV_File : public General_Data_Source
 {
	protected:
        char *fileName;
		std::ifstream *reader;
		std::ofstream *writer;
        //private Stream strm;

        //private StreamReader reader;
        //private StreamWriter writer;
        char csvDelimiter;
        bool isHeadline;
        char **headLine;
		void split(std::string & str,std::list<std::string> &rL);
		//void storeListToArray(std::list<std::string> l,std::string *a,int start,int end);
	public:
		void csvDelimiter_s(char d);
		char csvDelimiter_g();

		CSV_File(char *fN);
		void openForReading ();
		void setHeadLine(bool iH);
		void openForWriting(bool isAppend);
		void closeDoc ();
		virtual SimpleDataTable *getData();
		SimpleDataTable *getData(int startRow,int startColumn,int numberOfColumns);
		virtual void writeData(SimpleDataTable & source);
		virtual void writeData(std::string *line,int length);
		virtual void writeColumNames(std::string *line,int length);
 };

class DataBase {

public:
	virtual int connect ()=0;
	virtual int closeConnection()=0;
	virtual SimpleDataTable *getData(std::string cmd)=0;
	virtual void execSQLCmd (std::string cmd)=0;
};

class ODBC_DataBase : public DataBase {
protected:
	std::string usedDSN;
	std::string usedUserId,usedPwd;
	SQLHANDLE hdlEnv, hdlConn, hdlStmt, hdlDbc;
	SQLRETURN rV;
public:
	ODBC_DataBase();
	~ODBC_DataBase();
	ODBC_DataBase (std::string DSN,std::string userId,std::string pwd);
	void DSN_s(std::string DSN,std::string userId,std::string pwd);
	virtual SimpleDataTable *getData(std::string cmd);
	
	virtual int connect ();
	virtual int closeConnection();
	virtual void execSQLCmd (std::string cmd);
};

class DataBase_Table : public General_Data_Source
{
protected:
	DataBase *usedDb;
	std::string usedCmd;
	std::string usedTableName;
public:
	DataBase_Table();
	DataBase_Table(DataBase * db);
	DataBase_Table(DataBase * db,std::string cmd);

	~DataBase_Table();

	void Cmd_s(std::string cmd);
	void Db_s(DataBase *db);
	void TableName_s(std::string & tableName);
	virtual SimpleDataTable *getData();
	virtual void writeData(SimpleDataTable & source);
	virtual void writeColumNames(std::string *line,int length);
	virtual void writeData(std::string *line,int length);
};
#endif