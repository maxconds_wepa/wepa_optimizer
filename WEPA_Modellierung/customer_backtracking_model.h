#ifndef CBTMODEL_H
#define CBTMODEL_H

#include <vector>
#include <set>
#include <map>
#include <sstream>

#include "cplex_model.h"

using namespace std;



class CBTModel
{
public:
	CBTModel(
			 const int & nPlants, 
			 const int & nPeriods, 
			 const int & nMachines, 
			 const int & nCustomers, 
			 const vector<double> & start_inventory, 
			 const vector<vector<double> > & production, 
			 const vector<vector<vector<double> > > & ictrans, 
			 const vector<vector<vector<double> > > & ctrans, 
			 const vector<int> & machine_location, 
			 const string & expansion_stage
			 );

	const int & m_nPlants;
	const int & m_nPeriods;
	const int & m_nMachines;
	const int & m_nCustomers;
	//const static int m_nCustomers=0;
	const vector<double> & m_start_inventory;
	const vector<vector<double> > & m_production;
	const vector<vector<vector<double> > > & m_ictrans;
	const vector<vector<vector<double> > > & m_ctrans;
	const vector<int> & m_machine_location;
	const string & m_expansion_stage;

	vector<vector<vector<vector<int> > > > var_inventory;
	vector<vector<vector<vector<vector<int> > > > > var_ictrans;
	vector<vector<vector<vector<vector<int> > > > > var_ctrans;
	vector<vector<int> > var_start_inventory_plants;
	vector<vector<vector<int> > > var_transport_plants;
	vector<vector<vector<int> > > var_transport_customers;

	vector<vector<vector<int> > > var_dummy_ictrans_plus;
	vector<vector<vector<int> > > var_dummy_ictrans_minus;
	vector<vector<vector<int> > > var_dummy_ctrans_plus;
	vector<vector<vector<int> > > var_dummy_ctrans_minus;

	void init_vars();
	void init_constraints();
	void solve(vector<vector<vector<vector<vector<double> > > > > & solution,string modelFileName);

	CPLEX* cplex;
	stringstream namebuf;

	~CBTModel();

	vector<bool> producing_plant;

	double epsilon;

	bool isNoInventory;
};

#endif