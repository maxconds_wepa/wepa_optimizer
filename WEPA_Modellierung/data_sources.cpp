//#include "stdafx.h"
#include "data_sources.h"

/*
public class CSV_File: OptimizerDataSource,IDisposable 
{
private string fileName;
private Stream strm;

private StreamReader reader;
private StreamWriter writer;
private string csvDelimiter  = ",";

private bool isHeadline = true;
private string [] headLine;
*/

void storeListToArray(std::list<std::string> l,std::string *a,int start,int end)
{
	std::list<std::string>::iterator itL,itLEnd;
	std::list<std::string> splitLine;
	int i,j;	

	j=0;
	itL=l.begin ();
	itLEnd=l.end ();
	i=0;
	while (itL!=itLEnd)
	{
		if (j>=start && j<=end)
		{
			a[i]=*itL;
			i++;
			itL++;
		}
		j++;
	}
}


SimpleDataTable::SimpleDataTable (int nRows,int nCols)
{
	int i,j;
	int currPos,currBPos;

	base=new std::string [nRows*nCols];
	columns=new std::string [nCols];
	tab=new std::string *[nRows];

	currPos=currBPos=0;
	for (j=0;j<nRows;j++)
	{
		tab[j]=&base[j*nCols];
	}

	numRows =nRows;
	numCols=nCols;
	isHeadLine=false;
}

bool SimpleDataTable::IsHeadline_g()
{
	return isHeadLine;
}
void SimpleDataTable::IsHeadline_s(bool isHL)
{
	isHeadLine=isHL;
}

CSV_File::CSV_File(char *fN)
{
	fileName = fN;
	// strm = null;
	reader = NULL;
	writer = NULL;
}

void CSV_File::csvDelimiter_s(char d)
{
	csvDelimiter=d;
}

char CSV_File::csvDelimiter_g()
{
	return csvDelimiter;
}
/* public CSV_File (Stream rs )
{
strm = rs;
fileName = null;
}*/
/*
public bool IsHeadLine 
{
get { return isHeadline;}
set {isHeadline =value;}
}

public string [] HeadLine
{
get {return headLine;}
set {headLine=value;}
}

public string CSVDelimiter 
{
get{return csvDelimiter ;}
set {csvDelimiter =value;}
}*/
void CSV_File::setHeadLine(bool iH)
{
	isHeadline=iH;
}
void CSV_File::openForReading ()
{
	if (fileName !=NULL)
		reader=new std::ifstream (fileName );

	//reader=new ifstream
	/*if (fileName!=null)
	reader=new StreamReader (fileName);
	else if (strm != null)
	reader=new StreamReader (strm);*/
}

void CSV_File::openForWriting(bool isAppend)
{
	/*if (fileName!=null)
	writer=new StreamWriter (fileName,isAppend );
	else if (strm!=null)
	writer=new StreamWriter (strm);*/
	if (fileName!=NULL)
	{
		if (isAppend )
			writer=new std::ofstream (fileName,std::ofstream::app );
		else
			writer=new std::ofstream (fileName,std::ofstream::trunc);
	}
}

void CSV_File::closeDoc ()
{
	if (reader!=NULL)
		reader->close();
	else if (writer!=NULL)
		writer->close();
}

void CSV_File::writeColumNames(std::string *line,int length)
{
	writeData(line,length);
}

//public void writeData(SimpleDataTable source)
void CSV_File::writeData(SimpleDataTable & source)
{
	int i, j; 
	std::string currLine;

	if (source.IsHeadline_g())
	{
		/*currLine="";
		
		for (i=0;i<source.NumCols();i++)
		{
			currLine = currLine + source.columns[i];//Convert.ToString(r.ItemArray[i]);
			if (i<source.NumCols()-1)
				currLine+=csvDelimiter;
		}

		writer->write(currLine.c_str(),currLine.length());
		*/
		writeColumNames(source.columns,source.NumCols());
	}

	for (j=0;j<source.NumRows();j++)
	{
		/*
		currLine="";
		
		for (i=0;i<source.NumCols();i++)
		{
			currLine = currLine + source.tab[j][i];//Convert.ToString(r.ItemArray[i]);
			if (i<source.NumCols()-1)
				currLine+=csvDelimiter;
		}

		writer->write(currLine.c_str(),currLine.length());
		*/
		writeData(source.tab[j],source.NumCols());
	}
	//DataColumn col;
	/*
	if (isHeadline)
	{
		currLine = "";
		
		for (i=0;i<source.NumCols();i++)
		{
			
		}
		
		foreach (DataColumn col in source.Columns)
		{
			currLine = currLine + Convert.ToString(col.ColumnName );
			if (i < source.Columns.Count  - 1)
			currLine += csvDelimiter;
			i++;
		}
		writer.WriteLine(currLine);
	}
		*/
	/*
	foreach (DataRow r in source.Rows)
	{
	currLine="";

	for (i = 0; i < r.ItemArray.Length; i++)
	{
	currLine = currLine + Convert.ToString(r.ItemArray[i]);
	if (i<r.ItemArray .Length -1)
	currLine+=csvDelimiter;
	}
	writer.WriteLine (currLine);
	}
	*/
}

//public void writeData(string[] lineArr)
/*
void CSV_File::writeData(char **lineArr)
{
	/*
	int i;
	string currLine = "";

	for (i = 0; i < lineArr.Length; i++)
	{
	currLine = currLine + Convert.ToString(lineArr[i]);
	if (i < lineArr.Length - 1)
	currLine += csvDelimiter;
	}
	writer.WriteLine(currLine);
	*/
//}
void CSV_File::writeData(std::string *line,int length)
{
	int i;
	std::string currLine = "";

	for (i = 0; i < length; i++)
	{
		currLine = currLine + line[i];
		if (i < length - 1)
			currLine += csvDelimiter;
	}
	writer->write(currLine.c_str(),currLine.length());//.WriteLine(currLine);
}

void CSV_File::split(std::string & str,std::list<std::string>  & rL)
{
	int i;
	int sStart;
	bool isSep;

	rL.clear ();
	sStart=0;
	isSep=false;
	for (i=0;i<str.length();i++)
	{
		if (str[i]==';')
		{
			isSep=true;
			rL.push_back (str.substr(sStart,i-sStart));
		}
		else
		{
			if (isSep)
				sStart=i;

			isSep=false;
		}
	}
	rL.push_back (str.substr(sStart,i-sStart));
}

//public SimpleDataTable getData()
SimpleDataTable *CSV_File::getData()
{
	int i, j, numberOfCols;
	std::string currLine;
	SimpleDataTable *resultTab;
	char currLineCH[500];
	bool isReadHeadLine;
	bool isFirstLine;
	//bool isNotEOF;
	std::list<std::string> lineCollection;
	std::list<std::string> splitLine;
	std::list<std::string>::iterator it,itEnd;

	resultTab = NULL;
	isReadHeadLine = false;
	numberOfCols = 0;
	isFirstLine=true;

	while (!reader->eof())//(std::getline(*reader,currLine) )
	{
		reader->getline(currLineCH,500);
		currLine.clear ();
		currLine.append(currLineCH);

		if (isFirstLine)
		{	
			split(currLine,splitLine);
			isFirstLine=false;
			numberOfCols=splitLine.size ();

			//if (!isHeadline)
				//lineCollection.push_back(currLine);
		}
		//else

		lineCollection.push_back(currLine);
	}

	it=lineCollection .begin();
	itEnd =lineCollection .end();

	int numRows;

	if (isHeadline)
		numRows=lineCollection.size()-1;
	else
		numRows=lineCollection.size ();

	resultTab =new SimpleDataTable (numRows,numberOfCols);
	resultTab->IsHeadline_s(isHeadline);
	
	j=0;
	while (it!=itEnd)
	{
		split(*it,splitLine);//.Split(csvDelimiter[0]);
		if (isHeadline && !isReadHeadLine)
		{   
			storeListToArray (splitLine,resultTab->columns,0,INT_MAX);

			isReadHeadLine = true;

			/*for (j = 0; j < headLine.Length; j++)
			{
			resultTab.Columns.Add(headLine[j]);
			numberOfCols++;
			}*/

		}
		else
		{
			storeListToArray (splitLine,resultTab->tab[j],0,INT_MAX);
			j++;
			/*lineArr = currLine.Split(csvDelimiter[0]);
			if (lineArr.Length > numberOfCols)
			{
			for (j = numberOfCols; j < lineArr.Length; j++)
			{
			resultTab.Columns.Add();
			numberOfCols++;
			}
			}

			resultTab.Rows.Add(lineArr);*/
			//resultTab.Rows.Add(lineArr);
		}

		it++;
	}

	return resultTab;
}

//    Public Function TableToArray(ByVal startRow As Integer, ByVal startColumn As Integer, ByVal numberOfColumns As Integer)

SimpleDataTable *CSV_File::getData(int startRow,int startColumn,int numberOfColumns)
{
	int i, j, numberOfCols;
	int currRowNum, maxColumn;
	std::string currLine;
	SimpleDataTable *resultTab;
	bool isReadHeadLine;
	bool isFirstLine;
	std::list<std::string> lineCollection;
	std::list<std::string> splitLine;
	std::list<std::string>::iterator it,itEnd;

	resultTab = NULL;
	isReadHeadLine = false;
	numberOfCols = 0;
	j=0;
	isFirstLine=true;

	while (std::getline(*reader,currLine) )
	{	
		if ((isHeadline && isFirstLine) || j>=startRow)
		{
			if (isFirstLine)
			{
				split(currLine,splitLine);
				isFirstLine=false;
				numberOfCols=splitLine.size ()-startColumn+1;
			}

			lineCollection.push_back(currLine);
		}
		j++;
	}
	if (numberOfColumns>numberOfCols )
		maxColumn =startColumn +numberOfCols;
	else
		maxColumn =startColumn +numberOfColumns;
	it=lineCollection .begin();
	itEnd =lineCollection .end();
	resultTab =new SimpleDataTable (lineCollection .size (),numberOfCols);
	j=0;
	while (it!=itEnd)
	{
		split(*it,splitLine);//.Split(csvDelimiter[0]);
		if (isHeadline && !isReadHeadLine)
		{   
			storeListToArray (splitLine,resultTab->columns,startColumn ,maxColumn);

			isReadHeadLine = true;
			/*for (j = 0; j < headLine.Length; j++)
			{
			resultTab.Columns.Add(headLine[j]);
			numberOfCols++;
			}*/

		}
		else
		{
			storeListToArray (splitLine,resultTab->tab[j],startColumn ,maxColumn);
			j++;
			/*lineArr = currLine.Split(csvDelimiter[0]);
			if (lineArr.Length > numberOfCols)
			{
			for (j = numberOfCols; j < lineArr.Length; j++)
			{
			resultTab.Columns.Add();
			numberOfCols++;
			}
			}

			resultTab.Rows.Add(lineArr);*/
			//resultTab.Rows.Add(lineArr);

		}

		splitLine .clear ();
	}

	return resultTab;
	/*
	int i, j, currRowNum, maxColumn;
	string currLine;
	SimpleDataTable resultTab;
	string[] lineArr;
	string[] partialLineArr;

	resultTab = new SimpleDataTable();

	currRowNum=0;

	for (j = 0; j < numberOfColumns; j++)
	resultTab.Columns.Add();

	partialLineArr=new string [numberOfColumns];

	while (!reader.EndOfStream)
	{
	currLine = reader.ReadLine();

	if (currRowNum >=startRow )
	{
	lineArr = currLine.Split(csvDelimiter[0]);

	if (lineArr.Length >startColumn +numberOfColumns)
	maxColumn = startColumn + numberOfColumns;
	else
	maxColumn = lineArr.Length;

	for (j = startColumn; j < maxColumn; j++)
	partialLineArr [j-startColumn]=lineArr [j];

	if (maxColumn < partialLineArr.Length)
	{
	for (j = startColumn; j < maxColumn; j++)
	partialLineArr [j-startColumn]="";
	}

	resultTab.Rows.Add(partialLineArr);
	}

	currRowNum++;
	}

	return resultTab;
	*/
}

int SimpleDataTable::NumCols()
{
	return numCols ;
}
int SimpleDataTable::NumRows()
{
	return numRows;
}

ODBC_DataBase::ODBC_DataBase()
{
}
ODBC_DataBase::~ODBC_DataBase()
{
}
ODBC_DataBase::ODBC_DataBase (std::string DSN,std::string userId,std::string pwd)
{
	DSN_s(DSN,userId,pwd);
}
void ODBC_DataBase::DSN_s(std::string DSN,std::string userId,std::string pwd)
{
	usedDSN=DSN;
	usedUserId=userId;
	usedPwd=pwd;
}
int ODBC_DataBase::connect ()
{
	SQLRETURN rV;

	rV=SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &hdlEnv); 

	if (rV==SQL_ERROR)
		return EXIT_FAILURE;

	rV=SQLSetEnvAttr(hdlEnv,SQL_ATTR_ODBC_VERSION,(void *)SQL_OV_ODBC2,0);
	if (rV==SQL_ERROR)
		return EXIT_FAILURE;

	rV=SQLAllocHandle(SQL_HANDLE_DBC, hdlEnv, &hdlConn);
	if (rV==SQL_ERROR)
		return EXIT_FAILURE;

	rV=SQLConnect(hdlConn, (SQLCHAR*)usedDSN.c_str(),
							SQL_NTS,
							(SQLCHAR*)usedUserId.c_str(),
							SQL_NTS, 
							(SQLCHAR*)usedPwd.c_str(), 
							SQL_NTS
							);
		//(hdlEnv,SQL_ATTR_ODBC_VERSION,(void *)SQL_OV_ODBC2);
	if (rV==SQL_ERROR)
		return EXIT_FAILURE;

	if(rV==SQL_SUCCESS_WITH_INFO)
	{
		SQLCHAR state;
		SQLINTEGER si;
		SQLCHAR *msgText;
		SQLINTEGER nativeError;
		SQLSMALLINT tL;
		msgText=new SQLCHAR[1000];
		SQLGetDiagRec(SQL_HANDLE_DBC,hdlConn,1,&state,&nativeError,msgText,1000,&tL);
		
		//std::cout<<msgText;
	}
	return EXIT_SUCCESS;
}

int ODBC_DataBase::closeConnection()
{
	//SQLFreeConnect(hdlConn);
	SQLFreeHandle(SQL_HANDLE_DBC,hdlConn);
	SQLFreeHandle(SQL_HANDLE_ENV,hdlEnv);

	return EXIT_SUCCESS;
}

void ODBC_DataBase::execSQLCmd (std::string cmd)
{
	rV=SQLAllocHandle(SQL_HANDLE_STMT, hdlConn, &hdlStmt);
	rV=SQLExecDirect(hdlStmt, (SQLCHAR*)cmd.c_str(), SQL_NTS);
	
	SQLFreeHandle(SQL_HANDLE_STMT ,hdlStmt);
}


SimpleDataTable *ODBC_DataBase::getData(std::string cmd)
{
	SimpleDataTable *usedSimpleDataTable=NULL;
	char retVal[1000];
	bool isFirstRow=true;
	std::list<std::list<std::string>>rList;
	std::list<std::string> currRowList;
	SQLLEN cbData;
	//SQLRETURN rV;
	int i;
	SQLCHAR *t=(SQLCHAR*)"";
	rV=SQLAllocHandle(SQL_HANDLE_STMT, hdlConn, &hdlStmt);
	rV=SQLExecDirect(hdlStmt, (SQLCHAR*)cmd.c_str(), SQL_NTS);

	//SQLColumns(hdlStmt,t,0,t,0,t,0,t,0);
	while(SQLFetch(hdlStmt) == SQL_SUCCESS)
	{
		currRowList.clear();
		i=1;
		do {
			rV=SQLGetData(hdlStmt,i,SQL_C_CHAR,retVal,1000,&cbData);//DT_STRING,retVal,256,&cbData);
			if (rV==SQL_SUCCESS || rV==SQL_SUCCESS_WITH_INFO)
			{
				//std::cout<<retVal<<" ";
				currRowList.push_back(std::string(retVal));
			}
			i++;
		}
		while(rV==SQL_SUCCESS);

		rList.push_back(currRowList);
		
		//std::cout<<std::endl;
	}

	SQLFreeHandle(SQL_HANDLE_STMT ,hdlStmt);

	int nCOls=0,nRows=0;

	nRows=rList.size();
	if (nRows>0)
		nCOls=(*rList.begin()).size();
	usedSimpleDataTable=new SimpleDataTable(nRows,nCOls);
	std::list<std::list<std::string>>::iterator it,itEnd;

	it=rList.begin();
	itEnd=rList.end();
	i=0;
	while (it!=itEnd)
	{
		storeListToArray(*it,usedSimpleDataTable->tab[i],0,INT_MAX);
		it++;
		i++;
	}

	return usedSimpleDataTable;
}


DataBase_Table::DataBase_Table()
{
}
DataBase_Table::DataBase_Table(DataBase * db)
{
	Db_s(db);
}
DataBase_Table::DataBase_Table(DataBase * db,std::string cmd)
{
	Db_s(db);
	Cmd_s(cmd);
}

void DataBase_Table::TableName_s(std::string & tableName)
{
	usedTableName=tableName;
}

DataBase_Table::~DataBase_Table()
{
}

void DataBase_Table::Cmd_s(std::string cmd)
{
	usedCmd=cmd;
}

void DataBase_Table::Db_s(DataBase *db)
{
	usedDb=db;
}

SimpleDataTable *DataBase_Table::getData()
{
	return usedDb->getData(usedCmd);
}

void DataBase_Table::writeData(SimpleDataTable & source)
{
	int i, j; 
	std::string insertCmd;
	std::string currLine;

	for (j=0;j<source.NumRows();j++)
	{
		writeData(source.tab[j],source.NumCols());
		/*
		currLine="";
		
		for (i=0;i<source.NumCols();i++)
		{
			currLine = currLine + source.tab[j][i];//Convert.ToString(r.ItemArray[i]);
			if (i<source.NumCols()-1)
				currLine+=",";
		}

		///writer->write(currLine.c_str(),currLine.length);
		insertCmd="INSERT INTO "+this->usedTableName+" VALUES ("+currLine+")";
		usedDb->execSQLCmd(insertCmd);
		*/
	}
}

void DataBase_Table::writeColumNames(std::string *line,int length)
{
}

void DataBase_Table::writeData(std::string *line,int length)
{
	int i;
	std::string insertCmd;
	std::string currLine;

	currLine="";
		
	for (i=0;i<length;i++)
	{
		currLine = currLine + line[i];//Convert.ToString(r.ItemArray[i]);
		if (i<length-1)
			currLine+=",";
	}

	insertCmd="INSERT INTO "+this->usedTableName+" VALUES ("+currLine+")";

	usedDb->execSQLCmd(insertCmd);
}