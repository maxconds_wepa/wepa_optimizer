/*! \file
* general functions
*/
#ifndef BIB_H
#define BIB_H

#include <cmath>
#include <algorithm>
#include <vector>

using namespace std;

/*! 
* round the given number
* \param value number
* \param numbers number of decimal places
*/
double round(double value, int numbers);
/*!
* generate random number between lower and upper bound
* \param lower_bound lower bound
* \param upper_bound upper bound
*/
double  generate_random_double(double lower_bound, double upper_bound);
/*!
* divide a string into substrings
* \param line string
* \param daten vector in which the substrings are stored
* \param delimiter delimiter in line
*/
void readin_explode(const string & line, vector<string> & daten, char delimiter);
/*!
* get the median of the given values
* \param values values for which median has to be computed
*/
double get_median(const vector<double> & values);

#endif