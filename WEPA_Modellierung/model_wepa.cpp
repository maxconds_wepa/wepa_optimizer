#include "model_wepa.h"
#include <Windows.h>
#include <Psapi.h>
//To enable evaluation of the final inventory by transport costs uncomment the definition before compiling
#define TRANSPORT_EVAL_FINAL_INVENTORY

//To enable threshold on the production quantities obtained from the sales plan for fixations
#define FIXATION_THRESHOLD

Wepa::Wepa(const int & nPlants,
	const vector<string> & plants,
	const int & nCustomers,
	const vector<string> & customers,
	const int & nResources,
	const vector<string> & resources,
	const int & nProducts,
	const vector<string> & products,
	const int & nPeriods,
	const vector<string> & periods,
	const int & nProcesses,
	const vector<production> & processes,
	const vector<vector<double> > & capacity,
	const vector<transports> & transport_plant,
	const vector<transports> & transport_customer,
	const vector<demand> & demands,
	const vector<demand> & finalPeriodMinInventoryPerCustomer,
	const vector<double> & nPallets_per_ton,
	const double & min_runtime,
	const double & min_for_split,
	const int & lkw_capacity,
	const vector<vector<int> > & baskets,
	const vector<vector<int> > & basket_coherence,
	const vector<int> & customer_for_basket,
	const vector<vector<double> > & start_inventory,
	const vector<vector<double> > & min_inventory_v2,
	const vector<vector<vector<double> > > & min_inventory_v1,
	const vector<int> & plant_capacities,
	const vector<vector<double> > & fixations,
	const bool & set_fixations,
	const double & external_warehouse_cost,
	const vector<fixed_transport> & fixed_transports,
	const string & logfilename,
	const string & expansion_stage,
	const double fixationThreshold,
	//const vector<article_customer_fixations> & customer_fixations
	Article_customer_fixation_collection & custFixations,
	const vector<int> & resourcesWithFixations,
	Customer_product_related_transports_collection & cpTransports
) :
m_nPlants(nPlants),
m_plants(plants),
m_nCustomers(nCustomers),
m_customers(customers),
m_nResources(nResources),
m_resources(resources),
m_nProducts(nProducts),
m_products(products),
m_nPeriods(nPeriods),
m_periods(periods),
m_nProcesses(nProcesses),
m_processes(processes),
m_capacity(capacity),
m_transport_plant(transport_plant),
m_transport_customer(transport_customer),
m_demands(demands),
m_nPallets_per_ton(nPallets_per_ton),
m_min_runtime(min_runtime),
m_min_for_split(min_for_split),
m_lkw_capacity(lkw_capacity),
m_external_warehouse_cost(external_warehouse_cost),
m_bigM(nProcesses, 93),
m_baskets(baskets),
m_basket_coherence(basket_coherence),
m_customer_for_basket(customer_for_basket),
m_splittable_product(nProducts, vector<bool>(nPeriods - 1, false)),
m_set_fixations(set_fixations),
m_product_in_plant(nProducts, vector<bool>(nPlants, false)),
m_minruns(nProcesses, vector<double>(nPeriods - 1, min_runtime)),
m_minsplits(nProducts, vector<double>(nPeriods - 1, min_for_split)),
m_is_demand_for_customer_and_product(nCustomers, vector<bool>(nProducts, false)),
m_is_demand_for_customer_and_product_in_period(nCustomers, vector<vector<bool> >(nProducts, vector<bool>(nPeriods - 1, false))),
m_transport_indices_plant(nPlants, vector<int>(nPlants, -1)),
m_transport_indices_customer(nPlants, vector<int>(nCustomers, -1)),
m_processes_for_product(nProducts),
m_is_process_for_product(nProcesses, vector<bool>(nProducts, false)),
m_is_resource_for_product(nResources, vector<bool>(nProducts, false)),
m_is_demand_for_product(nProducts, false),
m_is_demand_for_product_and_period(nProducts, vector<bool>(nPeriods - 1, false)),
m_is_demand_for_customer(nCustomers, false),
m_transport_necessary_for_product(nPlants, vector<vector<bool> >(nPlants, vector<bool>(nProducts, false))),
m_plant_for_customer(nPlants, vector<bool>(nCustomers, false)),
m_dummy_cost(1000000.0),
m_var_run(nProcesses, vector<int>(nPeriods - 1, -1)),
m_var_run_setup(nProcesses, vector<int>(nPeriods - 1, -1)),
m_var_transport_customer(nPlants, vector<int>(m_demands.size(), -1)),
m_var_fictitious_transport_customer(nPlants, vector<int>(m_demands.size(), -1)),
m_var_transport_plant(nPlants, vector<vector<vector<int> > >(nPlants, vector<vector<int> >(nProducts, vector<int>(nPeriods - 1, -1)))),
m_var_inventory_plant(nPlants, vector<vector<int> >(nProducts, vector<int>(nPeriods, -1))),
m_var_external_inventory_plant(nPlants, vector<int>(nPeriods, -1)),
m_var_inventory_customer(nCustomers, vector<vector<int> >(nProducts, vector<int>(nPeriods - 1, -1))),
m_var_dummy(nResources, vector<int>(nPeriods - 1, -1)),
m_var_plant_run(nProducts, vector<vector<int> >(nPlants, vector<int>(nPeriods - 1, -1))),
m_var_split(nProducts, vector<int>(nPeriods - 1, -1)),
m_var_inventory_difference(nProducts, vector<vector<int> >(nPlants, vector<int>(nPeriods - 1, -1))),
m_var_customer_group_run(nProcesses, vector<vector<int>>(custFixations.numberACFixations(), vector<int>(nPeriods - 1, -1))),
m_var_customer_group_inventory(nPlants, vector<vector<int>>(custFixations.numberACFixations(), vector<int>(nPeriods, -1))),
m_var_customer_group_transport_plant(nPlants, vector<vector<vector<int>>>(nPlants, vector<vector<int>>(custFixations.numberACFixations(), vector<int>(nPeriods, -1)))),
m_cons_capacity(nResources, vector<int>(nPeriods, -1)),
m_cons_inventory_customer(nCustomers, vector<vector<int> >(nProducts, vector<int>(nPeriods, -1))),
m_cons_inventory_plant(nPlants, vector<vector<int> >(nProducts, vector<int>(nPeriods, -1))),
m_cons_customer_group_inventory_plant(nPlants, vector<vector<int>>(custFixations.numberACFixations(), vector<int>(nPeriods, -1))),
m_cons_minrun(nProcesses, vector<int>(nPeriods, -1)),
m_cons_external_inventory_plant(nPlants, vector<int>(nPeriods, -1)),
m_cons_maxrun(nProcesses, vector<int>(nPeriods, -1)),
m_cons_ns(nProducts, vector<int>(nPeriods, -1)),
m_cons_ns_connector(nProcesses, vector<vector<int> >(nPlants, vector<int>(nPeriods, -1))),
m_cons_ns_connector2(nProducts, vector<int>(nPeriods, -1)),
m_cons_min_inventory_v2(nProducts, vector<int>(nPeriods - 1, -1)),
m_cons_inventory_difference(nProducts, vector<int>(nPeriods - 1, -1)),
m_cons_fictitious_transports_quantity_per_customer_meet_minlevel_customer(nProducts, vector<int>(m_finalPeriodMinInventoryPerCustomer.size(), -1)),
m_cons_fictitious_transports_quantity_per_customer_meet_inventorylevel_plant(nProducts, vector<int>(m_nPlants, -1)),
m_cons_start_inventory(nPlants, vector<int>(nProducts)),
m_cons_product_plant_quantity_fixation(nProducts, vector<vector<int>>(nPlants, vector<int>(nPeriods))),
m_max_demand(0),
m_start_inventory(start_inventory),
m_min_inventory_v2(min_inventory_v2),
m_min_inventory_v1(min_inventory_v1),
m_finalPeriodMinInventoryPerCustomer(finalPeriodMinInventoryPerCustomer),
m_plant_capacities(plant_capacities),
m_fixations(fixations),
m_logfilename(logfilename),
m_fixed_transports(fixed_transports),
m_inventory_difference(nProducts, vector<double>(nPeriods - 1, 0)),
m_expansion_stage(expansion_stage),
m_fixationThreshold(fixationThreshold),
//m_customer_fixations (customer_fixations )
m_custFixations(custFixations),
m_cpTransports(cpTransports),
m_resourcesWithFixations(resourcesWithFixations),
TOLERANCE_BASKET_BINARIES_TRANSPORT_VARIABLE_CONNECTION(0.01),
m_customerProductionProcessDummyCost(nProcesses, 0.0),
m_isAllowedProcessesWithPrioritizations(custFixations.numberACFixations(), false)
{
	int i, j;
	double maxProdCost = 0;
	double maxProcessTime, minProcessTime;
	double currProcessTime;
	double maxProcessCostPerShift;
	double maxTransportCostCustomer;
	double sumICTransportCost;
	double currTransportCostCustomer;
	article_customer_fixations currFixations;

	cplex = new CPLEX(1);

	ofstream output(m_logfilename.c_str());
	if (output.fail())
	{
		cout << "Kann Datei " << m_logfilename << " nicht anlegen!" << endl;
		exit(1);
	}
	output.close();

	//Genauigkeit in Paletten bei Gleichheitsbedingungen
	m_accuracy_inventory = 2.0;
	m_accuracy_machine_production = 2.0;
	m_accuracy_plant_production = 2.0;
	resourcesWithFixationsBool = new bool[m_nResources];

	for (i = 0; i < m_nResources; i++)
		resourcesWithFixationsBool[i] = false;

	for (i = 0; i < m_resourcesWithFixations.size(); i++)
		resourcesWithFixationsBool[m_resourcesWithFixations.at(i)] = true;

	maxProcessTime = 0;
	production currProcessInfo;
	minProcessTime = DBL_MAX;
	maxProcessCostPerShift = 0;
	for (i = 0; i < m_nProcesses; i++)
	{
		currProcessInfo = m_processes.at(i);
		currProcessTime = 1 / (currProcessInfo.tonspershift*m_nPallets_per_ton.at(currProcessInfo.product));

		if (currProcessTime < minProcessTime)
			minProcessTime = currProcessTime;

		if (currProcessTime > maxProcessTime)
			maxProcessTime = currProcessTime;

		if (currProcessInfo.costspershift > maxProcessCostPerShift)
			maxProcessCostPerShift = currProcessInfo.costspershift;
	}
	//cplex->add_var_continuous(namebuf.str().c_str(), 0.0, CPX_INFBOUND, m_transport_plant.at(m_transport_indices_plant.at(plant1).at(plant2)).cost / (double)m_lkw_capacity, m_var_transport_plant.at(plant1).at(plant2).at(product).at(period-1));
	//m_transport_customer.at(get_transport_index_customer(plant, customer)).cost / (double)m_lkw_capacity;
	//if (m_transport_indices_customer.at(plant).at(customer) != -1)
	//for (i=0;i<m_i)

	sumICTransportCost = 0;

	for (i = 0; i < m_transport_plant.size(); i++)
	{
		sumICTransportCost += m_transport_plant.at(i).cost / (double)m_lkw_capacity;
	}

	//for (i = 0; i < m_nPlants-1; i++)
	//{
		//for (j = i + 1; j < m_nPlants; j++)
		//{
			//sumICTransportCost += m_transport_plant.at(m_transport_indices_plant.at(i).at(j)).cost / (double)m_lkw_capacity;
		//}
	//}
	maxTransportCostCustomer = 0;

	for (i = 0; i < m_transport_customer.size(); i++)
	{
		currTransportCostCustomer = m_transport_customer.at(i).cost / (double)m_lkw_capacity;

		if (currTransportCostCustomer > maxTransportCostCustomer)
			maxTransportCostCustomer = currTransportCostCustomer;
	}
	/*
	for (i = 0; i < m_nPlants; i++)
	{
		for (j = 0; j < m_nCustomers; j++)
		{
			if (m_transport_indices_customer.at(i).at(j) != -1)
			{
				currTransportCostCustomer = m_transport_customer.at(get_transport_index_customer(i, j)).cost / (double)m_lkw_capacity;

				if (currTransportCostCustomer > maxTransportCostCustomer)
					maxTransportCostCustomer = currTransportCostCustomer;
			}
		}
	}*/

	maxProdCost = maxProcessTime / minProcessTime*(maxProcessTime*maxProcessCostPerShift + sumICTransportCost + maxTransportCostCustomer)*m_nProducts / minProcessTime;

	for (i = 0; i < m_custFixations.numberACFixations(); i++)
	{
		currFixations = m_custFixations.get(i);
		for (j = 0; j < currFixations.allowedProcesses.size(); j++)
		{
			if (currFixations.allowedProcesses.at(j).isPrioritized)
			{
				m_isAllowedProcessesWithPrioritizations.at(i) = true;
				break;
			}
		}
	}

	for (i = 0; i < m_nProcesses; i++)//m_custFixations.numberACFixations();i++)
	{
		m_customerProductionProcessDummyCost.at(i) = maxProdCost;//customerProductionProcessDummyCost.push_back(maxProdCost);
	}
}
/*
void Wepa::init_vars_for_zero_period()
{
int i,j;

cout << "inventory plant..." << endl;
//variables could be restricted to products which are for plants relevant

for (i = 0; i < m_nPlants; ++i)
{
for (j = 0; j < m_nProducts; ++j)
{
namebuf.str("");
namebuf << "V_INV_PLANT_" << m_plants.at(i) << "_" << m_products.at(j) << "_" << 0;
//cplex->add_var_continuous(namebuf.str().c_str(), m_start_inventory.at(i).at(j), m_start_inventory.at(i).at(j), 0.0, m_var_inventory_plant.at(i).at(j).at(0));
cplex->add_var_continuous(namebuf.str().c_str(), 0, CPX_INFBOUND, 0.0, m_var_inventory_plant.at(i).at(j).at(0));
}

for (j = 0; j < m_custFixations.numberACFixations (); ++j)
{
namebuf.str("");
namebuf << "V_CUSTOMER_GROUP_NV_PLANT_" << m_plants.at(i) << "_" << j << "_" << 0;
//cplex->add_var_continuous(namebuf.str().c_str(), m_start_inventory.at(i).at(j), m_start_inventory.at(i).at(j), 0.0, m_var_inventory_plant.at(i).at(j).at(0));
//cplex->add_var_continuous(namebuf.str().c_str(), 0, CPX_INFBOUND, 0.0, m_var_inventory_plant.at(i).at(j).at(0));
cplex->add_var_continuous (namebuf .str().c_str (),0.0,CPX_INFBOUND,0.0,m_var_customer_group_inventory.at (i).at(j).at(0));
}
}
}
*/
void Wepa::resetVarsForZeroPeriod()
{
	int i, j;

	for (i = 0; i < m_nPlants; i++)
	{
		for (j = 0; j < m_nProducts; j++)
		{
			m_var_inventory_plant.at(i).at(j).at(0) = -1;
		}
	}

	for (i = 0; i < m_nPlants; i++)
	{
		for (j = 0; j < m_custFixations.numberACFixations(); j++)
		{
			m_var_customer_group_inventory.at(i).at(j).at(0) = -1;
		}
	}
}

void Wepa::resetVarsForPeriod(int period)
{
	int i, j,k,p;
	int usedPeriodI;

	usedPeriodI = period - 1;
	/*
	m_var_run(nProcesses, vector<int>(nPeriods - 1, -1)),
	m_var_run_setup(nProcesses, vector<int>(nPeriods - 1, -1)),
	m_var_transport_customer(nPlants, vector<int>(m_demands.size(), -1)),
	m_var_fictitious_transport_customer (nPlants,vector<int>(m_demands.size(),-1)),
	m_var_transport_plant(nPlants, vector<vector<vector<int> > >(nPlants, vector<vector<int> >(nProducts, vector<int>(nPeriods - 1, -1)))),
	m_var_inventory_plant(nPlants, vector<vector<int> >(nProducts, vector<int>(nPeriods, -1))),
	m_var_external_inventory_plant(nPlants, vector<int>(nPeriods, -1)),
	m_var_inventory_customer(nCustomers, vector<vector<int> >(nProducts, vector<int>(nPeriods - 1, -1))),
	m_var_dummy(nResources, vector<int>(nPeriods - 1, -1)),
	m_var_plant_run(nProducts, vector<vector<int> >(nPlants, vector<int>(nPeriods - 1, -1))),
	m_var_split(nProducts, vector<int>(nPeriods - 1, -1)),
	m_var_inventory_difference(nProducts, vector<vector<int> >(nPlants, vector<int>(nPeriods - 1, -1))),
	m_var_customer_group_run (nProcesses,vector<vector<int>>(custFixations.numberACFixations (),vector<int>(nPeriods-1,-1))),
	m_var_customer_group_inventory (nPlants,vector<vector<int>>(custFixations.numberACFixations (),vector<int>(nPeriods,-1))),
	m_var_customer_group_transport_plant (nPlants,vector<vector<vector<int>>>(nPlants,vector<vector<int>>(custFixations.numberACFixations (),vector<int>(nPeriods,-1)))),
	*/
	for (i = 0; i < m_nProcesses; i++)
	{
		m_var_run.at(i).at(usedPeriodI) = -1;
		m_var_run_setup.at(i).at(usedPeriodI) = -1;
		for (j = 0; j < m_custFixations.numberACFixations(); j++)
		{
			m_var_customer_group_run.at(i).at(j).at(usedPeriodI) = -1;
		}
	}
	
	

	for (i = 0; i < m_nPlants; i++)
	{
		m_var_external_inventory_plant.at(i).at(period) = -1;
		for (j = 0; j < m_demands.size(); j++)
		{
			if (m_demands.at(j).period == usedPeriodI)
			{
				m_var_transport_customer.at(i).at(j) = -1;
				m_var_fictitious_transport_customer.at(i).at(j) = -1;
			}
		}
	}

	for (p = 0; p < m_nProducts; p++)
	{
		for (i = 0; i < m_nPlants; i++)
		{
			for (j = 0; j < m_nPlants; j++)
			{
				m_var_transport_plant.at(i).at(j).at(p).at(usedPeriodI) = -1;
			}

			m_var_inventory_plant.at(i).at(p).at(period) = -1;
			m_var_inventory_difference.at(p).at(i).at(usedPeriodI) = -1;
			
			m_var_plant_run.at(p).at(i).at(usedPeriodI) = -1;
		}
		
		for (i = 0; i < m_nCustomers; i++)
		{
			m_var_inventory_customer.at(i).at(p).at(usedPeriodI) = -1;
		}

		m_var_split.at(p).at(usedPeriodI) = -1;
	}

	for (k = 0; k < m_custFixations.numberACFixations(); k++)
	{
		for (i = 0; i < m_nPlants; i++)
		{
			for (j = 0; j < m_nPlants; j++)
			{	
				m_var_customer_group_transport_plant.at(i).at(j).at(k).at(usedPeriodI) = -1;
			}

			m_var_customer_group_inventory.at(i).at(k).at(period) = -1;
		}
	}

	for (i = 0; i < m_nResources; i++)
	{
		m_var_dummy.at(i).at(usedPeriodI) = -1;
	}
}

bool Wepa::is_start_inventory_for_product_in_plant(const int & product, const int & plant)
{
	if (m_start_inventory.at(plant).at(product) > 0.001)
	{
		return true;
	}
	return false;
}

/*bool Wepa::is_product_in_basket(const int & basket, const int & product)
{
int i;
for (i = 0; i < m_baskets.at(basket).size(); ++i)
{
if (m_demands.at(m_baskets.at(basket).at(i)).product == product)
{
return true;
}
}
return false;
}*/

void Wepa::get_relevant_plants_for_basket(const int & basket, vector<bool> & relevant_plants)
{
	int i, j;

	relevant_plants.clear();
	relevant_plants.resize(m_nPlants, false);

	for (i = 0; i < m_baskets.at(basket).size(); ++i)
	{
		for (j = 0; j < m_nPlants; ++j)
		{
			if (produces_plant_product(j, m_demands.at(m_baskets.at(basket).at(i)).product) || is_start_inventory_for_product_in_plant(m_demands.at(m_baskets.at(basket).at(i)).product,j))
			{
				relevant_plants.at(j) = true;
			}
		}
	}
}

/*void Wepa::get_relevant_plants_for_basket(const int & customerindex, const int & basketindex, vector<bool> & relevant_plants, const int & period)
{
relevant_plants.clear();
relevant_plants.resize(m_nPlants,false);

int i,j;
for (i = 0; i < m_nProducts; ++i)
{
if (is_product_in_basket(i,customerindex,basketindex,period))
{
for (j = 0; j < m_nPlants; ++j)
{
if (produces_plant_product(j,i) || is_start_inventory_for_product_in_plant(i,j))
{
relevant_plants.at(j) = true;
}
}
}
}
}*/

bool Wepa::exists_variable(const int & index)
{
	if (index == -1)
	{
		return false;
	}
	return true;
}


/*void Wepa::add_ic_transports_for_basket(const int & customerindex, const int & basketindex, const int & period)
{
int i,j,k,l;

vector<bool> relevant_plants;
get_relevant_plants_for_basket(customerindex,basketindex,relevant_plants,period);

//for (i = 0; i < m_baskets_for_customer.at(customerindex).at(basketindex).size(); ++i)
for (i = 0; i < m_nProducts; ++i)
{
if (is_product_in_basket(i,customerindex,basketindex,period))
{
//for all transports between relevant_plants
for (j = 0; j < relevant_plants.size(); ++j)
{
for (k = j+1; k < relevant_plants.size(); ++k)
{
for (l = 1; l < m_nPeriods; ++l)
{
if (!exists_variable(m_var_transport_plant.at(j).at(k).at(i).at(l-1)))
{
add_ic_variable(j,k,i,l);
}
if (!exists_variable(m_var_transport_plant.at(k).at(j).at(i).at(l-1)))
{
add_ic_variable(k,j,i,l);
}
}
}
}
}
}
}*/

bool Wepa::add_ic_variable(const int & plant1, const int & plant2, const int & product, const int & period)
{
	//if variable already exists, then return
	if (exists_variable(m_var_transport_plant.at(plant1).at(plant2).at(product).at(period-1)) || plant1 == plant2 || !(exists_transport_possibility_plant(plant1,plant2)))
	{
		return false;
	}

	namebuf.str("");
	namebuf << "V_TRANS_PLANT_" << m_products.at(product) << "_from_" << m_plants.at(plant1) << "_to_" << m_plants.at(plant2) << "_" << m_periods.at(period-1);

	cplex->add_var_continuous(namebuf.str().c_str(), 0.0, CPX_INFBOUND, m_transport_plant.at(m_transport_indices_plant.at(plant1).at(plant2)).cost / (double)m_lkw_capacity, m_var_transport_plant.at(plant1).at(plant2).at(product).at(period-1));
	cplex->add_coeff_linear(m_cons_inventory_plant.at(plant1).at(product).at(period),m_var_transport_plant.at(plant1).at(plant2).at(product).at(period-1),1.0);
	cplex->add_coeff_linear(m_cons_inventory_plant.at(plant2).at(product).at(period),m_var_transport_plant.at(plant1).at(plant2).at(product).at(period-1),-1.0);

	return true;
}

bool Wepa::add_customer_group_ic_variable(const int & plant1, const int & plant2, const int & cProdGroup, const int & period)
{

	//if variable already exists, then return
	if (exists_variable (m_var_customer_group_transport_plant.at(plant1).at(plant2).at (cProdGroup).at (period-1))  || 
		plant1 == plant2 || 
		!(exists_transport_possibility_plant(plant1,plant2))
		) //(exists_variable(m_var_transport_plant.at(plant1).at(plant2).at(product).at(period-1)) || plant1 == plant2 || !(exists_transport_possibility_plant(plant1,plant2)))
	{
		return false;
	}

	//namebuf<<"V_CUSTOMER_GROUP_IC_TRANSPORT"<<m_plants.at(sourcePlant)<<"_"<<m_plants .at(destPlant )<<"_"<<m_custFixations .get (cProdGroup ).article<<"_"<<cProdGroup<<"_"<<m_periods.at (period-1)<<endl;

	namebuf.str("");
	namebuf << "V_CUSTOMER_GROUP_IC_TRANSPORT_" << m_plants.at(plant1) << "_" << m_plants.at(plant2)<<"_"<<m_custFixations .get (cProdGroup ).article<<"_"<<cProdGroup<<"_"<<m_periods.at (period-1)<<endl; //<< "_to_" << m_plants.at(plant2) << "_" << m_periods.at(period-1);

	//cplex->add_var_continuous(namebuf.str().c_str(), 0.0, CPX_INFBOUND, m_transport_plant.at(m_transport_indices_plant.at(plant1).at(plant2)).cost / (double)m_lkw_capacity, m_var_transport_plant.at(plant1).at(plant2).at(product).at(period-1));
	cplex->add_var_continuous (namebuf.str().c_str(),0.0,CPX_INFBOUND,m_transport_plant.at(m_transport_indices_plant.at(plant1).at(plant2)).cost / (double)m_lkw_capacity,m_var_customer_group_transport_plant.at(plant1).at(plant2).at (cProdGroup).at (period-1));

	//cplex->add_coeff_linear(m_cons_inventory_plant.at(plant1).at(product).at(period),m_var_transport_plant.at(plant1).at(plant2).at(product).at(period-1),1.0);
	//cplex->add_coeff_linear(m_cons_inventory_plant.at(plant2).at(product).at(period),m_var_transport_plant.at(plant1).at(plant2).at(product).at(period-1),-1.0);
	cplex->add_coeff_linear (m_cons_customer_group_inventory_plant.at(plant1).at(cProdGroup).at(period),m_var_customer_group_transport_plant.at(plant1).at(plant2).at(cProdGroup).at(period-1),1.0);
	cplex->add_coeff_linear (m_cons_customer_group_inventory_plant.at(plant2).at(cProdGroup).at(period),m_var_customer_group_transport_plant.at(plant1).at(plant2).at(cProdGroup).at(period-1),-1.0);

	return true;
}

/*void Wepa::add_customer_transport_variable(const int & plant, const int & customer, const int & product, const int & period, const double & fixation)
{
if (exists_variable(m_var_transport_customer.at(plant).at(customer).at(product).at(period-1)))
{
return;
}


namebuf.str("");

namebuf << "V_TRANS_CUST_" << m_products.at(product) << "_from_" << m_plants.at(plant) << "_to_" << m_customers.at(customer) << "_" << m_periods.at(period - 1);

cplex->add_var_continuous(namebuf.str().c_str(), fixation, CPX_INFBOUND, m_transport_customer.at(m_transport_indices_customer.at(plant).at(customer)).cost / (double)m_lkw_capacity, m_var_transport_customer.at(plant).at(customer).at(product).at(period-1));

cplex->add_coeff_linear(m_cons_inventory_customer.at(customer).at(product).at(period),m_var_transport_customer.at(plant).at(customer).at(product).at(period-1),-1.0);
cplex->add_coeff_linear(m_cons_inventory_plant.at(plant).at(product).at(period),m_var_transport_customer.at(plant).at(customer).at(product).at(period-1),1.0);
}*/

bool Wepa::is_demand_for_customer_and_product(const int & customer, const int & product)
{
	return m_is_demand_for_customer_and_product.at(customer).at(product);
}

bool Wepa::is_demand_for_customer_and_product_in_period(const int & customer, const int & product, const int & period)
{
	return m_is_demand_for_customer_and_product_in_period.at(customer).at(product).at(period-1);
}

void Wepa::get_relevant_plants_for_customer()
{
	int i, j, k;
	//if a plant produces a product and a customer needs the product, edge between plant and customer is necessary
	for (i = 0; i < m_nCustomers; ++i)
	{
		for (j = 0; j < m_nProducts; ++j)
		{
			if (is_demand_for_customer_and_product(i,j))
			{
				for (k = 0; k < m_nPlants; ++k)
				{
					if (produces_plant_product(k,j) || is_start_inventory_for_product_in_plant(j,k))
					{
						m_plant_for_customer.at(k).at(i) = true;
					}
				}
			}
		}
	}
}

//Hier optimieren !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
bool Wepa::is_in_customer_fixation_group (const int & product,const int & customer)
{
	int i,j;

	/*for (i=0;i<m_customer_fixations.size ();i++)
	{
	if (m_customer_fixations .at(i).article ==product)
	{
	for (j=0;j<m_customer_fixations.at (i).customerLocations .size ();j++)
	{
	if (m_customer_fixations .at(i).customerLocations .at(j)==customer )
	return true;
	}
	}
	}*/
	//return false;

	return m_custFixations.isCustomerLocationInFixationsForArticle (customer,product);
}



bool Wepa::produces_plant_product(const int & plant, const int & product)
{
	return m_product_in_plant.at(product).at(plant);
}

bool Wepa::exists_transport_possibility_plant(const int & plant1, const int & plant2)
{
	if (m_transport_indices_plant.at(plant1).at(plant2) != -1)
	{
		return true;
	}
	return false;
}

bool Wepa::exists_transport_possibility_customer(const int & plant, const int & customer)
{
	if (m_transport_indices_customer.at(plant).at(customer) != -1)
	{
		return true;
	}
	return false;
}

bool Wepa::is_product_relevant_for_customer(const int & product, const int & customer)
{
	return m_is_demand_for_customer_and_product.at(customer).at(product);
}

bool Wepa::is_plant_relevant_for_customer(const int & plant, const int & customer)
{
	return m_plant_for_customer.at(plant).at(customer);
}

double Wepa::get_ic_cost(const int & plant1, const int & plant2)
{
	return m_transport_plant.at(get_transport_index_plant(plant1, plant2)).cost / (double)m_lkw_capacity;
}

double Wepa::get_customer_cost(const int & plant, const int & customer)
{
	return m_transport_customer.at(get_transport_index_customer(plant, customer)).cost / (double)m_lkw_capacity;
}

void Wepa::set_transport_possibility(const int & product, const int & plant1, const int & plant2)
{
	m_transport_necessary_for_product.at(plant1).at(plant2).at(product) = true;
}

bool Wepa::is_transport_necessary(const int & product, const int & plant1, const int & plant2)
{
	return m_transport_necessary_for_product.at(plant1).at(plant2).at(product);
}

int Wepa::get_transport_index_plant(const int & plant1, const int & plant2)
{
	return m_transport_indices_plant.at(plant1).at(plant2);
}

int Wepa::get_transport_index_customer(const int & plant, const int & customer)
{
	return m_transport_indices_customer.at(plant).at(customer);
}

void Wepa::set_plant_relevant_for_customer(const int & plant, const int & customer)
{
	m_plant_for_customer.at(plant).at(customer) = true;
}

void Wepa::get_relevant_edges_without_baskets()
{
	int i,j,k,l;

	int cheapest_ic_plant;
	double cheapest_cost;
	double current_cost;
	int countConn;

	for (i = 0; i < m_nProducts; ++i)
	{
		if(m_products.at(i)=="309333")
			cout<<"";
		for (k = 0; k < m_nPlants; ++k)
		{
			if (
				produces_plant_product(k,i) || 
				is_start_inventory_for_product_in_plant(i,k)
				)
			{
				for (j = 0; j < m_nCustomers; ++j)
				{
					//if there exists a transport possibility and if the customer has a demand for the given product
					if (
						(exists_transport_possibility_customer(k,j) && 
						is_product_relevant_for_customer(i,j) ) &&
						m_cpTransports.isPlantValidForCustomerArticle (j,i,k)
						)
					{

						cheapest_ic_plant = -1;
						cheapest_cost = HUGE_VAL;

						for (l = 0; l < m_nPlants; ++l)
						{
							//if there exists a cheaper transport possibility including plant l
							if (exists_transport_possibility_plant(k,l) && 
								exists_transport_possibility_customer(l,j) && 
								m_cpTransports.isPlantValidForCustomerArticle (j,i,l)
								)
							{
								//compute the cost
								current_cost = get_ic_cost(k,l) + get_customer_cost(l,j);
								//if it is the cheapest transport possibility
								if (current_cost < get_customer_cost(k,j))
								{
									cheapest_ic_plant = l;
									cheapest_cost = current_cost;
								}
							}
						}
						//set the cheapest transport possibility
						if (cheapest_ic_plant != -1)
						{
							//std::cout<<"Produkt:"<<m_products.at(i)<<" Plant1:"<<k<<" Plant2:"<<cheapest_ic_plant<<std::endl;
							//std::cin.get();
							set_transport_possibility(i,k,cheapest_ic_plant);
							set_plant_relevant_for_customer(cheapest_ic_plant, j);
						}

					}

					//if there exists no transport possibility to the customer and if the customer has a demand for the given product
					if (!(
						exists_transport_possibility_customer(k, j) && 
						m_cpTransports.isPlantValidForCustomerArticle (j,i,k)
						) && 
						is_product_relevant_for_customer(i,j)
						)
					{
						
						list<int> pCP=m_cpTransports.getPlantsForCustomerProduct(j,i);
						if (pCP.size()>0)
						{
							list<int>::iterator it,itEnd;
							it=pCP.begin();
							itEnd=pCP.end();
							while (it!=itEnd)
							{
								set_transport_possibility(i,k,*it);//(i, k, cheapest_ic_plant);

								set_plant_relevant_for_customer(*it, j);
								it++;
							}	
						}
						else
						{
							cheapest_ic_plant = -1;
							cheapest_cost = HUGE_VAL;

							//try to find the cheapest transport possibility
							for (l = 0; l < m_nPlants; ++l)
							{
								if (exists_transport_possibility_customer(l, j) && 
									m_cpTransports.isPlantValidForCustomerArticle (j,i,l) && 
									exists_transport_possibility_plant(k,l) /* is_plant_relevant_for_customer(l,j)*/
									)
								{
									current_cost = get_ic_cost(k, l) + get_customer_cost(l, j);

									if (current_cost < cheapest_cost)
									{
										cheapest_cost = current_cost;
										cheapest_ic_plant = l;
									}
								}
							}
							if (cheapest_ic_plant != -1)
							{
								set_transport_possibility(i, k, cheapest_ic_plant);
								set_plant_relevant_for_customer(cheapest_ic_plant, j);
							}
						}
						//if a transport possibility had been found
						
					}
				}
			}
		}
		/*
		countConn=0;
		for (k=0;k<m_nPlants;k++)
		{
			for (l=0;l<m_nPlants;l++)
			{
				if (m_transport_necessary_for_product.at(k).at(l).at(i))
				{
					std::cout<<"DB Produkt:"<<m_products.at(i)<<" Plant1:"<<k<<" Plant2:"<<l<<std::endl;
								countConn++;
				}
			}
		}
		std::cout<<"DB #IC-Verbindungen f�r Produkt"<<m_products.at(i)<<":"<<countConn<<std::endl;
		std::cin.get();
		*/
	}
	return;
}


void Wepa::preprocessing()
{
	get_if_production_possible();
	get_transport_indices();
	get_processes_for_product();
	get_if_demand_for_product();
	get_relevant_plants_for_customer();

	if (get_infeasibilities_for_production())
	{
		cout << "Inkonsistenzen in den Daten (Produktion) -> Unzulaessigkeiten in der Optimierung! Siehe " << m_logfilename.c_str() << endl;
		exit(51);
	}



#ifndef DEBUG
	//F�r Debuggen auszukommentieren


	if (get_infeasibilities_for_transport())
	{
		cout << "Inkonsistenzen in den Daten (Transport) -> Siehe " << m_logfilename.c_str() << endl;
		exit(52);
	}
	

#endif


	get_dummy_cost();
	get_bigM();
	get_maximum_demand();

	//construct all relevant edges among plants and between plants and customers for optimization, just used for the possibility to replace customer transports bei cheaper interplant and customer transport
	get_relevant_edges_without_baskets();

	get_inventory_differences();
}

void Wepa::get_maximum_demand()
{
	int i;
	for (i = 0; i < m_demands.size(); ++i)
	{
		if (m_demands.at(i).amount > m_max_demand)
		{
			m_max_demand = m_demands.at(i).amount;
		}
	}
}


void Wepa::get_if_production_possible()
{
	int i;
	//for all processes
	for (i = 0; i < m_nProcesses; ++i)
	{
		//set plant for product to true
		m_product_in_plant.at(m_processes.at(i).product).at(m_processes.at(i).product_plant_index) = true;
	}
}

void Wepa::init_general_vars ()
{
	int i,j,k;

#ifdef TRANSPORT_EVAL_FINAL_INVENTORY
	cout<<"fictitious transports"<<endl;
	for (i=0;i<m_nPlants;i++)
	{
		for (j = 0; j < m_finalPeriodMinInventoryPerCustomer .size() ; ++j)
		{
			if (exists_transport_possibility_customer(i, m_finalPeriodMinInventoryPerCustomer.at(j).customer) && 
				m_cpTransports.isPlantValidForCustomerArticle (m_finalPeriodMinInventoryPerCustomer.at (j).customer,m_finalPeriodMinInventoryPerCustomer.at (j).product,i) && 
				m_finalPeriodMinInventoryPerCustomer.at(j).period+1==m_nPeriods -1)
			{
				//init_customer_transport_variable(i, j);
				//m_transport_customer.at(m_transport_indices_customer.at(plant).at(m_demands.at(demandindex).customer)).cost / (double)m_lkw_capacity, m_var_transport_customer.at(plant).at(demandindex)
				namebuf.str ("");
				namebuf<<"V_FICT_TRANSP_"<<m_plants.at (i)<<"_"<<m_finalPeriodMinInventoryPerCustomer.at (j).customer<<"_"<<m_finalPeriodMinInventoryPerCustomer.at (j).product  <<0;
				cplex->add_var_continuous (namebuf.str().c_str(),0.0,CPX_INFBOUND,m_transport_customer.at(m_transport_indices_customer.at (i).at(m_finalPeriodMinInventoryPerCustomer.at(j).customer)).cost /(double)m_lkw_capacity,m_var_fictitious_transport_customer .at (i).at (j));  //m_transport_indices_customer.at(m_plants .at(i))
			}
		}
	}
#endif
}

/*
namebuf .str("");
namebuf<<"V_CUSTOMER_GROUP_INVENTORY_"<<m_plants.at(plant)<<"_"<<cProdGroup<<"_"<<m_custFixations .get (cProdGroup ).article<<"_"<<cProdGroup<<"_"<<m_periods.at (period-1)<<endl;
cplex->add_var_continuous (namebuf .str().c_str (),0.0,CPX_INFBOUND,0.0,m_var_customer_group_inventory.at (plant).at(cProdGroup ).at(period));
*/
void Wepa::init_vars_for_zero_period()
{
	int i,j;

	cout << "inventory plant..." << endl;
	//variables could be restricted to products which are for plants relevant
	resetVarsForZeroPeriod();
	for (i = 0; i < m_nPlants; ++i)
	{
		for (j = 0; j < m_nProducts; ++j)
		{
			namebuf.str("");
			namebuf << "V_INV_PLANT_" << m_plants.at(i) << "_" << m_products.at(j) << "_" << 0;
			//cplex->add_var_continuous(namebuf.str().c_str(), m_start_inventory.at(i).at(j), m_start_inventory.at(i).at(j), 0.0, m_var_inventory_plant.at(i).at(j).at(0));
			cplex->add_var_continuous(namebuf.str().c_str(), 0, CPX_INFBOUND, 0.0, m_var_inventory_plant.at(i).at(j).at(0));
		}

		for (j = 0; j < m_custFixations.numberACFixations (); ++j)
		{
			namebuf.str("");
			namebuf << "V_CUSTOMER_GROUP_NV_PLANT_" << m_plants.at(i) << "_" << j << "_" << 0;
			//cplex->add_var_continuous(namebuf.str().c_str(), m_start_inventory.at(i).at(j), m_start_inventory.at(i).at(j), 0.0, m_var_inventory_plant.at(i).at(j).at(0));
			//cplex->add_var_continuous(namebuf.str().c_str(), 0, CPX_INFBOUND, 0.0, m_var_inventory_plant.at(i).at(j).at(0));
			cplex->add_var_continuous (namebuf .str().c_str (),0.0,CPX_INFBOUND,0.0,m_var_customer_group_inventory.at (i).at(j).at(0));
		}
	}
}

bool Wepa::init_inventory_diff_variable(const int & product, const int & plant, const int & period)
{
	if (exists_variable(m_var_inventory_difference.at(product).at(plant).at(period - 1)))
	{
		return false;
	}
	namebuf.str("");
	namebuf << "V_ID_" << m_products.at(product) << "_in_" << m_plants.at(plant) << "_in_" << m_periods.at(period - 1);
	cplex->add_var_continuous(namebuf.str().c_str(), 0.0, CPX_INFBOUND, 0.0, m_var_inventory_difference.at(product).at(plant).at(period - 1));

	return true;
}

bool Wepa::init_run_variable(const int & process, const int & period)
{
	if (exists_variable(m_var_run.at(process).at(period-1)))
	{
		return false;
	}

	namebuf.str("");
	namebuf << "V_RUN_" << m_products.at(m_processes.at(process).product) << "_on_" << m_resources.at(m_processes.at(process).machine) << "_" << m_periods.at(period-1);
	cplex->add_var_continuous(namebuf.str().c_str(), 0.0, CPX_INFBOUND, m_processes.at(process).costspershift, m_var_run.at(process).at(period-1));

	return true;
}

//bool Wepa::init_customer_group_production_variable(const int & resource, const int & plant, const int & cProdGroup,const int & period)
bool Wepa::init_customer_group_production_variable(CustFixationProcessInfo & processInfo,//const int & process,
												   const int & cProdGroup,const int & period,bool isConsiderPrioritization)
{
	double usedProductionCost;
	//if (exists_variable (m_var_customer_group_run.at(process).at(cProdGroup).at(period-1)))
	if (exists_variable (m_var_customer_group_run.at(processInfo.processNumber).at(cProdGroup).at(period-1)))
		return false;

	namebuf .str ("");
	//namebuf <<"V_CUSTOMER_GROUP_PRODUCTION_"<<m_resources.at(resource)<<"_"<<m_plants.at(plant)<<"_"<<cProdGroup<<"_"<<m_customer_fixations.at (cProdGroup).article <<"_"<<m_periods.at(period)<<endl;
	//namebuf << "V_CUSTOMER_GROUP_PRODUCTION_" << m_products.at(m_processes.at(process).product) << "_on_" << m_resources.at(m_processes.at(process).machine) <<"_at_"<<m_processes.at(process).product_plant_index << "_" << m_periods.at(period-1);
	namebuf << "V_CUSTOMER_GROUP_PRODUCTION_" << m_products.at(m_processes.at(processInfo.processNumber).product) << "_on_" << m_resources.at(m_processes.at(processInfo.processNumber).machine) <<"_at_"<<m_processes.at(processInfo.processNumber).product_plant_index << "_" << m_periods.at(period-1);
	
	usedProductionCost=m_processes.at(processInfo.processNumber).costspershift;

	if (isConsiderPrioritization && !processInfo.isPrioritized && m_isAllowedProcessesWithPrioritizations.at(cProdGroup))
	{
		usedProductionCost+=m_customerProductionProcessDummyCost.at(processInfo.processNumber);
	}

	cplex->add_var_continuous (namebuf.str().c_str (),0.0,CPX_INFBOUND, usedProductionCost,m_var_customer_group_run.at(processInfo.processNumber).at(cProdGroup).at(period-1));

	return true;
}

bool Wepa::init_run_setup_variable(const int & process, const int & period)
{
	if (exists_variable(m_var_run_setup.at(process).at(period-1)))
	{
		return false;
	}

	namebuf.str("");
	namebuf << "V_RUN_S" << m_products.at(m_processes.at(process).product) << "_on_" << m_resources.at(m_processes.at(process).machine) << "_" << m_periods.at(period - 1);
	cplex->add_var_binary(namebuf.str().c_str(), 0.0, m_var_run_setup.at(process).at(period-1));	

	return true;
}



bool Wepa::init_customer_transport_variable(const int & plant, const int & demandindex)
{
	//if (demandindex==16790)
		//	std::cout<<"Trying to initialize variable for "<<"PlantIndex:"<< plant<<" DemandIndex:" <<demandindex<<"."<<std::endl;
	if (exists_variable(m_var_transport_customer.at(plant).at(demandindex)))
	{
		//if (demandindex==16790)
			//std::cout<<"Variable for "<<"PlantIndex:"<< plant<<" DemandIndex:" <<demandindex<<" already exists."<<std::endl;
		return false;
	}

	namebuf.str("");
	namebuf << "V_TRANS_CUST_" << m_products.at(m_demands.at(demandindex).product) << "_from_" << m_plants.at(plant) << "_to_" << m_customers.at(m_demands.at(demandindex).customer) << "_" << m_periods.at(m_demands.at(demandindex).period);

	cplex->add_var_continuous(namebuf.str().c_str(), 0.0, CPX_INFBOUND, m_transport_customer.at(m_transport_indices_customer.at(plant).at(m_demands.at(demandindex).customer)).cost / (double)m_lkw_capacity, m_var_transport_customer.at(plant).at(demandindex));

	return true;
}

bool Wepa::init_plant_inventory_variable(const int & plant, const int & product, const int & period)
{
	if (exists_variable(m_var_inventory_plant.at(plant).at(product).at(period)))
	{
		return false;
	}

	namebuf.str("");
	namebuf << "V_INV_PLANT_" << m_plants.at(plant) << "_" << m_products.at(product) << "_" << m_periods.at(period - 1);
	//cplex->add_var_continuous(namebuf.str().c_str(), m_min_inventory_v1.at(plant).at(product).at(period-1), CPX_INFBOUND, 0.0, m_var_inventory_plant.at(plant).at(product).at(period));	
	cplex->add_var_continuous (namebuf.str().c_str(), 0, CPX_INFBOUND, 0.0, m_var_inventory_plant.at(plant).at(product).at(period));	
	return true;
}

bool Wepa::init_customer_group_inventory_variable(const int & plant, const int & cProdGroup,const int & period)
{
	if (exists_variable (m_var_customer_group_inventory.at(plant).at(cProdGroup).at(period)))
		return false;

	namebuf .str("");
	namebuf<<"V_CUSTOMER_GROUP_INVENTORY_"<<m_plants.at(plant)<<"_"<<cProdGroup<<"_"<<m_custFixations .get (cProdGroup ).article<<"_"<<cProdGroup<<"_"<<m_periods.at (period-1)<<endl;
	cplex->add_var_continuous (namebuf .str().c_str (),0.0,CPX_INFBOUND,0.0,m_var_customer_group_inventory.at (plant).at(cProdGroup ).at(period));

	return true;
}

bool Wepa::init_ic_variable(const int & product, const int & plant1, const int & plant2, const int & period)
{
	if (exists_variable(m_var_transport_plant.at(plant1).at(plant2).at(product).at(period-1)))
	{
		return false;
	}

	namebuf.str("");
	namebuf << "V_TRANS_PLANT_" << m_products.at(product) << "_from_" << m_plants.at(plant1) << "_to_" << m_plants.at(plant2) << "_" << period;
	cplex->add_var_continuous(namebuf.str().c_str(), 0.0, CPX_INFBOUND, m_transport_plant.at(m_transport_indices_plant.at(plant1).at(plant2)).cost / (double)m_lkw_capacity, m_var_transport_plant.at(plant1).at(plant2).at(product).at(period-1));

	return true;
}

bool Wepa::init_customer_group_ic_variable(const int & sourcePlant, const int & destPlant, const int & cProdGroup,const int & period)
{
	if (exists_variable (m_var_customer_group_transport_plant.at(sourcePlant).at(destPlant).at(cProdGroup ).at(period-1)))
		return false;

	namebuf.str("");
	namebuf<<"V_CUSTOMER_GROUP_IC_TRANSPORT_"<<m_plants.at(sourcePlant)<<"_"<<m_plants .at(destPlant )<<"_"<<m_custFixations .get (cProdGroup ).article<<"_"<<cProdGroup<<"_"<<m_periods.at (period-1)<<endl;
	cplex->add_var_continuous (namebuf .str().c_str (),0.0,CPX_INFBOUND,m_transport_plant.at(m_transport_indices_plant.at(sourcePlant).at(destPlant)).cost / (double)m_lkw_capacity,//0.0,
		m_var_customer_group_transport_plant.at(sourcePlant).at(destPlant).at(cProdGroup ).at(period-1));

	return true;
}

bool Wepa::init_customer_inventory_variable(const int & customer, const int & product, const int & period)
{
	if (exists_variable(m_var_inventory_customer.at(customer).at(product).at(period-1)))
	{
		return false;
	}

	namebuf.str("");
	namebuf << "V_INV_CUST_" << m_customers.at(customer) << "_" << m_products.at(product) << "_" << m_periods.at(period - 1);
	cplex->add_var_continuous(namebuf.str().c_str(), 0.0, CPX_INFBOUND, 0.0, m_var_inventory_customer.at(customer).at(product).at(period-1));

	return true;
}

bool Wepa::init_external_plant_inventory_variable(const int & plant, const int & period)
{
	if (exists_variable(m_var_external_inventory_plant.at(plant).at(period)))
	{
		return false;
	}

	namebuf.str("");
	namebuf << "V_EXT_INV_PLANT_" << m_plants.at(plant) << "_" << m_periods.at(period - 1);
	cplex->add_var_continuous(namebuf.str().c_str(), 0.0, CPX_INFBOUND, m_external_warehouse_cost, m_var_external_inventory_plant.at(plant).at(period));

	return true;
}

bool Wepa::init_dummy_variable(const int & resource, const int & period)
{
	if (exists_variable(m_var_dummy.at(resource).at(period-1)))
	{
		return false;
	}

	namebuf.str("");
	namebuf << "V_DUMMY_" << m_resources.at(resource) << "_" << m_periods.at(period - 1);
	cplex->add_var_continuous(namebuf.str().c_str(), 0.0, CPX_INFBOUND, m_dummy_cost, m_var_dummy.at(resource).at(period-1));

	return true;
}

bool Wepa::init_plant_production_variable(const int & plant, const int & product, const int & period)
{
	if (exists_variable(m_var_plant_run.at(product).at(plant).at(period-1)))
	{
		return false;
	}

	namebuf.str("");
	namebuf << "V_PP_" << m_products.at(product) << "_" << m_plants.at(plant) << "_" << m_periods.at(period - 1);
	cplex->add_var_continuous(namebuf.str().c_str(), 0.0, CPX_INFBOUND, 0.0, m_var_plant_run.at(product).at(plant).at(period-1));

	return true;
}

bool Wepa::init_split_variable(const int & product, const int & period)
{
	if (exists_variable(m_var_split.at(product).at(period-1)))
	{
		return false;
	}
	namebuf.str("");
	namebuf << "V_SPLIT_" << m_products.at(product) << "_" << m_periods.at(period - 1);
	cplex->add_var_binary(namebuf.str().c_str(), 0.0, m_var_split.at(product).at(period-1));


	return true;
}

void Wepa::init_vars_for_period(const int & period,bool isUseCapacityDummies,bool isConsiderPrioritization)
{
	int i,j,k,l;

	//if (period == 1)
		//resetVarsForZeroPeriod();

	resetVarsForPeriod(period);

	cout << "run and run setup..." << endl;
	for (i = 0; i < m_nProcesses; ++i)
	{
		init_run_variable(i,period);
		init_run_setup_variable(i,period);
	}
	
	std::cout<<"TotalV:"<<cplex->varcounter<<"ContV:"<<cplex->numContV<<" IntV:"<<cplex->numIntV<<" BinV:"<<cplex->numBinaryV<<std::endl;
	std::cout<<"TotalConstrs:"<<cplex->conscounter<<std::endl;
	//std::cin.get();
	
	cout << "transportation ic..." << endl;
	for (i = 0; i < m_nPlants; ++i)
	{
		for (j = 0; j < m_nPlants; ++j)
		{
			if (exists_transport_possibility_plant(i,j))
			{
				for (l = 0; l < m_nProducts; ++l)
				{
					if (is_transport_necessary(l,i,j))
					{
						init_ic_variable(l,i,j,period);
					}
				}
			}
		}
	}

	std::cout<<"TotalV:"<<cplex->varcounter<<"ContV:"<<cplex->numContV<<" IntV:"<<cplex->numIntV<<" BinV:"<<cplex->numBinaryV<<std::endl;
	std::cout<<"TotalConstrs:"<<cplex->conscounter<<std::endl;
	//std::cin.get();

	cout << "transportation customer..." << endl;
	for (i = 0; i < m_nPlants; ++i)
	{
		for (j = 0; j < m_demands.size(); ++j)
		{
			/*if (m_demands.at(j).customer==0 && m_demands.at(j).product==0 && m_demands.at(j).period==0)
			{
				std::cout<<"Initializing variable for customer transport."<<" PlantIndex:"<<i<<" DemandIndex:"<<j<<" Index:"<<m_var_transport_customer.at(i).at(j) <<std::endl;
			}
			*/
			if (m_demands.at(j).period==period-1)
			{
				/*if (m_demands.at(j).customer==0 && m_demands.at(j).product==0 && m_demands.at(j).period==0)
				{
					std::cout<<"Deeper in initializing variable for customer transport."
							 <<" PlantIndex:"<<i
							 <<" DemandIndex:"<<j
							 <<" Index:"<<m_var_transport_customer.at(i).at(j) 
							 <<" ExistsTransportPossibility between plant with PlantIndex:"<<i<<" CustomerIndex:"<<m_demands.at(j).customer<<":"<<exists_transport_possibility_customer(i, m_demands.at(j).customer)
							 <<" ValidConnection with respect to baskets between plant with PlantIndex:"<<i<<" and j:"<<j<<":"<<isValidConnectionWithRespectToBaskets(i,j)
							 <<std::endl;
				}*/
				/*
				if (m_plants.at(i) == "DE34431" &&
					m_customers.at(m_demands.at(j).customer) == "LIDLSE@SE@30262" &&
					m_products.at(m_demands.at(j).product) == "022353"
					)
				{
					std::cout << "exists_transport_possibility_customer(i, m_demands.at(j).customer):" << exists_transport_possibility_customer(i, m_demands.at(j).customer)
						<< "isValidConnectionWithRespectToBaskets(i,j):" << isValidConnectionWithRespectToBaskets(i, j)
						<< std::endl;
					cin.get();
				}
				if (m_plants.at(i) == "DE06237" &&
					m_customers.at(m_demands.at(j).customer) == "LIDLDK@DK@4600" &&
					m_products.at(m_demands.at(j).product) == "294573"
					)
				{
					std::cout << "exists_transport_possibility_customer(i, m_demands.at(j).customer):" << exists_transport_possibility_customer(i, m_demands.at(j).customer)
						<< "isValidConnectionWithRespectToBaskets(i,j):" << isValidConnectionWithRespectToBaskets(i, j)
						<< std::endl;
					cin.get();
				}
				*/
				if (m_customers.at(m_demands.at(j).customer) == "LIDLDK@DK@4600" &&
					m_plants.at(i) == "DE55120" &&
					(
						m_products.at(m_demands.at(j).product)=="115163" ||
						m_products.at(m_demands.at(j).product) == "154823" ||
						m_products.at(m_demands.at(j).product) == "294573"
						)
					)
				{
					std::cout<<"Product:"<< m_products.at(m_demands.at(j).product)
						<< " TransPossiblity:" << exists_transport_possibility_customer(i, m_demands.at(j).customer)
						<< " ValidConnBaskets:" << isValidConnectionWithRespectToBaskets(i, j)
						<< endl;
					//cin.get();
				}
				if (exists_transport_possibility_customer(i, m_demands.at(j).customer) &&
					isValidConnectionWithRespectToBaskets(i,j)
					//m_cpTransports.isPlantValidForCustomerArticle (m_demands.at(j).customer,m_demands.at(j).product,i)
					)
				{
					init_customer_transport_variable(i, j);
				}
			}
		}
	}

	std::cout<<"TotalV:"<<cplex->varcounter<<"ContV:"<<cplex->numContV<<" IntV:"<<cplex->numIntV<<" BinV:"<<cplex->numBinaryV<<std::endl;
	std::cout<<"TotalConstrs:"<<cplex->conscounter<<std::endl;
	//std::cin.get();

	cout << "inventory plant..." << endl;
	//variables could be restricted to products which are for plants relevant
	for (i = 0; i < m_nPlants; ++i)
	{
		for (j = 0; j < m_nProducts; ++j)
		{
			init_plant_inventory_variable(i,j,period);
		}
	}

	std::cout<<"TotalV:"<<cplex->varcounter<<"ContV:"<<cplex->numContV<<" IntV:"<<cplex->numIntV<<" BinV:"<<cplex->numBinaryV<<std::endl;
	std::cout<<"TotalConstrs:"<<cplex->conscounter<<std::endl;
	//std::cin.get();

	cout << "external inventory plant..." << endl;
	for (i = 0; i < m_nPlants; ++i)
	{
		init_external_plant_inventory_variable(i,period);
	}

	std::cout<<"TotalV:"<<cplex->varcounter<<"ContV:"<<cplex->numContV<<" IntV:"<<cplex->numIntV<<" BinV:"<<cplex->numBinaryV<<std::endl;
	std::cout<<"TotalConstrs:"<<cplex->conscounter<<std::endl;
	//std::cin.get();

	/*cout << "inventory customer..." << endl;
	for (i = 0; i < m_nCustomers; ++i)
	{
	for (j = 0; j < m_nProducts; ++j)
	{
	if (is_demand_for_customer_and_product_in_period(i,j,period))
	{
	init_customer_inventory_variable(i,j,period);
	}
	}
	}*/
	if (isUseCapacityDummies)
	{
		cout << "dummy resources..." << endl;
		for (i = 0; i < m_nResources; ++i)
		{
			init_dummy_variable(i,period);		
		}
	}
	std::cout<<"TotalV:"<<cplex->varcounter<<"ContV:"<<cplex->numContV<<" IntV:"<<cplex->numIntV<<" BinV:"<<cplex->numBinaryV<<std::endl;
	std::cout<<"TotalConstrs:"<<cplex->conscounter<<std::endl;
	//std::cin.get();

	cout << "production in plant..." << endl;
	for (i = 0; i < m_nProducts; ++i)
	{
		for (j = 0; j < m_nPlants; ++j)
		{
			if (produces_plant_product(j,i))
			{
				init_plant_production_variable(j,i,period);
			}
		}
	}

	std::cout<<"TotalV:"<<cplex->varcounter<<"ContV:"<<cplex->numContV<<" IntV:"<<cplex->numIntV<<" BinV:"<<cplex->numBinaryV<<std::endl;
	std::cout<<"TotalConstrs:"<<cplex->conscounter<<std::endl;
	//std::cin.get();

	cout << "customer production groups..."<<endl;
	
	for (i=0;i<m_custFixations.numberACFixations ();i++)
	{
		for (j=0;j<m_custFixations.get (i).allowedProcesses.size ();j++)
		{
			init_customer_group_production_variable (m_custFixations.get (i).allowedProcesses.at (j),i,period,isConsiderPrioritization);
		}

		for (j=0;j<m_nPlants ;j++)
		{
			init_customer_group_inventory_variable (j,i,period);
			for (l=0;l<m_nPlants ;l++)
			{
				if (exists_transport_possibility_plant(j,l) && is_transport_necessary (m_custFixations .get(i).article,j,l))
				{
					init_customer_group_ic_variable (j,l,i,period);
				}
			}
		}
	}

	std::cout<<"TotalV:"<<cplex->varcounter<<"ContV:"<<cplex->numContV<<" IntV:"<<cplex->numIntV<<" BinV:"<<cplex->numBinaryV<<std::endl;
	std::cout<<"TotalConstrs:"<<cplex->conscounter<<std::endl;
	//std::cin.get();

	cout << "splitting..." << endl;
	for (i = 0; i < m_nProducts; ++i)
	{
		init_split_variable(i,period);
	}

	std::cout<<"TotalV:"<<cplex->varcounter<<"ContV:"<<cplex->numContV<<" IntV:"<<cplex->numIntV<<" BinV:"<<cplex->numBinaryV<<std::endl;
	std::cout<<"TotalConstrs:"<<cplex->conscounter<<std::endl;
	//std::cin.get();

	cout << "inventory difference variables..." << endl;
	for (i = 0; i < m_nProducts; ++i)
	{
		for (j = 0; j < m_nPlants; ++j)
		{
			init_inventory_diff_variable(i, j, period);
		}
	}

	std::cout<<"TotalV:"<<cplex->varcounter<<"ContV:"<<cplex->numContV<<" IntV:"<<cplex->numIntV<<" BinV:"<<cplex->numBinaryV<<std::endl;
	std::cout<<"TotalConstrs:"<<cplex->conscounter<<std::endl;
	//std::cin.get();
}



void Wepa::init_vars()
{
	int i;

	init_vars_for_zero_period();

	//#ifdef TRANSPORT_EVAL_FINAL_INVENTORY
	init_general_vars ();
	//#endif

	for (i = 1; i < m_nPeriods; ++i)
	{
		init_vars_for_period(i,true,false);
	}
}

void Wepa::init_cons()
{
	int i;

	//#ifdef TRANSPORT_EVAL_FINAL_INVENTORY
	init_general_cons ();
	//#endif

	for (i = 1; i < m_nPeriods; ++i)
	{
		init_cons_for_period(i,true);
	}
}

bool Wepa::init_inventory_diff_constraint(const int & product, const int & period)
{
	int i;
	namebuf.str("");
	namebuf << "C_ID_" << m_products.at(product) << "_" << m_periods.at(period - 1);
	cplex->add_cons(namebuf.str().c_str(), 'L', m_inventory_difference.at(product).at(period - 1), m_cons_inventory_difference.at(product).at(period - 1));
	for (i = 0; i < m_nPlants; ++i)
	{
		cplex->add_coeff_linear(m_cons_inventory_difference.at(product).at(period - 1), m_var_inventory_difference.at(product).at(i).at(period - 1), 1.0);
	}
	return true;
}

bool Wepa::init_resource_constraint(const int & resource, const int & period,bool isUseCapacityDummies)
{
	int i,j;

	namebuf.str("");
	namebuf << "C_RES_" << m_resources.at(resource) << "_" << m_periods.at(period - 1);
	cplex->add_cons(namebuf.str().c_str(), 'L', m_capacity.at(resource).at(period - 1)+0.001, m_cons_capacity.at(resource).at(period));
	//if (resource!=14)
	
	if (isUseCapacityDummies)
		cplex->add_coeff_linear(m_cons_capacity.at(resource).at(period), m_var_dummy.at(resource).at(period-1), -1.0);

	for (i = 0; i < m_nProcesses; ++i)
	{
		if (m_processes.at(i).machine == resource)
		{
			cplex->add_coeff_linear(m_cons_capacity.at(resource).at(period), m_var_run.at(i).at(period-1), 1.0);
		}
	}

	for (i=0;i<m_custFixations .numberACFixations ();i++)
	{
		for (j=0;j<m_custFixations .get(i).allowedProcesses.size ();j++)
		{
			if (m_processes.at(m_custFixations.get(i).allowedProcesses.at (j).processNumber).machine ==resource)
				cplex->add_coeff_linear (m_cons_capacity.at(resource).at(period),m_var_customer_group_run.at (m_custFixations.get(i).allowedProcesses.at (j).processNumber).at (i).at (period-1),1.0);
		}
	}

	return true;
}

bool Wepa::init_plant_inventory_constraint(const int & plant, const int & product, const int & period)
{
	int i;

	namebuf.str("");
	namebuf << "C_INV_PLANT_" << m_plants.at(plant) << "_" << m_products.at(product) << "_" << m_periods.at(period - 1);
	cplex->add_cons(namebuf.str().c_str(), 'E', 0.0, m_cons_inventory_plant.at(plant).at(product).at(period));
	cplex->add_coeff_linear(m_cons_inventory_plant.at(plant).at(product).at(period), m_var_inventory_plant.at(plant).at(product).at(period), 1.0);

	if (m_expansion_stage == "1")
	{
		cplex->add_coeff_linear(m_cons_inventory_plant.at(plant).at(product).at(period), m_var_inventory_difference.at(product).at(plant).at(period - 1), -1.0);
	}
	if (m_expansion_stage == "2a")
	{
		cplex->add_coeff_linear(m_cons_inventory_plant.at(plant).at(product).at(period), m_var_inventory_plant.at(plant).at(product).at(period - 1), -1.0);
	}

	for(i = 0; i < m_nPlants; ++i)
	{
		if (exists_transport_possibility_plant(i,plant))
		{
			if (is_transport_necessary(product,i,plant))
			{
				cplex->add_coeff_linear(m_cons_inventory_plant.at(plant).at(product).at(period), m_var_transport_plant.at(i).at(plant).at(product).at(period-1), -1.0);
			}
		}
		if (exists_transport_possibility_plant(plant,i))
		{
			if (is_transport_necessary(product,plant,i))
			{
				cplex->add_coeff_linear(m_cons_inventory_plant.at(plant).at(product).at(period), m_var_transport_plant.at(plant).at(i).at(product).at(period-1), 1.0);
			}
		}
	}

	for (i = 0; i < m_demands.size(); ++i)
	{
		if (m_demands.at(i).product == product && 
			exists_transport_possibility_customer(plant, m_demands.at(i).customer) && 
			m_cpTransports.isPlantValidForCustomerArticle (m_demands.at(i).customer,m_demands.at(i).product,plant) &&
			is_plant_relevant_for_customer(plant, m_demands.at(i).customer) && 
			m_demands.at(i).period == period-1 &&
			!is_in_customer_fixation_group (m_demands.at(i).product ,m_demands.at(i).customer)
			)
		{
			cplex->add_coeff_linear(m_cons_inventory_plant.at(plant).at(product).at(period), m_var_transport_customer.at(plant).at(i), 1.0);
		}
	}

	for (i = 0; i < m_nProcesses; ++i)
	{
		if (m_processes.at(i).product_plant_index == plant && m_processes.at(i).product == product)
		{
			cplex->add_coeff_linear(m_cons_inventory_plant.at(plant).at(product).at(period), m_var_run.at(i).at(period-1), -m_processes.at(i).tonspershift*m_nPallets_per_ton.at(product));
		}
	}

	return true;
}

bool Wepa::init_customer_group_plant_inventory_constraint(const int & plant, const int & cProdGroup, const int & period)
{
	int i,j;
	//std::cout << "Plant Inv Contr:Plant " << plant << " CProdG:" << cProdGroup << " Period " << period << std::endl;
	namebuf.str("");
	if (m_plants.at(plant) == "IT03043")
		i = 0;
	namebuf << "C_CGROUP_INV_PLANT_" << m_plants.at(plant) << "_" <<cProdGroup <<"_"<<m_products.at(m_custFixations .get (cProdGroup ).article) << "_" << m_periods.at(period - 1);
	cplex->add_cons (namebuf.str ().c_str (),'E',0,m_cons_customer_group_inventory_plant .at (plant).at (cProdGroup).at(period));
	cplex->add_coeff_linear (m_cons_customer_group_inventory_plant .at (plant).at (cProdGroup).at(period),m_var_customer_group_inventory.at (plant).at(cProdGroup).at(period),1.0);

	if (m_expansion_stage == "1")
	{
		//cplex->add_coeff_linear(m_cons_inventory_plant.at(plant).at(product).at(period), m_var_inventory_difference.at(product).at(plant).at(period - 1), -1.0);
	}
	if (m_expansion_stage == "2a")
	{
		cplex->add_coeff_linear(m_cons_customer_group_inventory_plant.at(plant).at(cProdGroup).at(period),m_var_customer_group_inventory.at(plant).at(cProdGroup).at(period - 1),-1.0);
	}

	for(i = 0; i < m_nPlants; ++i)
	{
		if (exists_transport_possibility_plant(i,plant))
		{
			if (is_transport_necessary(m_custFixations .get (cProdGroup ).article ,i,plant))
			{
				cplex->add_coeff_linear(m_cons_customer_group_inventory_plant.at(plant).at(cProdGroup).at(period), m_var_customer_group_transport_plant.at (i).at(plant).at(cProdGroup).at(period-1),-1.0); //m_var_transport_plant.at(i).at(plant).at(product).at(period-1), -1.0);
			}
		}
		if (exists_transport_possibility_plant(plant,i))
		{
			if (is_transport_necessary(m_custFixations .get (cProdGroup ).article,plant,i))
			{
				cplex->add_coeff_linear(m_cons_customer_group_inventory_plant.at(plant).at(cProdGroup).at(period),  m_var_customer_group_transport_plant.at(plant).at(i).at(cProdGroup).at(period-1), 1.0);
			}
		}
	}

	for (i = 0; i < m_demands.size(); ++i)
	{
		if (m_demands.at(i).product == m_custFixations .get (cProdGroup ).article && 
			exists_transport_possibility_customer(plant, m_demands.at(i).customer) && 
			m_cpTransports.isPlantValidForCustomerArticle (m_demands.at(i).customer,m_demands.at(i).product,plant) &&
			is_plant_relevant_for_customer(plant, m_demands.at(i).customer) && 
			m_demands.at(i).period == period-1 &&
			is_in_customer_fixation_group (m_demands.at(i).product ,m_demands.at(i).customer)
			)
		{
			//cplex->add_coeff_linear(m_cons_inventory_plant.at(plant).at(product).at(period), m_var_transport_customer.at(plant).at(i), 1.0);
			cplex->add_coeff_linear (m_cons_customer_group_inventory_plant.at(plant).at(cProdGroup).at(period),m_var_transport_customer.at(plant).at(i),1.0);
		}
	}
	//if (cProdGroup>0)
	//{
	for (i=0;i<m_custFixations .get (cProdGroup ).allowedProcesses.size ();i++)
	{
		//m_processes.at (m_customer_fixations .at(cProdGroup).allowedProcesses.at(i)).product_plant_index 
		if (m_processes.at (m_custFixations .get (cProdGroup ).allowedProcesses.at(i).processNumber).product_plant_index==plant
			)//if (m_processes.at(m_customer_fixations .at(cProdGroup).allowedProcesses). //if (m_customer_fixations .at(cProdGroup).allowedProcesses.at (i).plant ==plant)
		{
			cplex->add_coeff_linear (m_cons_customer_group_inventory_plant.at (plant).at(cProdGroup).at(period),
				m_var_customer_group_run.at(m_custFixations .get (cProdGroup ).allowedProcesses.at(i).processNumber).at(cProdGroup).at(period-1),
				-m_processes.at(m_custFixations .get (cProdGroup ).allowedProcesses.at(i).processNumber).tonspershift*m_nPallets_per_ton.at(m_custFixations .get (cProdGroup ).article));
		}
	}
	//}
	return true;
}

bool Wepa::init_external_plant_inventory_constraint(const int & plant, const int & period)
{
	int i,j;

	namebuf.str("");
	namebuf << "C_EXT_INV_PLANT_" << m_plants.at(plant) << "_" << m_periods.at(period - 1);
	cplex->add_cons(namebuf.str().c_str(), 'G', -m_plant_capacities.at(plant), m_cons_external_inventory_plant.at(plant).at(period));
	cplex->add_coeff_linear(m_cons_external_inventory_plant.at(plant).at(period), m_var_external_inventory_plant.at(plant).at(period), 1.0);

	for (i = 0; i < m_nProducts; ++i)
	{
		cplex->add_coeff_linear(m_cons_external_inventory_plant.at(plant).at(period), m_var_inventory_plant.at(plant).at(i).at(period), -1.0);

		for (j=0;j<m_custFixations.numberACFixations ();j++)
		{
			if (m_custFixations .get (j ).article==i)
			{
				cplex->add_coeff_linear (m_cons_external_inventory_plant.at(plant).at(period),m_var_customer_group_inventory .at(plant).at(j).at(period),-1.0);
			}
		}
	}

	return true;
}

bool Wepa::init_customer_inventory_constraint(const int & customer, const int & product, const int & period)
{
	int i,j;

	namebuf.str("");
	namebuf << "C_INV_CUST_" << m_customers.at(customer) << "_" << m_products.at(product) << "_" << m_periods.at(period - 1);
	cplex->add_cons(namebuf.str().c_str(), 'E', 0.0, m_cons_inventory_customer.at(customer).at(product).at(period));

	//if (customer==5 && product==1044 && period==0)
		//i=0;

	for (i = 0; i < m_nPlants; ++i)
	{
		/*if (customer==0 && product==0 && period-1==0)
		{
			std::cout<<"PlantI:"<<i
					 <<" Plant:"<<m_plants.at(i)
					 <<" Customer:"<<m_customers.at(customer)
					 <<" Product:"<<m_products.at(product)
					 <<" Period:"<<m_periods.at(period-1)
					 <<" TranspPossibility:"<<exists_transport_possibility_customer(i, customer)
					 <<" RelevanceForCustomer:"<<is_plant_relevant_for_customer(i, customer)
					 <<" isPlantValidForCustomerArticle:"<<is_plant_relevant_for_customer(i, customer)
					 <<" Constraint:"<<namebuf.str()<<"#"<<m_cons_inventory_customer.at(customer).at(product).at(period)
					 <<std::endl;
		}*/
		if (
			exists_transport_possibility_customer(i, customer) && is_plant_relevant_for_customer(i, customer) &&
			m_cpTransports.isPlantValidForCustomerArticle (customer,product,i)
			)
		{
			/*if (customer==0 && product==0 && period-1==0)
			{
				std::cout<<"Plant considered as transport source:"<<std::endl;
			}*/
			bool isDemandFound=false;
			for (j = 0; j < m_demands.size(); ++j)
			{
				if (m_demands.at(j).customer == customer && m_demands.at(j).product == product && m_demands.at(j).period == period - 1)
				{
					/*if (customer==0 && product==0 && period-1==0)
					{
						std::cout<<"Adding customer transport variable "<<" PlantIndex:"<<i<<" DemandIndex:"<<j<<" Index:"<<m_var_transport_customer.at(i).at(j)<<" to constraint "<<m_cons_inventory_customer.at(customer).at(product).at(period)<<std::endl;
					}*/

					cplex->add_coeff_linear(m_cons_inventory_customer.at(customer).at(product).at(period), m_var_transport_customer.at(i).at(j), -1.0);
					isDemandFound=true;
				}
			}
			/*if (customer==0 && product==0 && period-1==0)
			{
				if (isDemandFound)
					std::cout<<"Demand found for product."<<std::endl;
			}*/
		}
	}


	return true;
}

void Wepa::add_demands(const int & period)
{
	int i;
	for (i = 0; i < m_demands.size(); ++i)
	{
		if (m_demands.at(i).period == period-1)
		{
			//adapt right side of the inventory constraint to the demand, implicitely it is assumed that there is at most one entry per customer and product in a period
			cplex->change_cons_rhs(m_cons_inventory_customer.at(m_demands.at(i).customer).at(m_demands.at(i).product).at(m_demands.at(i).period+1), -m_demands.at(i).amount);

			/*if (m_demands.at(i).product == 1)
			{
			cout << m_demands.at(i).amount << endl;
			char op;
			cin >> op;
			}*/
		}
	}
}

bool Wepa::init_minrun_constraint(const int & process, const int & period)
{
	int i,j;

	namebuf.str("");
	namebuf << "C_MINRUN_" << m_products.at(m_processes.at(process).product) << "_by_" << m_resources.at(m_processes.at(process).machine) << "_in_" << m_plants.at(m_processes.at(process).product_plant_index) << "_" << m_periods.at(period - 1);
	cplex->add_cons(namebuf.str().c_str(), 'G', 0.0, m_cons_minrun.at(process).at(period));
	cplex->add_coeff_linear(m_cons_minrun.at(process).at(period), m_var_run.at(process).at(period-1), 1.0);
	cplex->add_coeff_linear(m_cons_minrun.at(process).at(period), m_var_run_setup.at(process).at(period-1), -m_minruns.at(process).at(period-1));

	for (i=0;i<m_custFixations.numberACFixations ();i++)
	{	
		for (j=0;j<m_custFixations .get (i).allowedProcesses.size();j++)
		{
			if (m_custFixations .get (i).allowedProcesses .at (j).processNumber==process)
			{
				cplex->add_coeff_linear (m_cons_minrun.at(process).at(period),m_var_customer_group_run.at(process).at(i).at(period-1),1.0);
			}
		}
	}

	return true;
}

bool Wepa::init_maxrun_constraint(const int & process, const int & period)
{
	int i,j;

	namebuf.str("");

	namebuf << "C_MAXRUN_" << m_products.at(m_processes.at(process).product) << "_by_" << m_resources.at(m_processes.at(process).machine) << "_in_" << m_plants.at(m_processes.at(process).product_plant_index) << "_" << period;
	cplex->add_cons(namebuf.str().c_str(), 'L', 0.0, m_cons_maxrun.at(process).at(period));
	cplex->add_coeff_linear(m_cons_maxrun.at(process).at(period), m_var_run.at(process).at(period-1), 1.0);
	cplex->add_coeff_linear(m_cons_maxrun.at(process).at(period), m_var_run_setup.at(process).at(period-1), -m_bigM.at(process));

	for (i=0;i<m_custFixations.numberACFixations ();i++)
	{	
		for (j=0;j<m_custFixations .get (i ).allowedProcesses.size();j++)
		{
			if (m_custFixations .get (i).allowedProcesses .at (j).processNumber==process)
			{
				cplex->add_coeff_linear (m_cons_maxrun.at(process).at(period),m_var_customer_group_run.at(process).at(i).at(period-1),1.0);
			}
		}
	}

	return true;
}

bool Wepa::init_split_nr_of_plants_constraint(const int & product, const int & period)
{
	int i,j;

	namebuf.str("");
	namebuf << "C_DONTSPLIT_" << m_products.at(product) << "_" << m_periods.at(period - 1);
	cplex->add_cons(namebuf.str().c_str(), 'L', 1.0, m_cons_ns.at(product).at(period));

	cplex->add_coeff_linear(m_cons_ns.at(product).at(period), m_var_split.at(product).at(period-1), -m_nPlants);

	for (i = 0; i < m_nPlants; ++i)
	{
		if (exists_variable(m_var_plant_run.at(product).at(i).at(period-1)))
		{
			cplex->add_coeff_linear(m_cons_ns.at(product).at(period), m_var_plant_run.at(product).at(i).at(period-1), 1.0);
		}
	}

	return true;
}

bool Wepa::init_split_production_in_plant_constraint(const int & plant, const int & process, const int & period)
{
	namebuf.str("");
	namebuf << "C_SPLITCONNECTOR_" << m_products.at(m_processes.at(process).product) << "_" << m_resources.at(m_processes.at(process).machine) << "_" << m_plants.at(plant) << m_periods.at(period - 1);
	cplex->add_cons(namebuf.str().c_str(), 'G', 0.0, m_cons_ns_connector.at(process).at(plant).at(period));

	cplex->add_coeff_linear(m_cons_ns_connector.at(process).at(plant).at(period), m_var_plant_run.at(m_processes.at(process).product).at(plant).at(period-1), 1.0);
	cplex->add_coeff_linear(m_cons_ns_connector.at(process).at(plant).at(period), m_var_run_setup.at(process).at(period-1), -1.0);

	return true;
}

bool Wepa::init_split_allowed_constraint(const int & product, const int & period)
{
	int i;
	double totalFixationRunTime;	
	int currProcessI;

	
	totalFixationRunTime=0;
	/*
	for (i = 0; i < m_demands.size(); ++i)
	{
		if (m_demands.at(i).period == period-1 && m_demands.at(i).product==product)
		{
			totalDemand+=m_demands.at(i).amount;
		}
	}*/

	for (i=0;i<m_processes_for_product.at(product).size();i++)
	{
		currProcessI=m_processes_for_product.at(product).at(i);
		if (is_fixation(currProcessI,period))//(i,currProcessI))
		{
			totalFixationRunTime+=get_fixation_shifts(currProcessI,period);//(i,currProcessI);
		}
	}
	//std::cout<<"SplitMinRunTimeConstr: Produkt:"<<m_products.at(product) <<" Periode: "<<m_periods.at(period - 1)<<"MinRunTime:"<<m_minsplits.at(product).at(period-1)<<" FixRunTime:"<<totalFixationRunTime<<std::endl;
	namebuf.str("");
	namebuf << "C_SPLITCONNECTOR2_" << m_products.at(product) << "_" << m_periods.at(period - 1);
	cplex->add_cons(namebuf.str().c_str(), 'L', totalFixationRunTime, m_cons_ns_connector2.at(product).at(period));

	cplex->add_coeff_linear(m_cons_ns_connector2.at(product).at(period), m_var_split.at(product).at(period-1), m_minsplits.at(product).at(period-1));

	/*
	for (i = 0; i < m_nProcesses; ++i)
	{
		if (m_processes.at(i).product == product)
		{
			cplex->add_coeff_linear(m_cons_ns_connector2.at(product).at(period), m_var_run.at(i).at(period-1), -1.0);
		}
	}

	for (i=0;i<m_custFixations.numberACFixations ();i++)
	{
		if (m_custFixations .get (i).article ==product)
		{
			for (j=0;j<m_custFixations .get (i ).allowedProcesses.size();j++)
			{
				cplex->add_coeff_linear(m_cons_ns_connector2.at(product).at(period),m_var_customer_group_run .at(m_custFixations .get (i).allowedProcesses.at(j)).at(i).at (period-1),-1.0);
			}
		}
	}*/

	return true;
}

bool Wepa::init_minimum_inventory_constraint(const int & product, const int & period)
{
	int i,j;

	namebuf.str("");
	namebuf << "C_INV_MIN_" << m_products.at(product) << "_" << m_periods.at(period-1)<<0;
	cplex->add_cons(namebuf.str().c_str(), 'G', m_min_inventory_v2.at(product).at(period-1), m_cons_min_inventory_v2.at(product).at(period-1));

	for (i = 0; i < m_nPlants; ++i)
	{
		cplex->add_coeff_linear(m_cons_min_inventory_v2.at(product).at(period-1), m_var_inventory_plant.at(i).at(product).at(period), 1.0);

		for (j=0;j<m_custFixations.numberACFixations ();j++)
		{
			if (m_custFixations.get (j).article ==product)
			{
				cplex->add_coeff_linear (m_cons_min_inventory_v2.at(product).at(period-1),m_var_customer_group_inventory.at(i).at (j).at(period),1.0);
			}
		}
	}

	return true;
}

bool Wepa::init_product_plant_quantity_fixation_contraint(const int & product,const int &plant, const int & period,double quantity)
{
	namebuf.str("");
	//std::cout<<"C_PROD_PLANT_Quantity_" << m_products.at(product) << "_" << m_plants.at(plant) << "_" << m_periods.at(period)<<endl;
	namebuf << "C_PROD_PLANT_Quantity_" << m_products.at(product) << "_" << m_plants.at(plant) << "_" << m_periods.at(period)<<0;
	cplex->add_cons(
					namebuf.str().c_str(), 
					'G',
					quantity,
					m_cons_product_plant_quantity_fixation.at(product)
														  .at(plant)
														  .at(period)
					);// m_min_inventory_v2.at(product).at(period-1), m_cons_min_inventory_v2.at(product).at(period-1));
	return true;
}

void Wepa::init_minInv_LastPeriod_Delivery_Constraint()
{
	//int i,j;
	int plantI,productI,minCustInvI,custFixGI;


#ifdef TRANSPORT_EVAL_FINAL_INVENTORY
	for (minCustInvI=0;minCustInvI<m_finalPeriodMinInventoryPerCustomer.size();minCustInvI++)
	{
		namebuf .str("");
		namebuf<<"C_INV_MIN_TRANSPORT_MEET_MININVENTORY_FOR_CUSTOMER"<<m_finalPeriodMinInventoryPerCustomer.at(minCustInvI).product<<"_"<<m_finalPeriodMinInventoryPerCustomer.at(minCustInvI).customer<<0;
		cplex->add_cons (namebuf.str().c_str(),
			'E',
			0,//m_finalPeriodMinInventoryPerCustomer.at(minCustInvI).amount,
			m_cons_fictitious_transports_quantity_per_customer_meet_minlevel_customer .at(m_finalPeriodMinInventoryPerCustomer .at(minCustInvI).product ).at (m_finalPeriodMinInventoryPerCustomer .at(minCustInvI).customer));

		for (plantI=0;plantI<m_nPlants;plantI++)
		{
			if (
				exists_transport_possibility_customer(plantI,m_finalPeriodMinInventoryPerCustomer.at(minCustInvI).customer ) &&
				m_cpTransports.isPlantValidForCustomerArticle (m_finalPeriodMinInventoryPerCustomer .at(minCustInvI).customer,m_finalPeriodMinInventoryPerCustomer.at(minCustInvI).product,plantI)
				)
			{
				cplex->add_coeff_linear (m_cons_fictitious_transports_quantity_per_customer_meet_minlevel_customer .at(m_finalPeriodMinInventoryPerCustomer .at(minCustInvI).product ).at (m_finalPeriodMinInventoryPerCustomer .at(minCustInvI).customer),
					m_var_fictitious_transport_customer .at (plantI).at (minCustInvI),1);
			}
		}
	}

	for (plantI=0;plantI<m_nPlants;plantI++)
	{
		for (productI=0;productI<m_nProducts;productI++)
		{
			namebuf.str("");
			namebuf<<"C_MIN_INV_TRANSPORT_MEET_INVENTORY_AT_PLANT"<<plantI<<"_"<<productI<<0;

			cplex->add_cons (namebuf.str().c_str (),
				'L',0,m_cons_fictitious_transports_quantity_per_customer_meet_inventorylevel_plant.at(productI).at(plantI));

			cplex->add_coeff_linear (m_cons_fictitious_transports_quantity_per_customer_meet_inventorylevel_plant.at(productI).at(plantI),m_var_inventory_plant.at(plantI).at(productI).at(m_nPeriods-1),-1);

			for (custFixGI=0;custFixGI<m_custFixations.numberACFixations ();custFixGI++)
			{
				if (m_custFixations.get (custFixGI).article ==productI)
				{
					//m_var_customer_group_inventory.at(plant).at(cProdGroup).at(period - 1)
					cplex->add_coeff_linear(m_cons_fictitious_transports_quantity_per_customer_meet_inventorylevel_plant.at(productI).at(plantI),m_var_customer_group_inventory.at(plantI).at (custFixGI).at(m_nPeriods-1),-1);
				}
			}
		}

		for (minCustInvI=0;minCustInvI<m_finalPeriodMinInventoryPerCustomer.size ();minCustInvI++)
		{
			if (
				exists_transport_possibility_customer (plantI,m_finalPeriodMinInventoryPerCustomer .at(minCustInvI).customer) &&
				m_cpTransports.isPlantValidForCustomerArticle (m_finalPeriodMinInventoryPerCustomer .at(minCustInvI).customer, m_finalPeriodMinInventoryPerCustomer .at(minCustInvI).product,plantI)
				)
			{
				cplex->add_coeff_linear (m_cons_fictitious_transports_quantity_per_customer_meet_inventorylevel_plant.at(m_finalPeriodMinInventoryPerCustomer .at(minCustInvI).product).at(plantI),
					m_var_fictitious_transport_customer .at (plantI).at (minCustInvI),1);
			}
		}
	}
#endif
}

void Wepa::init_startInv_Constraint()
{
	int i,j,k;

	for (i=0;i<m_nProducts ;i++)
	{
		for (j=0;j<m_nPlants ;j++)
		{
			namebuf.str("");
			namebuf<<"C_START_INVENTORY_AT_PLANT"<<j<<"_"<<i<<0;
			cplex->add_cons (namebuf .str ().c_str (),'E',m_start_inventory.at (j).at(i),m_cons_start_inventory.at(j).at(i));
			cplex->add_coeff_linear (m_cons_start_inventory.at(j).at(i),m_var_inventory_plant.at(j).at(i).at(0),1.0);

			for (k=0;k<m_custFixations.numberACFixations ();k++)
			{
				if (m_custFixations .get (k).article ==i)
					cplex->add_coeff_linear (m_cons_start_inventory.at(j).at(i),m_var_customer_group_inventory.at(j).at(k).at(0),1.0);
			}
		}
	}
}

void Wepa::init_general_cons()
{
	init_minInv_LastPeriod_Delivery_Constraint ();
	init_startInv_Constraint();
}

void Wepa::init_cons_for_period(const int & period,bool isUseCapacityDummies)
{
	int i,j;

	cout << "resource capacities..." << endl;
	for (i = 0; i < m_nResources; ++i)
	{
		init_resource_constraint(i,period,isUseCapacityDummies);	
	}

	cout << "plant inventory..." << endl;
	for (i = 0; i < m_nPlants; ++i)
	{
		if (i == 8)
			i += 0;
		for (j = 0; j < m_nProducts; ++j)
		{
			//cout<<"Plant:"<<i<<"Product:"<<j<<endl;
			init_plant_inventory_constraint(i,j,period);
		}

		for (j=0;j<m_custFixations.numberACFixations ();j++)
		{
			init_customer_group_plant_inventory_constraint (i,j,period);
		}
	}

	cout << "external plant inventory..." << endl;
	for (i = 0; i < m_nPlants; ++i)
	{
		init_external_plant_inventory_constraint(i,period);
	}

	cout << "customer inventory..." << endl;
	for (i = 0; i < m_nCustomers; ++i)
	{

		for (j = 0; j < m_nProducts; ++j)
		{	
			if (is_demand_for_customer_and_product_in_period(i,j,period))
			{
				init_customer_inventory_constraint(i,j,period);
			}
		}
	}

	cout << "demand for customer inventory..." << endl;
	add_demands(period);

	cout << "minimum production, maximum production or bigM..." << endl;
	for (i = 0; i < m_nProcesses; ++i)
	{
		init_minrun_constraint(i,period);
		init_maxrun_constraint(i,period);
	}

	cout << "splitable products, number of plants..." << endl;
	for (i = 0; i < m_nProducts; ++i)
	{
		init_split_nr_of_plants_constraint(i,period);
	}

	cout << "splitable products, production in plant..." << endl;
	for (i = 0; i < m_nProcesses; ++i)
	{
		for (j = 0; j < m_nPlants; ++j)
		{
			if (produces_plant_product(j,m_processes.at(i).product) && 
				m_processes.at(i).product_plant_index == j
				)
			{
				init_split_production_in_plant_constraint(j,i,period);
			}
		}
	}

	cout << "splitable products, splitting allowed..." << endl;
	for (i = 0; i < m_nProducts; ++i)
	{
		init_split_allowed_constraint(i,period);
	}

	cout << "min inventory v2..." << endl;
	for (i = 0; i < m_nProducts; ++i)
	{
		init_minimum_inventory_constraint(i,period);
	}

	cout << "inventory difference..." << endl;
	for (i = 0; i < m_nProducts; ++i)
	{
		init_inventory_diff_constraint(i, period);
	}
}

bool Wepa::exist_fixed_transports()
{
	if (m_fixed_transports.size() == 0)
	{
		return false;
	}
	return true;
}

/*void Wepa::adapt_and_set_transport_fixations(const bool & set_fixations)
{
//if no fixed transports, return
if (!exist_fixed_transports())
{
return;
}

int i,j;

int plant;
int customer;
int product;
int period;

//for all fixed transports
for (i = 0; i < m_fixed_transports.size(); ++i)
{
//if the transports must be fixed
if (set_fixations)
{
//if the transpoprt variable exists, change the bound, i.e. fix the transport variable
if (exists_variable(m_var_transport_customer.at(m_fixed_transports.at(i).plant).at(m_fixed_transports.at(i).customer).at(m_fixed_transports.at(i).product).at(m_fixed_transports.at(i).period)))
{
cplex->change_bound(m_var_transport_customer.at(m_fixed_transports.at(i).plant).at(m_fixed_transports.at(i).customer).at(m_fixed_transports.at(i).product).at(m_fixed_transports.at(i).period),m_fixed_transports.at(i).amount,'L');
}
//if the transport variable doesn't exist, add the fixed transport variable
else
{
add_customer_transport_variable(m_fixed_transports.at(i).plant,m_fixed_transports.at(i).customer,m_fixed_transports.at(i).product,m_fixed_transports.at(i).period+1, m_fixed_transports.at(i).amount);
cplex->change_bound(m_var_transport_customer.at(m_fixed_transports.at(i).plant).at(m_fixed_transports.at(i).customer).at(m_fixed_transports.at(i).product).at(m_fixed_transports.at(i).period),m_fixed_transports.at(i).amount,'L');
}
}
//if no fixation and the transport variable doesn't exists, add the possibility
//transport variable may not exist if the corresponding product is never supplied from this place to customer
else if (!exists_variable(m_var_transport_customer.at(m_fixed_transports.at(i).plant).at(m_fixed_transports.at(i).customer).at(m_fixed_transports.at(i).product).at(m_fixed_transports.at(i).period)))
{
add_customer_transport_variable(m_fixed_transports.at(i).plant,m_fixed_transports.at(i).customer,m_fixed_transports.at(i).product,m_fixed_transports.at(i).period+1, 0.0);
}

plant = m_fixed_transports.at(i).plant;
customer = m_fixed_transports.at(i).customer;
product = m_fixed_transports.at(i).product;
period = m_fixed_transports.at(i).period+1;

//add all ic transports which could be relevant for the fixed transport
for (j = 0; j < m_nPlants; ++j)
{
if (j != m_fixed_transports.at(i).plant)
{
if (produces_plant_product(j,m_fixed_transports.at(i).product) || is_start_inventory_for_product_in_plant(m_fixed_transports.at(i).product,j))
{
if(exists_transport_possibility_plant(j,m_fixed_transports.at(i).plant))
{
add_ic_variable(j,m_fixed_transports.at(i).plant,m_fixed_transports.at(i).product,m_fixed_transports.at(i).period+1);
}
}
}
}
}
}*/

void Wepa::fix_customer_group_production_to_zero(const int & cGroup,const int & process, const int & period,bool isSoftFixation)
{
	int dummy,i,j,k;
	char usedSense;
	//forbid production on this machine

	if (isSoftFixation)
		//usedSense ='G';
			return;
	else
		usedSense ='E';

	namebuf.str("");

	namebuf << "C_Group_fix_zero_" <<cGroup<<"_" <<m_products.at(m_processes.at(process).product) << "_on_" << m_resources.at(m_processes.at(process).machine) << "_in_" << m_periods.at(period - 1);
	cplex->add_cons(namebuf.str().c_str(), usedSense, 0.0, dummy);//'G', 0.0, dummy);
	//cplex->add_coeff_linear(dummy, m_var_run.at(process).at(period-1), 1.0);
	cplex->add_coeff_linear (dummy,m_var_customer_group_run.at(process).at (cGroup).at (period-1),1.0);
}

void Wepa::fix_production_to_zero(const int & process, const int & period,bool isSoftFixation)
{
	int dummy,i,j,k;
	char usedSense;
	//forbid production on this machine

	if (isSoftFixation)
		//usedSense ='G';
			return;
	else
		usedSense ='E';

	namebuf.str("");

	namebuf << "C_fix_zero_" << m_products.at(m_processes.at(process).product) << "_on_" << m_resources.at(m_processes.at(process).machine) << "_in_" << m_periods.at(period - 1);

	cplex->add_cons(namebuf.str().c_str(), usedSense, 0.0, dummy);//'G', 0.0, dummy);
	cplex->add_coeff_linear(dummy, m_var_run.at(process).at(period-1), 1.0);

	
	for (i=0;i<m_custFixations.numberACFixations ();i++)
	{	
		for (j=0;j<m_custFixations .get (i).allowedProcesses.size ();j++)
		{
			if (m_custFixations .get (i).allowedProcesses .at(j).processNumber==process)
			{	
		//for (k=0;k<m_custFixations .get (i).customerLocations .size();k++)
		//{
		//cplex->add_coeff_linear (dummy,m_var_customer_group_run.at(process).at (m_custFixations .get (i).customerLocations.at(k)).at (period-1),1.0);
		//}
				cplex->add_coeff_linear (dummy,m_var_customer_group_run.at(process).at (i).at (period-1),1.0);
			}
		}
	}
	
	//cplex->change_bound(m_var_run.at(process).at(period - 1), 0.0, 'L');
	//cplex->change_bound(m_var_run.at(process).at(period - 1), /*m_accuracy_machine_production / m_nPallets_per_ton.at(m_processes.at(process).product) / m_processes.at(process).tonspershift*/0.0, 'U');
	
}




//Fixierungen auf kundenbezogene Produktionen ausdehnen
void Wepa::fix_production_to_sales_plan(const int & process, const int & period,bool isSoftFixation)
{
	int dummy,i,j,k;
	char usedSense;

	if (isSoftFixation)
		usedSense ='G';
	else
		usedSense ='E';

	//if (m_products.at(m_processes.at(process).product)=="021701")
		//std::cout<<"Hier."<<std::endl;
	namebuf.str("");
	namebuf << "C_fix_" << m_products.at(m_processes.at(process).product) << "_on_" << m_resources.at(m_processes.at(process).machine) << "_in_" << m_periods.at(period - 1);
	cplex->add_cons(namebuf.str().c_str(), usedSense , m_fixations.at(process).at(period - 1), dummy);//'G', m_fixations.at(process).at(period - 1), dummy);

	cplex->add_coeff_linear(dummy, m_var_run.at(process).at(period-1), 1.0);
	for (i=0;i<m_custFixations.numberACFixations ();i++)
	{
		for (j=0;j<m_custFixations.get(i).allowedProcesses.size ();j++)
		{
			if (m_custFixations .get (i).allowedProcesses .at(j).processNumber==process)
			{
				//for (k=0;k<m_custFixations .get (i).customerLocations .size();k++)
				//{
				//cplex->add_coeff_linear (dummy,m_var_customer_group_run.at(process).at (m_custFixations .get (i).customerLocations.at(k)).at (period-1),1.0);
				//}
				cplex->add_coeff_linear (dummy,m_var_customer_group_run.at(process).at (i).at (period-1),1.0);
			}
		}
	}

	//cplex->change_bound(m_var_run.at(process).at(period - 1), m_fixations.at(process).at(period - 1), 'L');
	//cplex->change_bound(m_var_run.at(process).at(period - 1), m_fixations.at(process).at(period - 1) /*+ m_accuracy_machine_production / m_nPallets_per_ton.at(m_processes.at(process).product) / m_processes.at(process).tonspershift*/, 'U');
}

void Wepa::fix_production_to_plant_production(const int & process, const int & period)
{
	int c,j;

	cplex->add_coeff_linear(
		m_cons_product_plant_quantity_fixation
		.at(m_processes.at(process).product)
		.at(m_processes.at(process)
		.product_plant_index).at(period-1),
		m_var_run.at(process).at(period-1),
		m_processes.at(process).tonspershift*
		m_nPallets_per_ton.at(m_processes.at(process).product)
		);

	for (c=0;c<m_custFixations.numberACFixations();c++)
	{	
		for (j=0;j<m_custFixations .get (c).allowedProcesses.size ();j++)
		{
			if (m_custFixations .get (c).allowedProcesses .at(j).processNumber==process)
			{
				cplex->add_coeff_linear(
							m_cons_product_plant_quantity_fixation
							.at(m_processes.at(process).product)
							.at(m_processes.at(process)
							.product_plant_index).at(period-1),
							m_var_customer_group_run.at(process).at (c).at (period-1),//m_var_run.at(process).at(period-1),
							m_processes.at(process).tonspershift*
									  m_nPallets_per_ton.at(m_processes.at(process).product)
						);
			}
		}
	}
}


void Wepa::adapt_split_injuries()
{
	int i, j, k;
	double runtime;
	int n_used_plants;

	for (k = 1; k < m_nPeriods; ++k)
	{
		for (i = 0; i < m_nProducts; ++i)
		{
			runtime = 0.0;
			n_used_plants = 0;
			vector<bool> used_plants(m_nPlants,false);
			for (j = 0; j < m_nProcesses; ++j)
			{
				//get if plant is used for production
				if (m_fixations.at(j).at(k-1) > 0.0 && 
					m_processes.at(j).product == i
					)
				{
					used_plants.at(m_processes.at(j).product_plant_index) = true;
					runtime += m_fixations.at(j).at(k-1);
				}
			}
			for (j = 0; j < m_nPlants; ++j)
			{
				if (used_plants.at(j))
				{
					n_used_plants++;
				}
			}

			if (n_used_plants > 1 && runtime < m_min_for_split)
			{
				m_minsplits.at(i).at(k-1) = runtime;
			}
		}
	}

}

void Wepa::get_production_per_plant_and_product(vector<vector<vector<double> > > & production_per_plant_and_product, const vector<vector<double> > & fixations)
{
	int i,j;

	production_per_plant_and_product.clear();
	production_per_plant_and_product.resize(m_nPlants,vector<vector<double> >(m_nProducts,vector<double>(m_nPeriods-1,0.0)));

	for (i = 0; i < fixations.size(); ++i)
	{
		for (j = 0; j < m_nPeriods-1; ++j)
		{
			if (fixations.at(i).at(j) <= 0.0)
			{
				continue;
			}
			production_per_plant_and_product.at(m_processes.at(i).product_plant_index).at(m_processes.at(i).product).at(j) += fixations.at(i).at(j) * m_processes.at(i).tonspershift * m_nPallets_per_ton.at(m_processes.at(i).product);
		}
	}
}

double Wepa::get_max_tonpershift(const int & plant, const int & product)
{
	int i;
	double max_tonspershift = -1.0;
	for (i = 0; i < m_nProcesses; ++i)
	{
		if (m_processes.at(i).product_plant_index == plant && m_processes.at(i).product == product)
		{
			if (m_processes.at(i).tonspershift > max_tonspershift)
			{
				max_tonspershift = m_processes.at(i).tonspershift;
			}
		}
	}

	return max_tonspershift;
}

void Wepa::fix_plant_production_for_period(int t)
{	
	/*int i;
	for (i = 0; i < m_nProcesses; ++i)
	{
	//for (j = 0; j < m_nPeriods - 1; ++j)
	//{
	if (!(m_fixations.at(i).at(t) >  0))//(!product_in_plant_in_period.at(m_processes.at(i).product).at(m_processes.at(i).product_plant_index).at(t))
	{

	fix_production_to_zero(i, t+1,false);
	}
	//}
	}*/
	//vector<vector<vector<bool> > > product_in_plant_in_period(m_nProducts, vector<vector<bool> >(m_nPlants, vector<bool>(m_nPeriods - 1)));
	int i,j,c;
	//Indicates whether a product is produced 
	vector<vector<bool> > product_in_plant_in_period(m_nProducts, vector<bool>(m_nPlants));//vector<bool>(m_nProcesses));
	vector<vector<vector<double> > > productQ_in_plant_in_period(m_nProducts, vector<vector<double>>(m_nPlants,vector<double>(m_nPeriods)));
	vector<int> numberOfRemainingProcesses (m_custFixations.numberACFixations(),0);

	for (i=0;i<m_nProducts;i++)
	{
		for (j=0;j<m_nPlants;j++)//m_nProcesses;j++)
		{
			product_in_plant_in_period
				.at(i)
				.at(j)=false;

			productQ_in_plant_in_period
				.at(i)
				.at(j)
				.at(t)=0;
		}
	}

	for (i = 0; i < m_nProcesses; ++i)
	{
		if (m_fixations.at(i).at(t) >  0)
		{
			product_in_plant_in_period
				.at(m_processes.at(i).product)
				.at(m_processes.at(i).product_plant_index)=true;//.at(i)= true;

			productQ_in_plant_in_period
				.at(m_processes.at(i).product)
				.at(m_processes.at(i).product_plant_index)
				.at(t)+=m_fixations.at(i).at(t)*
						m_processes.at(i).tonspershift*
						m_nPallets_per_ton.at(m_processes.at(i).product);
		}
	}

	for (i=0;i<m_nPlants;i++)
	{
		for (j=0;j<m_nProducts;j++)
		{	
			if (product_in_plant_in_period.at(j).at(i))//.at(i))
			{
				init_product_plant_quantity_fixation_contraint(
					j,
					i,
					t,
					productQ_in_plant_in_period
					.at(j)
					.at(i)
					.at(t)
					);
			}
		}
	}

	for (c=0;c<m_custFixations.numberACFixations();c++)
	{	
		for (j=0;j<m_custFixations .get (c).allowedProcesses.size ();j++)
		{
			if (
				product_in_plant_in_period
				.at(m_processes.at(m_custFixations .get (c).allowedProcesses.at(j).processNumber).product)
				.at(m_processes.at(m_custFixations .get (c).allowedProcesses.at(j).processNumber).product_plant_index)//.at(m_custFixations .get (c).allowedProcesses.at(j))
				)
				numberOfRemainingProcesses.at(c)++;
		}
	}

	for (i = 0; i < m_nProcesses; ++i)
	{
		if (!product_in_plant_in_period.at(m_processes.at(i).product).at(m_processes.at(i).product_plant_index))//.at(i))
		{
			fix_production_to_zero(i, t+1,false);
			
			for (c=0;c<m_custFixations.numberACFixations();c++)
			{	
				if (numberOfRemainingProcesses.at(c)>0)
				{
					for (j=0;j<m_custFixations .get (c).allowedProcesses.size ();j++)
					{
						if (m_custFixations .get (c).allowedProcesses .at(j).processNumber==i)
						{
							fix_customer_group_production_to_zero(c,i,t+1,false);
						}
					}
				}
				else
				{
					/*
					cout<<"Warning: No processes left if plant fixations are strictly applied. Ignore plant fixations and allow "
					<<m_resources[m_processes.at(i).machine]
					<< " for product "
					<<m_products[m_processes.at(i).product]
					<<" at plant "
					<<m_plants[m_processes.at(i).product_plant_index]
					<<endl;
					*/
					if (m_additionalAllowedProcessesPlantFixations.find(i)==m_additionalAllowedProcessesPlantFixations.end())
						m_additionalAllowedProcessesPlantFixations.insert(i);
				}
			}
		}
		else
		{
			fix_production_to_plant_production(i,t+1);
		}
	}

	//std::cout<<"Number of additional processes:"<<m_additionalAllowedProcessesPlantFixations.size()<<std::endl;

	/*
	int i,j;
	//vector<vector<vector<bool> > > product_in_plant_in_period(m_nProducts, vector<vector<bool> >(m_nPlants, vector<bool>(m_nPeriods - 1)));
	vector<vector<bool> > product_in_plant_in_period(m_nProducts, vector<bool>(m_nPlants));
	for (i = 0; i < m_nProcesses; ++i)
	{
	if (m_fixations.at(i).at(t) >  0)
	{
	product_in_plant_in_period.at(m_processes.at(i).product).at(m_processes.at(i).product_plant_index)= true;
	}
	}
	for (i = 0; i < m_nProcesses; ++i)
	{
	if (!product_in_plant_in_period.at(m_processes.at(i).product).at(m_processes.at(i).product_plant_index))
	{
	fix_production_to_zero(i, t+1,false);
	}
	}*/
}



void Wepa::fix_plant_production()
{
	int j;

	for (j=0;j<m_nPeriods-1;j++)
	{
		fix_plant_production_for_period (j);
	}
	/*

	int i,j;
	vector<vector<vector<bool> > > product_in_plant_in_period(m_nProducts, vector<vector<bool> >(m_nPlants, vector<bool>(m_nPeriods - 1)));
	for (i = 0; i < m_nProcesses; ++i)
	{
	for (j = 0; j < m_nPeriods - 1; ++j)
	{
	if (m_fixations.at(i).at(j) >  0)
	{
	product_in_plant_in_period.at(m_processes.at(i).product).at(m_processes.at(i).product_plant_index).at(j) = true;
	}
	}
	}
	for (i = 0; i < m_nProcesses; ++i)
	{
	for (j = 0; j < m_nPeriods - 1; ++j)
	{
	if (!product_in_plant_in_period.at(m_processes.at(i).product).at(m_processes.at(i).product_plant_index).at(j))
	{
	fix_production_to_zero(i, j+1,false);
	}
	}
	}
	*/
}

void Wepa::fix_production_per_plant_and_product(const vector<vector<vector<double> > > & production_per_plant_and_product)
{
	int i,j,k,l;
	int dummy;
	int dummy2;

	for (i = 0; i < m_nPlants; ++i)
	{
		for (j = 0; j < m_nProducts; ++j)
		{
			if (produces_plant_product(i,j))
			{
				for (k = 0; k < m_nPeriods-1; ++k)
				{
					namebuf.str("");
					namebuf << "fix_plant_production1_" << m_plants.at(i) << "_" << m_products.at(j) << "_" << m_periods.at(k);
					cplex->add_cons(namebuf.str().c_str(),'G',production_per_plant_and_product.at(i).at(j).at(k),dummy);

					if (get_max_tonpershift(i,j) < 0)
					{
						cout << "neg. Wert!" << endl;
						exit(10);
					}


					namebuf.str("");
					namebuf << "fix_plant_production2_" << m_plants.at(i) << "_" << m_products.at(j) << "_" << m_periods.at(k);
					cplex->add_cons(namebuf.str().c_str(),'L',production_per_plant_and_product.at(i).at(j).at(k)+m_accuracy_plant_production,dummy2);

					for (l = 0; l < m_nProcesses; ++l)
					{
						if (m_processes.at(l).product == j && m_processes.at(l).product_plant_index == i)
						{
							cplex->add_coeff_linear(dummy,m_var_run.at(l).at(k),m_processes.at(l).tonspershift*m_nPallets_per_ton.at(j));
							cplex->add_coeff_linear(dummy2,m_var_run.at(l).at(k),m_processes.at(l).tonspershift*m_nPallets_per_ton.at(j));
						}
					}
				}
			}
		}
	}
}

double Wepa::get_production(const int & product, const int & period)
{
	int i;
	double production = 0.0;

	for (i = 0; i < m_processes.size(); ++i)
	{
		if (m_processes.at(i).product == product)
		{
			if (m_fixations.at(i).at(period-1) > 0.0)
			{
				production += m_fixations.at(i).at(period-1) * m_processes.at(i).tonspershift * m_nPallets_per_ton.at(product);

				/*if (product == 804)
				{
				cout << "F: " << m_fixations.at(i).at(period-1) << endl;
				cout << "T: " << m_processes.at(i).tonspershift << endl;
				cout << "P: " << m_nPallets_per_ton.at(product) << endl;
				cout << "Prod: " << production << endl;

				char op;
				cin >> op;
				}*/
			}
		}
	}

	return production;
}

double Wepa::get_demand(const int & product, const int & period)
{
	int i;
	double demand = 0.0;
	for (i = 0; i < m_demands.size(); ++i)
	{
		if (m_demands.at(i).product == product && m_demands.at(i).period == period-1)
		{
			demand += m_demands.at(i).amount;
		}
	}

	return demand;
}

void Wepa::get_inventory_differences()
{
	int i, k;
	double overalldemand;
	double overallproduction;
	double difference;
	for (k = 1; k < m_nPeriods; ++k)
	{
		for (i = 0; i < m_nProducts; ++i)
		{
			difference = get_demand(i, k) - get_production(i, k);
			if (difference > 0)
			{
				m_inventory_difference.at(i).at(k-1) = difference;
			}
		}
	}
}

void Wepa::adapt_start_inventory()
{
	int i, j, k;
	if (m_expansion_stage == "1")
	{
		for (i = 0; i < m_start_inventory.size(); ++i)
		{
			for (k = 0; k < m_start_inventory.at(i).size(); ++k)
			{
				m_start_inventory.at(i).at(k) = 0;
			}
		}
	}

	double inventory;
	double new_inventory;

	double max_inventory;
	int max_plant;


	for (i = 0; i < m_nProducts; ++i)
	{
		inventory = 0.0;

		max_inventory = -1.0;
		max_plant = -1;

		for (j = 0; j < m_nPlants; ++j)
		{
			inventory += m_start_inventory.at(j).at(i);
			if (m_start_inventory.at(j).at(i) > max_inventory)
			{
				max_inventory = m_start_inventory.at(j).at(i);
				max_plant = j;
			}
		}
		for (k = 1; k < m_nPeriods; ++k)
		{
			new_inventory = inventory + get_production(i, k) - get_demand(i, k);
			if (new_inventory < -0.001)
			{
				m_start_inventory.at(max_plant).at(i) += (-new_inventory);
				inventory = 0.0;
			}
			else
			{
				inventory = new_inventory;
			}
		}
	}
}

double Wepa::get_fixation_shifts(const int & process, const int & period)
{
	if (m_fixations.at(process).at(period-1) <= 0.0)
	{
		return 0.0;
	}
	else
	{
		return m_fixations.at(process).at(period-1);
	}
}

bool Wepa::is_fixation(const int & process, const int & period)
{
	if (m_fixations.at(process).at(period-1) <= 0.0)
	{
		return false;
	}
	return true;
}

bool Wepa::is_threshold(const int & process, const int & period)
{
	int i;
	double cumFixQ=0;
	for (i=0;i<m_nProcesses;i++)
	{
		if (m_processes.at(i).product==m_processes.at(process).product)
		{
			if (is_fixation(i,period))
				cumFixQ+=m_fixations.at(i).at(period-1)*m_processes.at(i).tonspershift*m_nPallets_per_ton.at(m_processes.at(i).product);
		}
	}
/*
	if (m_products.at(m_processes.at(process).product)==std::string("029963"))
	{
		std::cout<<"Produkt:"<<m_products.at(m_processes.at(process).product)<<"Periode:"<<period<<" Prozess:"<<process<<" Fixierung:"<<cumFixQ<<endl;
		std::cin.get();
	}
	*/
	if (cumFixQ<m_fixationThreshold)// && m_fixations.at(process).at(period-1)>0)
	{
		return true;
	}
	return false;
	/*if (process==2)
	{

	cout<<"Treshold:"<<"Product"<<m_products.at(m_processes.at(process).product)<<" Value:"<<m_fixations.at(process).at(period - 1)*m_processes.at(process).tonspershift*m_nPallets_per_ton.at(m_processes.at(process).product)<<""<<endl;
	}*/
	/*
	if (m_products.at(m_processes.at(process).product)==std::string("029963"))
	{
		std::cout<<"Periode:"<<period<<" Prozess:"<<process<<" Fixierung:"<<m_fixations.at(process).at(period-1)*m_processes.at(process).tonspershift*m_nPallets_per_ton.at(m_processes.at(process).product)<<endl;
		std::cin.get();
	}*/
	/*
	if (m_fixations.at(process).at(period-1)*m_processes.at(process).tonspershift*m_nPallets_per_ton.at(m_processes.at(process).product)<m_fixationThreshold)// && m_fixations.at(process).at(period-1)>0)
	{
		return true;
	}
	return false;
	*/
}

void Wepa::set_production_fixations_on_threshold_for_period(const vector<vector<double> > & fixations,int t)
{
	int i, j;
#ifdef FIXATION_THRESHOLD
	for (i = 0; i < m_nProcesses; ++i)
	{
		//for (k = 1; k < m_nPeriods; ++k)
		//{
		if (is_threshold(i,t))//(is_fixation(i, k))
		{
			
			if (is_fixation (i,t))
				fix_production_to_sales_plan(i,t,true);
			else
			{
				fix_production_to_zero (i,t,false);
				
			}
		}
		//}
	}
#endif
}

void Wepa::set_production_fixations_on_threshold(const vector<vector<double> > & fixations)
{
	int k;
#ifdef FIXATION_THRESHOLD
	for (k = 1; k < m_nPeriods; ++k)
	{
		set_production_fixations_on_threshold_for_period(fixations,k);
	}
#endif
	/*
	int i, j, k;
	#ifdef FIXATION_THRESHOLD
	for (i = 0; i < m_nProcesses; ++i)
	{
	if (m_processes.at (i).product==901)
	i+=0;
	for (k = 1; k < m_nPeriods; ++k)
	{

	if (is_threshold(i,k)) //&& is_fixation(i, k))//(is_fixation(i, k))
	{
	if (is_fixation(i, k))
	fix_production_to_sales_plan(i, k,true);
	else
	fix_production_to_zero(i, k,true);
	}
	}
	}
	#endif
	*/
}

void Wepa::set_production_fixations_for_resources_with_fixations (const vector<vector<double>> & fixations)
{
	int k;

	for (k = 1; k < m_nPeriods; ++k)
	{
		set_production_fixations_for_resources_with_fixations_for_period(fixations,k);
	}

}

void Wepa::set_production_fixations_for_resources_with_fixations_for_period(const vector<vector<double> > & fixations,int t)
{
	int i,j;

	for (j=0;j<m_resourcesWithFixations.size();j++)
	{
		for (i = 0; i < m_nProcesses; ++i)
		{
			if (m_processes.at(i).machine==m_resourcesWithFixations.at(j))
			{
				if (is_fixation(i, t))	//Here we allow a resource with fixations if there is a positive amount in the sales plan to produce at least this amount (this is to avoid
										//infeasibilities for numerical reasons)
										//if the amount from the sales plan is zero the production is strictly fixed to zero
				{
					fix_production_to_sales_plan(i, t,true);
				}
				else
				{
					fix_production_to_zero(i, t,false);
				}
			}
		}
	}
}

void Wepa::set_production_fixations_for_period(const vector<vector<double> > & fixations,int t)
{
	int i, j;
	bool found;

	if (m_set_fixations)// == 1)
	{
		for (i = 0; i < m_nProcesses; ++i)
		{
			if (is_fixation(i, t))
			{
				fix_production_to_sales_plan(i, t,false);
			}
			else
			{
				fix_production_to_zero(i, t,false);
			}
		}
	}

	/*
	else if (m_set_fixations == 2)
	{
	for (i = 0; i < m_nProducts; ++i)
	{
	//for (k = 1; k < m_nPeriods; ++k)
	//{
	found = false;
	for (j = 0; j < m_nProcesses; ++j)
	{
	if (m_processes.at(j).product == i)
	{
	if (is_fixation(j, t))
	{
	found = true;
	}
	}
	}
	//}
	}
	}
	else if (m_set_fixations == 3)
	{
	for (i = 0; i < m_nProducts; ++i)
	{
	found = false;
	for (j = 0; j < m_nProcesses; ++j)
	{
	if (m_processes.at(j).product == i)
	{
	//for (k = 1; k < m_nPeriods; ++k)
	//{
	if (is_fixation(j, t))
	{
	found = true;
	}
	//}
	}
	}
	if (found == false)
	{
	//for (k = 1; k < m_nPeriods; ++k)
	//{
	m_min_inventory_v2.at(i).at(t - 1) = 0;
	//}
	}
	}
	}*/
}

void Wepa::set_production_fixations(const vector<vector<double> > & fixations)
{
	int k;

	for (k = 1; k < m_nPeriods; ++k)
	{
		set_production_fixations_for_period (fixations,k);
	}
	/*
	int i, j, k;
	bool found;

	if (m_set_fixations == 1)
	{
	for (i = 0; i < m_nProcesses; ++i)
	{
	for (k = 1; k < m_nPeriods; ++k)
	{
	if (is_fixation(i, k))
	{
	fix_production_to_sales_plan(i, k,false);
	}
	else
	{
	fix_production_to_zero(i, k,false);
	}
	}
	}
	}
	else if (m_set_fixations == 2)
	{
	for (i = 0; i < m_nProducts; ++i)
	{
	for (k = 1; k < m_nPeriods; ++k)
	{
	found = false;
	for (j = 0; j < m_nProcesses; ++j)
	{
	if (m_processes.at(j).product == i)
	{
	if (is_fixation(j, k))
	{
	found = true;
	}
	}
	}
	}
	}
	}
	else if (m_set_fixations == 3)
	{
	for (i = 0; i < m_nProducts; ++i)
	{
	found = false;
	for (j = 0; j < m_nProcesses; ++j)
	{
	if (m_processes.at(j).product == i)
	{
	for (k = 1; k < m_nPeriods; ++k)
	{
	if (is_fixation(j, k))
	{
	found = true;
	}
	}
	}
	}
	if (found == false)
	{
	for (k = 1; k < m_nPeriods; ++k)
	{
	m_min_inventory_v2.at(i).at(k - 1) = 0;
	}
	}
	}
	}
	*/
}




void Wepa::adapt_minimum_runtime(const int & process, const int & period, const double & value)
{
	//std::cout<<"Adapt minimum run time for process "<<process<<" at period"<<period<<"from value "<<m_minruns.at(process).at(period-1) <<" to value "<<value<<endl;
	m_minruns.at(process).at(period-1) = value;
}

double Wepa::get_minimum_runtime(const int & process, const int & period)
{
	return m_minruns.at(process).at(period-1);
}

void Wepa::adapt_minimum_production()
{
	int i,k;

	for (i = 0; i < m_nProcesses; ++i)
	{
		for (k = 1; k < m_nPeriods; ++k)
		{
			if (is_fixation(i,k))
			{
				if (get_fixation_shifts(i,k) < get_minimum_runtime(i,k))
				{
					adapt_minimum_runtime(i,k,get_fixation_shifts(i,k));
				}
			}
		}
	}
}

bool Wepa::solve_period_model
	(
						 int period,
						 const double & epgap, 
						 const bool & polish, 
						 double & gap,
						 bool is_fix_plant_production,
						 bool use_baskets,
						 bool set_fixations,
						 bool just_production_cost,
						 TM &tm,
						 bool isUseCapacityDummies,
						 bool isConsiderPrioritization,
						std::string probSuffix
						 )
{
	std::string lpName;
	bool changed;
	double cumObjVal;
	vector<vector<bool> > solutions;

	cplex->createprob ();

	cout<<"Solving problem for period "<<period<<endl;
	if (isUseCapacityDummies)
		cout<<"Using capacity dummies"<<endl;

	if (isConsiderPrioritization)
		cout<<"Considering prioritizations for customer related production processes"<<endl;
	cout << "init variables for period "<<period<<"..." << endl;
	//if (period == 1)
	//{
	//	init_vars_for_zero_period();
	//}
	init_vars_for_period (period,isUseCapacityDummies,isConsiderPrioritization);//true,false);
	cout << "init constraints for period "<<period<<"..." << endl;
	init_cons_for_period (period,isUseCapacityDummies);
	std::cout<<"TotalV:"<<cplex->varcounter
		  	 <<"ContV:"<<cplex->numContV
			 <<" IntV:"<<cplex->numIntV
			 <<" BinV:"<<cplex->numBinaryV<<std::endl;
	std::cout<<"TotalConstrs:"<<cplex->conscounter<<std::endl;
	//std::cin.get();
	if (is_fix_plant_production)
	{
		cout << "fix plant production for period "<<period<<"..." << endl;
				fix_plant_production_for_period (period-1);
				cout << endl;
	}

	if (set_fixations )
	{
		cout << "fixations for period "<<period<<"..." << endl;
		set_production_fixations_for_period(m_fixations,period);//(fixations);
	}
	else
	{
		cout<<"fixations for period"<<period<<" according to threshold"<<endl;
		set_production_fixations_on_threshold_for_period (m_fixations,period);//(fixations);
	}

	if (m_resourcesWithFixations.size()>0)
	{
		set_production_fixations_for_resources_with_fixations_for_period(m_fixations,period);
	}

	cout << endl;
	cout << "solving model..." << endl;
//	bool polish = false;

			//tm.add_event("Modellierung");
			/*
			#ifndef DEBUG
			if (speicherverbrauch() > 1000000000)
			{
			cout << "Speicherverbrauch zu hoch!" << endl;
			cout << "Pruefen Sie die Input-Daten!" << endl;
			cout << "Abbruch!" << endl;
			exit(1000);
			}
			#endif
			*/
			
	lpName="p"+to_string(period)+probSuffix;

	if (m_set_fixations)
		lpName+="Fix";
	std::cout << "Writing model " << lpName << std::endl;
	cplex->write_problem(lpName);
	cplex->solve_mip(epgap, polish);

	if (!cplex->isSucessFul)
		return false;
	
	gap = cplex->get_gap();
			
	cout <<"Verwendeter Gap"<<epgap<<" Erhaltener Gap:"<<gap<<endl;
	cout << endl;
	//std::cin.get();
			//tm.add_event("Loesung ohne Warenkoerbe");

	if (use_baskets && just_production_cost == false)
	{
		cout << "injured baskets..." << endl;
		

		separate_baskets_part1_for_period(solutions,period-1);
		cout << endl;

		int model = 0;

		changed = separate_baskets_part2_for_period(solutions,period-1);

		while (changed)
		{
			model++;
			changed = false;
					/*
					#ifndef DEBUG
					if (speicherverbrauch() > 1000000000)
					{
					cout << "Speicherverbrauch zu hoch!" << endl;
					cout << "Pruefen Sie die Input-Daten!" << endl;
					cout << "Abbruch!" << endl;
					exit(1000);
					}
					#endif
					*/
					//wepa->solve_model(epgap,polish,gap);//(epgap_start, polish, gap);

			std::string pFN;

			pFN="debugbasket"+std::to_string(period)+ probSuffix +".lp";
			std::cout << "Writing model " << pFN << std::endl;
			cplex->write_problem(pFN);
			cplex->solve_mip(epgap, polish);
			if (!cplex->isSucessFul)
				return false;
			gap = cplex->get_gap();
					
			cout <<"Verwendeter Gap"<<epgap<<" Erhaltener Gap:"<<gap<<endl;
					
			separate_baskets_part1_for_period(solutions,period-1);
			changed = separate_baskets_part2_for_period(solutions,period-1);
					
			if (changed == false)
			{
					break;
			}
			//std::cin.get();
		}
	}
	cout << endl;
	return true;
}

bool Wepa::checkDummies(int period)
{
	int i;

	for (i=0;i<m_nResources;i++)
	{	
		if (cplex->get_varsol(m_var_dummy.at(i).at(period - 1)) > 1)//TOLERANCE_BASKET_BINARIES_TRANSPORT_VARIABLE_CONNECTION)
		{
			std::cout << "Positive capacity dummies found." << std::endl;
			return true;
		}
	}

	return false;
}

double Wepa::solve_model(const double & epgap, 
						 const bool & polish, 
						 double & gap,
						 bool is_fix_plant_production,
						 bool use_baskets,
						 bool set_fixations,
						 bool just_production_cost,
						 TM &tm,
						 vector<production_result> & solution_production, 
						 vector<customer_group_production_result> & customer_group_solution_production,
						 vector<single_transport_result_plant> & solution_single_transport_plant, 
						 vector<single_customer_group_transport_result_plant> & customer_group_solution_single_transport_plant,
						 vector<single_transport_result_customer> & solution_single_transport_customer, 
						 vector<dummy_result> & solution_dummy, 
						 vector<inventory_result> & solution_inventory, 
						 vector<customer_group_inventory_result> & customer_group_solution_inventory,
						 vector<vector<pair<double, double> > > & solution_external_warehouse_info,
						 list<int> & solution_additionalAllowedProcessesPlantFixations,
						 bool considerCustomerProductionPriorities,
						 bool simultaneousConsiderationCapacityDummiesAndProductionProcessPriorities
						 )
{
	int i;
	bool changed;
	double cumObjVal;
	vector<vector<bool> > solutions;
	vector<vector<double> > dummies;
	unordered_set<int>::iterator it,itEnd;
	std::string lpName;
	bool isDummies;
	bool isSuccessful;
	if (m_expansion_stage=="1")
	{
		cumObjVal=0;
		m_additionalAllowedProcessesPlantFixations.clear();
		for (i=1;i<m_nPeriods;i++)
		{
			isSuccessful = false;
			isDummies = false;
			if (considerCustomerProductionPriorities)
			{
				std::cout << "Solving problem for period " << i << " with priorities and without capacity dummies." << endl;
				isSuccessful = solve_period_model(i, epgap, polish, gap, is_fix_plant_production, use_baskets, set_fixations, just_production_cost, tm, false, true, "PrioDummy");
				if (!isSuccessful)
					std::cout << "Infeasibility occurred during solution with priorities." << endl;
			}
			if (!isSuccessful)
			{
				std::cout << "Solving problem for period " << i << " without priorities and with capacity dummies." << endl;
				isSuccessful = solve_period_model(i, epgap, polish, gap, is_fix_plant_production, use_baskets, set_fixations, just_production_cost, tm, true, false, "CapDummy");
				isDummies = true;
			}
			
			if (!isSuccessful)
			{
				std::cout << "Fatal: Solution process stopped due to unresolvable infeasibility." << std::endl;
				//cin.get();
				exit(300);
			}

			
			/*std::cout << "Period " << i << " without prios solved. Considering prios:" << considerCustomerProductionPriorities << std::endl;
			//std::cin.get();

			if (considerCustomerProductionPriorities &&!checkDummies(i))
			{
				solve_period_model(i,epgap,polish,gap,is_fix_plant_production,use_baskets,set_fixations,just_production_cost,tm,false,true,"PrioDummy");
				isDummies = false;
			}*/
			
			cout << "write results..." << endl;

			tm.add_event("Separation der Warenkoerbe");

			write_results_for_period(solution_production,
				customer_group_solution_production,
				solution_single_transport_plant,
				customer_group_solution_single_transport_plant,
				solution_single_transport_customer, 
				/*solution_transport_plant, solution_transport_customer,*/ solution_dummy, 
				solution_inventory,
				customer_group_solution_inventory,
				i, 
				solution_external_warehouse_info,
				isDummies
				);
			std::cout << "Ergebnisse geschrieben." << std::endl;
			//std::cin.get();
			cumObjVal+=cplex->get_objval ();
			std::cout << "Teilzielfunktionswert:" << cplex->get_objval() << std::endl;
			cplex->freeprob ();
		}
		
		//solution_additionalAllowedProcessesPlantFixations.clear();
		//it=m_additionalAllowedProcessesPlantFixations.begin();
		//itEnd=m_additionalAllowedProcessesPlantFixations.end();
		//while (it!=itEnd)
		//{
		//solution_additionalAllowedProcessesPlantFixations.push_back(*it);
		//it++;
		//}
		//return cumObjVal;
	}
	else
	{
		cplex->createprob ();
		cout << "init variables..." << endl;
		init_vars();
		cout << endl;

		cout << "init constraints..." << endl;
		init_cons();
		cout << endl;

		if (is_fix_plant_production)
		{
			cout << "fix plant production..." << endl;
			fix_plant_production();
			cout << endl;
		}

		if (set_fixations)
		{
			cout << "fixations..." << endl;
			set_production_fixations(m_fixations);//(fixations);
		}
		else
		{
			cout<<"fixations according to threshold"<<endl;
			set_production_fixations_on_threshold (m_fixations);//(fixations);
		}

		cout << endl;
		cout << "solving model..." << endl;
		bool polish = false;

		tm.add_event("Modellierung");
		
		//#ifndef DEBUG
		//if (speicherverbrauch() > 1000000000)
		//{
		//cout << "Speicherverbrauch zu hoch!" << endl;
		//cout << "Pruefen Sie die Input-Daten!" << endl;
		//cout << "Abbruch!" << endl;
		//exit(1000);
		//}
		//#endif
		
		//wepa->solve_model(ePGap,false,gap);//(epgap_start, false, gap);
		cplex->solve_mip(epgap, polish);
		gap = cplex->get_gap();
		cout << endl;

		tm.add_event("Loesung ohne Warenkoerbe");

		if (use_baskets && just_production_cost == false)
		{
			cout << "injured baskets..." << endl;
			separate_baskets_part1(solutions);
			cout << endl;

			int model = 0;

			changed = separate_baskets_part2(solutions);

			while (changed)
			{
				model++;
				changed = false;
				
				//#ifndef DEBUG
				//if (speicherverbrauch() > 1000000000)
				//{
				//cout << "Speicherverbrauch zu hoch!" << endl;
				//cout << "Pruefen Sie die Input-Daten!" << endl;
				//cout << "Abbruch!" << endl;
				//exit(1000);
				//}
				//#endif
				
				//wepa->solve_model(epgap,polish,gap);//(epgap_start, polish, gap);
				cplex->solve_mip(epgap, polish);
				gap = cplex->get_gap();
				separate_baskets_part1(solutions);
				changed = separate_baskets_part2(solutions);

				if (changed == false)
				{
					break;
				}
			}
		}

		cout << endl;
		cout << "write results..." << endl;

		tm.add_event("Separation der Warenkoerbe");

		for (i = 1; i < m_nPeriods; ++i)
		{
			//write_results_for_period(solution_production, solution_single_transport_plant, solution_single_transport_customer, solution_dummy, solution_inventory, i, solution_external_warehouse_info);
			//write_results_for_period(solution_production, 
			//solution_single_transport_plant, 
			//solution_single_transport_customer, 
			//solution_dummy, solution_inventory, 
			//i, 
			//solution_external_warehouse_info
			//);
			
			write_results_for_period(solution_production,
				customer_group_solution_production,
				solution_single_transport_plant,
				customer_group_solution_single_transport_plant,
				solution_single_transport_customer, 
				/*solution_transport_plant, solution_transport_customer,*/ solution_dummy, 
				solution_inventory,
				customer_group_solution_inventory,
				i, 
				solution_external_warehouse_info,
				true
				);
		}
		cumObjVal=cplex->get_objval ();
		cplex->freeprob ();	
	}

	solution_additionalAllowedProcessesPlantFixations.clear();

	std::cout<<"Number of additional processes used in plant fixations:"<<m_additionalAllowedProcessesPlantFixations.size()<<std::endl;

	it=m_additionalAllowedProcessesPlantFixations.begin();
	itEnd=m_additionalAllowedProcessesPlantFixations.end();
	while (it!=itEnd)
	{
		solution_additionalAllowedProcessesPlantFixations.push_back(*it);
		it++;
	}

	//std::cout<<"Number of additional processes added to solution:"<<solution_additionalAllowedProcessesPlantFixations.size()<<std::endl;
	tm.add_event("Output");

	tm.end();
	//cplex->write_problem("debug.lp");

	return cumObjVal;//cplex->get_objval();
	
	//cplex->write_problem("debug.lp");
	//cplex->solve_mip(epgap, polish);
	//gap = cplex->get_gap();

	//return cplex->get_objval();
	
}

/*
double Wepa::solve_model(const double & epgap, 
						 const bool & polish, 
						 double & gap,
						 bool is_fix_plant_production,
						 bool use_baskets,
						 bool set_fixations,
						 bool just_production_cost,
						 TM &tm,
						 vector<production_result> & solution_production, 
						 vector<customer_group_production_result> & customer_group_solution_production,
						 vector<single_transport_result_plant> & solution_single_transport_plant, 
						 vector<single_customer_group_transport_result_plant> & customer_group_solution_single_transport_plant,
						 vector<single_transport_result_customer> & solution_single_transport_customer, 
						 vector<dummy_result> & solution_dummy, 
						 vector<inventory_result> & solution_inventory, 
						 vector<customer_group_inventory_result> & customer_group_solution_inventory,
						 vector<vector<pair<double, double> > > & solution_external_warehouse_info,
						 list<int> & solution_additionalAllowedProcessesPlantFixations,
						 bool considerCustomerProductionPriorities,
						 bool simultaneousConsiderationCapacityDummiesAndProductionProcessPriorities
						 )
{
	int i;
	bool changed;
	double cumObjVal;
	vector<vector<bool> > solutions;
	vector<vector<double> > dummies;
	unordered_set<int>::iterator it,itEnd;
	std::string lpName;

	if (m_expansion_stage=="1")
	{
		cumObjVal=0;
		m_additionalAllowedProcessesPlantFixations.clear();
		for (i=1;i<m_nPeriods;i++)
		{
			
			cplex->createprob ();
			cout << "init variables for period "<<i<<"..." << endl;
			init_vars_for_period (i,true,false);
			cout << "init constraints for period "<<i<<"..." << endl;
			init_cons_for_period (i,true);
			std::cout<<"TotalV:"<<cplex->varcounter<<"ContV:"<<cplex->numContV<<" IntV:"<<cplex->numIntV<<" BinV:"<<cplex->numBinaryV<<std::endl;
			std::cout<<"TotalConstrs:"<<cplex->conscounter<<std::endl;
			std::cin.get();
			if (is_fix_plant_production)
			{
				cout << "fix plant production for period "<<i<<"..." << endl;
				fix_plant_production_for_period (i-1);
				cout << endl;
			}

			if (set_fixations )
			{
				cout << "fixations for period "<<i<<"..." << endl;
				set_production_fixations_for_period(m_fixations,i);//(fixations);
			}
			else
			{
				cout<<"fixations for period"<<i<<" according to threshold"<<endl;
				set_production_fixations_on_threshold_for_period (m_fixations,i);//(fixations);
			}

			if (m_resourcesWithFixations.size()>0)
			{
				set_production_fixations_for_resources_with_fixations_for_period(m_fixations,i);
			}

			cout << endl;
			cout << "solving model..." << endl;
			bool polish = false;

			tm.add_event("Modellierung");
			
			//#ifndef DEBUG
			//if (speicherverbrauch() > 1000000000)
			//{
			//cout << "Speicherverbrauch zu hoch!" << endl;
			//cout << "Pruefen Sie die Input-Daten!" << endl;
			//cout << "Abbruch!" << endl;
			//exit(1000);
			//}
			//#endif
			
			lpName="p"+to_string(i);

			if (m_set_fixations)
				lpName+="Fix";
			
			cplex->write_problem(lpName);
			cplex->solve_mip(epgap, polish);
			
			gap = cplex->get_gap();
			
			cout <<"Verwendeter Gap"<<epgap<<" Erhaltener Gap:"<<gap<<endl;
			cout << endl;
			std::cin.get();
			tm.add_event("Loesung ohne Warenkoerbe");

			if (use_baskets && just_production_cost == false)
			{
				cout << "injured baskets..." << endl;
				separate_baskets_part1_for_period(solutions,i-1);
				cout << endl;

				int model = 0;

				changed = separate_baskets_part2_for_period(solutions,i-1);

				while (changed)
				{
					model++;
					changed = false;
					
					//#ifndef DEBUG
					//if (speicherverbrauch() > 1000000000)
					//{
					//cout << "Speicherverbrauch zu hoch!" << endl;
					//cout << "Pruefen Sie die Input-Daten!" << endl;
					//cout << "Abbruch!" << endl;
					//exit(1000);
					//}
					//#endif
					
					//wepa->solve_model(epgap,polish,gap);//(epgap_start, polish, gap);

					std::string pFN;

					pFN="debug"+std::to_string(i)+".lp";

					cplex->write_problem(pFN);
					cplex->solve_mip(epgap, polish);
					
					gap = cplex->get_gap();
					
					cout <<"Verwendeter Gap"<<epgap<<" Erhaltener Gap:"<<gap<<endl;
					
					separate_baskets_part1_for_period(solutions,i-1);
					changed = separate_baskets_part2_for_period(solutions,i-1);
					
					if (changed == false)
					{
						break;
					}
					std::cin.get();
				}
			}
			cout << endl;
			cout << "write results..." << endl;

			tm.add_event("Separation der Warenkoerbe");

			write_results_for_period(solution_production,
				customer_group_solution_production,
				solution_single_transport_plant,
				customer_group_solution_single_transport_plant,
				solution_single_transport_customer, 
				 solution_dummy, 
				solution_inventory,
				customer_group_solution_inventory,
				i, 
				solution_external_warehouse_info
				);

			cumObjVal+=cplex->get_objval ();
			cplex->freeprob ();
		}
		
		//solution_additionalAllowedProcessesPlantFixations.clear();
		//it=m_additionalAllowedProcessesPlantFixations.begin();
		//itEnd=m_additionalAllowedProcessesPlantFixations.end();
		//while (it!=itEnd)
		//{
		//solution_additionalAllowedProcessesPlantFixations.push_back(*it);
		//it++;
		//}
		//return cumObjVal;
	}
	else
	{
		cplex->createprob ();
		cout << "init variables..." << endl;
		init_vars();
		cout << endl;

		cout << "init constraints..." << endl;
		init_cons();
		cout << endl;

		if (is_fix_plant_production)
		{
			cout << "fix plant production..." << endl;
			fix_plant_production();
			cout << endl;
		}

		if (set_fixations)
		{
			cout << "fixations..." << endl;
			set_production_fixations(m_fixations);//(fixations);
		}
		else
		{
			cout<<"fixations according to threshold"<<endl;
			set_production_fixations_on_threshold (m_fixations);//(fixations);
		}

		cout << endl;
		cout << "solving model..." << endl;
		bool polish = false;

		tm.add_event("Modellierung");
		
		//#ifndef DEBUG
		//if (speicherverbrauch() > 1000000000)
		//{
		//cout << "Speicherverbrauch zu hoch!" << endl;
		//cout << "Pruefen Sie die Input-Daten!" << endl;
		//cout << "Abbruch!" << endl;
		//exit(1000);
		//}
		//#endif
		
		//wepa->solve_model(ePGap,false,gap);//(epgap_start, false, gap);
		cplex->solve_mip(epgap, polish);
		gap = cplex->get_gap();
		cout << endl;

		tm.add_event("Loesung ohne Warenkoerbe");

		if (use_baskets && just_production_cost == false)
		{
			cout << "injured baskets..." << endl;
			separate_baskets_part1(solutions);
			cout << endl;

			int model = 0;

			changed = separate_baskets_part2(solutions);

			while (changed)
			{
				model++;
				changed = false;
				
				//#ifndef DEBUG
				//if (speicherverbrauch() > 1000000000)
				//{
				//cout << "Speicherverbrauch zu hoch!" << endl;
				//cout << "Pruefen Sie die Input-Daten!" << endl;
				//cout << "Abbruch!" << endl;
				//exit(1000);
				//}
				//#endif
				
				//wepa->solve_model(epgap,polish,gap);//(epgap_start, polish, gap);
				cplex->solve_mip(epgap, polish);
				gap = cplex->get_gap();
				separate_baskets_part1(solutions);
				changed = separate_baskets_part2(solutions);

				if (changed == false)
				{
					break;
				}
			}
		}

		cout << endl;
		cout << "write results..." << endl;

		tm.add_event("Separation der Warenkoerbe");

		for (i = 1; i < m_nPeriods; ++i)
		{
			//write_results_for_period(solution_production, solution_single_transport_plant, solution_single_transport_customer, solution_dummy, solution_inventory, i, solution_external_warehouse_info);
			//write_results_for_period(solution_production, 
			//solution_single_transport_plant, 
			//solution_single_transport_customer, 
			//solution_dummy, solution_inventory, 
			//i, 
			//solution_external_warehouse_info
			//);
			
			write_results_for_period(solution_production,
				customer_group_solution_production,
				solution_single_transport_plant,
				customer_group_solution_single_transport_plant,
				solution_single_transport_customer, 
				solution_dummy, 
				solution_inventory,
				customer_group_solution_inventory,
				i, 
				solution_external_warehouse_info
				);
		}
		cumObjVal=cplex->get_objval ();
		cplex->freeprob ();	
	}

	solution_additionalAllowedProcessesPlantFixations.clear();

	std::cout<<"Number of additional processes used in plant fixations:"<<m_additionalAllowedProcessesPlantFixations.size()<<std::endl;

	it=m_additionalAllowedProcessesPlantFixations.begin();
	itEnd=m_additionalAllowedProcessesPlantFixations.end();
	while (it!=itEnd)
	{
		solution_additionalAllowedProcessesPlantFixations.push_back(*it);
		it++;
	}

	//std::cout<<"Number of additional processes added to solution:"<<solution_additionalAllowedProcessesPlantFixations.size()<<std::endl;
	tm.add_event("Output");

	tm.end();
	//cplex->write_problem("debug.lp");

	return cumObjVal;//cplex->get_objval();
	
	//cplex->write_problem("debug.lp");
	//cplex->solve_mip(epgap, polish);
	//gap = cplex->get_gap();

	//return cplex->get_objval();
	
}*/



void Wepa::write_results_for_period(vector<production_result> & solution_production,
									vector<customer_group_production_result> & solution_customer_group_production, 
									vector<single_transport_result_plant> & solution_single_transport_plant, 
									vector<single_customer_group_transport_result_plant> & solution_customer_group_single_transport_plant, 
									vector<single_transport_result_customer> & solution_single_transport_customer/*, vector<transport_result_plant> & solution_transport_plant, vector<transport_result_customer> & solution_transport_customer*/, 
									vector<dummy_result> & solution_dummy, 
									vector<inventory_result> & solution_inventory, 
									vector<customer_group_inventory_result> & solution_customer_group_inventory, 
									const int & period, 
									vector<vector<pair<double,double> > > & external_warehouse_info,
									bool isDummies
									)
{
	double epsilon = 0.0001;
	int i,j,k,l,m,n,o;

	//save solution for production
	production_result dummy_production;
	customer_group_production_result dummy_production_customer_group;

	std::cout << "Ergebnis-Prozesse" << std::endl;
	for (k = 0; k < m_nProcesses; ++k)
	{
		if (cplex->get_varsol(m_var_run.at(k).at(period-1)) > epsilon)
		{	
			dummy_production.product = m_processes.at(k).product;
			dummy_production.machine = m_processes.at(k).machine;
			dummy_production.period = period;
			dummy_production.runtime = cplex->get_varsol(m_var_run.at(k).at(period-1));
			dummy_production.process = k;

			solution_production.push_back(dummy_production);
		}

		dummy_production_customer_group.product	= m_processes .at(k).product;
		dummy_production_customer_group.machine = m_processes.at(k).machine;
		dummy_production_customer_group.period = period;
		dummy_production_customer_group.process =k;

		for (i=0;i<m_custFixations.numberACFixations ();i++)
		{
			for (j=0;j<m_custFixations .get (i).allowedProcesses .size();j++)
			{
				if (m_custFixations .get (i).allowedProcesses .at (j).processNumber==k)
				{
					//for (l=0;l<m_custFixations .get (i).customerLocations.size() ;l++)
					//{
					if (cplex->get_varsol (m_var_customer_group_run .at(k).at(i).at (period-1))>epsilon)
					{
						dummy_production_customer_group.cGroup =i;
						//dummy_production.runtime+=cplex->get_varsol (m_var_customer_group_run .at(k).at(m_customer_fixations .at(i).customerLocations.at (l)).at (period-1));
						dummy_production_customer_group.runtime = cplex->get_varsol (m_var_customer_group_run .at(k).at(i).at (period-1));

						solution_customer_group_production.push_back (dummy_production_customer_group);
					}
					//}
				}
			}
		}
	}

	//save solution for ic transports
	single_transport_result_plant dummy_single_plant;
	single_customer_group_transport_result_plant dummy_customer_group_single_plant;
	std::cout << "Ergebnis-IC-Transporte" << std::endl;
	for (i = 0; i < m_nPlants; ++i)
	{
		for (j = 0; j < m_nPlants; ++j)
		{
			if (i != j && m_transport_indices_plant.at(i).at(j) != -1)
			{
				for (l = 0; l < m_nProducts; ++l)
				{
					if (exists_variable(m_var_transport_plant.at(i).at(j).at(l).at(period-1)))
					{
						if (cplex->get_varsol(m_var_transport_plant.at(i).at(j).at(l).at(period-1))>epsilon)
						{
							dummy_single_plant.product = l;
							dummy_single_plant.plant1 = i;
							dummy_single_plant.plant2 = j;
							dummy_single_plant.period = period;
							dummy_single_plant.amount = cplex->get_varsol(m_var_transport_plant.at(i).at(j).at(l).at(period-1));

							solution_single_transport_plant.push_back(dummy_single_plant);
						}
					}


					dummy_customer_group_single_plant.product =l;
					dummy_customer_group_single_plant.plant1=i;
					dummy_customer_group_single_plant.plant2=j;
					dummy_customer_group_single_plant.period=period;

					for (m=0;m<m_custFixations.numberACFixations ();m++)
					{
						if (m_custFixations .get (m).article==l)
						{
							if (exists_variable(m_var_customer_group_transport_plant.at(i).at(j).at(m).at(period-1))>epsilon)
							{
								if (cplex->get_varsol (m_var_customer_group_transport_plant.at(i).at(j).at(m).at(period-1))>epsilon)
								{
									dummy_customer_group_single_plant .cGroup=m;
									dummy_customer_group_single_plant .amount =cplex->get_varsol (m_var_customer_group_transport_plant.at(i).at(j).at(m).at(period-1));
									solution_customer_group_single_transport_plant.push_back (dummy_customer_group_single_plant);
								}
							}
						}
					}
				}
			}
		}
	}

	//save solution for customer transports
	single_transport_result_customer dummy_single_customer;
	std::cout << "Ergebnis-Kunden-Transporte" << std::endl;
	for (i = 0; i < m_nPlants; ++i)
	{
		for (j = 0; j < m_demands.size(); ++j)
		{
			if (exists_variable(m_var_transport_customer.at(i).at(j)) && m_demands.at(j).period == period-1)
			{
				if (cplex->get_varsol(m_var_transport_customer.at(i).at(j)) > epsilon)
				{
					dummy_single_customer.product = m_demands.at(j).product;
					dummy_single_customer.plant = i;
					dummy_single_customer.customer = m_demands.at(j).customer;
					dummy_single_customer.period = period;
					dummy_single_customer.amount = cplex->get_varsol(m_var_transport_customer.at(i).at(j));

					solution_single_transport_customer.push_back(dummy_single_customer);
				}
			}
		}
	}

	if (isDummies)
	{
		//save solution for dummy resources	
		std::cout << "Ergebnis-Dummies" << std::endl;
		dummy_result dummy;
		for (k = 0; k < m_nResources; ++k)
		{
			if (cplex->get_varsol(m_var_dummy.at(k).at(period - 1)) > epsilon)
			{
				dummy.resource = k;
				dummy.period = period;
				dummy.value = cplex->get_varsol(m_var_dummy.at(k).at(period - 1));
				dummy.cost = dummy.value * m_dummy_cost;

				solution_dummy.push_back(dummy);
			}
		}
	}
	//save solution for inventory
	inventory_result dummy_inventory_result;
	customer_group_inventory_result dummy_customer_group_inventory_result;
	std::cout << "Ergebnis-Lagerbest�nde" << std::endl;
	for (i = 0; i < m_nPlants; ++i)
	{
		for (j = 0; j < m_nProducts; ++j)
		{
			dummy_inventory_result.plant = i;
			dummy_inventory_result.product = j;
			dummy_inventory_result.period = period;
			if (m_expansion_stage=="2a" && exists_variable ( m_var_inventory_plant.at(i).at(j).at(period-1)))
				dummy_inventory_result.amount_start = cplex->get_varsol(m_var_inventory_plant.at(i).at(j).at(period-1));
			else
				dummy_inventory_result.amount_start = 0;
			dummy_inventory_result.amount_end = cplex->get_varsol(m_var_inventory_plant.at(i).at(j).at(period));

			solution_inventory.push_back(dummy_inventory_result);

			dummy_customer_group_inventory_result .plant =i;
			dummy_customer_group_inventory_result .product =j;
			dummy_customer_group_inventory_result .period =period;

			for (m=0;m<m_custFixations.numberACFixations ();m++)
			{
				if (m_custFixations.get(m).article==j)
				{
					dummy_customer_group_inventory_result.cGroup=m;
					if (m_expansion_stage == "2a" && exists_variable(m_var_customer_group_inventory.at(i).at(m).at(period - 1)))
						dummy_customer_group_inventory_result.amount_start = cplex->get_varsol(m_var_customer_group_inventory.at(i).at(m).at(period - 1));
					else
						dummy_customer_group_inventory_result.amount_start = 0;
					dummy_customer_group_inventory_result.amount_end = cplex->get_varsol(m_var_customer_group_inventory.at(i).at(m).at(period));
					solution_customer_group_inventory.push_back (dummy_customer_group_inventory_result);
				}
			}
		}
	}
	std::cout << "Ergebnis-Externe Lagerbest�nde" << std::endl;
	for (i = 0; i < m_nPlants; ++i)
	{
		external_warehouse_info.at(i).at(period).first = cplex->get_varsol(m_var_external_inventory_plant.at(i).at(period));
		external_warehouse_info.at(i).at(period).second = m_external_warehouse_cost * cplex->get_varsol(m_var_external_inventory_plant.at(i).at(period));
	}


}



/*void Wepa::get_relevant_baskets_for_plant_customer_product(const int & period, const vector<vector<vector<bool> > > & solutions, const int & customer, const int & plant, const int & product, vector<bool> & relevant, int & nr_relevant)
{
int i,j,k;
bool one_relevant = false;
bool dominates;

//if the product is not transported from the plant to the customer
if (solutions.at(plant).at(customer).at(product) == false)
{
return;
}

relevant.clear();
relevant.resize(m_baskets_for_customer.at(period-1).at(customer).size(),false);

int startindex = -1;

//if there exist baskets for the considered customer
if (m_baskets_for_customer.at(period-1).at(customer).size() > 0)
{
//for all customer baskets
for (i = 0; i < m_baskets_for_customer.at(period-1).at(customer).size(); ++i)
{
//if the considered product in the basket
if (m_baskets_for_customer.at(period-1).at(customer).at(i).at(product) == true)
{
//set the first found basket to the current one
startindex = i;
relevant.at(i) = true;
break;
}
}
}

//for the remaining baskets for the customer
for (j = startindex+1; j < m_baskets_for_customer.at(period-1).at(customer).size(); ++j)
{
//if the considered product is not in the basket, constinue
if (m_baskets_for_customer.at(period-1).at(customer).at(j).at(product) == false)
{
continue;
}

//for all former considered baskets
for (i = startindex; i < j; ++i)
{
//if the basket was marked at relevant
if (relevant.at(i))
{
//if the considered product is not in the basket continue
if (m_baskets_for_customer.at(period-1).at(customer).at(i).at(product) == false)
{
continue;
}

//set the current basket to dominated
dominates = true;

//for all other products
for (k = 0; k < m_nProducts; ++k)
{
if (k != product)
{
//if the earlier basket is not a subset of the current basket
if (m_baskets_for_customer.at(period-1).at(customer).at(i).at(k)-m_baskets_for_customer.at(period-1).at(customer).at(j).at(k) > 0)
{
//set domination to false
dominates = false;	
}
}
}

//if the current basket is dominated by a former basket
if (dominates)
{
//current basket is not relevant
relevant.at(j) = false;
break;
}
}
//if the current basket is not dominated by a former basket
//current basket is relevant
relevant.at(j) = true;
}
}



int nr;
for (i = 0; i < relevant.size(); ++i)
{
//for all relevant baskets
if (relevant.at(i))
{
nr = 0;
//count the number of products in the basket
for (j = 0; j < m_nProducts; ++j)
{
if (m_baskets_for_customer.at(period-1).at(customer).at(i).at(j))
{
nr++;
}
}
//if the number of products is smaller or equal 1 no basket constraint for the product is necessary (all other baskets are dominated by the basket if there is exactly one product in it)
if (nr <= 1)
{
relevant.at(i) = false;
}
}
}


bool injured;
for (i = 0; i < relevant.size(); ++i)
{
//for all relevant baskets
if (relevant.at(i))
{
injured = false;
for (j = 0; j < m_nProducts; ++j)
{
if (j != product)
{
//the basket is injured if a product is in the basket which is not sent to the customer in the solution
if (m_baskets_for_customer.at(period-1).at(customer).at(i).at(j) == true && solutions.at(plant).at(customer).at(j) == false && m_is_demand_for_customer_and_product_in_period.at(customer).at(j).at(period-1))
{
injured = true;
}
}
}

//if basket is not injured
if (injured == false)
{
//all is ok
return;
relevant.at(i) = false;
}
}
}

//count the relevant baskets
nr_relevant = 0;
for (i = 0; i < relevant.size(); ++i)
{
if (relevant.at(i))
{
one_relevant = true;
nr_relevant++;
}
}
}*/

void Wepa::separate_baskets_part1_for_period(vector<vector<bool> > & solutions,int t)
{
	int i, j;

	solutions.clear();
	solutions.resize(m_nPlants, vector<bool>(m_demands.size(), false));
	//fsolutions.clear();
	//fsolutions.resize(m_nPlants, vector<double>(m_demands.size(), 0.0));

	for (i = 0; i < m_nPlants; ++i)
	{
		for (j = 0; j < m_demands.size(); ++j)
		{	
			/*if (m_plants.at(i) == "DE06237" &&
				m_products.at(m_demands.at(j).product) == "154823" &&
				m_customers.at(m_demands.at(j).customer) == "LIDLDK@DK@4600"
				)
			{
				std::cout << "Plant:" << m_plants.at(i) << "Product:" << m_products.at(m_demands.at(j).product) << "Customer:" << m_customers.at(m_demands.at(j).customer) << exists_variable(m_var_transport_customer.at(i).at(j))<<":"<< exists_variable(m_var_transport_customer.at(i).at(j)) << std::endl;
				cin.get();
			}*/
			solutions.at(i).at(j) = false;
			if (m_demands.at(j).period ==t)
			{
				if (exists_variable(m_var_transport_customer.at(i).at(j)))
				{

					if (cplex->get_varsol(m_var_transport_customer.at(i).at(j)) > 0.5)
					{
						solutions.at(i).at(j) = true;
						//fsolutions.at(i).at(j)=cplex->get_varsol(m_var_transport_customer.at(i).at(j)) ;
					}
				}
			}
		}
	}

	//std::cout << "Solution: 16:" << solutions.at(1).at(16) << " 28328:"<<solutions.at(1).at(28328) << std::endl;
	//cin.get();
}

void Wepa::separate_baskets_part1(vector<vector<bool> > & solutions)
{
	int i, j;

	solutions.clear();
	solutions.resize(m_nPlants, vector<bool>(m_demands.size(), false));

	for (i = 0; i < m_nPlants; ++i)
	{
		for (j = 0; j < m_demands.size(); ++j)
		{	
			if (exists_variable(m_var_transport_customer.at(i).at(j)))
			{
				if (cplex->get_varsol(m_var_transport_customer.at(i).at(j)) > 0.5)
				{
					solutions.at(i).at(j) = true;
				}
			}
		}
	}
}

//L�sung festhalten: Transporte pro Periode von Werk nach Kunde
/*void Wepa::separate_baskets_part1(vector<vector<vector<vector<bool> > > > & solutions)
{
int i,j,k,l;

bool changed = false;
solutions.clear();
solutions.resize(m_nPeriods, vector<vector<vector<bool> > >(m_nPlants, vector<vector<bool> >(m_nCustomers, vector<bool>(m_nProducts, false))));

for (i = 0; i < m_nPlants; ++i)
{
for (j = 0; j < m_demands.size(); ++j)
{
if (exists_variable(m_var_transport_customer.at(i).at(j)))
{
solutions.at(m_demands.at(j).period).at(i).at(m_demands.at(i).customer).at(m_demands.at(i).period) = true;
}
}
}
}*/


/*void Wepa::init_basket_variables_and_constraints(const int & plant, const int & customer, const int & product, const int & period, const vector<bool> & relevant_basket)
{
int vardummy;
int consdummy1;
int consdummy2;
int i, l, m;

namebuf.str("");
namebuf << "C_basket2_" << m_plants.at(plant) << "_" << m_customers.at(customer) << "_" << m_products.at(product);
cplex->add_cons(namebuf.str().c_str(), 'L', 0.0, consdummy2);
for (i = 0; i < m_demands.size(); ++i)
{
if (m_demands.at(i).customer == customer && m_demands.at(i).product == product && m_demands.at(i).period == period - 1)
{
cplex->add_coeff_linear(consdummy2, m_var_transport_customer.at(plant).at(i), 1.0);
break;
}
}

//for all injured baskets
for (l = 0; l < m_baskets_for_customer.at(period-1).at(customer).size(); ++l)
{
if (relevant_basket.at(l) == false)
{
continue;
}

//add ic variables
add_ic_transports_for_basket(l);

//add basket constraints
namebuf.str("");
namebuf << "V_basket_" << m_plants.at(plant) << "_" << m_customers.at(customer) << "_" << m_products.at(product) << "_basket_" << l << "_period_" << m_periods.at(period-1);
cplex->add_var_binary(namebuf.str().c_str(), 0.0, vardummy);
cplex->add_coeff_linear(consdummy2, vardummy, -m_max_demand);

for (m = 0; m < m_nProducts; ++m)
{
if (m != product && m_baskets_for_customer.at(period-1).at(customer).at(l).at(m) && m_is_demand_for_customer_and_product_in_period.at(customer).at(m).at(period-1))
{
namebuf.str("");
namebuf << "C_basket_" << m_plants.at(plant) << "_" << m_customers.at(customer) << "_" << m_products.at(product) << "_basket_" << l << "_" << m_products.at(m) << "_period_" << m_periods.at(period-1);
cplex->add_cons(namebuf.str().c_str(), 'L', 0.0, consdummy1);

cplex->add_coeff_linear(consdummy1, vardummy, 1.0);
cplex->add_coeff_linear(consdummy1, m_var_transport_customer.at(plant).at(customer).at(m).at(period-1), -1.0);

}
}
}
}*/

/*
void Wepa::separate_baskets_part1_for_period(vector<vector<bool> > & solutions,int t)
{
	int i, j;

	solutions.clear();
	solutions.resize(m_nPlants, vector<bool>(m_demands.size(), false));

	for (i = 0; i < m_nPlants; ++i)
	{
		for (j = 0; j < m_demands.size(); ++j)
		{	
			if (m_demands.at(j).period ==t)
			{
				if (exists_variable(m_var_transport_customer.at(i).at(j)))
				{
					if (cplex->get_varsol(m_var_transport_customer.at(i).at(j)) > 0.5)
					{
						solutions.at(i).at(j) = true;
					}
					else
					{
						///cout<<"Product:"<<m_products.at(m_demands.at(j).product)<<" TC:"<<cplex->get_varsol(m_var_transport_customer.at(i).at(j))<<endl;
					}
				}
			}
		}
	}
}
*/

int Wepa::get_relevant_baskets_for_plant_and_demand(const int & plant, const int & demandindex, const vector<vector<bool> > & solutions, vector<int> & relevant_baskets)
{
	if (plant == 1 && demandindex == 28328)
		std::cout << "SOL:" << solutions.at(plant).at(demandindex) << "RB:" << relevant_baskets.size() << endl;
	if (!solutions.at(plant).at(demandindex))
	{
		return 0;
	}

	int i, j;
	int basket;
	int nr = 0;
	bool feasible;

	for (i = 0; i < m_basket_coherence.at(demandindex).size(); ++i)
	{
		basket = m_basket_coherence.at(demandindex).at(i);
		feasible = true;

		if (m_baskets.at(basket).size() > 1)
		{
			for (j = 0; j < m_baskets.at(basket).size(); ++j)
			{
				if (!solutions.at(plant).at(m_baskets.at(basket).at(j))
					&& 
					exists_variable(m_var_transport_customer.at(plant)
															.at(m_baskets.at(basket).at(j)))
					)
				{
					relevant_baskets.push_back(basket);

					//if (exists_variable(m_var_transport_customer.at(plant).at(m_baskets.at(basket).at(j))))
						//cplex->get_varsol(m_var_transport_customer.at(plant).at(m_baskets.at(basket).at(j)));
					//cout<<"Product:"<<m_products.at(m_demands.at(m_baskets.at(basket).at(j)).product)<<" TC:"<<fsolutions.at(plant).at(m_baskets.at(basket).at(j))<<endl;
					nr++;
					feasible = false;
					break;
				}
				//else
					//cout<<"Product:"<<m_products.at(m_demands.at(m_baskets.at(basket).at(j)).product)<<" TC:"<<fsolutions.at(plant).at(m_baskets.at(basket).at(j))<<endl;
			}
		}

		if (feasible)
		{
			return 0;
		}
	}

	return nr;
}

void Wepa::add_ic_transports_for_basket(const int & plant, const int & basket, const int & customer)
{
	vector<bool> relevant_plants;
	int customerGroupIndex;
	get_relevant_plants_for_basket(basket, relevant_plants);

	int i,j;

	for (j = 0; j < m_nPlants; ++j)
	{
		if (relevant_plants.at(j) && j != plant)
		{
			for (i = 0; i < m_baskets.at(basket).size(); ++i)
			{
				customerGroupIndex=m_custFixations .getCustomerFixationIndex (customer,m_demands.at(m_baskets.at(basket).at (i)).product);

				//if (customerGroupIndex==100)
				//j+=0;

				if (customerGroupIndex!=-1)
				{
					if (!exists_variable (m_var_customer_group_transport_plant.at(plant).at(j).at(customerGroupIndex).at(m_demands.at(m_baskets.at(basket).at(i)).period)))
					{
						add_customer_group_ic_variable (plant,j,customerGroupIndex,m_demands.at(m_baskets.at(basket).at(i)).period+1);
					}

					if (!exists_variable (m_var_customer_group_transport_plant.at(j).at(plant).at(customerGroupIndex).at(m_demands.at(m_baskets.at(basket).at(i)).period)))
					{
						add_customer_group_ic_variable (j,plant,customerGroupIndex,m_demands.at(m_baskets.at(basket).at(i)).period+1);
					}
				}
				else
				{
					if (!exists_variable(m_var_transport_plant.at(plant).at(j).at(m_demands.at(m_baskets.at(basket).at(i)).product).at(m_demands.at(m_baskets.at(basket).at(i)).period)))
					{
						add_ic_variable(plant, j, m_demands.at(m_baskets.at(basket).at(i)).product, m_demands.at(m_baskets.at(basket).at(i)).period+1);
					}
					if (!exists_variable(m_var_transport_plant.at(j).at(plant).at(m_demands.at(m_baskets.at(basket).at(i)).product).at(m_demands.at(m_baskets.at(basket).at(i)).period)))
					{
						add_ic_variable(j, plant, m_demands.at(m_baskets.at(basket).at(i)).product, m_demands.at(m_baskets.at(basket).at(i)).period+1);
					}
				}
			}
		}
	}
}

void Wepa::init_basket_variables_and_constraints(const vector<int> & relevant_baskets, const int & plant, const int & demandindex)
{
	int vardummy;
	int consdummy1;
	int consdummy2;
	int i, j, l, m;
	
	/*for (i = 0; i < relevant_baskets.size(); ++i)
	{

	cout << "relevant" << endl;
	for (j = 0; j < m_baskets.at(relevant_baskets.at(i)).size(); ++j)
	{
	cout << m_baskets.at(relevant_baskets.at(i)).at(j) << " " << m_plants.at(plant) << " " << m_customers.at(m_demands.at(m_baskets.at(relevant_baskets.at(i)).at(j)).customer) << " " << m_products.at(m_demands.at(m_baskets.at(relevant_baskets.at(i)).at(j)).product) << endl;
	}
	cout << endl;

	char oi;
	cin >> oi;

	}*/
	/*
	if (m_plants.at(plant) == "DE06237" &&
		m_products.at(m_demands.at(demandindex).product) == "154823" &&
		m_customers.at(m_demands.at(demandindex).customer) == "LIDLDK@DK@4600"
		)
	{
		std::cout << "Plant:" << m_plants.at(plant) << "Product:" << m_products.at(m_demands.at(demandindex).product) << "Customer:" << m_customers.at(m_demands.at(demandindex).customer) << "Basketvariable" << std::endl;
		cin.get();
	}*/
	if (m_customers.at(m_demands.at(demandindex).customer) == "LIDLDK@DK@4600" &&
		m_plants.at(plant) == "DE55120" &&
		(
			//m_products.at(m_demands.at(j).product) == "115163" ||
			m_products.at(m_demands.at(demandindex).product) == "154823"// ||
			//m_products.at(m_demands.at(j).product) == "294573"
			)
		)
	{
		std::cout << "Product:" << m_products.at(m_demands.at(demandindex).product)
			<< " TransPossiblity:" << exists_transport_possibility_customer(plant, m_demands.at(demandindex).customer)
			<< " ValidConnBaskets:" << isValidConnectionWithRespectToBaskets(plant, demandindex)
			<< endl;
		//cin.get();
	}

	namebuf.str("");
	namebuf << "C_basket2_" << m_plants.at(plant) << "_" << m_customers.at(m_demands.at(demandindex).customer) << "_" << m_products.at(m_demands.at(demandindex).product);
	cplex->add_cons(namebuf.str().c_str(), 'L', 0.0, consdummy2);
	cplex->add_coeff_linear(consdummy2, m_var_transport_customer.at(plant).at(demandindex),1.0);

	for (i = 0; i < relevant_baskets.size(); ++i)
	{
		//m_demands.at(demandindex).
		//if(demandindex ==1506 && relevant_baskets.at(i)==3199)
		//i+=0;

		add_ic_transports_for_basket(plant, relevant_baskets.at(i),m_demands.at(demandindex).customer);

		namebuf.str("");
		namebuf << "V_basket_" << m_plants.at(plant) << "_" << relevant_baskets.at(i);
		/*
		if (relevant_baskets.at(i) ==64)//(namebuf.str().c_str() == "V_basket_DE06237_53")
		{
			std::cout << "Case:"<< namebuf.str().c_str()<<" Plant:" << m_plants.at(plant) << "Product:" << m_products.at(m_demands.at(demandindex).product) << "Customer:" << m_customers.at(m_demands.at(demandindex).customer) << "Basketvariable" << std::endl;
			cin.get();
		}
		*/
		cplex->add_var_binary(namebuf.str().c_str(), 0.0, vardummy);
		cplex->add_coeff_linear(consdummy2, vardummy, -m_max_demand);
		/*
		bool isAllValidTransports=true;

		for (j=0;j<m_baskets.at(relevant_baskets.at(i)).size();j++)
		{
			if (!m_cpTransports.isPlantValidForCustomerArticle (
			m_demands.at(m_baskets.at(relevant_baskets.at(i)).at(j)).customer,
			m_demands.at(m_baskets.at(relevant_baskets.at(i)).at(j)).product,
			plant))
			isAllValidTransports=false;
		}

		if (isAllValidTransports)
		{*/
			//f�r alle Bedarfe im Warenkorb
			//nur falls es Werks- Kundentransportbeziehung gibt
			if (
				//exists_transport_possibility_customer(plant, m_demands.at(m_baskets.at(relevant_baskets.at(i)).at(0)).customer) 
					isValidConnectionWithRespectToBaskets(plant,demandindex)
				/*&&
				m_cpTransports.isPlantValidForCustomerArticle (
				m_demands.at(m_baskets.at(relevant_baskets.at(i)).at(0)).customer,
				m_demands.at(m_baskets.at(relevant_baskets.at(i)).at(0)).product,
				plant)*/
				)
			{
				for (j = 0; j < m_baskets.at(relevant_baskets.at(i)).size(); ++j)
				{
					/*if (m_plants.at(plant)=="DE34431" && 
					m_customers.at(m_demands.at(m_baskets.at(relevant_baskets.at(i)).at(j)).customer)=="KODINA@HU@2022" &&
					m_products.at(m_demands.at(m_baskets.at(relevant_baskets.at(i)).at(j)).product)=="276085"	
					)
					j+=0;*/

					namebuf.str("");
					namebuf << "C_basket_" << m_plants.at(plant) << "_" << m_customers.at(m_demands.at(m_baskets.at(relevant_baskets.at(i)).at(j)).customer) << "_" << m_products.at(m_demands.at(m_baskets.at(relevant_baskets.at(i)).at(j)).product) << "_basket_" << relevant_baskets.at(i);
					//if (m_products.at(m_demands.at(m_baskets.at(relevant_baskets.at(i)).at(j)).product)=="154823")
						//std::cout << "Basket-Constraint:" << namebuf.str() << " hinzugef�gt." << "Rechte Seite:" << TOLERANCE_BASKET_BINARIES_TRANSPORT_VARIABLE_CONNECTION << //std::endl;
					cplex->add_cons(namebuf.str().c_str(), 'L', 
									TOLERANCE_BASKET_BINARIES_TRANSPORT_VARIABLE_CONNECTION,//0.0, //Here we allow a tolerance to cope with numerical imprecision of the continous transport variables leading
																								//infeasibilites although a feasible solution in fact exists
									consdummy1
									);

					cplex->add_coeff_linear(consdummy1, vardummy, 1.0);
					/*
					if (m_customers.at(m_demands.at(j).customer) == "LIDLDK@DK@4600" &&
						m_plants.at(i) == "DE55120" &&
						(
							m_products.at(m_demands.at(j).product) == "115163" ||
							m_products.at(m_demands.at(j).product) == "154823" ||
							m_products.at(m_demands.at(j).product) == "294573"
							)
						)
					{
						std::cout << "Product:" << m_products.at(m_demands.at(j).product)
							<< " TransPossiblity:" << exists_transport_possibility_customer(i, m_demands.at(j).customer)
							<< " ValidConnBaskets:" << isValidConnectionWithRespectToBaskets(i, j)
							<< endl;
						cin.get();
					}
					*/
					//if (
						//exists_transport_possibility_customer(plant, m_demands.at(demandindex).customer) &&
						//isValidConnectionWithRespectToBaskets(plant, demandindex)
						//)
					if (m_var_transport_customer.at(plant).at(m_baskets.at(relevant_baskets.at(i)).at(j))>0) //Check whether transport variable actually exists
						//as some plants may be excluded for transports due to the customer related transports
						//Otherwise we get infeasibilities due to -1 terms 
					cplex->add_coeff_linear(consdummy1, m_var_transport_customer.at(plant).at(m_baskets.at(relevant_baskets.at(i)).at(j)), -1.0);
				}
			}
		//}
	}
}

bool Wepa::separate_baskets_part2_for_period(const vector<vector<bool> > & solutions,int t)
{
	int i,j,k,n;
	int nr;
	int injured_basket_nr = 0;
	int injured_customer_nr = 0;

	vector<bool> injured_customer(m_nCustomers, false);

	//ofstream outputb("verletzteWK.csv",ios::app);
	list<int> vBs;
	for (i = 0; i < m_nPlants; ++i)
	{
		//std::cout<<"Werk:"<<i<<std::endl;
		for (j = 0; j < m_demands.size(); ++j)
		{
			/*
			if (i==10)
			{
			std::cout<<"Nachfrage "<<j<<std::endl;
			if (j==18636)
			i+=0;
			}
			*/
			if (m_demands.at(j).period ==t)
			{
				vector<int> relevant_baskets;
				nr = get_relevant_baskets_for_plant_and_demand(i, j, solutions, relevant_baskets);
				/*
				if (m_plants.at(i) == "DE34431" &&
					(m_products.at(m_demands.at(j).product) == "022353"|| m_products.at(m_demands.at(j).product) == "294573") &&
					m_customers.at(m_demands.at(j).customer) == "LIDLSE@SE@30262"
					)
				{
					std::cout << "DemIndex:"<<j<<" Plant:" << m_plants.at(i) << "Product:" << m_products.at(m_demands.at(j).product) << "Customer:" << m_customers.at(m_demands.at(j).customer) << exists_variable(m_var_transport_customer.at(i).at(j)) << "NR:" <<  nr<< std::endl;
					cin.get();
				}
				*/
				if (m_plants.at(i) == "DE55120" &&
					(m_products.at(m_demands.at(j).product) == "154823") &&
					m_customers.at(m_demands.at(j).customer) == "LIDLDK@DK@4600"
					)
				{
					std::cout << "DemIndex:" << j << " Plant:" << m_plants.at(i) << "Product:" << m_products.at(m_demands.at(j).product) << "Customer:" << m_customers.at(m_demands.at(j).customer) << exists_variable(m_var_transport_customer.at(i).at(j)) << "NR:" << nr << std::endl;
					//cin.get();
				}
				if (nr > 0)
				{
					//outputb << endl;
					//outputb << m_customers.at(m_demands.at(j).customer) << ";" << m_products.at(m_demands.at(j).product) << ";" << m_periods.at(m_demands.at(j).period) << ";" << m_demands.at(j).amount << endl;

					for (k = 0; k < relevant_baskets.size(); ++k)
					{
						injured_customer.at(m_customer_for_basket.at(relevant_baskets.at(k))) = true;
						vBs.push_back(relevant_baskets.at(k));
						for (n = 0; n < m_baskets.at(relevant_baskets.at(k)).size(); ++n)
						{
							//outputb << m_customers.at(m_demands.at(m_baskets.at(relevant_baskets.at(k)).at(n)).customer) << ";" << m_products.at(m_demands.at(m_baskets.at(relevant_baskets.at(k)).at(n)).product) << ";" << m_periods.at(m_demands.at(m_baskets.at(relevant_baskets.at(k)).at(n)).period) << ";" << m_demands.at(m_baskets.at(relevant_baskets.at(k)).at(n)).amount;
							//outputb << endl;
							
						}
						//outputb << endl;

					}

					//outputb << endl;

					injured_basket_nr += nr;
					//std::cout<<"Werk:"<<i<<" Nachfrage:"<<j<<std::endl;
					init_basket_variables_and_constraints(relevant_baskets,i,j);
				}
			}
		}
	}

	//outputb.close();

	for (k = 0; k < m_nCustomers; ++k)
	{
		injured_customer_nr += injured_customer.at(k);
	}
	list<int>::iterator it;
	cout << endl << "number of concerned baskets (violated constraints): " << injured_basket_nr <<":";//<< endl;
	it=vBs.begin();
	for (k = 0; k < vBs.size(); ++k)
	{	
			if (k>0)
				cout<<",";
		
			cout<<*it;
			
			it++;
	}
	cout<<endl << endl;
	cout << "number of concerned customers (violated constraints): " << injured_customer_nr << ":";//endl << endl;
	bool isFirst=true;
	for (k = 0; k < m_nCustomers; ++k)
	{
		if (injured_customer.at(k))
		{
			if (!isFirst)
				cout<<",";
		
			cout<<k;
			isFirst=false;
		}
	}
	cout<<endl << endl;
	if (injured_customer_nr == 0)
	{
		return false;
	}

	return true;
}

bool Wepa::separate_baskets_part2(const vector<vector<bool> > & solutions)
{
	int i,j,k,n;
	int nr;
	int injured_basket_nr = 0;
	int injured_customer_nr = 0;

	vector<bool> injured_customer(m_nCustomers, false);

	//ofstream outputb("verletzteWK.csv",ios::app);

	for (i = 0; i < m_nPlants; ++i)
	{
		//std::cout<<"Werk:"<<i<<std::endl;
		for (j = 0; j < m_demands.size(); ++j)
		{
			/*
			if (i==10)
			{
			std::cout<<"Nachfrage "<<j<<std::endl;
			if (j==18636)
			i+=0;
			}
			*/
			vector<int> relevant_baskets;
			//nr = get_relevant_baskets_for_plant_and_demand(i, j, solutions, relevant_baskets,);
			nr=0;
			if (nr > 0)
			{
				//outputb << endl;
				//outputb << m_customers.at(m_demands.at(j).customer) << ";" << m_products.at(m_demands.at(j).product) << ";" << m_periods.at(m_demands.at(j).period) << ";" << m_demands.at(j).amount << endl;

				for (k = 0; k < relevant_baskets.size(); ++k)
				{
					injured_customer.at(m_customer_for_basket.at(relevant_baskets.at(k))) = true;

					for (n = 0; n < m_baskets.at(relevant_baskets.at(k)).size(); ++n)
					{
						//outputb << m_customers.at(m_demands.at(m_baskets.at(relevant_baskets.at(k)).at(n)).customer) << ";" << m_products.at(m_demands.at(m_baskets.at(relevant_baskets.at(k)).at(n)).product) << ";" << m_periods.at(m_demands.at(m_baskets.at(relevant_baskets.at(k)).at(n)).period) << ";" << m_demands.at(m_baskets.at(relevant_baskets.at(k)).at(n)).amount;
						//outputb << endl;
					}
					//outputb << endl;

				}

				//outputb << endl;

				injured_basket_nr += nr;
				//std::cout<<"Werk:"<<i<<" Nachfrage:"<<j<<std::endl;
				init_basket_variables_and_constraints(relevant_baskets,i,j);
			}
		}
	}

	//outputb.close();

	for (k = 0; k < m_nCustomers; ++k)
	{
		injured_customer_nr += injured_customer.at(k);
	}

	cout << endl << "number of concerned baskets (violated constraints): " << injured_basket_nr << endl;
	cout << "number of concerned customers (violated constraints): " << injured_customer_nr << ":";
	bool isFirst=true;
	for (k = 0; k < m_nCustomers; ++k)
	{
		if (injured_customer.at(k))
		{
			if (!isFirst)
				cout<<",";
		
			cout<<k;
			isFirst=false;
		}
	}
	cout<<endl << endl;

	if (injured_customer_nr == 0)
	{
		return false;
	}

	return true;
}



void Wepa::get_if_demand_for_product()
{
	int i;

	for (i = 0; i < m_demands.size(); ++i)
	{
		m_is_demand_for_product.at(m_demands.at(i).product) = true;
		m_is_demand_for_product_and_period.at(m_demands.at(i).product).at(m_demands.at(i).period) = true;
		m_is_demand_for_customer.at(m_demands.at(i).customer) = true;
		m_is_demand_for_customer_and_product.at(m_demands.at(i).customer).at(m_demands.at(i).product) = true;
		m_is_demand_for_customer_and_product_in_period.at(m_demands.at(i).customer).at(m_demands.at(i).product).at(m_demands.at(i).period) = true;

	}
}

void Wepa::get_transport_indices()
{
	int i;

	//mapping from edge plant->plant to index in transport processes
	for (i = 0; i < m_transport_plant.size(); ++i)
	{
		m_transport_indices_plant.at(m_transport_plant.at(i).start).at(m_transport_plant.at(i).end) = i;
	}

	//mapping from edge plant->customer to index in transport processes
	for (i = 0; i < m_transport_customer.size(); ++i)
	{
		m_transport_indices_customer.at(m_transport_customer.at(i).start).at(m_transport_customer.at(i).end) = i;
	}
}

void Wepa::get_processes_for_product()
{
	int i;

	for (i = 0; i < m_nProcesses; ++i)
	{
		m_processes_for_product.at(m_processes.at(i).product).push_back(i);
		m_is_process_for_product.at(i).at(m_processes.at(i).product) = true;
		m_is_resource_for_product.at(m_processes.at(i).machine).at(m_processes.at(i).product) = true;
	}
}

double Wepa::get_overall_start_inventory_for_product(const int & product)
{
	double inventory = 0.0;

	int i;
	for (i = 0; i < m_nPlants; ++i)
	{
		inventory += m_start_inventory.at(i).at(product);
	}
	return inventory;

}

double Wepa::get_overall_demand_for_product_until_period(const int & product, const int & period)
{
	double demand = 0.0;
	int i;
	for (i = 0; i < m_demands.size(); ++i)
	{
		if (m_demands.at(i).product == product && m_demands.at(i).period <= period)
		{
			demand += m_demands.at(i).amount;
		}
	}
	return demand;
}

bool Wepa::get_infeasibilities_for_production()
{
	int i,j,k,l;

	bool error = false;
	bool process;

	ofstream output(m_logfilename.c_str(), ios::app);

	if (output.fail())
	{
		cout << "Kann nicht in Datei " << m_logfilename << " schreiben!" << endl;
		exit(1);
	}

	for (i = 0; i < m_nProducts; ++i)
	{
		if (m_is_demand_for_product.at(i))
		{
			if (m_processes_for_product.at(i).size() == 0)
			{
				output << "Keine Prozesse fuer Produkt " << m_products.at(i) << endl;
				error = true;
			}

			if (m_set_fixations)
			{
				for (k = 1; k < m_nPeriods; ++k)
				{
					if (m_is_demand_for_product_and_period.at(i).at(k - 1))
					{
						process = false;
						for (j = 0; j < m_processes_for_product.at(i).size(); ++j)
						{
							for (l = 1; l <= k; ++l)
							{
								if (!is_fixed_to_zero(m_processes_for_product.at(i).at(j), l))
								{
									process = true;
									break;
								}
							}
						}

						if (process == false)
						{		
							if(get_overall_start_inventory_for_product(i) < get_overall_demand_for_product_until_period(i,k))
							{
								for (j = 0; j < m_processes_for_product.at(i).size(); ++j)
								{
									process = true;
								}

								if (process == false)
								{

									output << "Kein nicht auf 0 fixierter Prozess fuer Produkt " << m_products.at(i) << " in Periode " << m_periods.at(k - 1) << endl;
									error = true;
								}
							}
						}
					}
				}
			}
			else
			{
			}
		}
	}

	output.close();
	return error;
}

void Wepa::dfs(int last, int current, const int & customer, vector<bool> visited_plants, double cost, double & best_cost, vector<int> way, vector<int> & best_way, const int & demandindex)
{
	int i, j, k;

	if (!visited_plants.at(current) && exists_transport_possibility_plant(last,current))
	{
		way.push_back(current);
		cost += get_ic_cost(last, current);

		if (cost >= best_cost)
		{
			return;
		}

		visited_plants.at(current) = true;

		if (exists_transport_possibility_customer(current, customer) && 
			m_cpTransports .isPlantValidForCustomerArticle (customer,m_demands.at(demandindex).product,current) 
			)
		{
			cost += get_customer_cost(current, customer);
			if (cost < best_cost)
			{
				best_way = way;
				best_cost = cost;

				/*for (k = 0; k < best_way.size(); ++k)
				{
				cout << best_way.at(k) << " ";
				}
				cout << endl;*/
			}

			return;
		}

		for (i = 0; i < m_nPlants; ++i)
		{
			dfs(current, i , customer, visited_plants, cost, best_cost, way, best_way, demandindex);
		}
	}
}

bool Wepa::get_infeasibilities_for_transport()
{
	int i, j, k;
	bool is_feasible;
	bool exists_infeasibility = false;

	//vector<demand> infeasible_demands;

	for (i = 0; i < m_demands.size(); ++i)
	{
		//cout<<"dem "<<i<<" von "<<m_demands.size()<<endl;

		if (m_demands.at(i).amount == 0)
		{
			continue;
		}
		is_feasible = false;
		for (j = 0; j < m_nPlants; ++j)
		{
			if (produces_plant_product(j, m_demands.at(i).product) /*|| is_start_inventory_for_product_in_plant(m_demands.at(i).product,j)*/)
			{
				vector<bool> visited_plants(m_nPlants, false);
				double best_cost = HUGE_VAL;
				double cost = 0.0;
				vector<int> way;
				vector<int> best_way;
				way.push_back(j);

				visited_plants.at(j) = true;

				if (exists_transport_possibility_customer(j, m_demands.at(i).customer) && 
					m_cpTransports.isPlantValidForCustomerArticle (m_demands.at(i).customer,m_demands.at(i).product,j) 
					)
				{
					is_feasible = true;
				}
				else
				{
					for (k = 0; k < m_nPlants; ++k)
					{
						dfs(j, k, m_demands.at(i).customer, visited_plants, cost, best_cost, way, best_way, i);
					}
					if (best_way.size() > 0)
					{
						set_plant_relevant_for_customer(best_way.at(best_way.size() - 1), m_demands.at(i).customer);
						for (k = 0; k < best_way.size(); ++k)
						{
							if (k > 0)
							{
								set_transport_possibility(m_demands.at(i).product, best_way.at(k - 1), best_way.at(k));
							}
						}
						is_feasible = true;
					}
				}


				
			}
		}
		if (!is_feasible)
		{
			ofstream output(m_logfilename.c_str(), ios::app);
			output << "Keine Transportm�glichkeit zum Kunden " << m_customers.at(m_demands.at(i).customer) << " f�r Produkt " << m_products.at(m_demands.at(i).product) << " vorhanden!" << endl;
			output.close();
			return true;
		}
	}

	return exists_infeasibility;
}
/*vector<int> way;
				way.push_back(j);
				double cost = 0.0;
				double best_cost = HUGE_VAL;
				vector<int> best_way;

				vector<bool> visited_plants(m_nPlants, false);*/

				/*	visited_plants.at(j) = true;

				if (exists_transport_possibility_customer(j, m_demands.at(i).customer))
				{
				is_feasible = true;
				}
				else if (dfs_transport_feasible(j, m_demands.at(i).customer, visited_plants, cost, best_cost, way, best_way, i))
				{
				for (k = 1; k < best_way.size(); ++k)
				{
				set_transport_possibility(m_demands.at(i).product, best_way.at(k - 1), best_way.at(k));
				}

				is_feasible = true;
				} */
bool Wepa::is_fixed_to_zero(const int & process, const int & period)
{
	if (!m_set_fixations)
	{
		return false;
	}
	if (m_fixations.at(process).at(period - 1) < -0.1)
	{
		return true;
	}
	return false;
}

void Wepa::get_dummy_cost()
{
	int i, j;

	double tonspershift;
	double maxcostpershift;
	double nPallets_per_ton;
	double max_ic_cost;
	double dummy_cost = -HUGE_VAL;
	double current;


	for (i = 0; i < m_nProcesses; ++i)
	{
		tonspershift = m_processes.at(i).tonspershift;
		maxcostpershift = -HUGE_VAL;
		nPallets_per_ton = m_nPallets_per_ton.at(m_processes.at(i).product);
		max_ic_cost = -HUGE_VAL;

		for (j = 0; j < m_nProcesses; ++j)
		{
			if (m_processes.at(j).product == m_processes.at(i).product)
			{
				if (m_processes.at(j).costspershift > maxcostpershift)
				{
					maxcostpershift = m_processes.at(j).costspershift;
				}

				if (exists_transport_possibility_plant(m_processes.at(i).product_plant_index, m_processes.at(j).product_plant_index))
				{
					if (get_ic_cost(m_processes.at(i).product_plant_index, m_processes.at(j).product_plant_index) > max_ic_cost)
					{
						max_ic_cost = get_ic_cost(m_processes.at(i).product_plant_index, m_processes.at(j).product_plant_index);
					}
				}
			}
		}

		if (max_ic_cost < 0)
		{
			max_ic_cost = 0.0;
		}

		current = 2.0 * ((m_nPeriods - 1) * m_external_warehouse_cost * tonspershift * nPallets_per_ton + max_ic_cost * tonspershift * nPallets_per_ton + maxcostpershift);
		if (current > dummy_cost)
		{
			dummy_cost = current;
		}
	}

	m_dummy_cost = dummy_cost;
}

/*void Wepa::get_dummy_cost()
{
int i;

double max_cost = 0.0;
double max_pallets = 0.0;
double max_ic_cost = 0.0;
double pallets;
for (i = 0; i < m_nProcesses; ++i)
{
if (m_processes.at(i).costspershift > max_cost)
{
max_cost = m_processes.at(i).costspershift;
}
pallets = m_processes.at(i).tonspershift * m_nPallets_per_ton.at(m_processes.at(i).product);
if (pallets > max_pallets)
{
max_pallets = pallets;
}
}
for (i = 0; i < m_transport_plant.size(); ++i)
{
if (m_transport_plant.at(i).cost > max_ic_cost)
{
max_ic_cost = m_transport_plant.at(i).cost;
}
}

if (max_cost > max_pallets * m_external_warehouse_cost)
{
m_dummy_cost = max_cost + max_pallets * max_ic_cost;
}
//falls Lagerung der Paletten teurer als Produktion sein k�nnte
else
{
m_dummy_cost = max_pallets * m_external_warehouse_cost + max_pallets * max_ic_cost;
}

m_dummy_cost = 1000000.0;
}*/

/*void Wepa::get_dummy_bounds()
{
int i,j,k;

//Produktion immer auf die Maschine mit der geringsten Auslastung verteilen, Auslastung der am meisten ausgelasteten Maschine ergibt bigM
int period;
int product;
int amount;
int model_period;
bool found;
double max_used;

vector<vector<pair<double, int> > > used_capacity(m_nPeriods - 1, vector<pair<double, int> >(m_nResources));
//zuerst wird genutzte Kapazit�t f�r alle Perioden auf 0 gesetzt
for (i = 0; i < m_nPeriods - 1; ++i)
{
for (j = 0; j < m_nResources; ++j)
{
used_capacity.at(i).at(j) = pair<double, int>(0.0, j);
}
}

//f�r alle Bedarfe
for (i = 0; i < m_demands.size(); ++i)
{
period = m_demands.at(i).period;
product = m_demands.at(i).product;
amount = m_demands.at(i).amount;
model_period = period + 1;

found = false;

for (j = 0; j < used_capacity.at(period).size(); ++j)
{
//falls Produkt auf Maschine hergestellt werden kann
if (m_is_resource_for_product.at(used_capacity.at(period).at(j).second).at(product))
{
for (k = 0; k < m_nProcesses; ++k)
{
if (m_processes.at(k).product == product && m_processes.at(k).machine == used_capacity.at(period).at(j).second)
{
//falls der Prozess auf 0 fixiert ist, kann keine Produktion erfolgen
if (is_fixed_to_zero(k, model_period))
{
continue;
}

//Prozess geht an die am wenigsten ausgelastete Maschine
used_capacity.at(period).at(j).first += (double)amount / (m_processes.at(k).tonspershift * m_nPallets_per_ton.at(product));
found = true;

break;
}
}
if (found)
{
//Neusortierung
sort(used_capacity.at(period).begin(), used_capacity.at(period).end());
break;
}
}
}
}

for (k = 0; k < m_nPeriods-1; ++k)
{
for (j = 0; j < m_nResources; ++j)
{

m_dummy_bounds.at(used_capacity.at(k).at(j).second).at(k) = used_capacity.at(k).at(j).first-m_capacity.at(used_capacity.at(k).at(j).second).at(k);

if (m_dummy_bounds.at(used_capacity.at(k).at(j).second).at(k) < 0)
{
m_dummy_bounds.at(used_capacity.at(k).at(j).second).at(k) = 0;
}

cout << m_resources.at(used_capacity.at(k).at(j).second) << " " << m_periods.at(k) << ": " << m_dummy_bounds.at(used_capacity.at(k).at(j).second).at(k) << endl;
}
}
}*/

void Wepa::get_bigM()
{
	int i,j,k;

	//Produktion immer auf die Maschine mit der geringsten Auslastung verteilen, Auslastung der am meisten ausgelasteten Maschine ergibt bigM
	int period;
	int product;
	double amount;
	int model_period;
	bool found;
	double max_used;

	vector<vector<pair<double, int> > > used_capacity(m_nPeriods - 1, vector<pair<double, int> >(m_nResources));
	//zuerst wird genutzte Kapazit�t f�r alle Perioden auf 0 gesetzt
	for (i = 0; i < m_nPeriods - 1; ++i)
	{
		for (j = 0; j < m_nResources; ++j)
		{
			used_capacity.at(i).at(j) = pair<double, int>(0.0, j);
		}
	}

	//f�r alle Bedarfe
	for (i = 0; i < m_demands.size(); ++i)
	{
		period = m_demands.at(i).period;
		product = m_demands.at(i).product;
		amount = m_demands.at(i).amount;
		model_period = period + 1;

		found = false;

		for (j = 0; j < used_capacity.at(period).size(); ++j)
		{
			//falls Produkt auf Maschine hergestellt werden kann
			if (m_is_resource_for_product.at(used_capacity.at(period).at(j).second).at(product))
			{
				for (k = 0; k < m_nProcesses; ++k)
				{
					if (m_processes.at(k).product == product && m_processes.at(k).machine == used_capacity.at(period).at(j).second)
					{
						//falls der Prozess auf 0 fixiert ist, kann keine Produktion erfolgen
						if (is_fixed_to_zero(k, model_period))
						{
							continue;
						}

						//Prozess geht an die am wenigsten ausgelastete Maschine
						used_capacity.at(period).at(j).first += (double)amount / (m_processes.at(k).tonspershift * m_nPallets_per_ton.at(product));
						found = true;

						break;
					}
				}
				if (found)
				{
					//Neusortierung
					sort(used_capacity.at(period).begin(), used_capacity.at(period).end());
					break;
				}
			}
		}
	}

	max_used = 0.0;
	for (i = 0; i < m_nPeriods-1; ++i)
	{
		if (used_capacity.at(period).at(m_nResources - 1).first > max_used)
		{
			max_used = used_capacity.at(period).at(m_nResources - 1).first;
		}
	}

	for (i = 0; i < m_nProcesses; ++i)
	{
		if (max_used > m_bigM.at(i))
		{
			m_bigM.at(i) = (int)ceil(max_used);
		}
		for (j = 0; j < m_nPeriods-1; ++j)
		{
			/*if (m_products.at(m_processes.at(i).product) == "101405" && j == 0)
			{
			cout << get_demand(m_processes.at(i).product, j+1) << endl;
			cout << (get_demand(m_processes.at(i).product, j + 1) + m_min_inventory_v2.at(m_processes.at(i).product).at(j) + 1) / m_nPallets_per_ton.at(m_processes.at(i).product) / m_processes.at(i).tonspershift;
			char oi;
			cin >> oi;
			}*/

			if (m_capacity.at(m_processes.at(i).machine).at(j) > m_bigM.at(i))
			{
				m_bigM.at(i) = m_capacity.at(m_processes.at(i).machine).at(j);
			}
			if (j == 0 && (get_demand(m_processes.at(i).product, j+1) + m_min_inventory_v2.at(m_processes.at(i).product).at(j) + 1) / m_nPallets_per_ton.at(m_processes.at(i).product) / m_processes.at(i).tonspershift > m_bigM.at(i))
			{
				m_bigM.at(i) = (get_demand(m_processes.at(i).product, j+1) + m_min_inventory_v2.at(m_processes.at(i).product).at(j) + 1) / m_nPallets_per_ton.at(m_processes.at(i).product) / m_processes.at(i).tonspershift;

				/*if (m_products.at(m_processes.at(i).product) == "101405" && j == 0)
				{
				cout << m_bigM.at(i) << endl;
				char oi;
				cin >> oi;
				}*/
			}
			else if (j > 0 && (get_demand(m_processes.at(i).product, j+1) + m_min_inventory_v2.at(m_processes.at(i).product).at(j) - m_min_inventory_v2.at(m_processes.at(i).product).at(j - 1) + 1) / m_nPallets_per_ton.at(m_processes.at(i).product) / m_processes.at(i).tonspershift > m_bigM.at(i))
			{
				m_bigM.at(i) = (get_demand(m_processes.at(i).product, j+1) + m_min_inventory_v2.at(m_processes.at(i).product).at(j) - m_min_inventory_v2.at(m_processes.at(i).product).at(j - 1) + 1) / m_nPallets_per_ton.at(m_processes.at(i).product) / m_processes.at(i).tonspershift;
			}
		}
	}
}


/*bool Wepa::is_product_in_basket(const int & product, const int & customer, const int & basketindex, const int & period)
{
return m_baskets_for_customer.at(period-1).at(customer).at(basketindex).at(product);
}*/


void Wepa::clean_model()
{
	cplex->freeprob();
	cplex = new CPLEX(1);
}

Wepa::~Wepa()
{
	delete cplex;
}
/*
for (i = 0; i < m_basket_coherence.at(demandindex).size(); ++i)
	{
		basket = m_basket_coherence.at(demandindex).at(i);
		feasible = true;

		if (m_baskets.at(basket).size() > 1)

*/

bool Wepa::isValidConnectionWithRespectToBaskets (int plant,int demandIndex)
{
	bool isAllValidTransports=true;
	
	int i,j;
	int basket;
	/*
	if (m_products.at(m_demands.at(demandIndex).product) == "294573")
	{
		cout << " ValidConnectionCheck for 294573 at demandindex:" << demandIndex 
			<< " Plant index:"<<plant
			<< " Basket coherence size (general):"<< m_basket_coherence.size() 
			<< " Basket coherence size (demandindex):" << m_basket_coherence.at(demandIndex).size() 
			<<std::endl;
		cin.get();
	}

	if (m_products.at(m_demands.at(demandIndex).product) == "022353")
	{
		cout << " ValidConnectionCheck for 022353 at demandindex:" << demandIndex
			<< " Plant index:" << plant
			<< " Basket coherence size (general):" << m_basket_coherence.size()
			<< " Basket coherence size (demandindex):" << m_basket_coherence.at(demandIndex).size()
			<< std::endl;
		cin.get();
	}
	*/
	if (m_basket_coherence.size()==0)
		return true;
	if (m_basket_coherence.at(demandIndex).size()==0)
	{
		//if (demandIndex==0)//16790)
		//{
		//	cout<<"No basket contraint."<<endl;
		//}
		return true;
	}
	/*
	if (m_products.at(m_demands.at(demandIndex).product) == "294573")
	{
		cout << "ValidConnectionCheck for 294573 at demandindex:" << demandIndex << std::endl;
		cin.get();
	}

	if (m_products.at(m_demands.at(demandIndex).product) == "022353")
	{
		cout << "ValidConnectionCheck for 022353 at demandindex:" << demandIndex << std::endl;
		cin.get();
	}*/

	for (i=0;i<m_basket_coherence.at(demandIndex).size();i++)
	{
		isAllValidTransports=true;
		basket= m_basket_coherence.at(demandIndex).at(i);

		//if (demandIndex==0)//16790)
		//{
		//	cout<<"Content for basket:"<<basket<<":";
		//	for (j=0;j<m_baskets.at(basket).size();j++)
		//	{
		//		if (j>0)
		//		cout<<",";
		//		cout<<m_demands.at(m_baskets.at(basket).at(j)).product;
		//	}
		//	std::cout<<std::endl;
		//}



		for (j=0;j<m_baskets.at(basket).size();j++)
		{
			/*
			if (m_products.at(m_demands.at(demandIndex).product) == "294573" ||//if (basket == 64)
				m_products.at(m_demands.at(demandIndex).product) == "022353")
			{
				std::cout << "IVCWB: "
					<<"Basket:"<<basket
					<< "Product:" << m_products.at(m_demands.at(m_baskets.at(basket).at(j)).product)
					<< " Customer:" << m_customers.at(m_demands.at(m_baskets.at(basket).at(j)).customer)
					<< " Plant:" << m_plants.at(plant) 
					<<" TP:"<< exists_transport_possibility_customer(plant,
						m_demands.at(m_baskets.at(basket).at(j)).customer
					)
					<<" MCPTP:"<< m_cpTransports.isPlantValidForCustomerArticle(
						m_demands.at(m_baskets.at(basket).at(j)).customer,
						m_demands.at(m_baskets.at(basket).at(j)).product,
						plant
					)
					<<std::endl;
				cin.get();
			}*/

			if (
				!exists_transport_possibility_customer(plant,
													   m_demands.at(m_baskets.at(basket).at(j)).customer
													) 
				||
				!m_cpTransports.isPlantValidForCustomerArticle (
															m_demands.at(m_baskets.at(basket).at(j)).customer,
															m_demands.at(m_baskets.at(basket).at(j)).product,
															plant
															)
				)
			{
				/*if (demandIndex==0)
				{
					std::cout<<"Tansport infeasible due to ProductIndex:"<<m_demands.at(m_baskets.at(basket).at(j)).product
							 << " TransPossibility:"<<exists_transport_possibility_customer(plant,m_demands.at(m_baskets.at(basket).at(j)).customer) 
							 <<"  PlantValidForCustomerArticle:"<<m_cpTransports.isPlantValidForCustomerArticle (
															m_demands.at(m_baskets.at(basket).at(j)).customer,
															m_demands.at(m_baskets.at(basket).at(j)).product,
															plant
															)
							 <<std::endl;
				}*/
				isAllValidTransports=false;
				break;
			}
		}
		if (isAllValidTransports)
		{
			/*if (demandIndex==0)
			{
				std::cout<<"Basket valid."<<endl;
			}*/
			/*
			if (m_products.at(m_demands.at(demandIndex).product) == "294573")
			{
				cout << "ValidConnectionCheck for 294573 at demandindex:" << demandIndex 
					<< " Plant index:" << plant
					 << " Basket coherence size:" << m_basket_coherence.size() 
					<<"Return: true"<< std::endl;
				cin.get();
			}
			if (m_products.at(m_demands.at(demandIndex).product) == "022353")
			{
				cout << "ValidConnectionCheck for 022353 at demandindex:" << demandIndex
					<< " Plant index:" << plant
					<< " Basket coherence size:" << m_basket_coherence.size()
					<< "Return: true" << std::endl;
				cin.get();
			}
			return true;
			*/
			return true;
		}
		else
		{
			/*
			if (m_products.at(m_demands.at(demandIndex).product) == "294573")
			{
				cout << "ValidConnectionCheck for 294573 at demandindex:" << demandIndex 
					<< " Plant index:" << plant
					<< " Basket coherence size:" << m_basket_coherence.size() 
					<< "Return: false" << std::endl;
				cin.get();
			}
			if (m_products.at(m_demands.at(demandIndex).product) == "022353")
			{
				cout << "ValidConnectionCheck for 022353 at demandindex:" << demandIndex
					<< " Plant index:" << plant
					<< " Basket coherence size:" << m_basket_coherence.size()
					<< "Return: false" << std::endl;
				cin.get();
			}*/
			//return false;
		}
	}

	return false;//isAllValidTransports;
}

/*
bool Wepa::isValidConnectionWithRespectToBaskets (int plant,int demandIndex)
{
	bool isAllValidTransports=true;
	//bool isAtLeastOneValidTransport=false;
	int i,j;
	int basket;

	if (m_basket_coherence.size()==0)
		return true;
	for (i=0;i<m_basket_coherence.at(demandIndex).size();i++)
	{
		basket= m_basket_coherence.at(demandIndex).at(i);

		if (demandIndex==16790)
		{
			cout<<"Content for basket:"<<basket<<":";
			for (j=0;j<m_baskets.at(basket).size();j++)
			{
				if (j>0)
				cout<<",";
				cout<<m_demands.at(m_baskets.at(basket).at(j)).product;
			}
			std::cout<<std::endl;
		}

		for (j=0;j<m_baskets.at(basket).size();j++)
		{
			if (
				!exists_transport_possibility_customer(plant,
													   m_demands.at(m_baskets.at(basket).at(j)).customer
													) 
				||
				!m_cpTransports.isPlantValidForCustomerArticle (
															m_demands.at(m_baskets.at(basket).at(j)).customer,
															m_demands.at(m_baskets.at(basket).at(j)).product,
															plant
															)
				)
			{
				if (demandIndex==16790)
				{
					std::cout<<"Tansport infeasible due to ProductIndex:"<<m_demands.at(m_baskets.at(basket).at(j)).product
							 << " TransPossibility:"<<exists_transport_possibility_customer(plant,m_demands.at(m_baskets.at(basket).at(j)).customer) 
							 <<"  PlantValidForCustomerArticle:"<<m_cpTransports.isPlantValidForCustomerArticle (
															m_demands.at(m_baskets.at(basket).at(j)).customer,
															m_demands.at(m_baskets.at(basket).at(j)).product,
															plant
															)
							 <<std::endl;
				}
				isAllValidTransports=false;
				break;
			}
		}
	}

	return isAllValidTransports;
}
*/