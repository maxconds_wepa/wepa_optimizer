﻿#include "bib.h"

double round(double value, int numbers)
{
	double tempvalue = pow(10.0, numbers);
	value = value * tempvalue;
	value += 0.5;
	value = floor(value);
	value = value / tempvalue;
	return value;
}

double  generate_random_double(double lower_bound, double upper_bound)
{
	double r = double(rand()) / (double(RAND_MAX) + 1.0);
	double y = lower_bound + r * (upper_bound - lower_bound);
	return y;
}

void readin_explode(const string & line, vector<string> & daten, char delimiter)
{
	string substring = "";
	int last = 0;

	for (int i = 0; i < line.size(); ++i)
	{
		if (line[i] == delimiter)
		{
			daten.push_back(substring);
			substring = "";
			last = i;
		}
		else //if (line[i] != ' ')
		{
			substring += line[i];
		}
	}

	if (last != daten.size() - 1 && substring.size() > 0)
	{
		daten.push_back(substring);
	}
}

double get_median(const vector<double> & values)
{
	vector<double> sorted_values(values.size());
	int i;
	int index1;
	int index2;
	for (i = 0; i < values.size(); ++i)
	{
		sorted_values.at(i) = values.at(i);
	}
	sort(sorted_values.begin(), sorted_values.end());

	if (sorted_values.size() % 2 == 1)
	{
		index1 = sorted_values.size() / 2;
		index2 = index1;
	}
	else
	{
		index1 = (sorted_values.size() - 1) / 2;
		index2 = sorted_values.size() / 2;
	}

	if (sorted_values.size() == 0)
	{
		return 0.0;
	}

	double median = (sorted_values.at(index1) + sorted_values.at(index2)) / 2.0;

	return median;

}