#include <Windows.h>
#include <sql.h>
#include <sqltypes.h>
#include <sqlext.h>
#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include<list>
#include "data.h"
#include "Params.h"
#include "Data_Sources.h"

class DataModule
{
public:
	virtual void get_input_data(std::vector<std::string> & plants, 
					  std::vector<std::string> & customers, 
					  std::vector<std::string> & resources, 
					  std::vector<std::string> & products, 
					  std::vector<std::string> & periods, 
					  std::vector<production> & processes, 
					  std::vector<std::vector<double> > & capacity, 
					  std::vector<transports> & transport_plant, 
					  std::vector<transports> & transport_customer, 
					  std::vector<demand> & demands, 
					  std::vector<double> & nPallets_per_ton, 
					  std::vector<std::vector<double> > & fixations, 
					  std::vector<std::vector<double> > & productions, 
					  std::vector<std::vector<std::vector<bool> > > & baskets_for_customer, 
					  std::vector<std::string> & product_names, 
					  std::vector<int> & plant_capacities, 
					  std::vector<std::vector<double> > & start_inventory, 
					  std::vector<std::vector<std::vector<double> > > & minimum_inventory_v1, 
					  std::vector<std::vector<double> > & minimum_inventory_v2, 
					  std::vector<fixed_transport> & fixed_transports,
					  Article_customer_fixation_collection & custFixations,
					  Customer_product_related_transports_collection & cpTransports,
					  std::vector<int> & resourcesWithFixations,
					  vector<bool> & m_is_demand,
					  vector<vector<bool> > & m_is_demand_for_customer_and_product,
					  vector<vector<vector<bool> > > & m_is_demand_for_customer_and_product_in_period,
					  vector<vector<double > > & demand_for_product_in_period,
					  WEPA_Opt_Params &params
					  )=0;
};

class DataModule_NewFormat : public DataModule
{
protected:
	General_Data_Source *articleInfoDS;
	General_Data_Source *basketDS;
	General_Data_Source *capacitiesDS;
	General_Data_Source *customerFixationGroupsDS;
	General_Data_Source *customerProductRelatedTransportsDS;
	General_Data_Source *customerLocationsDS;
	General_Data_Source *demandsDS;
	General_Data_Source *missingCustomerTransportsDS;
	General_Data_Source *scheduledProductionDS;
	General_Data_Source *plantLocationsDS;
	General_Data_Source *periodsInfoDS;
	General_Data_Source *productionProcessesDS;
	General_Data_Source *productionResourcesDS;
	General_Data_Source *customerTransportsDS;
	General_Data_Source *interplantDS;

public:
	virtual void get_input_data
						(
							std::vector<std::string> & plants, 
							std::vector<std::string> & customers, 
							std::vector<std::string> & resources, 
							std::vector<std::string> & products, 
							std::vector<std::string> & periods, 
							std::vector<production> & processes, 
							std::vector<std::vector<double> > & capacity, 
							std::vector<transports> & transport_plant, 
							std::vector<transports> & transport_customer, 
							std::vector<demand> & demands, 
							std::vector<double> & nPallets_per_ton, 
							std::vector<std::vector<double> > & fixations, 
							std::vector<std::vector<double> > & productions, 
							std::vector<std::vector<std::vector<bool> > > & baskets_for_customer, 
							std::vector<std::string> & product_names, 
							std::vector<int> & plant_capacities, 
							std::vector<std::vector<double> > & start_inventory, 
							std::vector<std::vector<std::vector<double> > > & minimum_inventory_v1, 
							std::vector<std::vector<double> > & minimum_inventory_v2, 
							std::vector<fixed_transport> & fixed_transports,
							Article_customer_fixation_collection & custFixations,
							Customer_product_related_transports_collection & cpTransports,
							std::vector<int> & resourcesWithFixations,
							vector<bool> & m_is_demand,
							vector<vector<bool> > & m_is_demand_for_customer_and_product,
							vector<vector<vector<bool> > > & m_is_demand_for_customer_and_product_in_period,
							vector<vector<double > > & demand_for_product_in_period,
							WEPA_Opt_Params &params
						  );
};

class DataModule_DB : public DataModule_NewFormat
{
	static const char *articleInfoTabBaseCmd;
	static const char *basketTabBaseCmd;
	static const char *capacitiesTabBaseCmd;
	static const char *customerFixationGroupsTabBaseCmd;
	static const char *customerProductRelatedTransportsTabBaseCmd;
	static const char *customerLocationsTabBaseCmd;
	static const char *demandsTabBaseCmd;
	static const char *missingCustomerTransportsTabBaseCmd;
	static const char *scheduledProductionTabBaseCmd;
	static const char *plantLocationsTabBaseCmd;
	static const char *fixationsTabBaseCmd;
	static const char *periodsInfoTabBaseCmd;
	static const char *productionProcessesTabBaseCmd;
	static const char *productionResourcesTabBaseCmd;
	static const char *customerTransportsTabBaseCmd;
	static const char *interplantTabBaseCmd;
protected:
	std::string articleInfoTabFinalizedCmd;
	std::string basketTabFinalizedCmd;
	std::string capacitiesTabFinalizedCmd;
	std::string customerFixationGroupsTabFinalizedCmd;
	std::string customerProductRelatedTransportsTabFinalizedCmd;
	std::string customerLocationsTabFinalizedCmd;
	std::string demandsTabFinalizedCmd;
	std::string missingCustomerTransportsTabFinalizedCmd;
	std::string scheduledProductionTabFinalizedCmd;
	std::string plantLocationsTabFinalizedCmd;
	std::string fixationsTabFinalizedCmd;
	std::string periodsInfoTabFinalizedCmd;
	std::string productionProcessesTabFinalizedCmd;
	std::string productionResourcesTabFinalizedCmd;
	std::string customerTransportsTabFinalizedCmd;
	std::string interplantTabFinalizedCmd;
	std::string usedOptFolderName;
	std::string usedUserId;
	int usedOptimizationRun;
	
	void updateSqlCmds();
	//void finalizeCmd (string &finalizedCmd,const char *baseCmd,std::list<std::string> & fillEls);
public:
	DataModule_DB();
	~DataModule_DB ();
	void setDB (DataBase *dB);
	void UserId_s(std::string userId);
	void OptimizationRun_s(int oR);
	void OptFolder_s (std::string optFolderName);
	/*
	virtual void get_input_data(std::vector<std::string> & plants, 
						  std::vector<std::string> & customers, 
						  std::vector<std::string> & resources, 
						  std::vector<std::string> & products, 
						  std::vector<std::string> & periods, 
						  std::vector<production> & processes, 
						  std::vector<std::vector<double> > & capacity, 
						  std::vector<transports> & transport_plant, 
						  std::vector<transports> & transport_customer, 
						  std::vector<demand> & demands, 
						  std::vector<double> & nPallets_per_ton, 
						  std::vector<std::vector<double> > & fixations, 
						  std::vector<std::vector<double> > & productions, 
						  std::vector<std::vector<std::vector<bool> > > & baskets_for_customer, 
						  std::vector<std::string> & product_names, 
						  std::vector<int> & plant_capacities, 
						  std::vector<std::vector<double> > & start_inventory, 
						  std::vector<std::vector<std::vector<double> > > & minimum_inventory_v1, 
						  std::vector<std::vector<double> > & minimum_inventory_v2, 
						  std::vector<fixed_transport> & fixed_transports,
						  Article_customer_fixation_collection & custFixations,
						  Customer_product_related_transports_collection & cpTransports,
						  std::vector<int> & resourcesWithFixations,
						  vector<bool> & m_is_demand,
						  vector<vector<bool> > & m_is_demand_for_customer_and_product,
						  vector<vector<vector<bool> > > & m_is_demand_for_customer_and_product_in_period,
						  vector<vector<double > > & demand_for_product_in_period,
						  WEPA
						  */
};


class DataModule_CSV : public DataModule
{
protected:
	
/*! \brief 
*	input filename for plants
*/
string plantfile;

/*! \brief
*	input filename for customers
*/
string customerfile;

/*! \brief
*	input filename for machines
*/
string resourcefile;

/*! \brief
*	input filename for products
*/
string productfile;

/*! \brief
*	input filename for periods
*/
string periodfile;

/*! \brief
*	input filename for processes
*/
string processfile;

/*! \brief
*	input filename for capacities
*/
string capacityfile;

/*! \brief
*	input filename for ic transports
*/
string transportplantfile;

/*! \brief
*	input filename for customer transports
*/
string transportcustomerfile;

/*! \brief
*	input filename for demands
*/
string demandfile;

/*! \brief
* input filename for pallet-ton dependencies
*/
string palletspertonfile;

/*! \brief
* input filename for customer baskets
*/
string basketsfile;

/*! \brief
* input filename for fixations from sales plan
*/
string fixationfile;

/*! \brief
* input filename for mapping article number to article name
*/
string artikelnamenfile;

/*! \brief
* input filename for plant capacities
*/
string plant_capacityfile;

/* \brief
* input filename for start inventory
*/
string start_inventoryfile;

/* \brief
* input filename for fixed transports
*/
string fixed_transport_file;

string logfilename;

/* \brief
* input filename for plant capacities
*/
string plant_capacity_file;

/* \brief
* input filename for customer fixation groups
*/
string customer_fixation_group_file;

string customer_prioritized_fixation_group_file;
/* \brief
* input filename for customer product related transports
*/
string customer_product_related_transport_file;

/* \brief
* input filename for resources with fixations as given in the sales plan
*/
string resources_with_fixations_file;
public:
	void setFileNames (string plantFN,
			string customerFN,
			string resourceFN,
			string productFN,
			string periodFN,
			string processFN,
			string capacityFN,
			string transportplantFN,
			string transportcustomerFN,
			string demandFN,
			string palletspertonFN,
			string basketsFN,
			string fixationFN,
			string artikelnamenFN,
			string plant_capacityFN,
			string start_inventoryFN,
			string fixed_transportFM,
			string logFN,
			string customer_fixation_groupFN,
			string customer_product_related_transportFN,
			string resources_with_fixationsFN,
			string customer_prioritized_fixation_groupFN
		);

	virtual void get_input_data(std::vector<std::string> & plants, 
					  std::vector<std::string> & customers, 
					  std::vector<std::string> & resources, 
					  std::vector<std::string> & products, 
					  std::vector<std::string> & periods, 
					  std::vector<production> & processes, 
					  std::vector<std::vector<double> > & capacity, 
					  std::vector<transports> & transport_plant, 
					  std::vector<transports> & transport_customer, 
					  std::vector<demand> & demands, 
					  std::vector<double> & nPallets_per_ton, 
					  std::vector<std::vector<double> > & fixations, 
					  std::vector<std::vector<double> > & productions, 
					  std::vector<std::vector<std::vector<bool> > > & baskets_for_customer, 
					  std::vector<std::string> & product_names, 
					  std::vector<int> & plant_capacities, 
					  std::vector<std::vector<double> > & start_inventory, 
					  std::vector<std::vector<std::vector<double> > > & minimum_inventory_v1, 
					  std::vector<std::vector<double> > & minimum_inventory_v2, 
					  std::vector<fixed_transport> & fixed_transports,
					  Article_customer_fixation_collection & custFixations,
					  Customer_product_related_transports_collection & cpTransports,
					  std::vector<int> & resourcesWithFixations,
					  vector<bool> & m_is_demand,
					  vector<vector<bool> > & m_is_demand_for_customer_and_product,
					  vector<vector<vector<bool> > > & m_is_demand_for_customer_and_product_in_period,
					  vector<vector<double > > & demand_for_product_in_period,
					  WEPA_Opt_Params &params
					  );
};