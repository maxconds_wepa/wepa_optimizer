#include "cplex_model.h"

CPLEX::CPLEX(bool cpx)
{
	numBinaryV=numContV=numIntV=0;
	_bufsize = 4096;
	_buffer = new char[_bufsize];
	env = NULL;
	lp = NULL;
	env = CPXopenCPLEX(&status);
	
	//lp = CPXcreateprob (env, &status, "wepa");
	if (cpx)
	{
	status = CPXsetintparam (env, CPX_PARAM_SCRIND, CPX_ON);
	}
	else
	{
		status = CPXsetintparam (env, CPX_PARAM_SCRIND, CPX_OFF);

	}
	status = CPXsetintparam(env, CPX_PARAM_POLISHAFTERINTSOL, 1);
	//status = CPXsetdblparam(env, CPX_PARAM_TILIM, 600.0);
	/*status = CPXsetintparam(env, CPX_PARAM_CLIQUES, 2);
	status = CPXsetintparam(env, CPX_PARAM_COVERS, 2);
	status = CPXsetintparam(env, CPX_PARAM_DISJCUTS, 2);
	status = CPXsetintparam(env, CPX_PARAM_FLOWCOVERS, 2);
	status = CPXsetintparam(env, CPX_PARAM_FLOWPATHS, 2);
	status = CPXsetintparam(env, CPX_PARAM_FRACCUTS, 2);
	status = CPXsetintparam(env, CPX_PARAM_GUBCOVERS, 2);
	status = CPXsetintparam(env, CPX_PARAM_IMPLBD, 2);
	status = CPXsetintparam(env, CPX_PARAM_MIRCUTS, 2);
	status = CPXsetintparam(env, CPX_PARAM_ZEROHALFCUTS, 2);*/
	//status = CPXsetdblparam (env, CPX_PARAM_EPGAP, 0.005);
	
	//status = CPXsetdblparam (env, CPX_PARAM_WORKMEM, 16.0);
	status = CPXsetintparam (env, CPX_PARAM_THREADS, 2);
	
	//*************************Speicherbedarf****************************************************************
	//status = CPXsetdblparam(env, CPX_PARAM_TRELIM, 1000.0);
	//*******************************************************************************************************

	//status = CPXsetintparam(env, CPX_PARAM_NODEFILEIND, 3);
	//status = CPXsetintparam (env, CPX_PARAM_MEMORYEMPHASIS, CPX_ON);
	//status = CPXsetintparam(env, CPX_PARAM_FLOWCOVERS, -1);
	//status = CPXsetintparam(env, CPX_PARAM_MIRCUTS, -1);
	//status = CPXsetintparam (env,CPX_PARAM_NUMERICALEMPHASIS,CPX_ON);
	//status = CPXsetintparam(env, CPX_PARAM_PROBE, 3);
	//status = CPXsetintparam(env, CPX_PARAM_MIPEMPHASIS, 1);

	
}

void CPLEX::_writeToBuffer(const char * name)
{
	int namesize = strlen(name)+1; 
	if(namesize > _bufsize)
	{
		if(_buffer)   
		{
			delete[] _buffer;
			_bufsize = 2 * namesize; 
			_buffer = new char[_bufsize];
		}
	}
	sprintf(_buffer, "%s",name);
}

int CPLEX::add_var_binary(const char* name, double obj, int & varindex)
{
	_writeToBuffer(name);
	char sense = 'B';
	status=CPXnewcols(env, lp, 1, & obj, NULL, NULL, &sense, &_buffer);            
	varindex = varcounter;
	varcounter++;
	numBinaryV++;
	return status;
}

int CPLEX::add_var_integer(const char* name, double lb, double ub, double obj, int & varindex)
{
	_writeToBuffer(name);
	char sense = 'I';
	status=CPXnewcols(env, lp, 1, & obj, &lb, &ub, &sense, &_buffer);            
	varindex = varcounter;
	varcounter++;
	numIntV++;
	return status;
}

int CPLEX::add_var_continuous(const char* name, double lb, double ub, double obj, int & varindex)
{
	_writeToBuffer(name);
	char sense = 'C';
	status=CPXnewcols(env, lp, 1, &obj, &lb, &ub, &sense, &_buffer);
	varindex = varcounter;	
	varcounter++;
	numContV++;
	return status;
}

int CPLEX::add_cons(const char* name, char sense, double rhs, int & consindex)
{
	_writeToBuffer(name);
	status = CPXnewrows(env,lp,1,&rhs,&sense,NULL,&_buffer);
	if (rhs < 0)
		std::cout << "Warnung: Constraint " << name << "Index: "<<consindex <<" hat negative rechte Seite:" << rhs << endl;
	consindex = conscounter;
	conscounter++;
	return status;
}

int CPLEX::change_cons_rhs(int cons, double rhs)
{
	status = CPXchgrhs(env,lp,1,&cons,&rhs);
	//if(rhs < 0)
		//std::cout << "Warnung: Constraint mit Index: " << cons << " hat negative rechte Seite:" << rhs << endl;
	return status;
}

int CPLEX::change_bound(int var, double bound, char what)
{
	if (var == -1)
	{
		cout << "Variable existiert nicht!" << endl;
		exit(99);
	}

	status = CPXchgbds(env,lp,1,&var,&what,&bound);
	return status;
}

int CPLEX::change_coeff(const int & var, const int & cons, const double & koeff)
{
	return CPXchgcoef(env, lp, cons, var, koeff);
}

int CPLEX::change_obj_coeff(const int & var, const double & koeff)
{
	return CPXchgcoef(env, lp, -1, var, koeff);
}

int CPLEX::add_coeff_linear(int & cons, int var, double koeff)
{
	status=CPXchgcoef(env,lp,cons,var,koeff);
	return status;
}

int CPLEX::solve_mip(const double & epgap, const bool & polish)
{
	if (polish)
	{
		status = CPXsetintparam(env, CPX_PARAM_POLISHAFTERINTSOL, 1);
	}
	else
	{
		status = CPXsetintparam(env, CPX_PARAM_POLISHAFTERINTSOL, 1000000);
	}
	status = CPXsetdblparam (env, CPX_PARAM_EPGAP, epgap);
	status = CPXmipopt(env, lp);
	solve_status = CPXgetstat (env, lp);


	if (solve_status == 103 || solve_status == 106 || solve_status == 108 || solve_status == 110 || solve_status == 114 || solve_status == 115 || solve_status == 117 || solve_status == 118 || solve_status == 119)
	{
		cout << "Fehler im CPLEX-Loesungsprozess: Das Problem ist unzulaessig oder unbeschraenkt!" << endl;
		//cin.get();
		//exit(300);
		isSucessFul = false;
		return status;
	}
	else if (solve_status == 112)
	{
		cout << "Der Baum im CPLEX-Loesungsprozess ueberschreitet die maximal erlaubte Groesse! Keine zulaessige Loesung vorhanden!" << endl;
		//cin.get();
		//exit(300);
		isSucessFul = false;
		return status;
	}
	else if (solve_status == 111)
	{
		cout << "Der Baum im CPLEX-Loesungsprozess ueberschreitet die maximal erlaubte Groesse! Setze Algorithmus mit bester gefundener zulaessiger Loesung fort!" << endl;
		isSucessFul = false;
		return status;
	}
	isSucessFul = true;

	return status;
}

int CPLEX::solve_lp()
{
	CPXchgprobtype(env,lp,CPXPROB_LP);
	solve_status = CPXlpopt(env,lp);
	return solve_status;
}

double CPLEX::get_objval()
{
	double objval;
	status = CPXgetobjval(env, lp, &objval);
	return objval;
}

void CPLEX::setObjSense(const char* sense)
{
	int cpxsense = (sense == "max" ? CPX_MAX : CPX_MIN);
 	CPXchgobjsen(env, lp, cpxsense);
}

void CPLEX::write_problem(string name)
{
	status = CPXwriteprob(env, lp, name.c_str(), "LP");
}

int CPLEX::get_varsol(int j, double & ret)
{
 	status = CPXgetx(env, lp, & ret, j,j);
 	return status;
}

double CPLEX::get_varsol(int j)
{
	double solution;
	status = CPXgetx(env, lp, & solution, j,j);
	return solution;
}

double CPLEX::get_gap()
{
	double gap;
	int status = CPXgetmiprelgap(env, lp, & gap);
	return gap;
}

void CPLEX::createprob() 
{
	int status;

	if (lp!=NULL)
		freeprob();

	lp=CPXcreateprob (env,&status,"Wepa");
	//std::cout << "Create new problem with status:" << status << endl;
	conscounter=0;
	varcounter=0;
}
void CPLEX::freeprob()
{
	status = CPXfreeprob(env, &lp);
}

/*int CPLEX::add_mipstart(const vector<int> & indices, const vector<double> & values, bool first)
{
	int i;
	int begin = 0;
	int level = 4;
	char* name = "mipstart";
	int* varindices;
	varindices = (int *)malloc(indices.size() * sizeof(int));
	double* varvalues;
	varvalues = (double *)malloc(indices.size()*sizeof(double));
	for (i = 0; i < indices.size(); ++i)
	{
		varindices[i] = indices.at(i);
		varvalues[i] = values.at(i);
	}
	//CPXdelmipstart();
	
	CPXaddmipstarts(env, lp, 1, indices.size(), &begin, varindices, varvalues, &level, &name);
		//CPXchgmipstarts(env, lp, 1, &begin, indices.size(), &begin, varindices, varvalues, &level);
	//}
}*/

CPLEX::~CPLEX()
{
	delete[] _buffer;
 			  				
	if ( lp != NULL ) {
      status = CPXfreeprob (env, &lp);
      if ( status ) {
         fprintf (stderr, "CPXfreeprob failed, error code %d.\n", status);
      }
   }

   /* Free up the CPLEX environment, if necessary */
   if ( env != NULL ) {
      status = CPXcloseCPLEX (&env);

      /* Note that CPXcloseCPLEX produces no output,
         so the only way to see the cause of the error is to use
         CPXgeterrorstring.  For other CPLEX routines, the errors will
         be seen if the CPX_PARAM_SCRIND indicator is set to CPX_ON. */

      if ( status ) {
         char  errmsg[CPXMESSAGEBUFSIZE];
         fprintf (stderr, "Could not close CPLEX environment.\n");
         CPXgeterrorstring (env, status, errmsg);
         fprintf (stderr, "%s", errmsg);
      }
   }
}
	

