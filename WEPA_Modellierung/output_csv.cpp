#include "output_csv.h"


#define DETAILED_ASSIGNMENT

Output_CSV::Output_CSV(//const string & filename, 
	const string & filename_pallets,
	const string & filename_tons,
	const string & filename_monthly,
	const string & filename_article,
	const vector<string> & periods,
	const vector<string> & products,
	const vector<string> & product_names,
	const vector<production_result> & solution_production,
	const vector<customer_group_production_result> & customer_group_solution_production,
	const vector<string> & plants,
	const vector<string> & machines,
	const vector<production> & processes,
	const vector<double> & nPallets_per_ton,
	const int & nMachines,
	const int & nProducts,
	const vector<inventory_result> & solution_inventory,
	const vector<customer_group_inventory_result> & customer_group_solution_inventory,
	const vector<single_transport_result_plant> & solution_single_transport_plant,
	const vector<single_customer_group_transport_result_plant> & customer_group_solution_single_transport_plant,
	const vector<single_transport_result_customer> & solution_single_transport_customer,
	const vector<transports> & transport_plant,
	const vector<transports> & transport_customer,
	const vector<demand> demands,
	const vector<string> & customers,
	const string & filename_graphic,
	const string & filename_names,
	const string & filename_graphic_ic,
	const Article_customer_fixation_collection & custFixations,
	const string & comparisonFile,
	const string & expansion_stage
):
//m_filename(filename),
m_filename_pallets(filename_pallets),
m_filename_tons(filename_tons),
m_filename_monthly(filename_monthly),
m_filename_article(filename_article),
m_periods(periods),
m_products(products),
m_product_names(product_names),
m_solution_production(solution_production),
m_customer_group_solution_production(customer_group_solution_production),
m_plants(plants),
m_machines(machines),
m_processes(processes),
m_nPallets_per_ton(nPallets_per_ton),
m_nMachines(nMachines),
m_nProducts(nProducts),
m_solution_inventory(solution_inventory),
m_customer_group_solution_inventory(customer_group_solution_inventory),
m_solution_single_transport_plant(solution_single_transport_plant),
m_customer_group_solution_single_transport_plant(customer_group_solution_single_transport_plant),
m_solution_single_transport_customer(solution_single_transport_customer),
m_transport_plant(transport_plant),
m_transport_customer(transport_customer),
m_demands(demands),
m_customers(customers),
m_filename_graphic(filename_graphic),
m_filename_names(filename_names),
m_filename_graphic_ic(filename_graphic_ic),
m_custFixations((Article_customer_fixation_collection &)custFixations),
m_comparisonFile(comparisonFile),
m_expansion_stage(expansion_stage)
{
	get_machine_locations();
	get_overall_productions();
	set_transport_indices_plant();
	set_transport_indices_customer();
	get_customers_for_products();
	get_production_plants();
	get_inventories();
	//get_inventories();
	get_all_customer_transports();
	get_all_ic_transports();
	get_3month_production();
	get_production_per_plant();
	get_overall_production_per_plant();
	get_customer_transports_per_plant();
	get_overall_customer_transports_per_plant();
	get_ic_transports_per_plant();
	get_overall_ic_transports_per_plant();
	get_ic_transports();
}
/*
void Output::output_comparison(const bool & german)
{
prepare_model_data();
int i, j, k;
set<int>::const_iterator it;
map<int,double>::iterator it2;

ofstream output(m_comparisonFile.c_str());
if (output.fail())
{
cout << "Kann Datei " << m_filename << " nicht anlegen!" << endl;
exit(1);
}
if (german) output.imbue(locale("german"));
output.setf(ios::fixed, ios::floatfield);
output.precision(12);

output << "Artikelnummer;Artikelbezeichnung;Kunden;Monat;Werk;Anlage;Anzahl Paletten;Anteil an der Gesamtproduktion;Laufzeit;Produktionskosten" << endl;

for (k = 1; k < m_periods.size() + 1; ++k)
{
for (i = 0; i < m_products.size(); ++i)
{
for (j = 0; j < m_solution_production.size(); ++j)
{
if (m_solution_production.at(j).period == k && m_solution_production.at(j).product == i)
{
//Factor Berechnung
double amount = 0.0;
for (it2 = backtrackings.at(i).at(m_solution_production.at(j).machine).at(k-1).begin(); it2 != backtrackings.at(i).at(m_solution_production.at(j).machine).at(k-1).end(); ++it2)
{
if((*it2).second > 0)
{
amount += (*it2).second/(m_solution_production.at(j).runtime*m_processes.at(m_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(j).product));
}

}
double factor = 1.0/amount;



for (it2 = backtrackings.at(i).at(m_solution_production.at(j).machine).at(k-1).begin(); it2 != backtrackings.at(i).at(m_solution_production.at(j).machine).at(k-1).end(); ++it2)
{
output << "\"" << m_products.at(i) << "\"" << ";" << m_product_names.at(i) << ";";
double anteil = (*it2).second/(m_solution_production.at(j).runtime*
m_processes.at(m_solution_production.at(j).process).tonspershift*
m_nPallets_per_ton.at(m_solution_production.at(j).product));

anteil  = anteil * factor;



output << m_customers.at((*it2).first);

output << ";" << m_periods.at(k - 1) << ";";
output << m_plants.at(machine_locations.at(m_solution_production.at(j).machine)) << ";";
output << m_machines.at(m_solution_production.at(j).machine) << ";";
output << get_production(j) *anteil <<";";
output << get_relation_production(j)*anteil << ";";
output << m_solution_production.at(j).runtime*anteil <<";";
output << get_production_cost(j)*anteil<<";"<<endl;
}
}
}

}
}

output.close();
}*/


void Output_CSV::output_comparison(const bool & german)
{
	prepare_model_data();
	int i, j, k, l, m;
	set<int>::const_iterator it;
	map<int, double>::iterator it2;

	vector<double> totalQuantity;
	vector<double> totalProductionCost;
	vector<double> totalProductionRunTime;
	vector<double> totalProductionRelation;
	vector<vector<double>>totalCustomerQuantity;

	ofstream output(m_comparisonFile.c_str());

	if (output.fail())
	{
		cout << "Kann Datei " << m_comparisonFile << " nicht anlegen!" << endl;
		exit(1);
	}

	if (german) output.imbue(locale("german"));

	output.setf(ios::fixed, ios::floatfield);
	output.precision(12);

	totalQuantity.resize(m_machines.size());
	totalProductionCost.resize(m_machines.size());
	totalProductionRunTime.resize(m_machines.size());
	totalProductionRelation.resize(m_machines.size());
	totalCustomerQuantity.resize(m_machines.size(), vector<double>(m_customers.size()));

	output << "Artikelnummer;Artikelbezeichnung;Kunden;Monat;Werk;Anlage;Anzahl Paletten;Anteil an der Gesamtproduktion;Laufzeit;Produktionskosten" << endl;


	for (k = 1; k < m_periods.size() + 1; ++k)
	{
		for (i = 0; i < m_products.size(); ++i)
		{

			for (m = 0; m<m_machines.size(); m++)
			{
				//std::cout<<"Init Maschine m "<<m<<endl;

				totalQuantity.at(m) = 0;
				totalProductionCost.at(m) = 0;
				totalProductionRunTime.at(m) = 0;
				totalProductionRelation.at(m) = 0;


				for (l = 0; l<m_customers.size(); l++)
				{
					totalCustomerQuantity.at(m).at(l) = 0;
				}
			}
			//std::cout<<"Init ok."<<std::endl;

			for (j = 0; j < m_solution_production.size(); ++j)
			{
				if (m_solution_production.at(j).period == k && m_solution_production.at(j).product == i)
				{

					totalQuantity.at(m_solution_production.at(j).machine) += m_solution_production.at(j).runtime*
						m_processes.at(m_solution_production.at(j).process).tonspershift*
						m_nPallets_per_ton.at(m_solution_production.at(j).product);
					totalProductionCost.at(m_solution_production.at(j).machine) += get_production_cost(j);
					totalProductionRunTime.at(m_solution_production.at(j).machine) += m_solution_production.at(j).runtime;
					totalProductionRelation.at(m_solution_production.at(j).machine) += get_relation_production_pallets(j);

					//Factor Berechnung
					double amount = 0.0;
					for (it2 = backtrackings.at(i).at(m_solution_production.at(j).machine).at(k - 1).begin(); it2 != backtrackings.at(i).at(m_solution_production.at(j).machine).at(k - 1).end(); ++it2)
					{
						if ((*it2).second > 0)
						{

							totalCustomerQuantity.at(m_solution_production.at(j).machine).at((*it2).first) += (*it2).second;
						}

					}

				}
			}
			//std::cout<<"Normale Produktion ok."<<std::endl;
			for (j = 0; j<m_customer_group_solution_production.size(); j++)
			{

				if (m_customer_group_solution_production.at(j).period == k && m_customer_group_solution_production.at(j).product == i)
				{


					totalQuantity.at(m_customer_group_solution_production.at(j).machine) += m_customer_group_solution_production.at(j).runtime*
						m_processes.at(m_customer_group_solution_production.at(j).process).tonspershift*
						m_nPallets_per_ton.at(m_customer_group_solution_production.at(j).product);

					totalProductionCost.at(m_customer_group_solution_production.at(j).machine) += get_customer_group_production_cost(j);//get_production_cost(j);
					totalProductionRunTime.at(m_customer_group_solution_production.at(j).machine) += m_customer_group_solution_production.at(j).runtime;
					totalProductionRelation.at(m_customer_group_solution_production.at(j).machine) += get_customer_group_relation_production_pallets(j);
					for (
						it2 = customer_group_backtrackings.at(m_customer_group_solution_production.at(j).cGroup)
						.at(m_customer_group_solution_production.at(j).machine)
						.at(m_customer_group_solution_production.at(j).period - 1)
						.begin();
						it2 != customer_group_backtrackings.at(m_customer_group_solution_production.at(j).cGroup)
						.at(m_customer_group_solution_production.at(j).machine)
						.at(m_customer_group_solution_production.at(j).period - 1)
						.end();
						++it2
						)
					{
						if ((*it2).second > 0)
						{

							totalCustomerQuantity.at(m_customer_group_solution_production.at(j).machine).at((*it2).first) += (*it2).second;
						}
					}
				}
			}

			for (m = 0; m<m_machines.size(); m++)
			{
				if (totalQuantity.at(m)>0)//(totalQuantity.find(m)!=totalQuantity.end())//(cumProductionQNC.at(i).at(m).at(k-1)>0)
				{
					double remainingQuantity;
					double anteil;
					remainingQuantity = totalQuantity.at(m);
					for (l = 0; l<m_customers.size(); l++)
					{
						if (totalCustomerQuantity.at(m).at(l)>0)//(totalCustomerQuantity.at(m).find(l)!=totalCustomerQuantity.at(m).end())//(cumProductionQ.at(i).at(m).at(k-1).at(l)>0)
						{
							anteil = totalCustomerQuantity.at(m).at(l) / totalQuantity.at(m);//cumProductionQ.at(i).at(m).at(k-1).at(l)/cumProductionQNC.at(i).at(m).at(k-1);

							output << "\"" << m_products.at(i) << "\"" << ";" << m_product_names.at(i) << ";";



							output << m_customers.at(l);

							output << ";" << m_periods.at(k - 1) << ";";
							output << m_plants.at(machine_locations.at(m)) << ";";
							output << m_machines.at(m) << ";";
							output << totalCustomerQuantity.at(m).at(l) << ";";//cumProductionQ.at(i).at(m).at(k-1).at(l)<<";";//get_production(j) *anteil <<";";
							output << totalProductionRelation.at(m)*anteil << ";";//get_relation_production(j)*anteil << ";";
							output << totalProductionRunTime.at(m)*anteil << ";";//m_solution_production.at(j).runtime*anteil <<";";
							output << totalProductionCost.at(m)*anteil << ";" << endl;//get_production_cost(j)*anteil<<";"<<endl;
							remainingQuantity -= totalCustomerQuantity.at(m).at(l);
						}
					}

					if (remainingQuantity>0.001)
					{
						output << "\"" << m_products.at(i) << "\"" << ";" << m_product_names.at(i) << ";";

						anteil = remainingQuantity / totalQuantity.at(m);

						output << "No customer";

						output << ";" << m_periods.at(k - 1) << ";";
						output << m_plants.at(machine_locations.at(m)) << ";";
						output << m_machines.at(m) << ";";
						output << remainingQuantity << ";";//cumProductionQ.at(i).at(m).at(k-1).at(l)<<";";//get_production(j) *anteil <<";";
						output << totalProductionRelation.at(m)*anteil << ";";//get_relation_production(j)*anteil << ";";
						output << totalProductionRunTime.at(m)*anteil << ";";//m_solution_production.at(j).runtime*anteil <<";";
						output << totalProductionCost.at(m)*anteil << ";" << endl;//get_production_cost(j)*anteil<<";"<<endl;
					}
				}
			}

		}
	}

	output.close();
}
/*
void Output::output_comparison(const bool & german) {
int i, j, k, l, m;
set < int > ::const_iterator it;
map < int, double > ::iterator it2;

vector < double > totalQuantity;
vector < double > totalProductionCost;
vector < double > totalProductionRunTime;
vector < double > totalProductionRelation;
vector < double > totalICTransportQuantities;
vector < double > totalICTransportCosts;
vector < vector < double >> totalCustomerQuantity;

ofstream output(m_comparisonFile.c_str());

if (output.fail()) {
cout << "Kann Datei " << m_filename << " nicht anlegen!" << endl;
exit(1);
}

if (german) {
output.imbue(locale("german"));
}

output.setf(ios::fixed, ios::floatfield);
output.precision(12);

totalQuantity.resize(m_machines.size());
totalProductionCost.resize(m_machines.size());
totalProductionRunTime.resize(m_machines.size());
totalProductionRelation.resize(m_machines.size());
totalICTransportQuantities.resize(m_machines.size());
totalICTransportCosts.resize(m_machines.size());
totalCustomerQuantity.resize(m_machines.size(), vector < double >(m_customers.size()));

output << "Artikelnummer" << ";"
<< "Artikelbezeichnung" << ";"
<< "Kunden" << ";"
<< "Monat" << ";"
<< "Werk" << ";"
<< "Anlage" << ";"
<< "Menge" << ";"
<< "Anteil der Gesamtmenge" << ";"
<< "Anzahl Schichten" << ";"
<< "Produktionskosten (Summe)" << ";"
<< "Produktionskosten (Palette)" << ";"
<< "Transport zu Kundenstandorten (Paletten)" << ";"
<< "Kundentransportkosten (Summe)" << ";"
<< "Kundentransportkosten (Palette)" << ";"
<< "Intercompany-Transporte (Paletten)" << ";"
<< "InterCompanykosten (Summe)" << ";"
<< "InterCompanykosten (Palette)"
<< endl;

for (k = 1; k < m_periods.size() + 1; ++k) {
for (i = 0; i < m_products.size(); ++i) {
for (m = 0; m < m_machines.size(); ++m) {
totalQuantity.at(m) = 0;
totalProductionCost.at(m) = 0;
totalProductionRunTime.at(m) = 0;
totalProductionRelation.at(m) = 0;
totalICTransportCosts.at(m) = 0;
totalICTransportQuantities.at(m) = 0;

for (l = 0; l < m_customers.size(); l++) {
totalCustomerQuantity.at(m).at(l) = 0;
}
}

for (j = 0; j < m_solution_production.size(); ++j) {
if (m_solution_production.at(j).period == k && m_solution_production.at(j).product == i) {
pair < double, double > ic_info = get_ic_transports(j);

totalQuantity.at(m_solution_production.at(j).machine) += m_solution_production.at(j).runtime *
m_processes.at(m_solution_production.at(j).process).tonspershift *
m_nPallets_per_ton.at(m_solution_production.at(j).product);

totalProductionCost.at(m_solution_production.at(j).machine) += get_production_cost(j);
totalProductionRunTime.at(m_solution_production.at(j).machine) += m_solution_production.at(j).runtime;
totalProductionRelation.at(m_solution_production.at(j).machine) += get_relation_production(j);

totalICTransportQuantities.at(m_solution_production.at(j).machine) += ic_info.first;
totalICTransportCosts.at(m_solution_production.at(j).machine) += ic_info.second;

double amount = 0.0;
for (it2 = backtrackings.at(i).at(m_solution_production.at(j).machine).at(k - 1).begin(); it2 != backtrackings.at(i).at(m_solution_production.at(j).machine).at(k - 1).end(); ++it2) {
if (it2 -> second > 0) {
totalCustomerQuantity.at(m_solution_production.at(j).machine).at(it2 -> first) += it2 -> second;
}

}
}
}

for (j = 0; j < m_customer_group_solution_production.size(); j++) {
if (m_customer_group_solution_production.at(j).period == k && m_customer_group_solution_production.at(j).product == i) {
pair < double, double > ic_info = get_customer_group_ic_transports(j);

totalQuantity.at(m_customer_group_solution_production.at(j).machine) += m_customer_group_solution_production.at(j).runtime *
m_processes.at(m_customer_group_solution_production.at(j).process).tonspershift *
m_nPallets_per_ton.at(m_customer_group_solution_production.at(j).product);

totalProductionCost.at(m_customer_group_solution_production.at(j).machine) += get_customer_group_production_cost(j);
totalProductionRunTime.at(m_customer_group_solution_production.at(j).machine) += m_customer_group_solution_production.at(j).runtime;
totalProductionRelation.at(m_customer_group_solution_production.at(j).machine) += get_customer_group_relation_production(j);
totalICTransportQuantities.at(m_solution_production.at(j).machine) += ic_info.first;
totalICTransportCosts.at(m_solution_production.at(j).machine) += ic_info.second;

for (
it2 = customer_group_backtrackings.at(m_customer_group_solution_production.at(j).cGroup)
.at(m_customer_group_solution_production.at(j).machine)
.at(m_customer_group_solution_production.at(j).period - 1)
.begin(); it2 != customer_group_backtrackings.at(m_customer_group_solution_production.at(j).cGroup)
.at(m_customer_group_solution_production.at(j).machine)
.at(m_customer_group_solution_production.at(j).period - 1)
.end();
++it2
) {
if ((*it2).second > 0) {
totalCustomerQuantity.at(m_customer_group_solution_production.at(j).machine).at((*it2).first) += (*it2).second;
}
}
}
}

for (m = 0; m < m_machines.size(); m++) {
if (totalQuantity.at(m) > 0) {
double remainingQuantity;
double anteil;
remainingQuantity = totalQuantity.at(m);
for (l = 0; l < m_customers.size(); l++) {
if (totalCustomerQuantity.at(m).at(l) > 0) {
anteil = totalCustomerQuantity.at(m).at(l) / totalQuantity.at(m);

output << "\"" << m_products.at(i) << "\"" << ";";
output << m_product_names.at(i) << ";";
output << m_customers.at(l) << ";";
output << m_periods.at(k - 1) << ";";
output << m_plants.at(machine_locations.at(m)) << ";";
output << m_machines.at(m) << ";";
output << totalCustomerQuantity.at(m).at(l) << ";";
output << totalProductionRelation.at(m) * anteil << ";";
output << totalProductionRunTime.at(m) * anteil << ";";
output << totalProductionCost.at(m) * anteil << ";";

if (totalCustomerQuantity.at(m).at(l) != 0) {
output << totalProductionCost.at(m) * anteil / totalCustomerQuantity.at(m).at(l) << ";";
}
else {
output << "0" << ";";
}

transport_information dummy = get_customer_transports(i, k, m);
output << dummy.amount * anteil << ";";
output << dummy.costsum * anteil << ";";
output << (dummy.amount == 0 ? 0 : dummy.costpallet) << ";";

output << totalICTransportQuantities.at(m) * anteil << ";";
output << totalICTransportCosts.at(m) * anteil << ";";
if (totalICTransportQuantities.at(m) != 0) {
output << totalICTransportCosts.at(m) / totalICTransportQuantities.at(m) * anteil << ";";
}
else {
output << "0;";
}

output << endl;

remainingQuantity -= totalCustomerQuantity.at(m).at(l);
}
}

if (remainingQuantity > 0.001) {
anteil = remainingQuantity / totalQuantity.at(m);

output << "\"" << m_products.at(i) << "\"" << ";";
output << m_product_names.at(i) << ";";
output << "No customer";
output << ";" << m_periods.at(k - 1) << ";";
output << m_plants.at(machine_locations.at(m)) << ";";
output << m_machines.at(m) << ";";
output << remainingQuantity << ";";
output << totalProductionRelation.at(m) * anteil << ";";
output << totalProductionRunTime.at(m) * anteil << ";";
output << totalProductionCost.at(m) * anteil << ";";
output << totalProductionCost.at(m) * anteil / remainingQuantity << ";";

transport_information dummy = get_customer_transports(i, k, m);
output << dummy.amount * anteil << ";";
output << dummy.costsum * anteil << ";";
output << (dummy.amount == 0 ? 0 : dummy.costpallet) << ";";

output << totalICTransportQuantities.at(m) * anteil << ";";
output << totalICTransportCosts.at(m) * anteil << ";";
if (totalICTransportQuantities.at(m) != 0) {
output << totalICTransportCosts.at(m) / totalICTransportQuantities.at(m) * anteil << ";";
}
else {
output << "0;";
}

output << endl;
}
}
}
}
}

output.close();
}
*/
void Output_CSV::output_wepa(const bool & german)
{
	prepare_model_data();
	output_wepa_pallets(german);
	output_wepa_ton(german);
}

void Output_CSV::output_wepa_pallets(const bool & german)
{
	double v;
	int m;
	double totalProduction;
	double totalRelationProduction;
	double totalRunTime;
	double totalProductionCost;
	double totalInitialInventory, totalEndInventory;
	double totalTransportAmount, totalTransportCost;
	double totalICTransportAmount, totalICTransportCost;
	int i, j, k, l;
	vector<vector<vector<double>>> cumProduction;
	//vector<vector<vector<double>>> cumProductionQ;
	set<int>::const_iterator it;
	map<int, double>::iterator it2;
	map<int, double>::iterator itEnd;
	pair<double, double> dummy2;
	//prepare_model_data();

#ifdef DETAILED_ASSIGNMENT

	ofstream output(m_filename_pallets.c_str());
	if (output.fail())
	{
		cout << "Kann Datei " << m_filename_pallets << " nicht anlegen!" << endl;
		exit(1);
	}

	if (german)
		output.imbue(locale("german"));

	output.setf(ios::fixed, ios::floatfield);
	output.precision(10);

	output << "Artikelnummer;Artikelbezeichnung;Kunden;Monat;Werk;Anlage;Menge;Anteil der Gesamtmenge;Anzahl Schichten;Produktionskosten (Summe);Produktionskosten (Palette);Anfangsbestand Werk;Endbestand Werk;Transport zu Kundenstandorten (Paletten);Kundentransportkosten (Summe);Kundentransportkosten (pro Palette);Intercompany-Transporte (Paletten);Intercompany-Kosten (Summe);IC-Kosten pro Palette" << endl;

	cumProduction.resize(m_products.size(), vector<vector<double>>(m_machines.size()));

	//cumProductionQ.resize (m_products.size(),vector<vector<double>>(m_machines.size()));

	for (i = 0; i<m_products.size(); i++)
	{
		for (j = 0; j<m_machines.size(); j++)
		{
			cumProduction.at(i).at(j).resize(m_periods.size(), 0);
			//cumProductionQ.at(i).at(j).resize(m_periods.size(),0);
		}
	}

	for (j = 0; j < m_solution_production.size(); ++j)
	{
		for (it2 = backtrackings.at(m_solution_production.at(j).product).at(m_solution_production.at(j).machine).at(m_solution_production.at(j).period - 1).begin(); it2 != backtrackings.at(m_solution_production.at(j).product).at(m_solution_production.at(j).machine).at(m_solution_production.at(j).period - 1).end(); it2++)// ++it2)
		{
			if ((*it2).second > 0)
			{
				cumProduction.at(m_solution_production.at(j).product).at(m_solution_production.at(j).machine).at(m_solution_production.at(j).period - 1) +=
					(*it2).second /
					(
						m_solution_production.at(j).runtime*
						m_processes.at(m_solution_production.at(j).process).tonspershift*
						m_nPallets_per_ton.at(m_solution_production.at(j).product)
						);
				/*
				cumProductionQ.at(m_solution_production.at (j).product)
				.at(m_solution_production.at(j).machine)
				.at(m_solution_production.at (j).period-1)
				+=(*it2).second;
				*/
			}
		}
	}

	for (j = 0; j<m_customer_group_solution_production.size(); j++)
	{
		for (
			it2 = customer_group_backtrackings.at(m_customer_group_solution_production.at(j).cGroup).at(m_customer_group_solution_production.at(j).machine).at(m_customer_group_solution_production.at(j).period - 1).begin();
			it2 != customer_group_backtrackings.at(m_customer_group_solution_production.at(j).cGroup).at(m_customer_group_solution_production.at(j).machine).at(m_customer_group_solution_production.at(j).period - 1).end();
			++it2
			)
		{
			if ((*it2).second > 0)
			{
				cumProduction.at(m_customer_group_solution_production.at(j).product)
					.at(m_customer_group_solution_production.at(j).machine)
					.at(m_customer_group_solution_production.at(j).period - 1) +=
					(*it2).second /
					(
						m_customer_group_solution_production.at(j).runtime*
						m_processes.at(m_customer_group_solution_production.at(j).process).tonspershift*
						m_nPallets_per_ton.at(m_customer_group_solution_production.at(j).product)
						);
				/*
				cumProductionQ.at(m_customer_group_solution_production.at(j).product).at(m_customer_group_solution_production.at(j).machine).at(m_customer_group_solution_production.at (j).period-1) +=
				(*it2).second;
				*/
			}
		}
	}

	unordered_map<int, double> totalCustQ;

	for (k = 1; k < m_periods.size() + 1; ++k)
	{
		for (m = 0; m<m_machines.size(); m++)
		{
			for (i = 0; i < m_products.size(); ++i)
			{
				//double factor = 1.0/cumProduction.at(i).at(m).at(k-1);//1.0/cumProduction.at(i).at(m).at(k-1);

				if (cumProduction.at(i).at(m).at(k - 1)>0)
				{
					totalCustQ.clear();

					totalProduction = 0;
					totalRelationProduction = 0;
					totalRunTime = 0;
					totalProductionCost = 0;
					totalInitialInventory = 0;
					totalEndInventory = 0;
					totalTransportAmount = totalTransportCost = 0;
					totalICTransportAmount = 0;
					totalICTransportCost = 0;

					for (j = 0; j < m_solution_production.size(); ++j)
					{
						if (
							m_solution_production.at(j).product == i &&
							m_solution_production.at(j).machine == m &&
							m_solution_production.at(j).period == k
							)
						{
							/*
							output << "\"" << m_products.at(i) << "\"" << ";" << m_product_names.at(i) << ";";
							*/
							for (
								it2 = backtrackings.at(i).at(m_solution_production.at(j).machine).at(k - 1).begin();
								it2 != backtrackings.at(i).at(m_solution_production.at(j).machine).at(k - 1).end();
								++it2
								)
							{
								if ((*it2).second > 0)
								{
									if (totalCustQ.find(it2->first) == totalCustQ.end())
									{
										totalCustQ.insert(pair<int, double>(it2->first, 0.0));
									}

									totalCustQ.at(it2->first) += (*it2).second;

									/*
									if (cumProduction.at(m_solution_production.at (j).product)
									.at(m_solution_production.at(j).machine)
									.at(m_solution_production.at (j).period-1) > 1.0
									)
									{
									output << m_customers.at((*it2).first)
									<< "("
									<< (*it2).second/(m_solution_production.at(j).runtime*m_processes.at(m_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(j).product))*factor
									<< ") ";
									}
									else
									{
									output << m_customers.at((*it2).first)
									<< "("
									<< (*it2).second/(m_solution_production.at(j).runtime*m_processes.at(m_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(j).product))
									<< ") ";
									}
									*/
								}
							}
							totalProduction += get_production_pallets(j);
							totalRelationProduction += get_relation_production_pallets(j);
							totalRunTime += m_solution_production.at(j).runtime;
							totalProductionCost += get_production_cost(j);
							totalInitialInventory += get_inventory_pallets(j).first;
							totalEndInventory += get_inventory_pallets(j).second;

							//transport_information dummy = get_customer_transports(j);
							//totalTransportAmount+=dummy.amount;
							//totalTransportCost+=dummy.costsum;

							dummy2 = get_ic_transports_pallets(j);
							totalICTransportAmount += dummy2.first;
							totalICTransportCost += dummy2.second;

							/*
							output << ";" << m_periods.at(k - 1) << ";";
							output << m_plants.at(machine_locations.at(m_solution_production.at(j).machine)) << ";";
							output << m_machines.at(m_solution_production.at(j).machine) << ";";
							output << get_production(j) << ";";
							output << get_relation_production(j) << ";";
							output << m_solution_production.at(j).runtime << ";";
							output << get_production_cost(j) << ";";
							output << get_production_cost_pallet(j) << ";";
							output << get_inventory(j).first << ";";
							output << get_inventory(j).second << ";";

							transport_information dummy = get_customer_transports(j);

							output << dummy.amount << ";";
							output << dummy.costsum << ";";
							output << dummy.costpallet << ";";

							pair<double,double> dummy2 = get_ic_transports(j);

							output << dummy2.first << ";";
							output << dummy2.second << "" << endl;
							*/
						}
					}

					for (j = 0; j<m_customer_group_solution_production.size(); j++)
					{
						if (
							m_customer_group_solution_production.at(j).product == i &&
							m_customer_group_solution_production.at(j).machine == m &&
							m_customer_group_solution_production.at(j).period == k
							)
						{
							/*
							output << "\"" << m_products.at(i) << "\"" << ";" << m_product_names.at(i) << ";";
							*/

							for (
								it2 = customer_group_backtrackings.at(m_customer_group_solution_production.at(j).cGroup).at(m_customer_group_solution_production.at(j).machine).at(k - 1).begin();
								it2 != customer_group_backtrackings.at(m_customer_group_solution_production.at(j).cGroup).at(m_customer_group_solution_production.at(j).machine).at(k - 1).end();
								++it2
								)
							{
								if ((*it2).second > 0)
								{
									if (totalCustQ.find(it2->first) == totalCustQ.end())
									{
										totalCustQ.insert(pair<int, double>(it2->first, 0.0));
									}

									totalCustQ.at(it2->first) += (*it2).second;
									/*
									if (cumProduction
									.at(m_customer_group_solution_production.at (j).product)
									.at(m_customer_group_solution_production.at (j).machine)
									.at(m_customer_group_solution_production.at (j).period-1) > 1.0)
									{
									output << m_customers.at((*it2).first) << "(" << (*it2).second/(m_customer_group_solution_production.at(j).runtime*m_processes.at(m_customer_group_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_customer_group_solution_production.at(j).product))*factor << ") ";
									}
									else
									{
									v=(*it2).second/(m_customer_group_solution_production.at(j).runtime*m_processes.at(m_customer_group_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_customer_group_solution_production.at(j).product));
									output << m_customers.at((*it2).first) << "(" << v << ") ";
									}
									*/
								}
							}

							totalProduction += get_customer_group_production_pallets(j);
							totalRelationProduction += get_customer_group_relation_production_pallets(j);
							totalRunTime += m_customer_group_solution_production.at(j).runtime;
							totalProductionCost += get_customer_group_production_cost(j);
							totalInitialInventory += get_customer_group_inventory_pallets(j).first;
							totalEndInventory += get_customer_group_inventory_pallets(j).second;

							//transport_information dummy = get_customer_group_customer_transports(j);
							//totalTransportAmount+=dummy.amount;
							//totalTransportCost+=dummy.costsum;

							pair<double, double> dummy2 = get_customer_group_ic_transports_pallets(j);
							totalICTransportAmount += dummy2.first;
							totalICTransportCost += dummy2.second;
							/*
							output << ";" << m_periods.at(k - 1) << ";";
							output << m_plants.at(machine_locations.at(m_customer_group_solution_production.at(j).machine)) << ";";
							output << m_machines.at(m_customer_group_solution_production.at(j).machine) << ";";
							output << get_customer_group_production(j) << ";";
							output << get_customer_group_relation_production(j) << ";";
							output << m_customer_group_solution_production.at(j).runtime << ";";
							output << get_customer_group_production_cost(j) << ";";
							output << get_customer_group_production_cost_pallet(j) << ";";
							output << get_customer_group_inventory(j).first << ";";
							output << get_customer_group_inventory(j).second << ";";

							transport_information dummy = get_customer_group_customer_transports(j);

							output << dummy.amount << ";";
							output << dummy.costsum << ";";
							output << dummy.costpallet << ";";

							pair<double,double> dummy2 = get_customer_group_ic_transports (j);

							output << dummy2.first << ";";
							output << dummy2.second << "" << endl;
							*/
						}
					}

					output << "\"" << m_products.at(i) << "\"" << ";" << m_product_names.at(i) << ";";

					unordered_map<int, double>::iterator tPIt, tPItEnd;

					tPIt = totalCustQ.begin();
					tPItEnd = totalCustQ.end();

					while (tPIt != tPItEnd)
					{
						output << m_customers.at((*tPIt).first)
							<< "("
							<< (*tPIt).second / totalProduction//cumProductionQ .at(i).at(m).at(k-1)  //(*it2).second/(m_solution_production.at(j).runtime*m_processes.at(m_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(j).product))*factor 
							<< ") ";

						tPIt++;
					}
					/*
					totalProduction+=get_production(j);
					totalRelationProduction+=get_relation_production(j);
					totalRunTime+=m_solution_production.at(j).runtime;
					totalProductionCost+=get_production_cost(j);
					totalInitialInventory+=get_inventory(j).first;
					totalEndInventory+=get_inventory(j).second;

					transport_information dummy = get_customer_group_customer_transports(j);
					totalTransportAmount+=dummy.amount;
					totalTransportCost+=dummy.costsum;

					pair<double,double> dummy2 = get_ic_transports(j);
					totalICTransportAmount+=dummy2.first;
					totalICTransportCost+=dummy2.second;
					*/
					output << ";" << m_periods.at(k - 1) << ";";
					output << m_plants.at(machine_locations.at(m)) << ";";
					output << m_machines.at(m) << ";";
					output << totalProduction << ";";
					output << totalRelationProduction << ";";
					output << totalRunTime << ";";
					output << totalProductionCost << ";";

					output << totalProductionCost / totalProduction << ";";
					output << totalInitialInventory << ";";
					output << totalEndInventory << ";";
					transport_information dummy = get_customer_transports_pallets(i, k, m);
					output << dummy.amount << ";";
					output << dummy.costsum << ";";
					output << dummy.costpallet << ";";
					output << totalICTransportAmount << ";";
					output << totalICTransportCost << ";";

					if (totalICTransportAmount == 0)
						output << "0" << ";";
					else
						output << totalICTransportCost / totalICTransportAmount << ";";

					output << endl;
				}
			}
		}
	}

	output.close();
#endif
}

void Output_CSV::output_wepa_ton(const bool & german)
{
	double v;
	int m;
	double totalProduction;
	double totalRelationProduction;
	double totalRunTime;
	double totalProductionCost;
	double totalInitialInventory, totalEndInventory;
	double totalTransportAmount, totalTransportCost;
	double totalICTransportAmount, totalICTransportCost;
	int i, j, k, l;
	vector<vector<vector<double>>> cumProduction;
	//vector<vector<vector<double>>> cumProductionQ;
	set<int>::const_iterator it;
	map<int, double>::iterator it2;
	map<int, double>::iterator itEnd;
	transport_information dummy;
	pair<double, double> dummy2;
	//prepare_model_data();

#ifdef DETAILED_ASSIGNMENT

	ofstream output(m_filename_tons.c_str());
	if (output.fail())
	{
		cout << "Kann Datei " << m_filename_tons << " nicht anlegen!" << endl;
		exit(1);
	}

	if (german)
		output.imbue(locale("german"));

	output.setf(ios::fixed, ios::floatfield);
	output.precision(10);

	output << "Artikelnummer;Artikelbezeichnung;Kunden;Monat;Werk;Anlage;Menge;Anteil der Gesamtmenge;Anzahl Schichten;Produktionskosten (Summe);Produktionskosten (Tonne);Anfangsbestand Werk;Endbestand Werk;Transport zu Kundenstandorten (Tonnen);Kundentransportkosten (Summe);Kundentransportkosten (pro Tonne);Intercompany-Transporte (Tonnen);Intercompany-Kosten (Summe);IC-Kosten pro Tonne" << endl;

	cumProduction.resize(m_products.size(), vector<vector<double>>(m_machines.size()));

	//cumProductionQ.resize (m_products.size(),vector<vector<double>>(m_machines.size()));

	for (i = 0; i<m_products.size(); i++)
	{
		for (j = 0; j<m_machines.size(); j++)
		{
			cumProduction.at(i).at(j).resize(m_periods.size(), 0);
			//cumProductionQ.at(i).at(j).resize(m_periods.size(),0);
		}
	}

	for (j = 0; j < m_solution_production.size(); ++j)
	{
		for (it2 = backtrackings.at(m_solution_production.at(j).product).at(m_solution_production.at(j).machine).at(m_solution_production.at(j).period - 1).begin(); it2 != backtrackings.at(m_solution_production.at(j).product).at(m_solution_production.at(j).machine).at(m_solution_production.at(j).period - 1).end(); it2++)// ++it2)
		{
			if ((*it2).second > 0)
			{
				cumProduction.at(m_solution_production.at(j).product).at(m_solution_production.at(j).machine).at(m_solution_production.at(j).period - 1) +=
					(*it2).second /
					(
						m_solution_production.at(j).runtime*
						m_processes.at(m_solution_production.at(j).process).tonspershift
						);
				/*
				cumProductionQ.at(m_solution_production.at (j).product)
				.at(m_solution_production.at(j).machine)
				.at(m_solution_production.at (j).period-1)
				+=(*it2).second;
				*/
			}
		}
	}

	for (j = 0; j<m_customer_group_solution_production.size(); j++)
	{
		for (
			it2 = customer_group_backtrackings.at(m_customer_group_solution_production.at(j).cGroup).at(m_customer_group_solution_production.at(j).machine).at(m_customer_group_solution_production.at(j).period - 1).begin();
			it2 != customer_group_backtrackings.at(m_customer_group_solution_production.at(j).cGroup).at(m_customer_group_solution_production.at(j).machine).at(m_customer_group_solution_production.at(j).period - 1).end();
			++it2
			)
		{
			if ((*it2).second > 0)
			{
				cumProduction.at(m_customer_group_solution_production.at(j).product)
					.at(m_customer_group_solution_production.at(j).machine)
					.at(m_customer_group_solution_production.at(j).period - 1) +=
					(*it2).second /
					(
						m_customer_group_solution_production.at(j).runtime*
						m_processes.at(m_customer_group_solution_production.at(j).process).tonspershift
						);
				/*
				cumProductionQ.at(m_customer_group_solution_production.at(j).product).at(m_customer_group_solution_production.at(j).machine).at(m_customer_group_solution_production.at (j).period-1) +=
				(*it2).second;
				*/
			}
		}
	}

	unordered_map<int, double> totalCustQ;

	for (k = 1; k < m_periods.size() + 1; ++k)
	{
		for (m = 0; m<m_machines.size(); m++)
		{
			for (i = 0; i < m_products.size(); ++i)
			{
				//double factor = 1.0/cumProduction.at(i).at(m).at(k-1);//1.0/cumProduction.at(i).at(m).at(k-1);

				if (cumProduction.at(i).at(m).at(k - 1)>0)
				{
					totalCustQ.clear();

					totalProduction = 0;
					totalRelationProduction = 0;
					totalRunTime = 0;
					totalProductionCost = 0;
					totalInitialInventory = 0;
					totalEndInventory = 0;
					totalTransportAmount = totalTransportCost = 0;
					totalICTransportAmount = 0;
					totalICTransportCost = 0;

					for (j = 0; j < m_solution_production.size(); ++j)
					{
						if (
							m_solution_production.at(j).product == i &&
							m_solution_production.at(j).machine == m &&
							m_solution_production.at(j).period == k
							)
						{
							/*
							output << "\"" << m_products.at(i) << "\"" << ";" << m_product_names.at(i) << ";";
							*/
							for (
								it2 = backtrackings.at(i).at(m_solution_production.at(j).machine).at(k - 1).begin();
								it2 != backtrackings.at(i).at(m_solution_production.at(j).machine).at(k - 1).end();
								++it2
								)
							{
								if ((*it2).second > 0)
								{
									if (totalCustQ.find(it2->first) == totalCustQ.end())
									{
										totalCustQ.insert(pair<int, double>(it2->first, 0.0));
									}

									totalCustQ.at(it2->first) += (*it2).second / m_nPallets_per_ton.at(i);

									/*
									if (cumProduction.at(m_solution_production.at (j).product)
									.at(m_solution_production.at(j).machine)
									.at(m_solution_production.at (j).period-1) > 1.0
									)
									{
									output << m_customers.at((*it2).first)
									<< "("
									<< (*it2).second/(m_solution_production.at(j).runtime*m_processes.at(m_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(j).product))*factor
									<< ") ";
									}
									else
									{
									output << m_customers.at((*it2).first)
									<< "("
									<< (*it2).second/(m_solution_production.at(j).runtime*m_processes.at(m_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(j).product))
									<< ") ";
									}
									*/
								}
							}
							totalProduction += get_production_tons(j);
							totalRelationProduction += get_relation_production_tons(j);
							totalRunTime += m_solution_production.at(j).runtime;
							totalProductionCost += get_production_cost(j);
							totalInitialInventory += get_inventory_tons(j).first;
							totalEndInventory += get_inventory_tons(j).second;

							//transport_information dummy = get_customer_transports(j);
							//totalTransportAmount+=dummy.amount;
							//totalTransportCost+=dummy.costsum;

							dummy2 = get_ic_transports_tons(j);
							totalICTransportAmount += dummy2.first;
							totalICTransportCost += dummy2.second;

							/*
							output << ";" << m_periods.at(k - 1) << ";";
							output << m_plants.at(machine_locations.at(m_solution_production.at(j).machine)) << ";";
							output << m_machines.at(m_solution_production.at(j).machine) << ";";
							output << get_production(j) << ";";
							output << get_relation_production(j) << ";";
							output << m_solution_production.at(j).runtime << ";";
							output << get_production_cost(j) << ";";
							output << get_production_cost_pallet(j) << ";";
							output << get_inventory(j).first << ";";
							output << get_inventory(j).second << ";";

							transport_information dummy = get_customer_transports(j);

							output << dummy.amount << ";";
							output << dummy.costsum << ";";
							output << dummy.costpallet << ";";

							pair<double,double> dummy2 = get_ic_transports(j);

							output << dummy2.first << ";";
							output << dummy2.second << "" << endl;
							*/
						}
					}

					for (j = 0; j<m_customer_group_solution_production.size(); j++)
					{
						if (
							m_customer_group_solution_production.at(j).product == i &&
							m_customer_group_solution_production.at(j).machine == m &&
							m_customer_group_solution_production.at(j).period == k
							)
						{
							/*
							output << "\"" << m_products.at(i) << "\"" << ";" << m_product_names.at(i) << ";";
							*/

							for (
								it2 = customer_group_backtrackings.at(m_customer_group_solution_production.at(j).cGroup).at(m_customer_group_solution_production.at(j).machine).at(k - 1).begin();
								it2 != customer_group_backtrackings.at(m_customer_group_solution_production.at(j).cGroup).at(m_customer_group_solution_production.at(j).machine).at(k - 1).end();
								++it2
								)
							{
								if ((*it2).second > 0)
								{
									if (totalCustQ.find(it2->first) == totalCustQ.end())
									{
										totalCustQ.insert(pair<int, double>(it2->first, 0.0));
									}

									totalCustQ.at(it2->first) += (*it2).second / m_nPallets_per_ton.at(i);
									/*
									if (cumProduction
									.at(m_customer_group_solution_production.at (j).product)
									.at(m_customer_group_solution_production.at (j).machine)
									.at(m_customer_group_solution_production.at (j).period-1) > 1.0)
									{
									output << m_customers.at((*it2).first) << "(" << (*it2).second/(m_customer_group_solution_production.at(j).runtime*m_processes.at(m_customer_group_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_customer_group_solution_production.at(j).product))*factor << ") ";
									}
									else
									{
									v=(*it2).second/(m_customer_group_solution_production.at(j).runtime*m_processes.at(m_customer_group_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_customer_group_solution_production.at(j).product));
									output << m_customers.at((*it2).first) << "(" << v << ") ";
									}
									*/
								}
							}

							totalProduction += get_customer_group_production_tons(j);
							totalRelationProduction += get_customer_group_relation_production_tons(j);
							totalRunTime += m_customer_group_solution_production.at(j).runtime;
							totalProductionCost += get_customer_group_production_cost(j);
							totalInitialInventory += get_customer_group_inventory_tons(j).first;
							totalEndInventory += get_customer_group_inventory_tons(j).second;

							//transport_information dummy = get_customer_group_customer_transports(j);
							//totalTransportAmount+=dummy.amount;
							//totalTransportCost+=dummy.costsum;

							dummy2 = get_customer_group_ic_transports_tons(j);
							totalICTransportAmount += dummy2.first;
							totalICTransportCost += dummy2.second;
							/*
							output << ";" << m_periods.at(k - 1) << ";";
							output << m_plants.at(machine_locations.at(m_customer_group_solution_production.at(j).machine)) << ";";
							output << m_machines.at(m_customer_group_solution_production.at(j).machine) << ";";
							output << get_customer_group_production(j) << ";";
							output << get_customer_group_relation_production(j) << ";";
							output << m_customer_group_solution_production.at(j).runtime << ";";
							output << get_customer_group_production_cost(j) << ";";
							output << get_customer_group_production_cost_pallet(j) << ";";
							output << get_customer_group_inventory(j).first << ";";
							output << get_customer_group_inventory(j).second << ";";

							transport_information dummy = get_customer_group_customer_transports(j);

							output << dummy.amount << ";";
							output << dummy.costsum << ";";
							output << dummy.costpallet << ";";

							pair<double,double> dummy2 = get_customer_group_ic_transports (j);

							output << dummy2.first << ";";
							output << dummy2.second << "" << endl;
							*/
						}
					}

					output << "\"" << m_products.at(i) << "\"" << ";" << m_product_names.at(i) << ";";

					unordered_map<int, double>::iterator tPIt, tPItEnd;

					tPIt = totalCustQ.begin();
					tPItEnd = totalCustQ.end();

					while (tPIt != tPItEnd)
					{
						output << m_customers.at((*tPIt).first)
							<< "("
							<< (*tPIt).second / totalProduction//cumProductionQ .at(i).at(m).at(k-1)  //(*it2).second/(m_solution_production.at(j).runtime*m_processes.at(m_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(j).product))*factor 
							<< ") ";

						tPIt++;
					}
					/*
					totalProduction+=get_production(j);
					totalRelationProduction+=get_relation_production(j);
					totalRunTime+=m_solution_production.at(j).runtime;
					totalProductionCost+=get_production_cost(j);
					totalInitialInventory+=get_inventory(j).first;
					totalEndInventory+=get_inventory(j).second;

					transport_information dummy = get_customer_group_customer_transports(j);
					totalTransportAmount+=dummy.amount;
					totalTransportCost+=dummy.costsum;

					pair<double,double> dummy2 = get_ic_transports(j);
					totalICTransportAmount+=dummy2.first;
					totalICTransportCost+=dummy2.second;
					*/
					output << ";" << m_periods.at(k - 1) << ";";
					output << m_plants.at(machine_locations.at(m)) << ";";
					output << m_machines.at(m) << ";";
					output << totalProduction << ";";
					output << totalRelationProduction << ";";
					output << totalRunTime << ";";
					output << totalProductionCost << ";";

					output << totalProductionCost / totalProduction << ";";
					output << totalInitialInventory << ";";
					output << totalEndInventory << ";";

					dummy = get_customer_transports_tons(i, k, m);
					output << dummy.amount << ";";
					output << dummy.costsum << ";";
					output << dummy.costpallet << ";";
					output << totalICTransportAmount << ";";
					output << totalICTransportCost << ";";

					if (totalICTransportAmount == 0)
						output << "0" << ";";
					else
						output << totalICTransportCost / totalICTransportAmount << ";";

					output << endl;
				}
			}
		}
	}

	output.close();
#endif
}
void Output_CSV::get_machine_locations()
{
	int i;

	machine_locations.clear();
	machine_locations.resize(m_nMachines);

	for (i = 0; i < m_processes.size(); ++i)
	{
		machine_locations.at(m_processes.at(i).machine) = m_processes.at(i).product_plant_index;
	}
}

void Output_CSV::get_overall_productions()
{
	int i;

	overall_production.clear();
	overall_production.resize(m_nProducts, 0.0);

	for (i = 0; i < m_solution_production.size(); ++i)
	{
		overall_production.at(m_solution_production.at(i).product) +=
			m_solution_production.at(i).runtime * m_processes.at(m_solution_production.at(i).process).tonspershift * m_nPallets_per_ton.at(m_solution_production.at(i).product);
	}
}

double Output_CSV::get_production_pallets(const int & solution_production_index)
{
	return m_solution_production.at(solution_production_index).runtime*m_processes.at(m_solution_production.at(solution_production_index).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(solution_production_index).product);
}

double Output_CSV::get_production_tons(const int & solution_production_index)
{
	return m_solution_production.at(solution_production_index).runtime*m_processes.at(m_solution_production.at(solution_production_index).process).tonspershift;
}

double Output_CSV::get_customer_group_production_pallets(const int & customer_group_solution_production_index)
{
	return m_customer_group_solution_production.at(customer_group_solution_production_index).runtime*
		m_processes.at(m_customer_group_solution_production.at(customer_group_solution_production_index).process).tonspershift*
		m_nPallets_per_ton.at(m_customer_group_solution_production.at(customer_group_solution_production_index).product);
}

double Output_CSV::get_customer_group_production_tons(const int & customer_group_solution_production_index)
{
	return m_customer_group_solution_production.at(customer_group_solution_production_index).runtime*
		m_processes.at(m_customer_group_solution_production.at(customer_group_solution_production_index).process).tonspershift;
}

double Output_CSV::get_relation_production_pallets(const int & solution_production_index)
{
	return get_production_pallets(solution_production_index) /
		production_per_article.at(m_solution_production.at(solution_production_index).product).at(m_solution_production.at(solution_production_index).period - 1).pallets;
	//overall_production.at(m_solution_production.at(solution_production_index).product);
}

double Output_CSV::get_relation_production_tons(const int & solution_production_index)
{
	return get_production_tons(solution_production_index) /
		production_per_article.at(m_solution_production.at(solution_production_index).product).at(m_solution_production.at(solution_production_index).period - 1).tons;
	//overall_production.at(m_solution_production.at(solution_production_index).product);
}


double Output_CSV::get_customer_group_relation_production_pallets(const int & customer_group_solution_production_index)
{
	return get_customer_group_production_pallets(customer_group_solution_production_index) /
		production_per_article.at(m_customer_group_solution_production.at(customer_group_solution_production_index).product)
		.at(m_customer_group_solution_production.at(customer_group_solution_production_index).period - 1)
		.pallets;
}

double Output_CSV::get_customer_group_relation_production_tons(const int & customer_group_solution_production_index)
{
	return get_customer_group_production_tons(customer_group_solution_production_index) /
		production_per_article.at(m_customer_group_solution_production.at(customer_group_solution_production_index).product)
		.at(m_customer_group_solution_production.at(customer_group_solution_production_index).period - 1)
		.tons;
}

double Output_CSV::get_production_cost(const int & solution_production_index)
{
	return m_solution_production.at(solution_production_index).runtime*m_processes.at(m_solution_production.at(solution_production_index).process).costspershift;
}

double Output_CSV::get_production_cost_pallet(const int & solution_production_index)
{
	return get_production_cost(solution_production_index) / get_production_pallets(solution_production_index);
}

double Output_CSV::get_production_cost_ton(const int & solution_production_index)
{
	return get_production_cost(solution_production_index) / get_production_tons(solution_production_index);
}

pair<double, double> Output_CSV::get_inventory_pallets(const int & solution_production_index)
{
	int i;
	//return m_solution_production.at(solution_production_index).runtime*m_processes.at(m_solution_production.at(solution_production_index).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(solution_production_index).product);
	for (i = 0; i < m_solution_inventory.size(); ++i)
	{
		if (m_solution_inventory.at(i).period == m_solution_production.at(solution_production_index).period && m_solution_inventory.at(i).plant == machine_locations.at(m_solution_production.at(solution_production_index).machine) && m_solution_inventory.at(i).product == m_solution_production.at(solution_production_index).product)
		{
			return pair<double, double>(m_solution_inventory.at(i).amount_start, m_solution_inventory.at(i).amount_end);
		}
	}
}


pair<double, double> Output_CSV::get_inventory_tons(const int & solution_production_index)
{
	int i;
	//return m_solution_production.at(solution_production_index).runtime*m_processes.at(m_solution_production.at(solution_production_index).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(solution_production_index).product);
	for (i = 0; i < m_solution_inventory.size(); ++i)
	{

		if (m_solution_inventory.at(i).period == m_solution_production.at(solution_production_index).period && m_solution_inventory.at(i).plant == machine_locations.at(m_solution_production.at(solution_production_index).machine) && m_solution_inventory.at(i).product == m_solution_production.at(solution_production_index).product)
		{
			return pair<double, double>(m_solution_inventory.at(i).amount_start / m_nPallets_per_ton.at(m_solution_production.at(solution_production_index).product),
				m_solution_inventory.at(i).amount_end / m_nPallets_per_ton.at(m_solution_production.at(solution_production_index).product)
				);
		}
	}
}

transport_information Output_CSV::get_customer_transports_pallets(int product, int period, int machine)
{

	transport_information dummy;
	dummy.amount = 0.0;
	dummy.costsum = 0.0;
	dummy.costpallet = 0.0;
	int i;
	for (i = 0; i < m_solution_single_transport_customer.size(); ++i)
	{
		if (m_solution_single_transport_customer.at(i).period == period)
		{
			if (m_solution_single_transport_customer.at(i).product == product)
			{
				if (m_solution_single_transport_customer.at(i).plant == machine_locations.at(machine))
				{
					dummy.amount += m_solution_single_transport_customer.at(i).amount;
					dummy.costsum += m_solution_single_transport_customer.at(i).amount *
						m_transport_customer.at(
							transport_indices_customer.at(m_solution_single_transport_customer.at(i).plant).at(m_solution_single_transport_customer.at(i).customer)
						).cost / 34.0;

				}
			}
		}
	}

	if (dummy.amount > 0)
	{
		dummy.costpallet = dummy.costsum / dummy.amount;
	}

	return dummy;
}

transport_information Output_CSV::get_customer_transports_tons(int product, int period, int machine)
{

	transport_information dummy;
	dummy.amount = 0.0;
	dummy.costsum = 0.0;
	dummy.costpallet = 0.0;
	int i;
	for (i = 0; i < m_solution_single_transport_customer.size(); ++i)
	{
		if (m_solution_single_transport_customer.at(i).period == period)
		{
			if (m_solution_single_transport_customer.at(i).product == product)
			{
				if (m_solution_single_transport_customer.at(i).plant == machine_locations.at(machine))
				{
					dummy.amount += m_solution_single_transport_customer.at(i).amount / m_nPallets_per_ton.at(product);
					dummy.costsum += m_solution_single_transport_customer.at(i).amount *
						m_transport_customer.at(
							transport_indices_customer.at(m_solution_single_transport_customer.at(i).plant).at(m_solution_single_transport_customer.at(i).customer)
						).cost / 34.0;

				}
			}
		}
	}

	if (dummy.amount > 0)
	{
		dummy.costpallet = dummy.costsum / dummy.amount;
	}

	return dummy;
}

/*
transport_information Output::get_customer_transports(const int & solution_production_index)
{

transport_information dummy;
dummy.amount = 0.0;
dummy.costsum = 0.0;
dummy.costpallet = 0.0;
int i;
for (i = 0; i < m_solution_single_transport_customer.size(); ++i)
{
if (m_solution_single_transport_customer.at(i).period == m_solution_production.at(solution_production_index).period)
{
if (m_solution_single_transport_customer.at(i).product == m_solution_production.at(solution_production_index).product)
{
if (m_solution_single_transport_customer.at(i).plant == machine_locations.at(m_processes.at(m_solution_production.at(solution_production_index).process).machine))
{
dummy.amount += m_solution_single_transport_customer.at(i).amount;
dummy.costsum += m_solution_single_transport_customer.at(i).amount * m_transport_customer.at(transport_indices_customer.at(m_solution_single_transport_customer.at(i).plant).at(m_solution_single_transport_customer.at(i).customer)).cost/34.0;

}
}
}
}

if (dummy.amount > 0)
{
dummy.costpallet = dummy.costsum / dummy.amount;
}

return dummy;
}
*/
/*
transport_information Output::get_customer_group_customer_transports(const int & customer_group_solution_production_index)
{
transport_information dummy;
dummy.amount = 0.0;
dummy.costsum = 0.0;
dummy.costpallet = 0.0;
int i;
for (i = 0; i < m_solution_single_transport_customer.size(); ++i)
{
if (m_solution_single_transport_customer.at(i).period == m_customer_group_solution_production.at(customer_group_solution_production_index).period)
{
if (m_solution_single_transport_customer.at(i).product == m_customer_group_solution_production.at(customer_group_solution_production_index).product)
{
if (m_solution_single_transport_customer.at(i).plant == machine_locations.at(m_processes.at(m_customer_group_solution_production.at(customer_group_solution_production_index).process).machine))
{
dummy.amount += m_solution_single_transport_customer.at(i).amount;
dummy.costsum += m_solution_single_transport_customer.at(i).amount *
m_transport_customer.at(transport_indices_customer.at(m_solution_single_transport_customer.at(i).plant).at(m_solution_single_transport_customer.at(i).customer)).cost/34.0;

}
}
}
}

if (dummy.amount > 0)
{
dummy.costpallet = dummy.costsum / dummy.amount;
}

return dummy;
}
*/

pair<double, double> Output_CSV::get_ic_transports_pallets(const int & solution_production_index)
{
	double dummy1 = 0.0;
	double dummy2 = 0.0;

	int i;
	for (i = 0; i < m_solution_single_transport_plant.size(); ++i)
	{
		if (m_solution_single_transport_plant.at(i).period == m_solution_production.at(solution_production_index).period)
		{
			if (m_solution_single_transport_plant.at(i).product == m_solution_production.at(solution_production_index).product)
			{
				if (m_solution_single_transport_plant.at(i).plant1 == machine_locations.at(m_processes.at(m_solution_production.at(solution_production_index).process).machine))
				{
					dummy1 += m_solution_single_transport_plant.at(i).amount;
					dummy2 += m_solution_single_transport_plant.at(i).amount *
						m_transport_plant.at(transport_indices_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2)).cost / 34.0;
				}
			}
		}
	}
	return pair<double, double>(dummy1, dummy2);
}

pair<double, double> Output_CSV::get_ic_transports_tons(const int & solution_production_index)
{
	double dummy1 = 0.0;
	double dummy2 = 0.0;

	int i;
	for (i = 0; i < m_solution_single_transport_plant.size(); ++i)
	{
		if (m_solution_single_transport_plant.at(i).period == m_solution_production.at(solution_production_index).period)
		{
			if (m_solution_single_transport_plant.at(i).product == m_solution_production.at(solution_production_index).product)
			{
				if (m_solution_single_transport_plant.at(i).plant1 == machine_locations.at(m_processes.at(m_solution_production.at(solution_production_index).process).machine))
				{
					dummy1 += m_solution_single_transport_plant.at(i).amount / m_nPallets_per_ton.at(m_solution_production.at(solution_production_index).product);
					dummy2 += m_solution_single_transport_plant.at(i).amount *
						m_transport_plant.at(transport_indices_plant
							.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2)).cost / 34.0;
				}
			}
		}
	}
	return pair<double, double>(dummy1, dummy2);
}
//pair<double,double> Output::get_customer_group_ic_transports(const int & customer_group_solution_production_index)
//{
//	double dummy1 = 0.0;
//	double dummy2 = 0.0;
//
//	int i;
//	
//	for (i = 0; i < m_customer_group_solution_single_transport_plant .size(); ++i)
//	{
//		if (m_customer_group_solution_single_transport_plant.at(i).period == m_customer_group_solution_production.at(customer_group_solution_production_index).period)
//		{
//			if (m_customer_group_solution_single_transport_plant.at(i).product == m_customer_group_solution_production.at(customer_group_solution_production_index).product)
//			{
//				if (m_customer_group_solution_single_transport_plant.at(i).plant1 == machine_locations.at(m_processes.at(m_customer_group_solution_production.at(customer_group_solution_production_index).process).machine))
//				{
//					dummy1 += m_solution_single_transport_plant.at(i).amount;
//					dummy2 += m_solution_single_transport_plant.at(i).amount * m_transport_plant.at(transport_indices_plant.at(m_customer_group_solution_single_transport_plant.at(i).plant1).at(m_customer_group_solution_single_transport_plant.at(i).plant2)).cost/34.0;
//				}
//			}
//		}
//	}
//	return pair<double,double> (dummy1,dummy2);
//}





double Output_CSV::get_customer_group_production_cost(const int & customer_group_solution_production_index)
{
	return m_customer_group_solution_production.at(customer_group_solution_production_index).runtime*
		m_processes.at(m_customer_group_solution_production.at(customer_group_solution_production_index).process).costspershift;
}

double Output_CSV::get_customer_group_production_cost_pallet(const int & customer_group_solution_production_index)
{
	return get_customer_group_production_cost(customer_group_solution_production_index) / get_customer_group_production_pallets(customer_group_solution_production_index);
}

double Output_CSV::get_customer_group_production_cost_ton(const int & customer_group_solution_production_index)
{
	return get_customer_group_production_cost(customer_group_solution_production_index) / get_customer_group_production_tons(customer_group_solution_production_index);
}


pair<double, double> Output_CSV::get_customer_group_inventory_pallets(const int & customer_group_solution_production_index)
{
	int i;

	for (i = 0; i < m_customer_group_solution_inventory.size(); ++i)
	{
		if (m_customer_group_solution_inventory.at(i).period ==
			m_customer_group_solution_production.at(customer_group_solution_production_index).period &&
			m_customer_group_solution_inventory.at(i).plant == machine_locations.at(m_customer_group_solution_production.at(customer_group_solution_production_index).machine) &&
			m_customer_group_solution_inventory.at(i).product == m_customer_group_solution_production.at(customer_group_solution_production_index).product)
		{
			return pair<double, double>(m_customer_group_solution_inventory.at(i).amount_start, m_customer_group_solution_inventory.at(i).amount_end);
		}
	}
}

pair<double, double> Output_CSV::get_customer_group_inventory_tons(const int & customer_group_solution_production_index)
{
	int i;

	for (i = 0; i < m_customer_group_solution_inventory.size(); ++i)
	{
		if (m_customer_group_solution_inventory.at(i).period ==
			m_customer_group_solution_production.at(customer_group_solution_production_index).period &&
			m_customer_group_solution_inventory.at(i).plant == machine_locations.at(m_customer_group_solution_production.at(customer_group_solution_production_index).machine) &&
			m_customer_group_solution_inventory.at(i).product == m_customer_group_solution_production.at(customer_group_solution_production_index).product)
		{
			return pair<double, double>(
				m_customer_group_solution_inventory.at(i).amount_start /
				m_nPallets_per_ton.at(m_customer_group_solution_production.at(customer_group_solution_production_index).product),
				m_customer_group_solution_inventory.at(i).amount_end /
				m_nPallets_per_ton.at(m_customer_group_solution_production.at(customer_group_solution_production_index).product)
				);
		}
	}
}



pair<double, double> Output_CSV::get_customer_group_ic_transports_pallets(const int & customer_group_solution_production_index)
{
	double dummy1 = 0.0;
	double dummy2 = 0.0;

	int i;
	for (i = 0; i < m_customer_group_solution_single_transport_plant.size(); ++i)
	{
		if (m_customer_group_solution_single_transport_plant.at(i).period == m_customer_group_solution_production.at(customer_group_solution_production_index).period)
		{
			if (m_customer_group_solution_single_transport_plant.at(i).product == m_customer_group_solution_production.at(customer_group_solution_production_index).product)
			{
				if (m_customer_group_solution_single_transport_plant.at(i).plant1 == machine_locations.at(m_processes.at(m_customer_group_solution_production.at(customer_group_solution_production_index).process).machine))
				{
					dummy1 += m_customer_group_solution_single_transport_plant.at(i).amount;
					dummy2 += m_customer_group_solution_single_transport_plant.at(i).amount *
						m_transport_plant.at(transport_indices_plant.at(m_customer_group_solution_single_transport_plant.at(i).plant1).at(m_customer_group_solution_single_transport_plant.at(i).plant2)).cost / 34.0;
				}
			}
		}
	}
	return pair<double, double>(dummy1, dummy2);
}


pair<double, double> Output_CSV::get_customer_group_ic_transports_tons(const int & customer_group_solution_production_index)
{
	double dummy1 = 0.0;
	double dummy2 = 0.0;

	int i;
	for (i = 0; i < m_customer_group_solution_single_transport_plant.size(); ++i)
	{
		if (m_customer_group_solution_single_transport_plant.at(i).period == m_customer_group_solution_production.at(customer_group_solution_production_index).period)
		{
			if (m_customer_group_solution_single_transport_plant.at(i).product == m_customer_group_solution_production.at(customer_group_solution_production_index).product)
			{
				if (m_customer_group_solution_single_transport_plant.at(i).plant1 == machine_locations.at(m_processes.at(m_customer_group_solution_production.at(customer_group_solution_production_index).process).machine))
				{
					dummy1 += m_customer_group_solution_single_transport_plant.at(i).amount / m_nPallets_per_ton.at(m_customer_group_solution_single_transport_plant.at(i).product);
					dummy2 += m_customer_group_solution_single_transport_plant.at(i).amount *
						m_transport_plant.at(transport_indices_plant.at(m_customer_group_solution_single_transport_plant.at(i).plant1).at(m_customer_group_solution_single_transport_plant.at(i).plant2)).cost / 34.0;
				}
			}
		}
	}
	return pair<double, double>(dummy1, dummy2);
}


void Output_CSV::set_transport_indices_plant()
{
	transport_indices_plant.clear();
	transport_indices_plant.resize(m_plants.size(), vector<int>(m_plants.size(), -1));

	int i;
	for (i = 0; i < m_transport_plant.size(); ++i)
	{
		transport_indices_plant.at(m_transport_plant.at(i).start).at(m_transport_plant.at(i).end) = i;
	}
}

void Output_CSV::set_transport_indices_customer()
{
	transport_indices_customer.clear();
	transport_indices_customer.resize(m_plants.size(), vector<int>(m_customers.size(), -1));

	int i;
	for (i = 0; i < m_transport_customer.size(); ++i)
	{
		transport_indices_customer.at(m_transport_customer.at(i).start).at(m_transport_customer.at(i).end) = i;
	}
}

/*int Output::get_transport_index_plant(const int & plant1, const int & plant2)
{

}

int Output::get_transport_index_customer(const int & plant, const int & customer)
{

}*/

void Output_CSV::article_to_merge()
{
	ofstream output(m_filename_article.c_str());
	if (output.fail())
	{
		cout << "Kann Datei " << m_filename_article << " nicht anlegen!" << endl;
		exit(1);
	}

	output << "Artikelnummer;Artikelbezeichnung;Kunden;Produktionsmenge;Anzahl Schichten;Produktionskosten (Summe);Produktionskosten (Palette);Anfangsbestand;Endbestand;Transport zu Kundenstandorten (Paletten);Kundentransportkosten (Summe);Kundentransportkosten (pro Palette);Intercompany Transporte (Paletten);Intercompany-Kosten (Summe)" << endl;

	int i;
	set<int>::const_iterator it;
	for (i = 0; i < m_nProducts; ++i)
	{
		output << m_products.at(i) << ";\"" << m_product_names.at(i) << "\";";
		for (it = customers_for_products.at(i).begin(); it != customers_for_products.at(i).end(); ++it)
		{
			output << m_customers.at((*it)) << " ";
		}

		output << ";" << threemonth_production.at(i).pallets << ";";
		output << threemonth_production.at(i).shifts << ";";
		output << threemonth_production.at(i).costsum << ";";
		output << threemonth_production.at(i).costpallet << ";";
		output << inventories.at(i).at(0).first << ";" << inventories.at(i).at(2).second << ";";
		output << threemonth_customer_transports.at(i).amount << ";";
		output << threemonth_customer_transports.at(i).costsum << ";" << threemonth_customer_transports.at(i).costpallet << ";";
		output << threemonth_ic_transports.at(i).amount << ";" << threemonth_ic_transports.at(i).costsum << endl;
	}

	output.close();
}

void Output_CSV::monthly_to_merge()
{

	ofstream output(m_filename_monthly.c_str());
	if (output.fail())
	{
		cout << "Kann Datei " << m_filename_monthly << " nicht anlegen!" << endl;
		exit(1);
	}

	int i, j, k;
	set<int>::const_iterator it;

	output << "Artikelnummer;Artikelbezeichnung;Kunde;Monat;Werke;Anlagen;Menge;Anzahl Schichten;Produktionskosten (Summe);Produktionskosten (Palette);Anfangsbestand;Endbestand;Transport zu Kundenstandorten (Paletten);Kundentransportkosten (Summe);Kundentransportkosten (pro Palette);Intercompany-Transporte (Paletten);Intercompany-Kosten (Summe)" << endl;


	for (k = 1; k < m_periods.size() + 1; ++k)
	{
		for (i = 0; i < m_products.size(); ++i)
		{
			output << "\"" << m_products.at(i) << "\"" << ";" << m_product_names.at(i) << ";";
			for (it = customers_for_products.at(i).begin(); it != customers_for_products.at(i).end(); ++it)
			{
				output << m_customers.at((*it)) << " ";
			}
			output << ";" << m_periods.at(k - 1) << ";";

			for (j = 0; j < production_per_plant.at(i).at(k - 1).size(); ++j)
			{
				output << m_plants.at(production_per_plant.at(i).at(k - 1).at(j).first) << "(" << round(production_per_plant.at(i).at(k - 1).at(j).second / production_per_article.at(i).at(k - 1).pallets, 2) << ")" << " ";
			}
			output << ";";

			for (j = 0; j < production_per_machine.at(i).at(k - 1).size(); ++j)
			{
				output << m_machines.at(production_per_machine.at(i).at(k - 1).at(j).first) << "(" << round(production_per_machine.at(i).at(k - 1).at(j).second / production_per_article.at(i).at(k - 1).pallets, 2) << ")" << " ";
			}


			output << ";" << production_per_article.at(i).at(k - 1).pallets;
			output << ";" << production_per_article.at(i).at(k - 1).shifts << ";";
			output << production_per_article.at(i).at(k - 1).costsum << ";";
			output << production_per_article.at(i).at(k - 1).costpallet << ";";
			output << inventories.at(i).at(k - 1).first << ";" << inventories.at(i).at(k - 1).second << ";";

			output << customer_transports.at(i).at(k - 1).amount << ";" << customer_transports.at(i).at(k - 1).costsum << ";" << customer_transports.at(i).at(k - 1).costpallet << ";";

			output << ic_transports.at(i).at(k - 1).amount << ";" << ic_transports.at(i).at(k - 1).costsum << endl;
		}
	}

	output.close();
}


void Output_CSV::get_all_customer_transports()
{
	int i, k;

	customer_transports.clear();
	customer_transports.resize(m_nProducts, vector<transport_information>(m_periods.size()));

	threemonth_customer_transports.clear();
	threemonth_customer_transports.resize(m_nProducts);

	for (i = 0; i < m_nProducts; ++i)
	{
		threemonth_customer_transports.at(i).amount = 0;
		threemonth_customer_transports.at(i).costsum = 0;

		for (k = 0; k < m_periods.size(); ++k)
		{
			customer_transports.at(i).at(k).amount = 0;
			customer_transports.at(i).at(k).costsum = 0;
		}
	}

	for (i = 0; i < m_solution_single_transport_customer.size(); ++i)
	{
		customer_transports.at(m_solution_single_transport_customer.at(i).product).at(m_solution_single_transport_customer.at(i).period - 1).amount += m_solution_single_transport_customer.at(i).amount;
		customer_transports.at(m_solution_single_transport_customer.at(i).product).at(m_solution_single_transport_customer.at(i).period - 1).costsum += m_solution_single_transport_customer.at(i).amount * m_transport_customer.at(transport_indices_customer.at(m_solution_single_transport_customer.at(i).plant).at(m_solution_single_transport_customer.at(i).customer)).cost / 34.0;

		threemonth_customer_transports.at(m_solution_single_transport_customer.at(i).product).amount += m_solution_single_transport_customer.at(i).amount;
		threemonth_customer_transports.at(m_solution_single_transport_customer.at(i).product).costsum += m_solution_single_transport_customer.at(i).amount  * m_transport_customer.at(transport_indices_customer.at(m_solution_single_transport_customer.at(i).plant).at(m_solution_single_transport_customer.at(i).customer)).cost / 34.0;
	}

	for (i = 0; i < m_nProducts; ++i)
	{
		for (k = 0; k < m_periods.size(); ++k)
		{
			customer_transports.at(i).at(k).costpallet = customer_transports.at(i).at(k).costsum / customer_transports.at(i).at(k).amount;
		}
		threemonth_customer_transports.at(i).costpallet = threemonth_customer_transports.at(i).costsum / threemonth_customer_transports.at(i).amount;
	}
}

void Output_CSV::get_all_ic_transports()
{
	ic_transports.clear();
	ic_transports.resize(m_nProducts, vector<transport_information>(m_periods.size()));

	int i, k;

	threemonth_ic_transports.clear();
	threemonth_ic_transports.resize(m_nProducts);

	for (i = 0; i < m_nProducts; ++i)
	{
		threemonth_ic_transports.at(i).amount = 0;
		threemonth_ic_transports.at(i).costsum = 0;

		for (k = 0; k < m_periods.size(); ++k)
		{
			ic_transports.at(i).at(k).amount = 0;
			ic_transports.at(i).at(k).costsum = 0;
		}
	}

	for (i = 0; i < m_solution_single_transport_plant.size(); ++i)
	{
		ic_transports.at(m_solution_single_transport_plant.at(i).product).at(m_solution_single_transport_plant.at(i).period - 1).amount += m_solution_single_transport_plant.at(i).amount;
		ic_transports.at(m_solution_single_transport_plant.at(i).product).at(m_solution_single_transport_plant.at(i).period - 1).costsum += m_solution_single_transport_plant.at(i).amount *
			m_transport_plant.at(transport_indices_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2)).cost / 34.0;


		threemonth_ic_transports.at(m_solution_single_transport_plant.at(i).product).amount += m_solution_single_transport_plant.at(i).amount;
		threemonth_ic_transports.at(m_solution_single_transport_plant.at(i).product).costsum += m_solution_single_transport_plant.at(i).amount  * m_transport_plant.at(transport_indices_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2)).cost / 34.0;
	}

	for (i = 0; i<m_customer_group_solution_single_transport_plant.size(); i++)
	{
		ic_transports.at(m_customer_group_solution_single_transport_plant.at(i).product).at(m_customer_group_solution_single_transport_plant.at(i).period - 1).amount += m_customer_group_solution_single_transport_plant.at(i).amount;
		ic_transports.at(m_customer_group_solution_single_transport_plant.at(i).product).at(m_customer_group_solution_single_transport_plant.at(i).period - 1).costsum += m_customer_group_solution_single_transport_plant.at(i).amount *
			m_transport_plant.at(transport_indices_plant.at(m_customer_group_solution_single_transport_plant.at(i).plant1).at(m_customer_group_solution_single_transport_plant.at(i).plant2)).cost / 34.0;
	}

	for (i = 0; i < m_nProducts; ++i)
	{
		for (k = 0; k < m_periods.size(); ++k)
		{
			ic_transports.at(i).at(k).costpallet = ic_transports.at(i).at(k).costsum / ic_transports.at(i).at(k).amount;
		}
		threemonth_ic_transports.at(i).costpallet = threemonth_ic_transports.at(i).costsum / threemonth_ic_transports.at(i).amount;
	}
}
void Output_CSV::get_3month_production()
{
	int i, k;

	threemonth_production.clear();
	threemonth_production.resize(m_nProducts);

	for (i = 0; i < m_nProducts; ++i)
	{
		threemonth_production.at(i).costsum = 0;
		threemonth_production.at(i).shifts = 0;
		threemonth_production.at(i).pallets = 0;
		for (k = 0; k < m_periods.size(); ++k)
		{
			threemonth_production.at(i).costsum += production_per_article.at(i).at(k).costsum;
			threemonth_production.at(i).shifts += production_per_article.at(i).at(k).shifts;
			threemonth_production.at(i).pallets += production_per_article.at(i).at(k).pallets;
		}
		if (threemonth_production.at(i).pallets > 0)
		{
			threemonth_production.at(i).costpallet = threemonth_production.at(i).costsum / threemonth_production.at(i).pallets;
		}
	}
}

void Output_CSV::get_production_plants()
{
	int i, k;

	production_per_plant.clear();
	production_per_plant.resize(m_nProducts, vector<vector<pair<int, double> > >(m_periods.size()));

	production_per_article.clear();
	production_per_article.resize(m_nProducts, vector<production_information>(m_periods.size()));

	production_per_machine.clear();
	production_per_machine.resize(m_nProducts, vector<vector<pair<int, double> > >(m_periods.size()));

	for (i = 0; i < m_solution_production.size(); ++i)
	{
		production_per_plant.at(m_solution_production.at(i).product).at(m_solution_production.at(i).period - 1)
			.push_back(pair<int, double>(machine_locations.at(m_solution_production.at(i).machine),
				m_solution_production.at(i).runtime*m_processes.at(m_solution_production.at(i).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(i).product)
				)
			);

		production_per_article.at(m_solution_production.at(i).product).at(m_solution_production.at(i).period - 1).shifts += m_solution_production.at(i).runtime;
		production_per_article.at(m_solution_production.at(i).product).at(m_solution_production.at(i).period - 1).costsum += m_solution_production.at(i).runtime * m_processes.at(m_solution_production.at(i).process).costspershift;
		production_per_article.at(m_solution_production.at(i).product).at(m_solution_production.at(i).period - 1).pallets += m_solution_production.at(i).runtime * m_processes.at(m_solution_production.at(i).process).tonspershift * m_nPallets_per_ton.at(m_solution_production.at(i).product);
		production_per_article.at(m_solution_production.at(i).product).at(m_solution_production.at(i).period - 1).tons += m_solution_production.at(i).runtime * m_processes.at(m_solution_production.at(i).process).tonspershift;

		production_per_machine.at(m_solution_production.at(i).product).at(m_solution_production.at(i).period - 1).push_back(pair<int, double>(m_solution_production.at(i).machine, m_solution_production.at(i).runtime*m_processes.at(m_solution_production.at(i).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(i).product)));
	}

	for (i = 0; i<m_customer_group_solution_production.size(); i++)
	{
		production_per_plant.at(m_customer_group_solution_production.at(i).product).at(m_customer_group_solution_production.at(i).period - 1).push_back(pair<int, double>(machine_locations.at(m_customer_group_solution_production.at(i).machine), m_customer_group_solution_production.at(i).runtime*m_processes.at(m_customer_group_solution_production.at(i).process).tonspershift*m_nPallets_per_ton.at(m_customer_group_solution_production.at(i).product)));

		production_per_article.at(m_customer_group_solution_production.at(i).product).at(m_customer_group_solution_production.at(i).period - 1).shifts += m_customer_group_solution_production.at(i).runtime;
		production_per_article.at(m_customer_group_solution_production.at(i).product).at(m_customer_group_solution_production.at(i).period - 1).costsum += m_customer_group_solution_production.at(i).runtime * m_processes.at(m_customer_group_solution_production.at(i).process).costspershift;
		production_per_article.at(m_customer_group_solution_production.at(i).product).at(m_customer_group_solution_production.at(i).period - 1).pallets += m_customer_group_solution_production.at(i).runtime * m_processes.at(m_customer_group_solution_production.at(i).process).tonspershift * m_nPallets_per_ton.at(m_customer_group_solution_production.at(i).product);
		production_per_article.at(m_customer_group_solution_production.at(i).product).at(m_customer_group_solution_production.at(i).period - 1).tons += m_customer_group_solution_production.at(i).runtime * m_processes.at(m_customer_group_solution_production.at(i).process).tonspershift;

		production_per_machine.at(m_customer_group_solution_production.at(i).product).at(m_customer_group_solution_production.at(i).period - 1).push_back(pair<int, double>(m_customer_group_solution_production.at(i).machine, m_customer_group_solution_production.at(i).runtime*m_processes.at(m_customer_group_solution_production.at(i).process).tonspershift*m_nPallets_per_ton.at(m_customer_group_solution_production.at(i).product)));
	}

	for (k = 1; k < m_periods.size() + 1; ++k)
	{
		for (i = 0; i < m_products.size(); ++i)
		{
			if (production_per_article.at(i).at(k - 1).pallets > 0)
			{
				production_per_article.at(i).at(k - 1).costpallet = production_per_article.at(i).at(k - 1).costsum / production_per_article.at(i).at(k - 1).pallets;
			}
			else
			{
				production_per_article.at(i).at(k - 1).costpallet = 0.0;
			}

			if (production_per_article.at(i).at(k - 1).tons > 0)
			{
				production_per_article.at(i).at(k - 1).costton = production_per_article.at(i).at(k - 1).costsum / production_per_article.at(i).at(k - 1).tons;
			}
			else
			{
				production_per_article.at(i).at(k - 1).costton = 0.0;
			}
		}
	}
}

void Output_CSV::get_inventories()
{
	int i;

	inventories.clear();
	inventories.resize(m_nProducts, vector<pair<double, double> >(m_periods.size(), pair<double, double>(0.0, 0.0)));

	for (i = 0; i < m_solution_inventory.size(); ++i)
	{
		inventories.at(m_solution_inventory.at(i).product).at(m_solution_inventory.at(i).period - 1).first += m_solution_inventory.at(i).amount_start;
		inventories.at(m_solution_inventory.at(i).product).at(m_solution_inventory.at(i).period - 1).second += m_solution_inventory.at(i).amount_end;
	}

	for (i = 0; i<m_customer_group_solution_inventory.size(); i++)
	{
		inventories.at(m_customer_group_solution_inventory.at(i).product).at(m_customer_group_solution_inventory.at(i).period - 1).first += m_customer_group_solution_inventory.at(i).amount_start;
		inventories.at(m_customer_group_solution_inventory.at(i).product).at(m_customer_group_solution_inventory.at(i).period - 1).second += m_customer_group_solution_inventory.at(i).amount_end;
	}
}



void Output_CSV::get_customers_for_products()
{
	customers_for_products.clear();
	customers_for_products.resize(m_nProducts);

	int i;
	for (i = 0; i < m_demands.size(); ++i)
	{
		customers_for_products.at(m_demands.at(i).product).insert(m_demands.at(i).customer);
	}
}

void Output_CSV::output_for_graphic_ic(const bool & german)
{
	int i, j, k;

	ofstream output(m_filename_graphic_ic.c_str());
	if (output.fail())
	{
		cout << "Kann Datei " << m_filename_graphic_ic << " nicht anlegen!" << endl;
		exit(1);
	}

	if (german) output.imbue(locale("german"));
	output << fixed;

	output << "Werk1;Periode;Werk2;eingehende IC-Transporte in Paletten;eingehende IC-Transportkosten;ausgehende IC-Transporte in Paletten;ausgehende IC-Transportkosten" << endl;

	for (i = 0; i < m_plants.size(); ++i)
	{
		for (j = 0; j < m_plants.size(); ++j)
		{
			if (i != j)
			{
				for (k = 1; k <= m_periods.size(); ++k)
				{
					output << m_plants.at(i) << ";";
					output << m_plants.at(j) << ";";
					output << m_periods.at(k - 1) << ";";
					output << ic_in_transports.at(i).at(j).at(k - 1).amount << ";";
					output << ic_in_transports.at(i).at(j).at(k - 1).costsum << ";";
					output << ic_out_transports.at(i).at(j).at(k - 1).amount << ";";
					output << ic_out_transports.at(i).at(j).at(k - 1).costsum << ";";
					//output << ic_transport.at(i).at(j).at(k - 1).amount << ";";
					//output << ic_transport.at(i).at(j).at(k - 1).costsum;

					output << endl;
				}
			}
		}
	}

	output.close();

}

void Output_CSV::output_for_graphic(const bool & german)
{
	ofstream output(m_filename_graphic.c_str());
	if (output.fail())
	{
		cout << "Kann Datei " << m_filename_graphic << " nicht anlegen!" << endl;
		exit(1);
	}

	if (german) output.imbue(locale("german"));
	output << fixed;

	output << "Werk;Periode;Produktion Paletten;Produktion Tonnen;Produktionskosten;Produktionsschichten;Kundentransporte Paletten;Kundentransportkosten;ausgehende IC-Transporte in Paletten;ausgehende IC-Transportkosten;eingehende IC-Transporte in Paletten;eingehende IC-Transportkosten" << endl;

	int i, k;
	for (i = 0; i < m_plants.size(); ++i)
	{
		for (k = 1; k < m_periods.size() + 1; ++k)
		{
			output << m_plants.at(i) << ";" << m_periods.at(k - 1) << ";" << productions_per_plant.at(i).at(k - 1).pallets << ";" << productions_per_plant.at(i).at(k - 1).tons << ";" << productions_per_plant.at(i).at(k - 1).costsum << ";" << productions_per_plant.at(i).at(k - 1).shifts << ";" << customer_transports_per_plant.at(i).at(k - 1).amount << ";" << customer_transports_per_plant.at(i).at(k - 1).costsum << ";" << ic_out_transports_per_plant.at(i).at(k - 1).amount << ";" << ic_out_transports_per_plant.at(i).at(k - 1).costsum << ";" << ic_in_transports_per_plant.at(i).at(k - 1).amount << ";" << ic_in_transports_per_plant.at(i).at(k - 1).costsum << endl;
		}
	}

	output.close();
}

void Output_CSV::get_production_per_plant()
{
	int i, k;
	production_information dummy;
	dummy.costsum = 0.0;
	dummy.costpallet = 0.0;
	dummy.shifts = 0.0;
	dummy.pallets = 0.0;
	dummy.tons = 0.0;

	productions_per_plant.clear();
	productions_per_plant.resize(m_plants.size(), vector<production_information>(m_periods.size(), dummy));

	for (i = 0; i < m_solution_production.size(); ++i)
	{
		productions_per_plant.at(machine_locations.at(m_solution_production.at(i).machine)).at(m_solution_production.at(i).period - 1).shifts += m_solution_production.at(i).runtime;
		productions_per_plant.at(machine_locations.at(m_solution_production.at(i).machine)).at(m_solution_production.at(i).period - 1).pallets += m_solution_production.at(i).runtime*m_processes.at(m_solution_production.at(i).process).tonspershift * m_nPallets_per_ton.at(m_solution_production.at(i).product);
		productions_per_plant.at(machine_locations.at(m_solution_production.at(i).machine)).at(m_solution_production.at(i).period - 1).costsum += m_solution_production.at(i).runtime*m_processes.at(m_solution_production.at(i).process).costspershift;
		productions_per_plant.at(machine_locations.at(m_solution_production.at(i).machine)).at(m_solution_production.at(i).period - 1).tons += m_solution_production.at(i).runtime * m_processes.at(m_solution_production.at(i).process).tonspershift;
	}

	for (i = 0; i<m_customer_group_solution_production.size(); i++)
	{
		productions_per_plant.at(machine_locations.at(m_customer_group_solution_production.at(i).machine)).at(m_customer_group_solution_production.at(i).period - 1).shifts += m_customer_group_solution_production.at(i).runtime;
		productions_per_plant.at(machine_locations.at(m_customer_group_solution_production.at(i).machine)).at(m_customer_group_solution_production.at(i).period - 1).pallets += m_customer_group_solution_production.at(i).runtime*m_processes.at(m_customer_group_solution_production.at(i).process).tonspershift * m_nPallets_per_ton.at(m_customer_group_solution_production.at(i).product);
		productions_per_plant.at(machine_locations.at(m_customer_group_solution_production.at(i).machine)).at(m_customer_group_solution_production.at(i).period - 1).costsum += m_customer_group_solution_production.at(i).runtime*m_processes.at(m_customer_group_solution_production.at(i).process).costspershift;
		productions_per_plant.at(machine_locations.at(m_customer_group_solution_production.at(i).machine)).at(m_customer_group_solution_production.at(i).period - 1).tons += m_customer_group_solution_production.at(i).runtime * m_processes.at(m_customer_group_solution_production.at(i).process).tonspershift;
	}

	for (i = 0; i < m_plants.size(); ++i)
	{
		for (k = 0; k < m_periods.size(); ++k)
		{
			if (productions_per_plant.at(i).at(k).pallets > 0)
			{
				productions_per_plant.at(i).at(k).costpallet = productions_per_plant.at(i).at(k).costsum / productions_per_plant.at(i).at(k).pallets;
			}
			else
			{
				productions_per_plant.at(i).at(k).costpallet = 0;
			}
		}
	}
}

void Output_CSV::get_overall_production_per_plant()
{
	int i, k;
	production_information dummy;
	dummy.costsum = 0.0;
	dummy.costpallet = 0.0;
	dummy.shifts = 0.0;
	dummy.pallets = 0.0;
	dummy.tons = 0.0;

	overall_productions_per_plant.clear();
	overall_productions_per_plant.resize(m_plants.size(), dummy);

	for (i = 0; i < m_plants.size(); ++i)
	{
		for (k = 0; k < m_periods.size(); ++k)
		{
			overall_productions_per_plant.at(i).costsum += productions_per_plant.at(i).at(k).costsum;
			overall_productions_per_plant.at(i).shifts += productions_per_plant.at(i).at(k).shifts;
			overall_productions_per_plant.at(i).pallets += productions_per_plant.at(i).at(k).pallets;
			overall_productions_per_plant.at(i).tons += productions_per_plant.at(i).at(k).tons;
		}
		if (overall_productions_per_plant.at(i).pallets > 0)
		{
			overall_productions_per_plant.at(i).costpallet += overall_productions_per_plant.at(i).costsum / overall_productions_per_plant.at(i).pallets;
		}
		else
		{
			overall_productions_per_plant.at(i).costpallet = 0;
		}
	}
}

void Output_CSV::get_customer_transports_per_plant()
{
	int i, k;

	transport_information dummy;
	dummy.amount = 0.0;
	dummy.costsum = 0.0;
	dummy.costpallet = 0.0;

	customer_transports_per_plant.clear();
	customer_transports_per_plant.resize(m_plants.size(), vector<transport_information>(m_periods.size(), dummy));

	for (i = 0; i < m_solution_single_transport_customer.size(); ++i)
	{
		customer_transports_per_plant.at(m_solution_single_transport_customer.at(i).plant).at(m_solution_single_transport_customer.at(i).period - 1).amount +=
			m_solution_single_transport_customer.at(i).amount;
		customer_transports_per_plant.at(m_solution_single_transport_customer.at(i).plant).at(m_solution_single_transport_customer.at(i).period - 1).costsum +=
			m_solution_single_transport_customer.at(i).amount * m_transport_customer.at(transport_indices_customer.at(m_solution_single_transport_customer.at(i).plant).at(m_solution_single_transport_customer.at(i).customer)).cost / 34.0;
	}

	for (i = 0; i < m_plants.size(); ++i)
	{
		for (k = 0; k < m_periods.size(); ++k)
		{
			if (customer_transports_per_plant.at(i).at(k).amount > 0)
			{
				customer_transports_per_plant.at(i).at(k).costpallet = customer_transports_per_plant.at(i).at(k).costsum / customer_transports_per_plant.at(i).at(k).amount;
			}
			else
			{
				customer_transports_per_plant.at(i).at(k).costpallet = 0;
			}
		}
	}
}

void Output_CSV::get_overall_customer_transports_per_plant()
{
	int i, k;
	transport_information dummy;
	dummy.costsum = 0.0;
	dummy.costpallet = 0.0;
	dummy.amount = 0.0;

	overall_customer_transports_per_plant.clear();
	overall_customer_transports_per_plant.resize(m_plants.size(), dummy);

	for (i = 0; i < m_plants.size(); ++i)
	{
		for (k = 0; k < m_periods.size(); ++k)
		{
			overall_customer_transports_per_plant.at(i).costsum += customer_transports_per_plant.at(i).at(k).costsum;
			overall_customer_transports_per_plant.at(i).amount += customer_transports_per_plant.at(i).at(k).amount;
		}
		if (overall_customer_transports_per_plant.at(i).amount > 0)
		{
			overall_customer_transports_per_plant.at(i).costpallet += overall_customer_transports_per_plant.at(i).costsum / overall_customer_transports_per_plant.at(i).amount;
		}
		else
		{
			overall_customer_transports_per_plant.at(i).costpallet = 0;
		}
	}
}

void Output_CSV::get_ic_transports()
{
	int i;

	ic_in_transports.clear();
	ic_out_transports.clear();
	ic_transport.clear();

	transport_information dummy;
	dummy.amount = 0.0;
	dummy.costsum = 0.0;
	dummy.costpallet = 0.0;

	ic_in_transports.resize(m_plants.size(), vector<vector<transport_information> >(m_plants.size(), vector<transport_information>(m_periods.size(), dummy)));
	ic_out_transports.resize(m_plants.size(), vector<vector<transport_information> >(m_plants.size(), vector<transport_information>(m_periods.size(), dummy)));
	ic_transport.resize(m_plants.size(), vector<vector<transport_information> >(m_plants.size(), vector<transport_information>(m_periods.size(), dummy)));

	for (i = 0; i < m_solution_single_transport_plant.size(); ++i)
	{
		ic_in_transports.at(m_solution_single_transport_plant.at(i).plant2).at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).period - 1).amount += m_solution_single_transport_plant.at(i).amount;
		ic_in_transports.at(m_solution_single_transport_plant.at(i).plant2).at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).period - 1).costsum += m_solution_single_transport_plant.at(i).amount * m_transport_plant.at(transport_indices_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2)).cost / 34.0;
		ic_out_transports.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2).at(m_solution_single_transport_plant.at(i).period - 1).amount += m_solution_single_transport_plant.at(i).amount;
		ic_out_transports.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2).at(m_solution_single_transport_plant.at(i).period - 1).costsum += m_solution_single_transport_plant.at(i).amount * m_transport_plant.at(transport_indices_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2)).cost / 34.0;
		ic_transport.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2).at(m_solution_single_transport_plant.at(i).period - 1).amount += m_solution_single_transport_plant.at(i).amount;
		ic_transport.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2).at(m_solution_single_transport_plant.at(i).period - 1).costsum += m_solution_single_transport_plant.at(i).amount * m_transport_plant.at(transport_indices_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2)).cost / 34.0;
		ic_transport.at(m_solution_single_transport_plant.at(i).plant2).at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).period - 1).amount += m_solution_single_transport_plant.at(i).amount;
		ic_transport.at(m_solution_single_transport_plant.at(i).plant2).at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).period - 1).costsum += m_solution_single_transport_plant.at(i).amount * m_transport_plant.at(transport_indices_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2)).cost / 34.0;
	}

	for (i = 0; i<m_customer_group_solution_single_transport_plant.size(); i++)
	{
		ic_in_transports.at(m_customer_group_solution_single_transport_plant.at(i).plant2).at(m_customer_group_solution_single_transport_plant.at(i).plant1).at(m_customer_group_solution_single_transport_plant.at(i).period - 1).amount += m_customer_group_solution_single_transport_plant.at(i).amount;
		ic_in_transports.at(m_customer_group_solution_single_transport_plant.at(i).plant2).at(m_customer_group_solution_single_transport_plant.at(i).plant1).at(m_customer_group_solution_single_transport_plant.at(i).period - 1).costsum += m_customer_group_solution_single_transport_plant.at(i).amount * m_transport_plant.at(transport_indices_plant.at(m_customer_group_solution_single_transport_plant.at(i).plant1).at(m_customer_group_solution_single_transport_plant.at(i).plant2)).cost / 34.0;
		ic_out_transports.at(m_customer_group_solution_single_transport_plant.at(i).plant1).at(m_customer_group_solution_single_transport_plant.at(i).plant2).at(m_customer_group_solution_single_transport_plant.at(i).period - 1).amount += m_customer_group_solution_single_transport_plant.at(i).amount;
		ic_out_transports.at(m_customer_group_solution_single_transport_plant.at(i).plant1).at(m_customer_group_solution_single_transport_plant.at(i).plant2).at(m_customer_group_solution_single_transport_plant.at(i).period - 1).costsum += m_customer_group_solution_single_transport_plant.at(i).amount * m_transport_plant.at(transport_indices_plant.at(m_customer_group_solution_single_transport_plant.at(i).plant1).at(m_customer_group_solution_single_transport_plant.at(i).plant2)).cost / 34.0;
		ic_transport.at(m_customer_group_solution_single_transport_plant.at(i).plant1).at(m_customer_group_solution_single_transport_plant.at(i).plant2).at(m_customer_group_solution_single_transport_plant.at(i).period - 1).amount += m_customer_group_solution_single_transport_plant.at(i).amount;
		ic_transport.at(m_customer_group_solution_single_transport_plant.at(i).plant1).at(m_customer_group_solution_single_transport_plant.at(i).plant2).at(m_customer_group_solution_single_transport_plant.at(i).period - 1).costsum += m_customer_group_solution_single_transport_plant.at(i).amount * m_transport_plant.at(transport_indices_plant.at(m_customer_group_solution_single_transport_plant.at(i).plant1).at(m_customer_group_solution_single_transport_plant.at(i).plant2)).cost / 34.0;
		ic_transport.at(m_customer_group_solution_single_transport_plant.at(i).plant2).at(m_customer_group_solution_single_transport_plant.at(i).plant1).at(m_customer_group_solution_single_transport_plant.at(i).period - 1).amount += m_customer_group_solution_single_transport_plant.at(i).amount;
		ic_transport.at(m_customer_group_solution_single_transport_plant.at(i).plant2).at(m_customer_group_solution_single_transport_plant.at(i).plant1).at(m_customer_group_solution_single_transport_plant.at(i).period - 1).costsum += m_customer_group_solution_single_transport_plant.at(i).amount * m_transport_plant.at(transport_indices_plant.at(m_customer_group_solution_single_transport_plant.at(i).plant1).at(m_customer_group_solution_single_transport_plant.at(i).plant2)).cost / 34.0;
	}
}

void Output_CSV::get_ic_transports_per_plant()
{
	int i, k;

	ic_in_transports_per_plant.clear();
	ic_out_transports_per_plant.clear();
	ic_transports_per_plant.clear();

	transport_information dummy;
	dummy.amount = 0.0;
	dummy.costsum = 0.0;
	dummy.costpallet = 0.0;

	ic_in_transports_per_plant.resize(m_plants.size(), vector<transport_information>(m_periods.size(), dummy));
	ic_out_transports_per_plant.resize(m_plants.size(), vector<transport_information>(m_periods.size(), dummy));
	ic_transports_per_plant.resize(m_plants.size(), vector<transport_information>(m_periods.size(), dummy));

	for (i = 0; i < m_solution_single_transport_plant.size(); ++i)
	{
		ic_out_transports_per_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).period - 1).amount += m_solution_single_transport_plant.at(i).amount;

		ic_in_transports_per_plant.at(m_solution_single_transport_plant.at(i).plant2).at(m_solution_single_transport_plant.at(i).period - 1).amount += m_solution_single_transport_plant.at(i).amount;

		ic_transports_per_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).period - 1).amount += m_solution_single_transport_plant.at(i).amount;
		ic_transports_per_plant.at(m_solution_single_transport_plant.at(i).plant2).at(m_solution_single_transport_plant.at(i).period - 1).amount += m_solution_single_transport_plant.at(i).amount;

		ic_out_transports_per_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).period - 1).costsum += m_solution_single_transport_plant.at(i).amount * m_transport_plant.at(transport_indices_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2)).cost / 34.0;

		ic_in_transports_per_plant.at(m_solution_single_transport_plant.at(i).plant2).at(m_solution_single_transport_plant.at(i).period - 1).costsum += m_solution_single_transport_plant.at(i).amount * m_transport_plant.at(transport_indices_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2)).cost / 34.0;

		ic_transports_per_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).period - 1).amount += m_solution_single_transport_plant.at(i).amount * m_transport_plant.at(transport_indices_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2)).cost / 34.0;

		ic_transports_per_plant.at(m_solution_single_transport_plant.at(i).plant2).at(m_solution_single_transport_plant.at(i).period - 1).amount += m_solution_single_transport_plant.at(i).amount * m_transport_plant.at(transport_indices_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2)).cost / 34.0;

	}

	for (i = 0; i<m_customer_group_solution_single_transport_plant.size(); i++)
	{
		ic_out_transports_per_plant.at(m_customer_group_solution_single_transport_plant.at(i).plant1).at(m_customer_group_solution_single_transport_plant.at(i).period - 1).amount += m_customer_group_solution_single_transport_plant.at(i).amount;

		ic_in_transports_per_plant.at(m_customer_group_solution_single_transport_plant.at(i).plant2).at(m_customer_group_solution_single_transport_plant.at(i).period - 1).amount += m_customer_group_solution_single_transport_plant.at(i).amount;

		ic_transports_per_plant.at(m_customer_group_solution_single_transport_plant.at(i).plant1).at(m_customer_group_solution_single_transport_plant.at(i).period - 1).amount += m_customer_group_solution_single_transport_plant.at(i).amount;
		ic_transports_per_plant.at(m_customer_group_solution_single_transport_plant.at(i).plant2).at(m_customer_group_solution_single_transport_plant.at(i).period - 1).amount += m_customer_group_solution_single_transport_plant.at(i).amount;

		ic_out_transports_per_plant.at(m_customer_group_solution_single_transport_plant.at(i).plant1).at(m_customer_group_solution_single_transport_plant.at(i).period - 1).costsum += m_customer_group_solution_single_transport_plant.at(i).amount * m_transport_plant.at(transport_indices_plant.at(m_customer_group_solution_single_transport_plant.at(i).plant1).at(m_customer_group_solution_single_transport_plant.at(i).plant2)).cost / 34.0;

		ic_in_transports_per_plant.at(m_customer_group_solution_single_transport_plant.at(i).plant2).at(m_customer_group_solution_single_transport_plant.at(i).period - 1).costsum += m_customer_group_solution_single_transport_plant.at(i).amount * m_transport_plant.at(transport_indices_plant.at(m_customer_group_solution_single_transport_plant.at(i).plant1).at(m_customer_group_solution_single_transport_plant.at(i).plant2)).cost / 34.0;

		ic_transports_per_plant.at(m_customer_group_solution_single_transport_plant.at(i).plant1).at(m_customer_group_solution_single_transport_plant.at(i).period - 1).amount += m_customer_group_solution_single_transport_plant.at(i).amount * m_transport_plant.at(transport_indices_plant.at(m_customer_group_solution_single_transport_plant.at(i).plant1).at(m_customer_group_solution_single_transport_plant.at(i).plant2)).cost / 34.0;

		ic_transports_per_plant.at(m_customer_group_solution_single_transport_plant.at(i).plant2).at(m_customer_group_solution_single_transport_plant.at(i).period - 1).amount += m_customer_group_solution_single_transport_plant.at(i).amount * m_transport_plant.at(transport_indices_plant.at(m_customer_group_solution_single_transport_plant.at(i).plant1).at(m_customer_group_solution_single_transport_plant.at(i).plant2)).cost / 34.0;

	}

	for (i = 0; i < m_plants.size(); ++i)
	{
		for (k = 0; k < m_periods.size(); ++k)
		{
			if (ic_out_transports_per_plant.at(i).at(k).amount > 0)
			{
				ic_out_transports_per_plant.at(i).at(k).costpallet = ic_out_transports_per_plant.at(i).at(k).costsum / ic_out_transports_per_plant.at(i).at(k).amount;
			}
			else
			{
				ic_out_transports_per_plant.at(i).at(k).costpallet = 0;
			}
			if (ic_in_transports_per_plant.at(i).at(k).amount > 0)
			{
				ic_in_transports_per_plant.at(i).at(k).costpallet = ic_in_transports_per_plant.at(i).at(k).costsum / ic_in_transports_per_plant.at(i).at(k).amount;
			}
			else
			{
				ic_in_transports_per_plant.at(i).at(k).costpallet = 0;
			}
			if (ic_transports_per_plant.at(i).at(k).amount > 0)
			{
				ic_transports_per_plant.at(i).at(k).costpallet = ic_transports_per_plant.at(i).at(k).costsum / ic_transports_per_plant.at(i).at(k).amount;
			}
			else
			{
				ic_transports_per_plant.at(i).at(k).costpallet = 0;
			}
		}
	}
}

void Output_CSV::get_overall_ic_transports_per_plant()
{
	int i, k;
	transport_information dummy;
	dummy.costsum = 0.0;
	dummy.costpallet = 0.0;
	dummy.amount = 0.0;

	overall_ic_transports_per_plant.clear();
	overall_ic_transports_per_plant.resize(m_plants.size(), dummy);
	overall_ic_in_transports_per_plant.clear();
	overall_ic_in_transports_per_plant.resize(m_plants.size(), dummy);
	overall_ic_out_transports_per_plant.clear();
	overall_ic_out_transports_per_plant.resize(m_plants.size(), dummy);

	for (i = 0; i < m_plants.size(); ++i)
	{
		for (k = 0; k < m_periods.size(); ++k)
		{
			overall_ic_transports_per_plant.at(i).costsum += ic_transports_per_plant.at(i).at(k).costsum;
			overall_ic_transports_per_plant.at(i).amount += ic_transports_per_plant.at(i).at(k).amount;
			overall_ic_in_transports_per_plant.at(i).costsum += ic_in_transports_per_plant.at(i).at(k).costsum;
			overall_ic_in_transports_per_plant.at(i).amount += ic_in_transports_per_plant.at(i).at(k).amount;
			overall_ic_out_transports_per_plant.at(i).costsum += ic_out_transports_per_plant.at(i).at(k).costsum;
			overall_ic_out_transports_per_plant.at(i).amount += ic_out_transports_per_plant.at(i).at(k).amount;
		}
	}

	for (i = 0; i < m_plants.size(); ++i)
	{
		if (overall_ic_transports_per_plant.at(i).amount > 0)
		{
			overall_ic_transports_per_plant.at(i).costpallet = overall_ic_transports_per_plant.at(i).costsum / overall_ic_transports_per_plant.at(i).amount;
		}
		else
		{
			overall_ic_transports_per_plant.at(i).costpallet = 0;
		}
		if (overall_ic_in_transports_per_plant.at(i).amount > 0)
		{
			overall_ic_in_transports_per_plant.at(i).costpallet = overall_ic_in_transports_per_plant.at(i).costsum / overall_ic_in_transports_per_plant.at(i).amount;
		}
		else
		{
			overall_ic_transports_per_plant.at(i).costpallet = 0;
		}
		if (overall_ic_out_transports_per_plant.at(i).amount > 0)
		{
			overall_ic_out_transports_per_plant.at(i).costpallet = overall_ic_out_transports_per_plant.at(i).costsum / overall_ic_out_transports_per_plant.at(i).amount;
		}
		else
		{
			overall_ic_out_transports_per_plant.at(i).costpallet = 0;
		}
	}
}

void Output_CSV::write_article_names()
{
	ofstream output(m_filename_names.c_str());
	if (output.fail())
	{
		cout << "Kann Datei " << m_filename_names << " nicht anlegen!" << endl;
		exit(1);
	}
	int i;
	for (i = 0; i < m_products.size(); ++i)
	{
		output << "\"" << m_products.at(i) << "\";" << m_product_names.at(i) << endl;
	}
	output.close();
}
//map<int,double>::iterator it2Test;
//map<int,double>::iterator itEndTest;
void Output_CSV::prepare_model_data()
{
	int i, j, k;

	pallet_production_model.clear();
	pallet_production_model.resize(m_nProducts, vector<map<int, double> >(m_periods.size()));


	for (i = 0; i < m_solution_production.size(); ++i)
	{
		pallet_production_model.at(m_solution_production.at(i).product)
			.at(m_solution_production.at(i).period - 1)
			.insert(
				pair<int, double>(
					m_solution_production.at(i).machine,
					m_solution_production.at(i).runtime*m_processes.at(m_solution_production.at(i).process).tonspershift * m_nPallets_per_ton.at(m_solution_production.at(i).product)
					)
			);
	}

	customer_group_pallet_production_model.clear();
	customer_group_pallet_production_model.resize(m_custFixations.numberACFixations(), vector<map<int, double>>(m_periods.size())); //(m_customer_fixations .size (),vector<map<int,double>>(m_periods.size()));

	for (i = 0; i<m_customer_group_solution_production.size(); ++i)
	{
		customer_group_pallet_production_model.at(m_customer_group_solution_production.at(i).cGroup)
			.at(m_customer_group_solution_production.at(i).period - 1)
			.insert(
				pair<int, double>(
					m_customer_group_solution_production.at(i).machine,
					m_customer_group_solution_production.at(i).runtime*m_processes.at(m_customer_group_solution_production.at(i).process).tonspershift*m_nPallets_per_ton.at(m_customer_group_solution_production.at(i).product)
					)
			);
	}

	transport_per_customer_model.clear();
	transport_per_customer_model.resize(m_nProducts, vector<map<int, double> >(m_periods.size()));

	customer_group_transport_per_customer_model.clear();
	customer_group_transport_per_customer_model.resize(m_custFixations.numberACFixations(), vector<map<int, double>>(m_periods.size()));

	detailled_transports_customer_model.clear();
	detailled_transports_customer_model.resize(m_nProducts, vector<vector<transport_information2> >(m_periods.size()));

	customer_group_detailled_transports_customer_model.clear();
	customer_group_detailled_transports_customer_model.resize(m_custFixations.numberACFixations(), vector<vector<transport_information2>>(m_periods.size())); //(m_custFixations.numberACFixations(),vector<vector<vector<transport_information2 >>>((const int)m_nProducts));

	map<int, double>::iterator it;

	transport_information2 dummy;

	for (i = 0; i < m_solution_single_transport_customer.size(); ++i)
	{
		dummy.amount = m_solution_single_transport_customer.at(i).amount;
		dummy.cost = m_solution_single_transport_customer.at(i).amount * m_transport_customer.at(transport_indices_customer.at(m_solution_single_transport_customer.at(i).plant).at(m_solution_single_transport_customer.at(i).customer)).cost / 34.0;
		dummy.start_index = m_solution_single_transport_customer.at(i).plant;
		dummy.end_index = m_solution_single_transport_customer.at(i).customer;

		if (!m_custFixations.isCustomerLocationInFixationsForArticle(dummy.end_index, m_solution_single_transport_customer.at(i).product))
		{
			detailled_transports_customer_model.at(m_solution_single_transport_customer.at(i).product).at(m_solution_single_transport_customer.at(i).period - 1).push_back(dummy);

			it = transport_per_customer_model.at(m_solution_single_transport_customer.at(i).product).at(m_solution_single_transport_customer.at(i).period - 1).find(m_solution_single_transport_customer.at(i).customer);

			if (it == transport_per_customer_model.at(m_solution_single_transport_customer.at(i).product).at(m_solution_single_transport_customer.at(i).period - 1).end())
			{
				transport_per_customer_model.at(m_solution_single_transport_customer.at(i).product).at(m_solution_single_transport_customer.at(i).period - 1).insert(pair<int, double>(m_solution_single_transport_customer.at(i).customer, m_solution_single_transport_customer.at(i).amount));
			}
			else
			{
				(*it).second += m_solution_single_transport_customer.at(i).amount;
			}
		}
		else
		{
			for (j = 0; j<m_custFixations.numberACFixations(); j++)
			{
				if (m_custFixations.get(j).article == m_solution_single_transport_customer.at(i).product)
				{
					for (k = 0; k<m_custFixations.get(j).customerLocations.size(); k++)
					{
						if (m_custFixations.get(j).customerLocations.at(k) == m_solution_single_transport_customer.at(i).customer)
						{
							customer_group_detailled_transports_customer_model.at(j).at(m_solution_single_transport_customer.at(i).period - 1).push_back(dummy);
							//it=customer_group_transport_per_customer_model.at(
							it = customer_group_transport_per_customer_model.at(j).at(m_solution_single_transport_customer.at(i).period - 1).find(m_solution_single_transport_customer.at(i).customer);

							if (it == customer_group_transport_per_customer_model.at(j).at(m_solution_single_transport_customer.at(i).period - 1).end())
							{
								customer_group_transport_per_customer_model
									.at(j)
									.at(m_solution_single_transport_customer
										.at(i).period - 1)
									.insert(pair<int, double>(m_solution_single_transport_customer.at(i).customer, m_solution_single_transport_customer.at(i).amount));
							}
							else
							{
								(*it).second += m_solution_single_transport_customer.at(i).amount;
							}
							break;
						}
					}
				}
			}


			/*it = transport_per_customer_model.at(m_solution_single_transport_customer.at(i).product).at(m_solution_single_transport_customer.at(i).period-1).find(m_solution_single_transport_customer.at(i).customer);

			if (it == transport_per_customer_model.at(m_solution_single_transport_customer.at(i).product).at(m_solution_single_transport_customer.at(i).period-1).end())
			{
			transport_per_customer_model.at(m_solution_single_transport_customer.at(i).product).at(m_solution_single_transport_customer.at(i).period-1).insert(pair<int,double>(m_solution_single_transport_customer.at(i).customer,m_solution_single_transport_customer.at(i).amount));
			}
			else
			{
			(*it).second += m_solution_single_transport_customer.at(i).amount;*/
			//}
		}
	}

	detailled_transports_plants_model.clear();
	detailled_transports_plants_model.resize(m_nProducts, vector<vector<transport_information2> >(m_periods.size()));

	for (i = 0; i < m_solution_single_transport_plant.size(); ++i)
	{
		dummy.amount = m_solution_single_transport_plant.at(i).amount;
		dummy.cost = m_solution_single_transport_plant.at(i).amount * m_transport_plant.at(transport_indices_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2)).cost / 34.0;
		dummy.start_index = m_solution_single_transport_plant.at(i).plant1;
		dummy.end_index = m_solution_single_transport_plant.at(i).plant2;

		detailled_transports_plants_model.at(m_solution_single_transport_plant.at(i).product).at(m_solution_single_transport_plant.at(i).period - 1).push_back(dummy);
	}


	customer_group_detailled_transports_plants_model.clear();
	customer_group_detailled_transports_plants_model.resize(m_custFixations.numberACFixations(), vector<vector<transport_information2 >>(m_periods.size()));//(m_customer_fixations.size (),vector<vector<transport_information2>>(m_periods.size()));

	for (i = 0; i<m_customer_group_solution_single_transport_plant.size(); i++)
	{
		dummy.amount = m_customer_group_solution_single_transport_plant.at(i).amount;
		dummy.cost = m_customer_group_solution_single_transport_plant.at(i).amount *m_transport_plant.at(transport_indices_plant.at(m_customer_group_solution_single_transport_plant.at(i).plant1).at(m_customer_group_solution_single_transport_plant.at(i).plant2)).cost / 34.0;
		dummy.start_index = m_customer_group_solution_single_transport_plant.at(i).plant1;
		dummy.end_index = m_customer_group_solution_single_transport_plant.at(i).plant2;

		customer_group_detailled_transports_plants_model.at(m_customer_group_solution_single_transport_plant.at(i).cGroup).at(m_customer_group_solution_single_transport_plant.at(i).period - 1).push_back(dummy);
	}


	inventory_model.clear();
	inventory_model.resize(m_nProducts, vector<vector<pair<double, double> > >(m_plants.size(), vector<pair<double, double> >(m_periods.size())));

	for (i = 0; i < m_solution_inventory.size(); ++i)
	{
		inventory_model.at(m_solution_inventory.at(i).product).at(m_solution_inventory.at(i).plant).at(m_solution_inventory.at(i).period - 1).first = m_solution_inventory.at(i).amount_start;
		inventory_model.at(m_solution_inventory.at(i).product).at(m_solution_inventory.at(i).plant).at(m_solution_inventory.at(i).period - 1).second = m_solution_inventory.at(i).amount_end;
	}

	customer_group_inventory_model.clear();
	customer_group_inventory_model.resize(m_custFixations.numberACFixations(), vector<vector<pair<double, double>>>(m_plants.size(), vector<pair<double, double>>(m_periods.size())));//(m_customer_fixations.size(),vector<vector<pair<double,double>>>(m_plants .size(),vector<pair<double,double>>(m_periods.size())));

	for (i = 0; i<m_customer_group_solution_inventory.size(); i++)
	{
		customer_group_inventory_model.at(m_customer_group_solution_inventory.at(i).cGroup).at(m_customer_group_solution_inventory.at(i).plant).at(m_customer_group_solution_inventory.at(i).period - 1).first = m_customer_group_solution_inventory.at(i).amount_start;
		customer_group_inventory_model.at(m_customer_group_solution_inventory.at(i).cGroup).at(m_customer_group_solution_inventory.at(i).plant).at(m_customer_group_solution_inventory.at(i).period - 1).second = m_customer_group_solution_inventory.at(i).amount_end;
	}


	//for (i=0;i<m_custFixations .numberACFixations ();i++)//(i=0;i<m_customer_fixations .size();i++)
	//{	
	//	#ifdef DEBUG
	//	cout << m_products.at(m_custFixations.get (i).article) << " " << i << endl;
	//	#endif

	//	DCBT *dcbt1=new DCBT(m_plants .size(),m_periods .size());

	//	dcbt1->map_production_information (m_nMachines,customer_group_pallet_production_model.at(i),machine_locations );
	//	dcbt1->map_customer_information (m_customers.size (),customer_group_transport_per_customer_model.at(i),customer_group_detailled_transports_customer_model .at(i)); //(m_customer_fixations.at(i).customerLocations .size (),

	//	dcbt1->map_ic_information(customer_group_detailled_transports_plants_model.at (i));

	//	dcbt1->map_inventory_information (customer_group_inventory_model .at (i));

	//	CBTModel *cbtModel=new CBTModel(m_plants.size(),
	//									m_periods.size (),
	//									dcbt1->new_nMachines, 
	//									dcbt1->new_nCustomers,
	//									dcbt1->start_inventory ,
	//									dcbt1->machine_production,
	//									dcbt1->ic_transports,
	//									dcbt1->customer_transports,
	//									dcbt1->new_machine_location
	//									);

	//	cbtModel->init_vars ();
	//	cbtModel->init_constraints ();

	//	vector<vector<vector<vector<vector<double> > > > > solution;

	//	cbtModel->solve (solution);
	//	dcbt1->map_solution (solution,m_nMachines);
	//	customer_group_backtrackings.push_back (dcbt1->backtracking);
	//	
	//	delete cbtModel;
	//	delete dcbt1;
	//}

	//std::_Container_base12 *cB1,*cB2;
#ifdef DETAILED_ASSIGNMENT
	string modelFileName;
	for (i = 0; i <m_nProducts; ++i)
	{
#ifdef DEBUG
		cout << m_products.at(i) << " " << i << endl;
#endif
		if (i == 88)
		{
			i += 0;
		}
		if (m_products.at(i) == "029731")
			i += 0;
		DCBT* dcbt1 = new DCBT(m_plants.size(), m_periods.size());

		dcbt1->map_production_information(m_nMachines, pallet_production_model.at(i), machine_locations);

		dcbt1->map_customer_information(m_customers.size(), transport_per_customer_model.at(i), detailled_transports_customer_model.at(i));

		dcbt1->map_ic_information(detailled_transports_plants_model.at(i));

		dcbt1->map_inventory_information(inventory_model.at(i));

		CBTModel* cbtmodel1 = new CBTModel(m_plants.size(),
			m_periods.size(),
			dcbt1->new_nMachines,
			dcbt1->new_nCustomers,
			dcbt1->start_inventory,
			dcbt1->machine_production,
			dcbt1->ic_transports,
			dcbt1->customer_transports,
			dcbt1->new_machine_location,
			m_expansion_stage
		);

		cbtmodel1->init_vars();
		cbtmodel1->init_constraints();
		vector<vector<vector<vector<vector<double> > > > > solution1;

		modelFileName = "outputModel" + to_string(i) + ".mod";
		cbtmodel1->solve(solution1, modelFileName);
		dcbt1->map_solution(solution1, m_nMachines);
		backtrackings.push_back(dcbt1->backtracking);

		/*for (j=0;j<m_solution_production .size ();j++)
		{
		if (m_solution_production.at(j).product ==i)
		{
		it2Test = dcbt1->backtracking.at(m_solution_production.at(j).machine).at(m_solution_production.at (j).period-1).begin();
		itEndTest=dcbt1->backtracking.at(m_solution_production.at(j).machine).at(m_solution_production.at (j).period-1).end();
		cB1=(std::_Container_base12 *)itEndTest._Getcont();
		cB2=(std::_Container_base12 *)it2Test._Getcont();

		if (i==7)
		{
		cB1=(std::_Container_base12 *)backtrackings.at(m_solution_production.at(6).product).at(m_solution_production.at(6).machine).at(m_solution_production.at (6).period-1).begin()._Getcont();
		cB2=(std::_Container_base12 *)backtrackings.at(m_solution_production.at(6).product).at(m_solution_production.at(6).machine).at(m_solution_production.at (6).period-1).end()._Getcont();
		}
		}
		}


		if (i==7)
		{

		cB1=(std::_Container_base12 *)backtrackings.at(m_solution_production.at(6).product).at(m_solution_production.at(6).machine).at(m_solution_production.at (6).period-1).begin()._Getcont();
		cB2=(std::_Container_base12 *)backtrackings.at(m_solution_production.at(6).product).at(m_solution_production.at(6).machine).at(m_solution_production.at (6).period-1).end()._Getcont();

		}*/
		delete cbtmodel1;
		delete dcbt1;
	}


	//cB1=(std::_Container_base12 *)backtrackings.at(m_solution_production.at(6).product).at(m_solution_production.at(6).machine).at(m_solution_production.at (6).period-1).begin()._Getcont();
	//cB2=(std::_Container_base12 *)backtrackings.at(m_solution_production.at(6).product).at(m_solution_production.at(6).machine).at(m_solution_production.at (6).period-1).end()._Getcont();
	for (i = 0; i<m_custFixations.numberACFixations(); i++)//(i=0;i<m_customer_fixations .size();i++)
	{
#ifdef DEBUG
		cout << m_products.at(m_custFixations.get(i).article) << " " << i << endl;
#endif

		DCBT *dcbt1 = new DCBT(m_plants.size(), m_periods.size());

		dcbt1->map_production_information(m_nMachines, customer_group_pallet_production_model.at(i), machine_locations);
		dcbt1->map_customer_information(m_customers.size(), customer_group_transport_per_customer_model.at(i), customer_group_detailled_transports_customer_model.at(i)); //(m_customer_fixations.at(i).customerLocations .size (),

		dcbt1->map_ic_information(customer_group_detailled_transports_plants_model.at(i));

		dcbt1->map_inventory_information(customer_group_inventory_model.at(i));

		CBTModel *cbtModel = new CBTModel(m_plants.size(),
			m_periods.size(),
			dcbt1->new_nMachines,
			dcbt1->new_nCustomers,
			dcbt1->start_inventory,
			dcbt1->machine_production,
			dcbt1->ic_transports,
			dcbt1->customer_transports,
			dcbt1->new_machine_location,
			m_expansion_stage
		);

		cbtModel->init_vars();
		cbtModel->init_constraints();

		vector<vector<vector<vector<vector<double> > > > > solution;
		modelFileName = "outputModelCG" + to_string(i) + ".mod";
		cbtModel->solve(solution, modelFileName);

		dcbt1->map_solution(solution, m_nMachines);
		customer_group_backtrackings.push_back(dcbt1->backtracking);

		delete cbtModel;
		delete dcbt1;

	}

	//cB1=(std::_Container_base12 *)backtrackings.at(m_solution_production.at(6).product).at(m_solution_production.at(6).machine).at(m_solution_production.at (6).period-1).begin()._Getcont();
	//cB2=(std::_Container_base12 *)backtrackings.at(m_solution_production.at(6).product).at(m_solution_production.at(6).machine).at(m_solution_production.at (6).period-1).end()._Getcont();
#endif
}


//void Output::output_wepa(const bool & german)
//{
//	prepare_model_data();
//	int i, j, k,l;
//	vector<vector<double>> cumProduction;
//	vector<vector<double>> cumProductionQ;
//	set<int>::const_iterator it;
//	map<int,double>::iterator it2;
//	map<int,double>::iterator itEnd;
//	
//#ifdef DETAILED_ASSIGNMENT
//
//	ofstream output(m_filename.c_str());
//	if (output.fail())
//	{
//		cout << "Kann Datei " << m_filename << " nicht anlegen!" << endl;
//		exit(1);
//	}
//
//	if (german) 
//		output.imbue(locale("german"));
//
//	output.setf(ios::fixed, ios::floatfield);
//	output.precision(2);
//
//	output << "Artikelnummer;Artikelbezeichnung;Kunden;Monat;Werk;Anlage;Menge;Anteil der Gesamtmenge;Anzahl Schichten;Produktionskosten (Summe);Produktionskosten (Palette);Anfangsbestand Werk;Endbestand Werk;Transport zu Kundenstandorten (Paletten);Kundentransportkosten (Summe);Kundentransportkosten (pro Palette);Intercompany-Transporte (Paletten);Intercompany-Kosten (Summe);" << endl;
//	cumProduction.resize (m_products.size(),vector<double>(m_periods .size (),0.0));
//	cumProductionQ.resize (m_products.size(),vector<double>(m_periods .size (),0.0));
//	//std::_Container_base12 *cB1,*cB2;	
//	//cB1=(std::_Container_base12 *)backtrackings.at(m_solution_production.at(6).product).at(m_solution_production.at(6).machine).at(m_solution_production.at (6).period-1).begin()._Getcont();
//	//cB2=(std::_Container_base12 *)backtrackings.at(m_solution_production.at(6).product).at(m_solution_production.at(6).machine).at(m_solution_production.at (6).period-1).end()._Getcont();
//	//for (j = 0; j < m_solution_production.size(); ++j)
//	//{
//	//	cB1=(std::_Container_base12 *)backtrackings.at(m_solution_production.at(6).product).at(m_solution_production.at(6).machine).at(m_solution_production.at (6).period-1).begin()._Getcont();
//	//	cB2=(std::_Container_base12 *)backtrackings.at(m_solution_production.at(6).product).at(m_solution_production.at(6).machine).at(m_solution_production.at (6).period-1).end()._Getcont();
//	//			//if (m_solution_production.at(j).period == k && m_solution_production.at(j).product == i)
//	//			//{
//	//				//output << "\"" << m_products.at(i) << "\"" << ";" << m_product_names.at(i) << ";";
//	//				
//	//				//double amount = 0.0;
//	//	it2 = backtrackings.at(m_solution_production.at (j).product).at(m_solution_production.at(j).machine).at(m_solution_production.at (j).period-1).begin();	
//	//	
//	//	it2 = backtrackings.at(m_solution_production.at (j).product).at(m_solution_production.at(j).machine).at(m_solution_production.at (j).period-1).end();	*/
//	//	itEnd=backtrackings.at(j).at(m_solution_production.at(j).machine).at(m_solution_production.at (j).period-1).end();
//	//	cB1=(std::_Container_base12 *)itEnd._Getcont();
//	//	cB2=(std::_Container_base12 *)it2._Getcont();
//	//}
//	//for (k = 1; k < m_periods.size() + 1; ++k)
//	//{
//		//for (i = 0; i < m_products.size(); ++i)
//		//{
//	for (j = 0; j < m_solution_production.size(); ++j)
//	{
//			//std::_Container_base12 *cB1,*cB2;	
//				//if (m_solution_production.at(j).period == k && m_solution_production.at(j).product == i)
//				//{
//					//output << "\"" << m_products.at(i) << "\"" << ";" << m_product_names.at(i) << ";";
//					
//					//double amount = 0.0;
//		//it2 = backtrackings.at(m_solution_production.at (j).product).at(m_solution_production.at(j).machine).at(m_solution_production.at (j).period-1).begin();	
//		//
//		//it2 = backtrackings.at(m_solution_production.at (j).product).at(m_solution_production.at(j).machine).at(m_solution_production.at (j).period-1).end();	*/
//		//itEnd=backtrackings.at(j).at(m_solution_production.at(j).machine).at(m_solution_production.at (j).period-1).end();
//		//cB1=(std::_Container_base12 *)itEnd._Getcont();
//		//cB2=(std::_Container_base12 *)it2._Getcont();
//		//if (it2 != backtrackings.at(j).at(m_solution_production.at(j).machine).at(m_solution_production.at (j).period-1).end())
//		//	it2++;		
//		for (it2 = backtrackings.at(m_solution_production.at (j).product).at(m_solution_production.at(j).machine).at(m_solution_production.at (j).period-1).begin(); it2 != backtrackings.at(m_solution_production.at (j).product).at(m_solution_production.at(j).machine).at(m_solution_production.at (j).period-1).end();it2++)// ++it2)
//		{				
//			if((*it2).second > 0)
//			{
//							///*cout << (*it2).first << endl;
//
//							//cout << m_solution_production.at(j).runtime*m_processes.at(m_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(j).product) << endl;
//
//							//cout << (*it2).second << endl;
//
//							//char op;
//							//cin >> op;*/
//				if (m_products.at(m_solution_production.at (j).product)=="022283")
//				{
//					cout<<"Produkt:"
//						<<m_products.at(m_solution_production.at (j).product)
//						<<"Periode:"
//						<<m_solution_production.at (j).period
//						<<"Kunde:"<<(*it2).first
//						<<"Menge:"<<(*it2).second
//						<<"Produzierte Gesamtmenge:"<<m_solution_production.at(j).runtime*m_processes.at(m_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(j).product)
//						<<endl;
//				
//				}
//				cumProduction .at(m_solution_production.at (j).product).at(m_solution_production.at (j).period-1) += 
//					(*it2).second/
//					(
//						m_solution_production.at(j).runtime*m_processes.at(m_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(j).product)
//					);
//
//				cumProductionQ.at(m_solution_production.at (j).product).at(m_solution_production.at (j).period-1)+=(*it2).second;
//							//output << m_customers.at((*it2).first) << "(" << (*it2).second/(m_solution_production.at(j).runtime*m_processes.at(m_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(j).product)) << ") ";
//			}		
//		}
//	}
//
//	for (j=0;j<m_customer_group_solution_production .size();j++)
//	{	
//		//m_customer_group_solution_production.at (j).product
//		for (it2 = customer_group_backtrackings .at(m_customer_group_solution_production.at(j).cGroup).at(m_customer_group_solution_production.at(j).machine).at(m_customer_group_solution_production.at (j).period-1).begin(); it2 != customer_group_backtrackings.at(m_customer_group_solution_production.at(j).cGroup).at(m_customer_group_solution_production.at(j).machine).at(m_customer_group_solution_production.at (j).period-1).end(); ++it2)
//		{
//			if((*it2).second > 0)
//			{
//				cumProduction .at(m_customer_group_solution_production.at(j).product).at(m_customer_group_solution_production.at (j).period-1) += (*it2).second/
//					(
//						m_customer_group_solution_production.at(j).runtime*
//						m_processes.at(m_customer_group_solution_production.at(j).process).tonspershift*
//						m_nPallets_per_ton.at(m_customer_group_solution_production.at(j).product)
//						);
//
//				cumProductionQ.at(m_customer_group_solution_production.at(j).product).at(m_customer_group_solution_production.at (j).period-1) += (*it2).second;
//			}
//		}
//	}
//			//}
//		//}
//	//}
//	for (k = 1; k < m_periods.size() + 1; ++k)
//	{
//		for (i = 0; i < m_products.size(); ++i)
//		{
//			//cout<<"Periode:"<<k<<" Produkt:"<<i<<endl;
//			if (cumProduction .at(i).at(k-1)>0)//((m_solution_production.at(j).period == k && m_solution_production.at(j).product == i))//)
//			{	
//				double factor = 1.0/cumProduction .at(i).at(k-1);	
//
//				cout<<"Produkt:"<<m_products.at(i)<<" Periode:"<<m_periods.at(k-1)<<" Faktor:"<<factor<<" CumProduction:"<<cumProduction.at(i).at(k-1)<<" CumProductionQ:"<<cumProductionQ.at(i).at(k-1)<< endl;
//				//cin.get();
//				for (j = 0; j < m_solution_production.size(); ++j)
//				{
//					if (m_solution_production .at(j).product ==i && m_solution_production .at(j).period ==k)
//					{	
//						if (m_products.at(i)=="029731")
//							cout<<"Produktion fόr Produkt "
//								<<m_products.at(i)
//								<<" in Periode "
//								<<k
//								<<" auf Maschine "
//								<<m_machines.at(  m_solution_production .at(j).machine)
//								<<" mit "
//								<<backtrackings.at(i).at(m_solution_production.at(j).machine).at(k-1).size()
//								<<" Eintrδgen " 
//								<<" und Gesamtproduktion "<<cumProduction .at(i).at(k-1)
//								<<endl;
//
//						output << "\"" << m_products.at(i) << "\"" << ";" << m_product_names.at(i) << ";";
//						//double factor = 1.0/cumProduction .at(m_solution_production.at (j).product).at(m_solution_production.at (j).period);
//						for (it2 = backtrackings.at(i).at(m_solution_production.at(j).machine).at(k-1).begin(); it2 != backtrackings.at(i).at(m_solution_production.at(j).machine).at(k-1).end(); ++it2)
//						{	
//							if (m_products.at(i)=="029731")
//								 cout<<"Second:"<<(*it2).second<<"Customer mit Index "<<(*it2).first<<" und Name:"<<m_customers.at((*it2).first)<<endl;
//							if((*it2).second > 0)
//							{	
//								if (m_products.at(i)=="029731")
//								 cout<<"Customer mit Index "<<(*it2).first<<" und Name:"<<m_customers.at((*it2).first)<<endl;
//								if (cumProduction .at(m_solution_production.at (j).product).at(m_solution_production.at (j).period-1) > 1.0)
//								{
//									//cout<<m_customers.at((*it2).first) << "(" << (*it2).second/(m_solution_production.at(j).runtime*m_processes.at(m_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(j).product))*factor << ") ";
//									output << m_customers.at((*it2).first) << "(" << (*it2).second/(m_solution_production.at(j).runtime*m_processes.at(m_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(j).product))*factor << ") ";
//								}
//								else
//								{
//									//cout<< m_customers.at((*it2).first) << "(" << (*it2).second/(m_solution_production.at(j).runtime*m_processes.at(m_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(j).product)) << ") ";
//									output << m_customers.at((*it2).first) << "(" << (*it2).second/(m_solution_production.at(j).runtime*m_processes.at(m_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(j).product)) << ") ";
//								}
//							}
//						}
//
//						output << ";" << m_periods.at(k - 1) << ";";
//						output << m_plants.at(machine_locations.at(m_solution_production.at(j).machine)) << ";";
//						output << m_machines.at(m_solution_production.at(j).machine) << ";";
//						output << get_production(j) << ";";
//						output << get_relation_production(j) << ";";
//						output << m_solution_production.at(j).runtime << ";";
//						output << get_production_cost(j) << ";";
//						output << get_production_cost_pallet(j) << ";";
//						output << get_inventory(j).first << ";";
//						output << get_inventory(j).second << ";";
//
//						transport_information dummy = get_customer_transports(j);
//
//						output << dummy.amount << ";";
//						output << dummy.costsum << ";";
//						output << dummy.costpallet << ";";
//
//						pair<double,double> dummy2 = get_ic_transports(j);
//					
//						output << dummy2.first << ";";
//						output << dummy2.second << "" << endl;
//					}
//				}
//
//				for (j=0;j<m_customer_group_solution_production .size();j++)
//				{
//					if (m_customer_group_solution_production.at(j).product ==i && m_customer_group_solution_production.at(j).period ==k)
//					{
//						//cout<<"Periode:"<<k<<" Produkt:"<<i<<"Kundengruppe"<<j<<endl;
//						output << "\"" << m_products.at(i) << "\"" << ";" << m_product_names.at(i) << ";";
//						if (k==3 && i==1149 && j==2128)
//							k+=0;
//						for (it2 = customer_group_backtrackings.at(m_customer_group_solution_production.at(j).cGroup).at(m_customer_group_solution_production.at(j).machine).at(k-1).begin(); it2 != customer_group_backtrackings.at(m_customer_group_solution_production.at(j).cGroup).at(m_customer_group_solution_production.at(j).machine).at(k-1).end(); ++it2)
//						{	
//							if((*it2).second > 0)
//							{	
//								if (cumProduction .at(m_customer_group_solution_production.at (j).product).at(m_customer_group_solution_production.at (j).period-1) > 1.0)
//								{
//									output << m_customers.at((*it2).first) << "(" << (*it2).second/(m_customer_group_solution_production.at(j).runtime*m_processes.at(m_customer_group_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_customer_group_solution_production.at(j).product))*factor << ") ";
//								}
//								else
//								{
//									double v;
//									v=(*it2).second/(m_customer_group_solution_production.at(j).runtime*m_processes.at(m_customer_group_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_customer_group_solution_production.at(j).product));
//									output << m_customers.at((*it2).first) << "(" << v << ") ";
//								}
//							}
//						}
//
//						output << ";" << m_periods.at(k - 1) << ";";
//						output << m_plants.at(machine_locations.at(m_customer_group_solution_production.at(j).machine)) << ";";
//						output << m_machines.at(m_customer_group_solution_production.at(j).machine) << ";";
//						output << get_customer_group_production(j) << ";";
//						output << get_customer_group_relation_production(j) << ";";
//						output << m_customer_group_solution_production.at(j).runtime << ";";
//						output << get_customer_group_production_cost(j) << ";";
//						output << get_customer_group_production_cost_pallet(j) << ";";
//						output << get_customer_group_inventory(j).first << ";";
//						output << get_customer_group_inventory(j).second << ";";
//
//						transport_information dummy = get_customer_group_customer_transports(j);
//
//						output << dummy.amount << ";";
//						output << dummy.costsum << ";";
//						output << dummy.costpallet << ";";
//
//						pair<double,double> dummy2 = get_customer_group_ic_transports (j);//get_ic_transports(j);
//					
//						output << dummy2.first << ";";
//						output << dummy2.second << "" << endl;
//					}
//
//				}
//			}
//		}
//	}
//
//	//
//
//	//for (k = 1; k < m_periods.size() + 1; ++k)
//	//{
//	//	for (i = 0; i < m_products.size(); ++i)
//	//	{
//	//		for (j = 0; j < m_solution_production.size(); ++j)
//	//		{
//	//			
//	//			if (m_solution_production.at(j).period == k && m_solution_production.at(j).product == i)
//	//			{
//	//				output << "\"" << m_products.at(i) << "\"" << ";" << m_product_names.at(i) << ";";
//	//				
//	//				double amount = 0.0;
//	//		
//	//				for (it2 = backtrackings.at(i).at(m_solution_production.at(j).machine).at(k-1).begin(); it2 != backtrackings.at(i).at(m_solution_production.at(j).machine).at(k-1).end(); ++it2)
//	//				{
//	//					
//	//					if((*it2).second > 0)
//	//					{
//	//						/*cout << (*it2).first << endl;
//
//	//						cout << m_solution_production.at(j).runtime*m_processes.at(m_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(j).product) << endl;
//
//	//						cout << (*it2).second << endl;
//
//	//						char op;
//	//						cin >> op;*/
//	//						amount += (*it2).second/(m_solution_production.at(j).runtime*m_processes.at(m_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(j).product));
//	//						//output << m_customers.at((*it2).first) << "(" << (*it2).second/(m_solution_production.at(j).runtime*m_processes.at(m_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(j).product)) << ") ";
//	//					}
//	//				
//	//				}
//
//	//				
//
//	//				double factor = 1.0/amount;
//	//				for (it2 = backtrackings.at(i).at(m_solution_production.at(j).machine).at(k-1).begin(); it2 != backtrackings.at(i).at(m_solution_production.at(j).machine).at(k-1).end(); ++it2)
//	//				{
//	//					
//	//					if((*it2).second > 0)
//	//					{
//	//						/*cout << (*it2).first << endl;
//
//	//						cout << m_solution_production.at(j).runtime*m_processes.at(m_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(j).product) << endl;
//
//	//						cout << (*it2).second << endl;
//
//	//						char op;
//	//						cin >> op;*/
//	//						if (amount > 1.0)
//	//						{
//	//						output << m_customers.at((*it2).first) << "(" << (*it2).second/(m_solution_production.at(j).runtime*m_processes.at(m_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(j).product))*factor << ") ";
//	//						}
//	//						else
//	//						{
//	//							output << m_customers.at((*it2).first) << "(" << (*it2).second/(m_solution_production.at(j).runtime*m_processes.at(m_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(j).product)) << ") ";
//	//						}
//	//					}
//	//				
//	//				}
//	//				
//	//				cumProduction.at (i).at(k-1)+=amount;
//	//				
//	//				output << ";" << m_periods.at(k - 1) << ";";
//	//				output << m_plants.at(machine_locations.at(m_solution_production.at(j).machine)) << ";";
//	//				output << m_machines.at(m_solution_production.at(j).machine) << ";";
//	//				output << get_production(j) << ";";
//	//				output << get_relation_production(j) << ";";
//	//				output << m_solution_production.at(j).runtime << ";";
//	//				output << get_production_cost(j) << ";";
//	//				output << get_production_cost_pallet(j) << ";";
//	//				output << get_inventory(j).first << ";";
//	//				output << get_inventory(j).second << ";";
//
//	//				transport_information dummy = get_customer_transports(j);
//
//	//				output << dummy.amount << ";";
//	//				output << dummy.costsum << ";";
//	//				output << dummy.costpallet << ";";
//
//	//				pair<double,double> dummy2 = get_ic_transports(j);
//	//				
//	//				output << dummy2.first << ";";
//	//				output << dummy2.second << "" << endl;
//	//			}
//	//		}
//
//	//	}
//	//}
//	output.close();
//#endif
//}