/*! \file
* class for time measurement
*/
#ifndef TM_H
#define TM_H

#include <vector>
#include <ctime>

using namespace std;

/*! 
* \brief
* class for time measurement
*/
class TM
{
public:
	/*!
	* \brief constructor
	*/
	TM();
	/*!
	* \brief destructor
	*/
	~TM();

	/*!
	* add an event with name and duration since initialization or last event
	* \param event_name event name
	*/
	void add_event(const string & event_name);
	/*!
	* get the name of an event
	* \param index index of the event
	*/
	string get_eventname(const int & index);
	/*!
	* get the duration of an event
	* \param index index of the event
	*/
	double get_eventduration(const int & index);
	/*!
	* \brief get the duration from initialization to end
	*/
	double get_overallduration();
	/*!
	* \brief initialization
	*/
	void init();
	/*!
	* \brief ending
	*/
	void end();
	/*!
	* \brief get current number of events
	*/
	int get_event_number();

private:
	/*!
	* \brief time of initialization
	*/
	double m_start_time;
	/*!
	* \brief start time of last event
	*/
	double m_last_time;
	/*!
	* \brief ending time
	*/
	double m_end_time;
	/*!
	* \brief overall duration
	*/
	double m_overall_duration;
	/*!
	* \brief list of events with name and duration in seconds
	*/
	vector<pair<string, double> > m_events;
	/*!
	* get the duration in seconds for given start and end time
	* \param start start time
	* \param end end time
	*/
	double get_duration(const double & start, const double & end);
	
};


#endif