#include "timemeasurement.h"

TM::TM() 
{
	init();
}

void TM::init()
{
	m_start_time = clock();
	m_last_time = m_start_time;
}

void TM::add_event(const string & event_name)
{
	double current_time = clock();

	m_events.push_back(pair<string, double>(event_name, get_duration(m_last_time, current_time)));

	m_last_time = current_time;

}

double TM::get_duration(const double & start, const double & end)
{
	return (end - start) / CLOCKS_PER_SEC;
}

string TM::get_eventname(const int & index)
{
	return m_events.at(index).first;
}

double TM::get_eventduration(const int & index)
{
	return m_events.at(index).second;
}

double TM::get_overallduration()
{
	return m_overall_duration;
}

int TM::get_event_number()
{
	return m_events.size();
}

void TM::end()
{
	double current_time = clock();
	m_overall_duration = (current_time - m_start_time) / CLOCKS_PER_SEC;
}

TM::~TM()
{

}