#include "cplex_model.h"
#include "customer_backtracking_model.h"

CBTModel::CBTModel(
				   const int & nPlants, 
				   const int & nPeriods, 
				   const int & nMachines, 
				   const int & nCustomers, 
				   const vector<double> & start_inventory, 
				   const vector<vector<double> > & production, 
				   const vector<vector<vector<double> > > & ictrans, 
				   const vector<vector<vector<double> > > & ctrans, 
				   const vector<int> & machine_location, 
				   const string & expansion_stage
				   ) :
m_nPlants(nPlants),
m_nPeriods(nPeriods),
m_nMachines(nMachines),
m_nCustomers(nCustomers),
m_start_inventory(start_inventory),
m_production(production),
m_ictrans(ictrans),
m_ctrans(ctrans),
m_machine_location(machine_location),
m_expansion_stage(expansion_stage)
{	
	cplex = new CPLEX(false);
	cplex->createprob ();
	var_inventory.resize(nMachines, vector<vector<vector<int> > >(nPeriods+1, vector<vector<int> >(nPlants, vector<int>(nPeriods + 1,-1))));
	var_ictrans.resize(nMachines, vector<vector<vector<vector<int> > > >(nPeriods+1, vector<vector<vector<int> > >(nPlants, vector<vector<int> >(nPlants, vector<int>(nPeriods+1,-1)))));
	var_ctrans.resize(nMachines, vector<vector<vector<vector<int> > > >(nPeriods+1, vector<vector<vector<int> > >(nPlants, vector<vector<int> >(nCustomers, vector<int>(nPeriods+1,-1)))));
	var_start_inventory_plants.resize(nPlants, vector<int>(nPeriods + 1, -1));
	var_transport_plants.resize(nPlants, vector<vector<int> >(nPlants, vector<int>(nPeriods + 1, -1)));
	var_transport_customers.resize(nPlants, vector<vector<int> >(nCustomers, vector<int>(nPeriods + 1, -1)));
	var_dummy_ictrans_plus.resize(nPlants,vector<vector<int> >(nPlants,vector<int>(nPeriods+1,-1)));
	var_dummy_ictrans_minus.resize(nPlants,vector<vector<int> >(nPlants,vector<int>(nPeriods+1,-1)));
	var_dummy_ctrans_plus.resize(nPlants,vector<vector<int> >(nCustomers,vector<int>(nPeriods+1,-1)));
	var_dummy_ctrans_minus.resize(nPlants,vector<vector<int> >(nCustomers,vector<int>(nPeriods+1,-1)));


	producing_plant.resize(nPlants, false);
	int i;
	for (i = 0; i < m_nMachines; ++i)
	{
		producing_plant.at(machine_location.at(i)) = true;
	}

	epsilon = 0.1;

	if (m_expansion_stage=="1")
		isNoInventory=true;
	else
		isNoInventory=false;
}

CBTModel::~CBTModel()
{
	delete cplex;
}

void CBTModel::init_vars()
{
	int m;
	int pk;
	int k;
	int p;
	int c;
	int p2;
	
	//Bestand nicht produzierende Werke
	for (p = 0; p < m_nPlants; ++p)
	{
		for (k = 0; k < m_nPeriods + 1; ++k)
		{
			namebuf.str("");
			namebuf << "VAR_INV_" << p << "_" << k;

			if (k == 0)
			{
				cplex->add_var_continuous(namebuf.str().c_str(), m_start_inventory.at(p), m_start_inventory.at(p), 0.0, var_start_inventory_plants.at(p).at(k));
			}
			else
			{
				cplex->add_var_continuous(namebuf.str().c_str(), 0.0, CPX_INFBOUND, 0.0, var_start_inventory_plants.at(p).at(k));
			}
		}
	}

	//IC-Transporte Werke f�r Startbest�nde
	for (p = 0; p < m_nPlants; ++p)
	{
		for (p2 = 0; p2 < m_nPlants; ++p2)
		{
			if (p != p2)
			{
				for (k = 1; k < m_nPeriods+1; ++k)
				{
					namebuf.str("");
					namebuf << "VAR_IC_" << p << "_" << p2 << "_" << k;

					cplex->add_var_continuous(namebuf.str().c_str(), 0.0, CPX_INFBOUND, 0.0, var_transport_plants.at(p).at(p2).at(k));
				}
			}
		}
	}

	//Kunden-Transporte Werke f�r Startbest�nde
	for (p = 0; p < m_nPlants; ++p)
	{
		for (p2 = 0; p2 < m_nCustomers; ++p2)
		{
			for (k = 1; k < m_nPeriods + 1; ++k)
			{
				namebuf.str("");
				namebuf << "VAR_CUST_" << p << "_" << p2 << "_" << k;

				cplex->add_var_continuous(namebuf.str().c_str(), 0.0, CPX_INFBOUND, 0.0, var_transport_customers.at(p).at(p2).at(k));
			}
		}
	}


	//Best�nde f�r Produktionen
	for (m = 0; m < m_nMachines; ++m)
	{
		//produzierende Periode
		for (pk = 1; pk < m_nPeriods+1; ++pk)
		{
			for (p = 0; p < m_nPlants; ++p)
			{
				for (k = 0; k < m_nPeriods+1; ++k)
				{
					namebuf.str("");
					namebuf << "VAR_INV_" << m << "_" << pk << "_" << p << "_" << k;

					if (k == 0)
					{
						cplex->add_var_continuous(namebuf.str().c_str(), 0.0, 0.0, 0.0, var_inventory.at(m).at(pk).at(p).at(k));
					}
					else
					{
						cplex->add_var_continuous(namebuf.str().c_str(), 0.0, CPX_INFBOUND, 10.0, var_inventory.at(m).at(pk).at(p).at(k));
					}
				}

			}
		}
	}


	//IC-Transporte f�r Produktionen
	for (m = 0; m < m_nMachines; ++m)
	{
			for (p = 0; p < m_nPlants; ++p)
			{
				for (c = 0; c < m_nPlants; ++c)
				{
					if (p != c)
					{
						for (k = 1; k < m_nPeriods+1; ++k)
						{
							for (pk = 1; pk <= k; ++pk)
							{
								namebuf.str("");
								namebuf << "VAR_ICTRANS_" << m << "_" << pk << "_" << p << "_" << c << "_" << k;

								cplex->add_var_continuous(namebuf.str().c_str(), 0.0, CPX_INFBOUND, 0.0, var_ictrans.at(m).at(pk).at(p).at(c).at(k));
							}
						}
					}
				}
			}
		
	}

	//Kunden-Transporte f�r Produktionen
	for (m = 0; m < m_nMachines; ++m)
	{
			for (p = 0; p < m_nPlants; ++p)
			{
				for (c = 0; c < m_nCustomers; ++c)
				{
					for (k = 1; k < m_nPeriods+1; ++k)
					{
						for (pk = 1; pk <= k; ++pk)
						{
							namebuf.str("");
							namebuf << "VAR_CTRANS_" << m << "_" << pk << "_" << p << "_" << c << "_" << k;

							if (isNoInventory && k>pk)
								cplex->add_var_continuous(namebuf.str().c_str(), 0.0, 0.0, 0.0, var_ctrans.at(m).at(pk).at(p).at(c).at(k));
							else
								cplex->add_var_continuous(namebuf.str().c_str(), 0.0, CPX_INFBOUND, 0.0, var_ctrans.at(m).at(pk).at(p).at(c).at(k));
							string v=namebuf.str();
							/*if (v=="VAR_CTRANS_0_1_4_0_1")
								std::cout<<v<<":"<<var_ctrans.at(m).at(pk).at(p).at(c).at(k)<<endl;
							if (v=="VAR_CTRANS_0_1_4_0_3")
								std::cout<<v<<":"<<var_ctrans.at(m).at(pk).at(p).at(c).at(k)<<endl;*/
						}
					}
				}
			}
	}	

	//Hilfsvariablen f�r Ersatz von Gleichheit
	for (p = 0; p < m_nPlants; ++p)
	{
		for (p2 = 0; p2 < m_nPlants; ++p2)
		{
			for (k = 1; k < m_nPeriods+1; ++k)
			{
				if (p != p2)
				{
					namebuf.str("");
					namebuf << "VAR_DUMMY_IC_PL_" << p << "_" << p2 << "_" << k;
					cplex->add_var_continuous(namebuf.str().c_str(), 0.0, CPX_INFBOUND, 100.0, var_dummy_ictrans_plus.at(p).at(p2).at(k));

					namebuf.str("");
					namebuf << "VAR_DUMMY_IC_MI_" << p << "_" << p2 << "_" << k;
					cplex->add_var_continuous(namebuf.str().c_str(), 0.0, CPX_INFBOUND, 100.0, var_dummy_ictrans_minus.at(p).at(p2).at(k));
				}
			}
		}
	}

	for (p = 0; p < m_nPlants; ++p)
	{
		for (p2 = 0; p2 < m_nCustomers; ++p2)
		{
			for (k = 1; k < m_nPeriods+1; ++k)
			{
				namebuf.str("");
				namebuf << "VAR_DUMMY_CT_PL_" << p << "_" << p2 << "_" << k;
				cplex->add_var_continuous(namebuf.str().c_str(), 0.0, CPX_INFBOUND, 100.0, var_dummy_ctrans_plus.at(p).at(p2).at(k));

				namebuf.str("");
				namebuf << "VAR_DUMMY_CT_MI_" << p << "_" << p2 << "_" << k;
				cplex->add_var_continuous(namebuf.str().c_str(), 0.0, CPX_INFBOUND, 100.0, var_dummy_ctrans_minus.at(p).at(p2).at(k));
			}
		}
	}


}

void CBTModel::init_constraints()
{
	int m;
	int pk;
	int k;
	int p;
	int p2;
	int consindex;

	
	//Bestandsaktualisierung Werke
	for (p = 0; p < m_nPlants; ++p)
	{
		for (k = 1; k < m_nPeriods + 1; ++k)
		{
			namebuf.str("");
			namebuf << "CONS_INV_PLANT_" << p << "_" << k;
			cplex->add_cons(namebuf.str().c_str(), 'E', 0.0, consindex);

			cplex->add_coeff_linear(consindex, var_start_inventory_plants.at(p).at(k), 1.0);
			cplex->add_coeff_linear(consindex, var_start_inventory_plants.at(p).at(k - 1), -1.0);

			for (p2 = 0; p2 < m_nPlants; ++p2)
			{
				if (p != p2)
				{
					cplex->add_coeff_linear(consindex, var_transport_plants.at(p).at(p2).at(k), 1.0);
					cplex->add_coeff_linear(consindex, var_transport_plants.at(p2).at(p).at(k), -1.0);
				}
			}
			for (p2 = 0; p2 < m_nCustomers; ++p2)
			{
				cplex->add_coeff_linear(consindex, var_transport_customers.at(p).at(p2).at(k), 1.0);
			}

		}
	}

	
	for (m = 0; m < m_nMachines; ++m)
	{
		for (pk = 1; pk < m_nPeriods + 1; ++pk)
		{
			for (p = 0; p < m_nPlants; ++p)
			{
				for (k=pk;k<m_nPeriods+1;k++)//(k=pk+1;k<m_nPeriods+1;k++)//(k = 1; k < m_nPeriods + 1; ++k)
				{
					namebuf.str("");
					namebuf << "CONS_INV_" << m << "_" << pk << "_" << p << "_" << k;

					if (namebuf.str()=="CONS_INV_0_2_0_1")
						m+=0;
					if (pk != k || m_machine_location.at(m) != p)
					{
						cplex->add_cons(namebuf.str().c_str(), 'E', 0.0, consindex);
					}
					else
					{
						cplex->add_cons(namebuf.str().c_str(), 'E', m_production.at(m).at(k - 1), consindex);
					}

					cplex->add_coeff_linear(consindex, var_inventory.at(m).at(pk).at(p).at(k),1.0);
					cplex->add_coeff_linear(consindex, var_inventory.at(m).at(pk).at(p).at(k - 1), -1.0);
					
					for (p2 = 0; p2 < m_nPlants; ++p2)
					{
						if (p != p2)
						{
							cplex->add_coeff_linear(consindex, var_ictrans.at(m).at(pk).at(p).at(p2).at(k), 1.0);
							cplex->add_coeff_linear(consindex, var_ictrans.at(m).at(pk).at(p2).at(p).at(k), -1.0);
						}
					}
					
					for (p2 = 0; p2 < m_nCustomers; ++p2)
					{
						cplex->add_coeff_linear(consindex, var_ctrans.at(m).at(pk).at(p).at(p2).at(k), 1.0);
					}
					
				}
			}
		}
	}




	


	for (p = 0; p < m_nPlants; ++p)
	{
		for (p2 = 0; p2 < m_nPlants; ++p2)
		{
			if (p != p2)
			{
				for (k = 1; k < m_nPeriods + 1; ++k)
				{
					namebuf.str("");
					namebuf << "CONS_ICTRANS_" << p << "_" << p2 << "_" << k;
					cplex->add_cons(namebuf.str().c_str(), 'E', m_ictrans.at(p).at(p2).at(k - 1), consindex);


					for (m = 0; m < m_nMachines; ++m)
					{
						for (pk = 1; pk <= k; ++pk)
						{
							cplex->add_coeff_linear(consindex, var_ictrans.at(m).at(pk).at(p).at(p2).at(k), 1.0);
						}
					}

					cplex->add_coeff_linear(consindex, var_transport_plants.at(p).at(p2).at(k), 1.0);
					cplex->add_coeff_linear(consindex, var_dummy_ictrans_plus.at(p).at(p2).at(k),1.0);
					cplex->add_coeff_linear(consindex, var_dummy_ictrans_minus.at(p).at(p2).at(k),-1.0);
					
				}
			}
		}
	}



	for (p = 0; p < m_nPlants; ++p)
	{
		for (p2 = 0; p2 < m_nCustomers; ++p2)
		{
			for (k = 1; k < m_nPeriods + 1; ++k)
			{
				namebuf.str("");
				namebuf << "CONS_CTRANS_" << p << "_" << p2 << "_" << k;
				cplex->add_cons(namebuf.str().c_str(), 'E', m_ctrans.at(p).at(p2).at(k - 1), consindex);

				for (m = 0; m < m_nMachines; ++m)
				{
					for (pk = 1; pk <= k; ++pk)
					{
						cplex->add_coeff_linear(consindex, var_ctrans.at(m).at(pk).at(p).at(p2).at(k), 1.0);
					}
				}
				cplex->add_coeff_linear(consindex, var_transport_customers.at(p).at(p2).at(k), 1.0);
				cplex->add_coeff_linear(consindex, var_dummy_ctrans_plus.at(p).at(p2).at(k),1.0);
				cplex->add_coeff_linear(consindex, var_dummy_ctrans_minus.at(p).at(p2).at(k),-1.0);
			}
		}
	}


}


void CBTModel::solve(vector<vector<vector<vector<vector<double> > > > > & solution,string modelFileName)
{
	//cplex->write_problem("debug_cbtm.lp");

	solution.clear();
	solution.resize(m_nMachines, vector<vector<vector<vector<double> > > >(m_nPeriods, vector<vector<vector<double> > >(m_nPlants, vector<vector<double> >(m_nCustomers, vector<double>(m_nPeriods, 0.0)))));

	bool polish = false;
	double gap = 0.0;
	//cplex->write_problem(modelFileName);
	//cplex->solve_mip(gap, polish);
	cplex->solve_lp();
	//std::cout<<"Zielfunktionswert:"<<cplex->get_objval()<<endl;
	int m;
	int pk;
	int k;
	int p;
	int c;

	for (m = 0; m < m_nMachines; ++m)
	{
		for (pk = 1; pk < m_nPeriods+1; ++pk)
		{
			for (p = 0; p < m_nPlants; ++p)
			{
				for (c = 0; c < m_nCustomers; ++c)
				{
					for (k = 1; k < m_nPeriods+1; ++k)
					{
						

						if (var_ctrans.at(m).at(pk).at(p).at(c).at(k) == -1)
						{
							continue;
						}
					
						solution.at(m).at(pk-1).at(p).at(c).at(k-1) = cplex->get_varsol(var_ctrans.at(m).at(pk).at(p).at(c).at(k));
						/*
						if (solution.at(m).at(pk-1).at(p).at(c).at(k-1)>0)
						{
							std::cout<<"m:"<<m<<" pk:"<<pk<<" p:"<<p<<" c:"<<c<<" k:"<<k<<" solution:"<<solution.at(m).at(pk-1).at(p).at(c).at(k-1)<<endl;
						}
						*/
						if (solution.at(m).at(pk-1).at(p).at(c).at(k-1) <= epsilon)
						{
							solution.at(m).at(pk-1).at(p).at(c).at(k-1) = 0;
						}
						/*if (solution.at(m).at(pk).at(p).at(c).at(k) > 0)
						{
							cout << solution.at(m).at(pk).at(p).at(c).at(k) << endl;
							cout << "auf Maschine " << m << " produziert in " << pk << endl;
							cout << "Transport von " << p << " nach " << c << " in " << k << endl;

							char oi;
							cin >> oi;
						}*/
					}
				}
			}
		}
	}
}

