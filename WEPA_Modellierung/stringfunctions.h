#include <string>
#include<sstream>
#include<list>
#ifndef STRINGFUNCTIONS

#define STRINGFUNCTIONS
//using namespace std;
//using namespace System;

std::string *charDelete (std::string s,char c);
std::string *charReplace (std::string s,char oldChar,char newChar);

//char *fromSystemStringToCChar(String ^s);
//
//void fromSystemStringToStlString(String ^s, string & stlStr);
/*
template<class T> T fromString(const std::string& s)
{
     istringstream stream (s);
     T t;
     stream >> t;
     return t;
}

template <typename T> std::string toString (const T &t) {
	    ostringstream os;
	    os << t;
	    return os.str();
}
*/
//template <> string toString<int> (const int &t);

//template <> std::string toString<double> (const double &t);
//string doubleConvert (double v);

void setNumberDigits(int n);
std::string doubleToString(const double t);
std::string xFormatStr(std::string & source,std::list<std::string> & replacements);
#endif