#include "output.h"

Output::Output(const string & filename, const string & filename_monthly, const string & filename_article, const vector<string> & periods, const vector<string> & products, const vector<string> & product_names, const vector<production_result> & solution_production, const vector<string> & plants, const vector<string> & machines, const vector<production> & processes, const vector<double> & nPallets_per_ton, const int & nMachines, const int & nProducts, const vector<inventory_result> & solution_inventory, const vector<single_transport_result_plant> & solution_single_transport_plant, const vector<single_transport_result_customer> & solution_single_transport_customer, const vector<transports> & transport_plant, const vector<transports> & transport_customer, const vector<demand> demands, const vector<string> & customers, const string & filename_graphic, const string & filename_names, const string & filename_graphic_ic) :
m_filename(filename),
m_filename_monthly(filename_monthly),
m_filename_article(filename_article),
m_periods(periods),
m_products(products),
m_product_names(product_names),
m_solution_production(solution_production),
m_plants(plants),
m_machines(machines),
m_processes(processes),
m_nPallets_per_ton(nPallets_per_ton),
m_nMachines(nMachines),
m_nProducts(nProducts),
m_solution_inventory(solution_inventory),
m_solution_single_transport_plant(solution_single_transport_plant),
m_solution_single_transport_customer(solution_single_transport_customer),
m_transport_plant(transport_plant),
m_transport_customer(transport_customer),
m_demands(demands),
m_customers(customers),
m_filename_graphic(filename_graphic),
m_filename_names(filename_names),
m_filename_graphic_ic(filename_graphic_ic)
{
	get_machine_locations();
	get_overall_productions();
	set_transport_indices_plant();
	set_transport_indices_customer();
	get_customers_for_products();
	get_production_plants();
	get_inventories();
	get_inventories();
	get_all_customer_transports();
	get_all_ic_transports();
	get_3month_production();
	get_production_per_plant();
	get_overall_production_per_plant();
	get_customer_transports_per_plant();
	get_overall_customer_transports_per_plant();
	get_ic_transports_per_plant();
	get_overall_ic_transports_per_plant();
	get_ic_transports();
	//output_for_graphic();
	//write_article_names();
}

void Output::output_wepa(const bool & german)
{
	prepare_model_data();

	int i, j, k;
	set<int>::const_iterator it;
	map<int,double>::iterator it2;

	ofstream output(m_filename.c_str());
	if (output.fail())
	{
		cout << "Kann Datei " << m_filename << " nicht anlegen!" << endl;
		exit(1);
	}
	if (german) output.imbue(locale("german"));
	output.setf(ios::fixed, ios::floatfield);
	output.precision(2);

	output << "Artikelnummer;Artikelbezeichnung;Kunden;Monat;Werk;Anlage;Menge;Anteil der Gesamtmenge;Anzahl Schichten;Produktionskosten (Summe);Produktionskosten (Palette);Anfangsbestand Werk;Endbestand Werk;Transport zu Kundenstandorten (Paletten);Kundentransportkosten (Summe);Kundentransportkosten (pro Palette);Intercompany-Transporte (Paletten);Intercompany-Kosten (Summe);" << endl;

	for (k = 1; k < m_periods.size() + 1; ++k)
	{
		for (i = 0; i < m_products.size(); ++i)
		{
			for (j = 0; j < m_solution_production.size(); ++j)
			{
				if (m_solution_production.at(j).period == k && m_solution_production.at(j).product == i)
				{
					output << "\"" << m_products.at(i) << "\"" << ";" << m_product_names.at(i) << ";";
					/*for (it = customers_for_products.at(i).begin(); it != customers_for_products.at(i).end(); ++it)
					{
						output << m_customers.at((*it)) << " ";
					*/
				

					double amount = 0.0;
					for (it2 = backtrackings.at(i).at(m_solution_production.at(j).machine).at(k-1).begin(); it2 != backtrackings.at(i).at(m_solution_production.at(j).machine).at(k-1).end(); ++it2)
					{
						
						if((*it2).second > 0)
						{
							/*cout << (*it2).first << endl;

							cout << m_solution_production.at(j).runtime*m_processes.at(m_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(j).product) << endl;

							cout << (*it2).second << endl;

							char op;
							cin >> op;*/
							amount += (*it2).second/(m_solution_production.at(j).runtime*m_processes.at(m_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(j).product));
							//output << m_customers.at((*it2).first) << "(" << (*it2).second/(m_solution_production.at(j).runtime*m_processes.at(m_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(j).product)) << ") ";
						}
					
					}

					double factor = 1.0/amount;
					for (it2 = backtrackings.at(i).at(m_solution_production.at(j).machine).at(k-1).begin(); it2 != backtrackings.at(i).at(m_solution_production.at(j).machine).at(k-1).end(); ++it2)
					{
						
						if((*it2).second > 0)
						{
							/*cout << (*it2).first << endl;

							cout << m_solution_production.at(j).runtime*m_processes.at(m_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(j).product) << endl;

							cout << (*it2).second << endl;

							char op;
							cin >> op;*/
							if (amount > 1.0)
							{
							output << m_customers.at((*it2).first) << "(" << (*it2).second/(m_solution_production.at(j).runtime*m_processes.at(m_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(j).product))*factor << ") ";
							}
							else
							{
								output << m_customers.at((*it2).first) << "(" << (*it2).second/(m_solution_production.at(j).runtime*m_processes.at(m_solution_production.at(j).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(j).product)) << ") ";
							}
						}
					
					}
					

					
					output << ";" << m_periods.at(k - 1) << ";";
					output << m_plants.at(machine_locations.at(m_solution_production.at(j).machine)) << ";";
					output << m_machines.at(m_solution_production.at(j).machine) << ";";
					output << get_production(j) << ";";
					output << get_relation_production(j) << ";";
					output << m_solution_production.at(j).runtime << ";";
					output << get_production_cost(j) << "�;";//" EUR;";//"�;";
					output << get_production_cost_pallet(j) << "�;";//" EUR;";//"�;";
					output << get_inventory(j).first << ";";
					output << get_inventory(j).second << ";";

					transport_information dummy = get_customer_transports(j);

					output << dummy.amount << ";";
					output << dummy.costsum << "�;";//" EUR;";//"�;";
					output << dummy.costpallet << "�;";//" EUR;";//"�;";

					pair<double,double> dummy2 = get_ic_transports(j);
					
					output << dummy2.first << ";";
					output << dummy2.second << "�" << endl;//" EUR"<<endl;//"�" << endl;
				}
			}

		}
	}
	output.close();
}

void Output::get_machine_locations()
{
	int i;

	machine_locations.clear();
	machine_locations.resize(m_nMachines);

	for (i = 0; i < m_processes.size(); ++i)
	{
		machine_locations.at(m_processes.at(i).machine) = m_processes.at(i).product_plant_index;
	}
}

void Output::get_overall_productions()
{
	int i;

	overall_production.clear();
	overall_production.resize(m_nProducts, 0.0);

	for (i = 0; i < m_solution_production.size(); ++i)
	{
		overall_production.at(m_solution_production.at(i).product) += m_solution_production.at(i).runtime * m_processes.at(m_solution_production.at(i).process).tonspershift * m_nPallets_per_ton.at(m_solution_production.at(i).product);
	}
}

double Output::get_production(const int & solution_production_index)
{
	return m_solution_production.at(solution_production_index).runtime*m_processes.at(m_solution_production.at(solution_production_index).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(solution_production_index).product);
}

double Output::get_relation_production(const int & solution_production_index)
{
	return get_production(solution_production_index) / production_per_article.at(m_solution_production.at(solution_production_index).product).at(m_solution_production.at(solution_production_index).period - 1).pallets;
		//overall_production.at(m_solution_production.at(solution_production_index).product);
}


double Output::get_production_cost(const int & solution_production_index)
{
	return m_solution_production.at(solution_production_index).runtime*m_processes.at(m_solution_production.at(solution_production_index).process).costspershift;
}

double Output::get_production_cost_pallet(const int & solution_production_index)
{
	return get_production_cost(solution_production_index) / get_production(solution_production_index);
}

pair<double, double> Output::get_inventory(const int & solution_production_index)
{
	int i;
	
	for (i = 0; i < m_solution_inventory.size(); ++i)
	{
		if (m_solution_inventory.at(i).period == m_solution_production.at(solution_production_index).period && m_solution_inventory.at(i).plant == machine_locations.at(m_solution_production.at(solution_production_index).machine) && m_solution_inventory.at(i).product == m_solution_production.at(solution_production_index).product)
		{
			return pair<double, double>(m_solution_inventory.at(i).amount_start, m_solution_inventory.at(i).amount_end);
		}
	}
}

transport_information Output::get_customer_transports(const int & solution_production_index)
{

	transport_information dummy;
	dummy.amount = 0.0;
	dummy.costsum = 0.0;
	dummy.costpallet = 0.0;
	int i;
	for (i = 0; i < m_solution_single_transport_customer.size(); ++i)
	{
		if (m_solution_single_transport_customer.at(i).period == m_solution_production.at(solution_production_index).period)
		{
			if (m_solution_single_transport_customer.at(i).product == m_solution_production.at(solution_production_index).product)
			{
				if (m_solution_single_transport_customer.at(i).plant == machine_locations.at(m_processes.at(m_solution_production.at(solution_production_index).process).machine))
				{
					dummy.amount += m_solution_single_transport_customer.at(i).amount;
					dummy.costsum += m_solution_single_transport_customer.at(i).amount * m_transport_customer.at(transport_indices_customer.at(m_solution_single_transport_customer.at(i).plant).at(m_solution_single_transport_customer.at(i).customer)).cost/34.0;
				
				}
			}
		}
	}

	if (dummy.amount > 0)
	{
		dummy.costpallet = dummy.costsum / dummy.amount;
	}

	return dummy;
}

pair<double,double> Output::get_ic_transports(const int & solution_production_index)
{
	double dummy1 = 0.0;
	double dummy2 = 0.0;

	int i;
	for (i = 0; i < m_solution_single_transport_plant.size(); ++i)
	{
		if (m_solution_single_transport_plant.at(i).period == m_solution_production.at(solution_production_index).period)
		{
			if (m_solution_single_transport_plant.at(i).product == m_solution_production.at(solution_production_index).product)
			{
				if (m_solution_single_transport_plant.at(i).plant1 == machine_locations.at(m_processes.at(m_solution_production.at(solution_production_index).process).machine))
				{
					dummy1 += m_solution_single_transport_plant.at(i).amount;
					dummy2 += m_solution_single_transport_plant.at(i).amount * m_transport_plant.at(transport_indices_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2)).cost/34.0;
				}
			}
		}
	}
	return pair<double,double> (dummy1,dummy2);
}


void Output::set_transport_indices_plant()
{
	transport_indices_plant.clear();
	transport_indices_plant.resize(m_plants.size(),vector<int>(m_plants.size(),-1));

	int i;
	for (i = 0; i < m_transport_plant.size(); ++i)
	{
		transport_indices_plant.at(m_transport_plant.at(i).start).at(m_transport_plant.at(i).end) = i;
	}
}

void Output::set_transport_indices_customer()
{
	transport_indices_customer.clear();
	transport_indices_customer.resize(m_plants.size(),vector<int>(m_customers.size(),-1));

	int i;
	for (i = 0; i < m_transport_customer.size(); ++i)
	{
		transport_indices_customer.at(m_transport_customer.at(i).start).at(m_transport_customer.at(i).end) = i;
	}
}

/*int Output::get_transport_index_plant(const int & plant1, const int & plant2)
{

}

int Output::get_transport_index_customer(const int & plant, const int & customer)
{

}*/

void Output::article_to_merge()
{
	ofstream output(m_filename_article.c_str());
	if (output.fail())
	{
		cout << "Kann Datei " << m_filename_article << " nicht anlegen!" << endl;
		exit(1);
	}

	output << "Artikelnummer;Artikelbezeichnung;Kunden;Produktionsmenge;Anzahl Schichten;Produktionskosten (Summe);Produktionskosten (Palette);Anfangsbestand;Endbestand;Transport zu Kundenstandorten (Paletten);Kundentransportkosten (Summe);Kundentransportkosten (pro Palette);Intercompany Transporte (Paletten);Intercompany-Kosten (Summe)" << endl;

	int i;
	set<int>::const_iterator it;
	for (i = 0; i < m_nProducts; ++i)
	{
		output << m_products.at(i) << ";\"" << m_product_names.at(i) << "\";";
		for (it = customers_for_products.at(i).begin(); it != customers_for_products.at(i).end(); ++it)
		{
			output << m_customers.at((*it)) << " ";
		}
		
		output << ";" << threemonth_production.at(i).pallets << ";";
		output << threemonth_production.at(i).shifts << ";";
		output << threemonth_production.at(i).costsum << ";";
		output << threemonth_production.at(i).costpallet << ";";
		output << inventories.at(i).at(0).first << ";" << inventories.at(i).at(2).second << ";";
		output << threemonth_customer_transports.at(i).amount << ";";
		output << threemonth_customer_transports.at(i).costsum << ";" << threemonth_customer_transports.at(i).costpallet << ";";
		output << threemonth_ic_transports.at(i).amount << ";" << threemonth_ic_transports.at(i).costsum << endl;
	}

	output.close();
}

void Output::monthly_to_merge()
{

	ofstream output(m_filename_monthly.c_str());
	if (output.fail())
	{
		cout << "Kann Datei " << m_filename_monthly << " nicht anlegen!" << endl;
		exit(1);
	}

	int i, j, k; 
	set<int>::const_iterator it;

	output << "Artikelnummer;Artikelbezeichnung;Kunde;Monat;Werke;Anlagen;Menge;Anzahl Schichten;Produktionskosten (Summe);Produktionskosten (Palette);Anfangsbestand;Endbestand;Transport zu Kundenstandorten (Paletten);Kundentransportkosten (Summe);Kundentransportkosten (pro Palette);Intercompany-Transporte (Paletten);Intercompany-Kosten (Summe)" << endl;


	for (k = 1; k < m_periods.size() + 1; ++k)
	{
		for (i = 0; i < m_products.size(); ++i)
		{
			output << "\"" << m_products.at(i) << "\"" << ";" << m_product_names.at(i) << ";";
			for (it = customers_for_products.at(i).begin(); it != customers_for_products.at(i).end(); ++it)
			{
				output << m_customers.at((*it)) << " ";
			}
			output << ";" << m_periods.at(k - 1) << ";";

			for (j = 0; j < production_per_plant.at(i).at(k-1).size(); ++j)
			{
				output << m_plants.at(production_per_plant.at(i).at(k-1).at(j).first) << "(" << round(production_per_plant.at(i).at(k-1).at(j).second / production_per_article.at(i).at(k-1).pallets, 2) << ")" << " ";
			}
			output << ";";

			for (j = 0; j < production_per_machine.at(i).at(k-1).size(); ++j)
			{
				output << m_machines.at(production_per_machine.at(i).at(k-1).at(j).first) << "(" << round(production_per_machine.at(i).at(k-1).at(j).second / production_per_article.at(i).at(k-1).pallets, 2) << ")" << " ";
			}

			
			output << ";" << production_per_article.at(i).at(k-1).pallets;
			output << ";" << production_per_article.at(i).at(k-1).shifts << ";";
			output << production_per_article.at(i).at(k-1).costsum << ";";
			output << production_per_article.at(i).at(k-1).costpallet << ";";
			output << inventories.at(i).at(k-1).first << ";" << inventories.at(i).at(k-1).second << ";";
		
			output << customer_transports.at(i).at(k-1).amount << ";" << customer_transports.at(i).at(k-1).costsum << ";" << customer_transports.at(i).at(k-1).costpallet << ";";

			output << ic_transports.at(i).at(k-1).amount << ";" << ic_transports.at(i).at(k-1).costsum << endl;
		}
	}

	output.close();
}


void Output::get_all_customer_transports()
{
	int i,k;

	customer_transports.clear();
	customer_transports.resize(m_nProducts, vector<transport_information>(m_periods.size()));

	threemonth_customer_transports.clear();
	threemonth_customer_transports.resize(m_nProducts);

	for (i = 0; i < m_nProducts; ++i)
	{
		threemonth_customer_transports.at(i).amount = 0;
		threemonth_customer_transports.at(i).costsum = 0;

		for (k = 0; k < m_periods.size(); ++k)
		{
			customer_transports.at(i).at(k).amount = 0;
			customer_transports.at(i).at(k).costsum = 0;
		}
	}

	for (i = 0; i < m_solution_single_transport_customer.size(); ++i)
	{
		customer_transports.at(m_solution_single_transport_customer.at(i).product).at(m_solution_single_transport_customer.at(i).period-1).amount += m_solution_single_transport_customer.at(i).amount;
		customer_transports.at(m_solution_single_transport_customer.at(i).product).at(m_solution_single_transport_customer.at(i).period-1).costsum += m_solution_single_transport_customer.at(i).amount * m_transport_customer.at(transport_indices_customer.at(m_solution_single_transport_customer.at(i).plant).at(m_solution_single_transport_customer.at(i).customer)).cost/34.0;

		threemonth_customer_transports.at(m_solution_single_transport_customer.at(i).product).amount += m_solution_single_transport_customer.at(i).amount;
		threemonth_customer_transports.at(m_solution_single_transport_customer.at(i).product).costsum += m_solution_single_transport_customer.at(i).amount  * m_transport_customer.at(transport_indices_customer.at(m_solution_single_transport_customer.at(i).plant).at(m_solution_single_transport_customer.at(i).customer)).cost/34.0;
	}

	for (i = 0; i < m_nProducts; ++i)
	{
		for (k = 0; k < m_periods.size(); ++k)
		{
			customer_transports.at(i).at(k).costpallet = customer_transports.at(i).at(k).costsum/customer_transports.at(i).at(k).amount;
		}
		threemonth_customer_transports.at(i).costpallet = threemonth_customer_transports.at(i).costsum/threemonth_customer_transports.at(i).amount;
	}
}

void Output::get_all_ic_transports()
{
	ic_transports.clear();
	ic_transports.resize(m_nProducts, vector<transport_information>(m_periods.size()));
	
	int i,k;
	
	threemonth_ic_transports.clear();
	threemonth_ic_transports.resize(m_nProducts);

	for (i = 0; i < m_nProducts; ++i)
	{
		threemonth_ic_transports.at(i).amount = 0;
		threemonth_ic_transports.at(i).costsum = 0;

		for (k = 0; k < m_periods.size(); ++k)
		{
			ic_transports.at(i).at(k).amount = 0;
			ic_transports.at(i).at(k).costsum = 0;
		}
	}

	for (i = 0; i < m_solution_single_transport_plant.size(); ++i)
	{
		ic_transports.at(m_solution_single_transport_plant.at(i).product).at(m_solution_single_transport_plant.at(i).period-1).amount += m_solution_single_transport_plant.at(i).amount;
		ic_transports.at(m_solution_single_transport_plant.at(i).product).at(m_solution_single_transport_plant.at(i).period-1).costsum += m_solution_single_transport_plant.at(i).amount * m_transport_plant.at(transport_indices_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2)).cost/34.0;

		
		threemonth_ic_transports.at(m_solution_single_transport_plant.at(i).product).amount += m_solution_single_transport_plant.at(i).amount;
		threemonth_ic_transports.at(m_solution_single_transport_plant.at(i).product).costsum += m_solution_single_transport_plant.at(i).amount  * m_transport_plant.at(transport_indices_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2)).cost/34.0;
	}

	for (i = 0; i < m_nProducts; ++i)
	{
		for (k = 0; k < m_periods.size(); ++k)
		{
			ic_transports.at(i).at(k).costpallet = ic_transports.at(i).at(k).costsum/ic_transports.at(i).at(k).amount;
		}
		threemonth_ic_transports.at(i).costpallet = threemonth_ic_transports.at(i).costsum/threemonth_ic_transports.at(i).amount;
	}
}
void Output::get_3month_production()
{
	int i,k;

	threemonth_production.clear();
	threemonth_production.resize(m_nProducts);

	for (i = 0; i < m_nProducts; ++i)
	{
		threemonth_production.at(i).costsum = 0;
		threemonth_production.at(i).shifts = 0;
		threemonth_production.at(i).pallets = 0;
		for (k = 0; k < m_periods.size(); ++k)
		{
			threemonth_production.at(i).costsum += production_per_article.at(i).at(k).costsum;
			threemonth_production.at(i).shifts += production_per_article.at(i).at(k).shifts;
			threemonth_production.at(i).pallets += production_per_article.at(i).at(k).pallets;
		}
		if (threemonth_production.at(i).pallets > 0)
		{
			threemonth_production.at(i).costpallet = threemonth_production.at(i).costsum / threemonth_production.at(i).pallets;
		}
	}
}

void Output::get_production_plants()
{
	production_per_plant.clear();
	production_per_plant.resize(m_nProducts, vector<vector<pair<int, double> > >(m_periods.size()));

	production_per_article.clear();
	production_per_article.resize(m_nProducts, vector<production_information>(m_periods.size()));

	production_per_machine.clear();
	production_per_machine.resize(m_nProducts, vector<vector<pair<int, double> > >(m_periods.size()));

	int i, k;

	for (i = 0; i < m_solution_production.size(); ++i)
	{
		production_per_plant.at(m_solution_production.at(i).product).at(m_solution_production.at(i).period-1).push_back(pair<int, double>(machine_locations.at(m_solution_production.at(i).machine), m_solution_production.at(i).runtime*m_processes.at(m_solution_production.at(i).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(i).product)));

		production_per_article.at(m_solution_production.at(i).product).at(m_solution_production.at(i).period-1).shifts += m_solution_production.at(i).runtime;
		production_per_article.at(m_solution_production.at(i).product).at(m_solution_production.at(i).period-1).costsum += m_solution_production.at(i).runtime * m_processes.at(m_solution_production.at(i).process).costspershift;
		production_per_article.at(m_solution_production.at(i).product).at(m_solution_production.at(i).period-1).pallets += m_solution_production.at(i).runtime * m_processes.at(m_solution_production.at(i).process).tonspershift * m_nPallets_per_ton.at(m_solution_production.at(i).product);

		production_per_machine.at(m_solution_production.at(i).product).at(m_solution_production.at(i).period-1).push_back(pair<int, double>(m_solution_production.at(i).machine, m_solution_production.at(i).runtime*m_processes.at(m_solution_production.at(i).process).tonspershift*m_nPallets_per_ton.at(m_solution_production.at(i).product)));
	}

	for (k = 1; k < m_periods.size() + 1; ++k)
	{
		for (i = 0; i < m_products.size(); ++i)
		{
			if (production_per_article.at(i).at(k - 1).pallets > 0)
			{
				production_per_article.at(i).at(k - 1).costpallet = production_per_article.at(i).at(k - 1).costsum / production_per_article.at(i).at(k - 1).pallets;
			}
			else
			{
				production_per_article.at(i).at(k - 1).costpallet = 0.0;
			}
		}
	}
}

void Output::get_inventories()
{
	inventories.clear();
	inventories.resize(m_nProducts, vector<pair<double, double> >(m_periods.size(), pair<double, double>(0.0, 0.0)));

	int i;
	for (i = 0; i < m_solution_inventory.size(); ++i)
	{
		inventories.at(m_solution_inventory.at(i).product).at(m_solution_inventory.at(i).period-1).first += m_solution_inventory.at(i).amount_start;
		inventories.at(m_solution_inventory.at(i).product).at(m_solution_inventory.at(i).period-1).second += m_solution_inventory.at(i).amount_end;
	}
}



void Output::get_customers_for_products()
{
	customers_for_products.clear();
	customers_for_products.resize(m_nProducts);

	int i;
	for (i = 0; i < m_demands.size(); ++i)
	{
		customers_for_products.at(m_demands.at(i).product).insert(m_demands.at(i).customer);
	}
}

void Output::output_for_graphic_ic(const bool & german)
{
	int i, j, k;

	ofstream output(m_filename_graphic_ic.c_str());
	if (output.fail())
	{
		cout << "Kann Datei " << m_filename_graphic_ic << " nicht anlegen!" << endl;
		exit(1);
	}

	if (german) output.imbue(locale("german"));
	output << fixed;

	output << "Werk1;Periode;Werk2;eingehende IC-Transporte in Paletten;eingehende IC-Transportkosten;ausgehende IC-Transporte in Paletten;ausgehende IC-Transportkosten" << endl;

	for (i = 0; i < m_plants.size(); ++i)
	{
		for (j = 0; j < m_plants.size(); ++j)
		{
			if (i != j)
			{
				for (k = 1; k <= m_periods.size(); ++k)
				{
					output << m_plants.at(i) << ";";
					output << m_plants.at(j) << ";";
					output << m_periods.at(k - 1) << ";";
					output << ic_in_transports.at(i).at(j).at(k - 1).amount << ";";
					output << ic_in_transports.at(i).at(j).at(k - 1).costsum << ";";
					output << ic_out_transports.at(i).at(j).at(k - 1).amount << ";";
					output << ic_out_transports.at(i).at(j).at(k - 1).costsum << ";";
					//output << ic_transport.at(i).at(j).at(k - 1).amount << ";";
					//output << ic_transport.at(i).at(j).at(k - 1).costsum;

					output << endl;
				}
			}
		}
	}

	output.close();

}

void Output::output_for_graphic(const bool & german)
{
	ofstream output(m_filename_graphic.c_str());
	if (output.fail())
	{
		cout << "Kann Datei " << m_filename_graphic << " nicht anlegen!" << endl;
		exit(1);
	}


	if (german) output.imbue(locale("german"));
	output << fixed;

	output << "Werk;Periode;Produktion Paletten;Produktion Tonnen;Produktionskosten;Produktionsschichten;Kundentransporte Paletten;Kundentransportkosten;ausgehende IC-Transporte in Paletten;ausgehende IC-Transportkosten;eingehende IC-Transporte in Paletten;eingehende IC-Transportkosten" << endl;

	int i, k;
	for (i = 0; i < m_plants.size(); ++i)
	{
		for (k = 1; k < m_periods.size()+1; ++k)
		{
			output << m_plants.at(i) << ";" << m_periods.at(k-1) << ";" << productions_per_plant.at(i).at(k - 1).pallets << ";" << productions_per_plant.at(i).at(k - 1).tons << ";" << productions_per_plant.at(i).at(k - 1).costsum << ";" << productions_per_plant.at(i).at(k - 1).shifts << ";" << customer_transports_per_plant.at(i).at(k - 1).amount << ";" << customer_transports_per_plant.at(i).at(k - 1).costsum << ";" << ic_out_transports_per_plant.at(i).at(k - 1).amount << ";" << ic_out_transports_per_plant.at(i).at(k - 1).costsum << ";" << ic_in_transports_per_plant.at(i).at(k - 1).amount << ";" << ic_in_transports_per_plant.at(i).at(k - 1).costsum << endl;
		}
	}

	output.close();
}

void Output::get_production_per_plant()
{
	int i, k;
	production_information dummy;
	dummy.costsum = 0.0;
	dummy.costpallet = 0.0;
	dummy.shifts = 0.0;
	dummy.pallets = 0.0;
	dummy.tons = 0.0;

	productions_per_plant.clear();
	productions_per_plant.resize(m_plants.size(), vector<production_information> (m_periods.size(),dummy));
	
	for (i = 0; i < m_solution_production.size(); ++i)
	{
		productions_per_plant.at(machine_locations.at(m_solution_production.at(i).machine)).at(m_solution_production.at(i).period-1).shifts += m_solution_production.at(i).runtime;
		productions_per_plant.at(machine_locations.at(m_solution_production.at(i).machine)).at(m_solution_production.at(i).period - 1).pallets += m_solution_production.at(i).runtime*m_processes.at(m_solution_production.at(i).process).tonspershift * m_nPallets_per_ton.at(m_solution_production.at(i).product);
		productions_per_plant.at(machine_locations.at(m_solution_production.at(i).machine)).at(m_solution_production.at(i).period - 1).costsum += m_solution_production.at(i).runtime*m_processes.at(m_solution_production.at(i).process).costspershift;
		productions_per_plant.at(machine_locations.at(m_solution_production.at(i).machine)).at(m_solution_production.at(i).period - 1).tons += m_solution_production.at(i).runtime * m_processes.at(m_solution_production.at(i).process).tonspershift;
	}

	for (i = 0; i < m_plants.size(); ++i)
	{
		for (k = 0; k < m_periods.size(); ++k)
		{
			if (productions_per_plant.at(i).at(k).pallets > 0)
			{
				productions_per_plant.at(i).at(k).costpallet = productions_per_plant.at(i).at(k).costsum / productions_per_plant.at(i).at(k).pallets;
			}
			else
			{
				productions_per_plant.at(i).at(k).costpallet = 0;
			}
		}
	}
}

void Output::get_overall_production_per_plant()
{
	int i, k;
	production_information dummy;
	dummy.costsum = 0.0;
	dummy.costpallet = 0.0;
	dummy.shifts = 0.0;
	dummy.pallets = 0.0;
	dummy.tons = 0.0;

	overall_productions_per_plant.clear();
	overall_productions_per_plant.resize(m_plants.size(), dummy);

	for (i = 0; i < m_plants.size(); ++i)
	{
		for (k = 0; k < m_periods.size(); ++k)
		{
			overall_productions_per_plant.at(i).costsum += productions_per_plant.at(i).at(k).costsum;
			overall_productions_per_plant.at(i).shifts += productions_per_plant.at(i).at(k).shifts;
			overall_productions_per_plant.at(i).pallets += productions_per_plant.at(i).at(k).pallets;
			overall_productions_per_plant.at(i).tons += productions_per_plant.at(i).at(k).tons;
		}
		if (overall_productions_per_plant.at(i).pallets > 0)
		{
			overall_productions_per_plant.at(i).costpallet += overall_productions_per_plant.at(i).costsum / overall_productions_per_plant.at(i).pallets;
		}
		else
		{
			overall_productions_per_plant.at(i).costpallet = 0;
		}
	}
}

void Output::get_customer_transports_per_plant()
{
	int i, k;

	transport_information dummy;
	dummy.amount = 0.0;
	dummy.costsum = 0.0;
	dummy.costpallet = 0.0;

	customer_transports_per_plant.clear();
	customer_transports_per_plant.resize(m_plants.size(), vector<transport_information>(m_periods.size(), dummy));

	for (i = 0; i < m_solution_single_transport_customer.size(); ++i)
	{
		customer_transports_per_plant.at(m_solution_single_transport_customer.at(i).plant).at(m_solution_single_transport_customer.at(i).period - 1).amount += m_solution_single_transport_customer.at(i).amount;
		customer_transports_per_plant.at(m_solution_single_transport_customer.at(i).plant).at(m_solution_single_transport_customer.at(i).period - 1).costsum += m_solution_single_transport_customer.at(i).amount * m_transport_customer.at(transport_indices_customer.at(m_solution_single_transport_customer.at(i).plant).at(m_solution_single_transport_customer.at(i).customer)).cost / 34.0;
	}
	for (i = 0; i < m_plants.size(); ++i)
	{
		for (k = 0; k < m_periods.size(); ++k)
		{
			if (customer_transports_per_plant.at(i).at(k).amount > 0)
			{
				customer_transports_per_plant.at(i).at(k).costpallet = customer_transports_per_plant.at(i).at(k).costsum / customer_transports_per_plant.at(i).at(k).amount;
			}
			else
			{
				customer_transports_per_plant.at(i).at(k).costpallet = 0;
			}
		}
	}
}

void Output::get_overall_customer_transports_per_plant()
{
	int i, k;
	transport_information dummy;
	dummy.costsum = 0.0;
	dummy.costpallet = 0.0;
	dummy.amount = 0.0;

	overall_customer_transports_per_plant.clear();
	overall_customer_transports_per_plant.resize(m_plants.size(), dummy);
	
	for (i = 0; i < m_plants.size(); ++i)
	{
		for (k = 0; k < m_periods.size(); ++k)
		{
			overall_customer_transports_per_plant.at(i).costsum += customer_transports_per_plant.at(i).at(k).costsum;
			overall_customer_transports_per_plant.at(i).amount += customer_transports_per_plant.at(i).at(k).amount;
		}
		if (overall_customer_transports_per_plant.at(i).amount > 0)
		{
			overall_customer_transports_per_plant.at(i).costpallet += overall_customer_transports_per_plant.at(i).costsum / overall_customer_transports_per_plant.at(i).amount;
		}
		else
		{
			overall_customer_transports_per_plant.at(i).costpallet = 0;
		}
	}
}

void Output::get_ic_transports()
{
	int i;

	ic_in_transports.clear();
	ic_out_transports.clear();
	ic_transport.clear();

	transport_information dummy;
	dummy.amount = 0.0;
	dummy.costsum = 0.0;
	dummy.costpallet = 0.0;

	ic_in_transports.resize(m_plants.size(), vector<vector<transport_information> >(m_plants.size(), vector<transport_information>(m_periods.size(), dummy)));
	ic_out_transports.resize(m_plants.size(), vector<vector<transport_information> >(m_plants.size(), vector<transport_information>(m_periods.size(), dummy)));
	ic_transport.resize(m_plants.size(), vector<vector<transport_information> >(m_plants.size(), vector<transport_information>(m_periods.size(), dummy)));

	for (i = 0; i < m_solution_single_transport_plant.size(); ++i)
	{
		ic_in_transports.at(m_solution_single_transport_plant.at(i).plant2).at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).period - 1).amount += m_solution_single_transport_plant.at(i).amount;
		ic_in_transports.at(m_solution_single_transport_plant.at(i).plant2).at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).period - 1).costsum += m_solution_single_transport_plant.at(i).amount * m_transport_plant.at(transport_indices_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2)).cost / 34.0;
		ic_out_transports.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2).at(m_solution_single_transport_plant.at(i).period - 1).amount += m_solution_single_transport_plant.at(i).amount;
		ic_out_transports.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2).at(m_solution_single_transport_plant.at(i).period - 1).costsum += m_solution_single_transport_plant.at(i).amount * m_transport_plant.at(transport_indices_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2)).cost / 34.0;
		ic_transport.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2).at(m_solution_single_transport_plant.at(i).period - 1).amount += m_solution_single_transport_plant.at(i).amount;
		ic_transport.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2).at(m_solution_single_transport_plant.at(i).period - 1).costsum += m_solution_single_transport_plant.at(i).amount * m_transport_plant.at(transport_indices_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2)).cost / 34.0;
		ic_transport.at(m_solution_single_transport_plant.at(i).plant2).at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).period - 1).amount += m_solution_single_transport_plant.at(i).amount;
		ic_transport.at(m_solution_single_transport_plant.at(i).plant2).at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).period - 1).costsum += m_solution_single_transport_plant.at(i).amount * m_transport_plant.at(transport_indices_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2)).cost / 34.0;
	}
}

void Output::get_ic_transports_per_plant()
{
	ic_in_transports_per_plant.clear();
	ic_out_transports_per_plant.clear();
	ic_transports_per_plant.clear();

	transport_information dummy;
	dummy.amount = 0.0;
	dummy.costsum = 0.0;
	dummy.costpallet = 0.0;

	ic_in_transports_per_plant.resize(m_plants.size(), vector<transport_information>(m_periods.size(), dummy));
	ic_out_transports_per_plant.resize(m_plants.size(), vector<transport_information>(m_periods.size(), dummy));
	ic_transports_per_plant.resize(m_plants.size(), vector<transport_information>(m_periods.size(), dummy));

	int i, k;
	for (i = 0; i < m_solution_single_transport_plant.size(); ++i)
	{
		ic_out_transports_per_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).period - 1).amount += m_solution_single_transport_plant.at(i).amount;
		ic_in_transports_per_plant.at(m_solution_single_transport_plant.at(i).plant2).at(m_solution_single_transport_plant.at(i).period - 1).amount += m_solution_single_transport_plant.at(i).amount;
		ic_transports_per_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).period - 1).amount += m_solution_single_transport_plant.at(i).amount;
		ic_transports_per_plant.at(m_solution_single_transport_plant.at(i).plant2).at(m_solution_single_transport_plant.at(i).period - 1).amount += m_solution_single_transport_plant.at(i).amount;

		ic_out_transports_per_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).period - 1).costsum += m_solution_single_transport_plant.at(i).amount * m_transport_plant.at(transport_indices_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2)).cost / 34.0;
		ic_in_transports_per_plant.at(m_solution_single_transport_plant.at(i).plant2).at(m_solution_single_transport_plant.at(i).period - 1).costsum += m_solution_single_transport_plant.at(i).amount * m_transport_plant.at(transport_indices_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2)).cost / 34.0;
		ic_transports_per_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).period - 1).amount += m_solution_single_transport_plant.at(i).amount * m_transport_customer.at(transport_indices_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2)).cost / 34.0;
		ic_transports_per_plant.at(m_solution_single_transport_plant.at(i).plant2).at(m_solution_single_transport_plant.at(i).period - 1).amount += m_solution_single_transport_plant.at(i).amount * m_transport_customer.at(transport_indices_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2)).cost / 34.0;
	}

	for (i = 0; i < m_plants.size(); ++i)
	{
		for (k = 0; k < m_periods.size(); ++k)
		{
			if (ic_out_transports_per_plant.at(i).at(k).amount > 0)
			{
				ic_out_transports_per_plant.at(i).at(k).costpallet = ic_out_transports_per_plant.at(i).at(k).costsum / ic_out_transports_per_plant.at(i).at(k).amount;
			}
			else
			{
				ic_out_transports_per_plant.at(i).at(k).costpallet = 0;
			}
			if (ic_in_transports_per_plant.at(i).at(k).amount > 0)
			{
				ic_in_transports_per_plant.at(i).at(k).costpallet = ic_in_transports_per_plant.at(i).at(k).costsum / ic_in_transports_per_plant.at(i).at(k).amount;
			}
			else
			{
				ic_in_transports_per_plant.at(i).at(k).costpallet = 0;
			}
			if (ic_transports_per_plant.at(i).at(k).amount > 0)
			{
				ic_transports_per_plant.at(i).at(k).costpallet = ic_transports_per_plant.at(i).at(k).costsum / ic_transports_per_plant.at(i).at(k).amount;
			}
			else
			{
				ic_transports_per_plant.at(i).at(k).costpallet = 0;
			}
		}
	}
}

void Output::get_overall_ic_transports_per_plant()
{
	int i, k;
	transport_information dummy;
	dummy.costsum = 0.0;
	dummy.costpallet = 0.0;
	dummy.amount = 0.0;

	overall_ic_transports_per_plant.clear();
	overall_ic_transports_per_plant.resize(m_plants.size(), dummy);
	overall_ic_in_transports_per_plant.clear();
	overall_ic_in_transports_per_plant.resize(m_plants.size(), dummy);
	overall_ic_out_transports_per_plant.clear();
	overall_ic_out_transports_per_plant.resize(m_plants.size(), dummy);

	for (i = 0; i < m_plants.size(); ++i)
	{
		for (k = 0; k < m_periods.size(); ++k)
		{
			overall_ic_transports_per_plant.at(i).costsum += ic_transports_per_plant.at(i).at(k).costsum;
			overall_ic_transports_per_plant.at(i).amount += ic_transports_per_plant.at(i).at(k).amount;
			overall_ic_in_transports_per_plant.at(i).costsum += ic_in_transports_per_plant.at(i).at(k).costsum;
			overall_ic_in_transports_per_plant.at(i).amount += ic_in_transports_per_plant.at(i).at(k).amount;
			overall_ic_out_transports_per_plant.at(i).costsum += ic_out_transports_per_plant.at(i).at(k).costsum;
			overall_ic_out_transports_per_plant.at(i).amount += ic_out_transports_per_plant.at(i).at(k).amount;
		}
	}

	for (i = 0; i < m_plants.size(); ++i)
	{
		if (overall_ic_transports_per_plant.at(i).amount > 0)
		{
			overall_ic_transports_per_plant.at(i).costpallet = overall_ic_transports_per_plant.at(i).costsum / overall_ic_transports_per_plant.at(i).amount;
		}
		else
		{
			overall_ic_transports_per_plant.at(i).costpallet = 0;
		}
		if (overall_ic_in_transports_per_plant.at(i).amount > 0)
		{
			overall_ic_in_transports_per_plant.at(i).costpallet = overall_ic_in_transports_per_plant.at(i).costsum / overall_ic_in_transports_per_plant.at(i).amount;
		}
		else
		{
			overall_ic_transports_per_plant.at(i).costpallet = 0;
		}
		if (overall_ic_out_transports_per_plant.at(i).amount > 0)
		{
			overall_ic_out_transports_per_plant.at(i).costpallet = overall_ic_out_transports_per_plant.at(i).costsum / overall_ic_out_transports_per_plant.at(i).amount;
		}
		else
		{
			overall_ic_out_transports_per_plant.at(i).costpallet = 0;
		}
	}
}

void Output::write_article_names()
{
	ofstream output(m_filename_names.c_str());
	if (output.fail())
	{
		cout << "Kann Datei " << m_filename_names << " nicht anlegen!" << endl;
		exit(1);
	}
	int i;
	for (i = 0; i < m_products.size(); ++i)
	{
		output << "\"" << m_products.at(i) << "\";" << m_product_names.at(i) << endl;
	}
	output.close();
}

void Output::prepare_model_data()
{
	pallet_production_model.clear();
	pallet_production_model.resize(m_nProducts,vector<map<int,double> >(m_periods.size()));

	int i;
	
	for (i = 0; i < m_solution_production.size(); ++i)
	{		
		pallet_production_model.at(m_solution_production.at(i).product).at(m_solution_production.at(i).period-1).insert(pair<int,double>(m_solution_production.at(i).machine,m_solution_production.at(i).runtime*m_processes.at(m_solution_production.at(i).process).tonspershift * m_nPallets_per_ton.at(m_solution_production.at(i).product)));
	}

	transport_per_customer_model.clear();
	transport_per_customer_model.resize(m_nProducts,vector<map<int,double> >(m_periods.size()));

	detailled_transports_customer_model.clear();
	detailled_transports_customer_model.resize(m_nProducts,vector<vector<transport_information2> >(m_periods.size()));

	map<int,double>::iterator it;

	transport_information2 dummy;

	for (i = 0; i < m_solution_single_transport_customer.size(); ++i)
	{
		dummy.amount = m_solution_single_transport_customer.at(i).amount;
		dummy.cost = m_solution_single_transport_customer.at(i).amount * m_transport_customer.at(transport_indices_customer.at(m_solution_single_transport_customer.at(i).plant).at(m_solution_single_transport_customer.at(i).customer)).cost/34.0;
		dummy.start_index = m_solution_single_transport_customer.at(i).plant;
		dummy.end_index = m_solution_single_transport_customer.at(i).customer;

		detailled_transports_customer_model.at(m_solution_single_transport_customer.at(i).product).at(m_solution_single_transport_customer.at(i).period-1).push_back(dummy);

		it = transport_per_customer_model.at(m_solution_single_transport_customer.at(i).product).at(m_solution_single_transport_customer.at(i).period-1).find(m_solution_single_transport_customer.at(i).customer);

		if (it == transport_per_customer_model.at(m_solution_single_transport_customer.at(i).product).at(m_solution_single_transport_customer.at(i).period-1).end())
		{
			transport_per_customer_model.at(m_solution_single_transport_customer.at(i).product).at(m_solution_single_transport_customer.at(i).period-1).insert(pair<int,double>(m_solution_single_transport_customer.at(i).customer,m_solution_single_transport_customer.at(i).amount));
		}
		else
		{
			(*it).second += m_solution_single_transport_customer.at(i).amount;
		}
	}

	detailled_transports_plants_model.clear();
	detailled_transports_plants_model.resize(m_nProducts,vector<vector<transport_information2> >(m_periods.size()));

	for (i = 0; i < m_solution_single_transport_plant.size(); ++i)
	{
		dummy.amount = m_solution_single_transport_plant.at(i).amount;
		dummy.cost = m_solution_single_transport_plant.at(i).amount * m_transport_customer.at(transport_indices_plant.at(m_solution_single_transport_plant.at(i).plant1).at(m_solution_single_transport_plant.at(i).plant2)).cost/34.0;
		dummy.start_index = m_solution_single_transport_plant.at(i).plant1;
		dummy.end_index = m_solution_single_transport_plant.at(i).plant2;

		detailled_transports_plants_model.at(m_solution_single_transport_plant.at(i).product).at(m_solution_single_transport_plant.at(i).period-1).push_back(dummy);
	}
	
	inventory_model.clear();
	inventory_model.resize(m_nProducts,vector<vector<pair<double,double> > >(m_plants.size(),vector<pair<double,double> >(m_periods.size())));

	for (i = 0; i < m_solution_inventory.size(); ++i)
	{

		inventory_model.at(m_solution_inventory.at(i).product).at(m_solution_inventory.at(i).plant).at(m_solution_inventory.at(i).period-1).first = m_solution_inventory.at(i).amount_start;
		inventory_model.at(m_solution_inventory.at(i).product).at(m_solution_inventory.at(i).plant).at(m_solution_inventory.at(i).period-1).second = m_solution_inventory.at(i).amount_end;
	}


	for (i = 0; i < m_nProducts; ++i)
	{
		//cout << m_products.at(i) << " " << i << endl;

		DCBT* dcbt1 = new DCBT(m_plants.size(), m_periods.size());
#ifdef DEBUG
		cout<<"i:"<<i<<endl;
#endif
		dcbt1->map_production_information(m_nMachines, pallet_production_model.at(i),machine_locations);
	
		dcbt1->map_customer_information(m_customers.size(), transport_per_customer_model.at(i), detailled_transports_customer_model.at(i));
	
		dcbt1->map_ic_information(detailled_transports_plants_model.at(i));
	
		dcbt1->map_inventory_information(inventory_model.at(i));
		
		CBTModel* cbtmodel1 = new CBTModel(m_plants.size(), m_periods.size(), dcbt1->new_nMachines, dcbt1->new_nCustomers, dcbt1->start_inventory, dcbt1->machine_production, dcbt1->ic_transports, dcbt1->customer_transports, dcbt1->new_machine_location);
		cbtmodel1->init_vars();
		cbtmodel1->init_constraints();
		vector<vector<vector<vector<vector<double> > > > > solution1;
	
		cbtmodel1->solve(solution1);
		dcbt1->map_solution(solution1, m_nMachines);
		backtrackings.push_back(dcbt1->backtracking);
	
		delete cbtmodel1;
		delete dcbt1;

		/*if (i == 5)
		{
			char oi;
			cin >> oi;
		}*/

	
	}

}
