#pragma once

class WEPA_Opt_Params
{
public:
	/*!
* \brief epGap: Relative gap used for the optimization runs
*/
double ePGap;
	/*!
* Threshold for fixations
*/
double fixationThreshold;	
	/*!
	* \brief usage of customer_fixations
	*/
bool customer_fixation_usage;

bool customer_prioritized_fixations_usage;

	/*! \brief
* capacities of machines are multiplied with this factor
*/
double capacity_factor;

/*! \brief 
* regarding (1) or not regarding transport cost
*/
bool just_production_cost;
/*! \brief
* set production fixations?
*/
bool set_fixations;


/*! \brief
* german (1) or english formatted output (0)
*/
bool german;

/*! \brief 
* regarding (1) or not regarding transport cost
*/
//bool just_production_cost;

/*! \brief
* fix production to plants from manual planning
*/
bool fix_plant_production;

/*! \brief
* is it allowed to injure the split constraints in optimization (1) if they are injured in fixation or not (0)
*/
bool split_injure;

/*! \brief
* capacities of machines are multiplied with this factor
*/
//double capacity_factor;

/*! \brief
* factor for minimum inventory
*/
double minimum_inventory_factor;

/*! \brief
* set production fixations?
*/
//bool set_fixations;

/*! \brief
* minimum runtime in shifts per process
*/
double min_runtime;

/*! \brief
* minimum runtime in shifts for which splitting among different plants is allowed
*/
double min_for_split;

/*! \brief
* intercompany transport costs
*/
vector<vector<double> > transport_cost_plant;

/*! \brief
* customer transport costs
*/
vector<vector<double> > transport_cost_customer;

/*! \brief
* take into account baskets?
*/
bool use_baskets;

/*! \brief
* truck capacity
*/
int lkw_capacity;// = 34;

/*! \brief
* external warehouse cost
*/
double external_warehouse_cost;// = 10.0;

/*! \brief
* gap after solving
*/
double gap;

};