/*! \file
* main program
*	\retval 0 normal
*   \retval 1 problem with reading or writing file
*	\retval 2 error concerning first argument in command line
*	\retval 3 error concerning second argument in command line
*	\retval 4 error concerning third argument in command line
*	\retval 5 error concerning fourth argument in command line
*	\retval 6 error concerning fifth argument in command line
*	\retval 7 error concerning sixth argument in command line
*	\retval 8 error concerning seventh argument in command line
*	\retval 9 error concerning tenth argument in command line
*	\retval 10 error concerning eleventh argument in command line
*   \retval 14 wrong index for plant in start inventory file
*   \retval 15 wrong index for product in start inventory file
*	\retval 16 negative amount in start inventory file
*	\retval 21 negative capacities in file
*	\retval 22 more capacities in file than periods
*	\retval 23 more capacities in file than machines
*	\retval 24 less capacities in file than periods
*	\retval 25 less capacities in file than machines
*	\retval 26 wrong start index for ic-transport
*	\retval 27 wrong end index for ic-transport
*	\retval 28 negative costs for ic-transport
*	\retval 29 wrong start index for customer transport
*	\retval 30 wrong end index for customer transport
*	\retval 31 negative costs for customer transport
*	\retval 32 wrong customer index in demands
*	\retval 33 wrong product index in demands
*	\retval 34 wrong period index in demands
*	\retval 35 wrong amount in demands
*	\retval 36 non positive value for pallets per ton
*	\retval 37 not enough values for pallets per ton
*	\retval 38 too much values for pallets per ton
*	\retval 39 wrong customer index in baskets
*	\retval 41 wrong process index in fixations
*	\retval 42 wrong period index in fixations
*	\retval 43 wrong amount in fixations
*	\retval 44 wrong product index in processes
*	\retval 45 wrong machine index in processes
*	\retval 46 wrong value for tons per shift in processes
*	\retval 47 wrong value vor costs per shift in processes
*	\retval 48 wrong plant index in processes
*	\retval 51 inconsistence of data concerning production
*	\retval 52 inconsistence of data concerning transportation
*	\retval 61 negative plant index in plant capacity file
*   \retval 62 negative capacity in plant capacity file
*	\retval 71 missing interplant transportation information
*	\retval 99 cplex variable for which bound should be changed does not exist
*	\retval 100 wrong number of command line parameters
*   \retval 200 plant without capacity information
*	\retval 300 cplex solution process: problem is infeasible or unbounded
*/

#include <iostream>
#include <vector>
#include <sstream>
#include <fstream>
#include <string>
#include <algorithm>
#include <locale>
#include <iomanip>
#include <set>
#include <Windows.h>
#include <Psapi.h>
#include "model_wepa.h"
#include "external_input.h"
#include "timemeasurement.h"
#include "output_db.h"
#include "output_csv.h"
#include <conio.h>
#include<list>
#include "DataModule.h"
//#include<chrono>
#include "Data_Sources.h"
using namespace std;

/*! 
*	generating and solving the model
*	\param plants ordered names of plants
*	\param customers ordered names of customers
*	\param resources ordered names of machines
*	\param products ordered article numbers
*	\param periods ordered names of periods
*	\param processes ordered processes consisting of product index, machine index, tons per shift, cost per shift, plant index
*	\param capacity capacities per machine and period
*	\param transport_plant ordered ic-transport informations consisting of index of start plant, index of end plant, transport cost per pallet
*	\param transport_customer ordered customer transport informations consisting of index of plant, index of customer, transport cost per pallet
*	\param demands demand list consisting of customer index, product index, period index and amount in pallets
*	\param nPallets_per_ton amount of pallets per ton for ordered products
*	\param solution_production production results consisting of product index, machine index, period index, runtime in shifts and process index
*	\param solution_single_transport_plant ic-transport results consisting of product index, start plant index, end plant index, period index, transported amount in pallets
*	\param solution_single_transport_customer customer transport results consisting of product index, plant index, customer index, period index, transported amount in pallets
*	\param solution_dummy results for usage of dummy capacities consisting of machine index, period index, amount of dummy usage
*	\param solution_inventory results for inventory per plant and product
*	\param fixations runtimes of processes per period from the sales plan 
*	\param baskets set of baskets
*	\param basket_coherence basket indices per demand index
*	\param customer_for_basket customer index per basket index
*	\param start_inventory start inventory per product and plant
*   \param min_inventory_v2 minimum inventory per product and period
*	\param min_inventory_v1 minimum inventory per product, plant and product
*	\param plant_capacities internal warehouse capacities of plants
*	\param solution_external_warehouse_info solution: amount of palles in external warehouses 
*	\param fixed_transports fixed customer transports 
*/
unsigned long int fak(int n);

void solveit(const vector<string> & plants, 
			 const vector<string> & customers, 
			 const vector<string> & resources, 
			 const vector<string> & products, 
			 const vector<string> & periods, 
			 const vector<production> & processes, 
			 const vector<vector<double> > & capacity, 
			 const vector<transports> & transport_plant, 
			 const vector<transports> & transport_customer, 
			 const vector<demand> & demands, 
			 const vector<double> & nPallets_per_ton, 
			 vector<production_result> & solution_production, 
			 vector<customer_group_production_result> & customer_group_solution_production,
			 vector<single_transport_result_plant> & solution_single_transport_plant, 
			 vector<single_customer_group_transport_result_plant> & customer_group_single_transport_plant,
			 vector<single_transport_result_customer> & solution_single_transport_customer, 
			 vector<dummy_result> & solution_dummy, 
			 vector<inventory_result> & solution_inventory, 
			 vector<customer_group_inventory_result> & customer_group_solution_inventory,
			 const vector<vector<double> > & fixations,
			 const vector<vector<int> > & baskets, 
			 const vector<vector<int> > & basket_coherence, 
			 const vector<int> & customer_for_basket, 
			 const vector<vector<double> > & start_inventory, 
			 const vector<vector<double> > & min_inventory_v2, 
			 const vector<vector<vector<double> > > & min_inventory_v1, 
			 const vector<int> & plant_capacities, 
			 vector<vector<pair<double, double> > > & solution_external_warehouse_info, 
			 const vector<fixed_transport> & fixed_transports, 
			 const string & expansion_stage,
			 vector<demand> & minInventoryUsageFinalPeriod,
			 //const double fixationThreshold,
			  WEPA_Opt_Params & usedParams,
			 //const vector<article_customer_fixations> & customer_fixations
			 Article_customer_fixation_collection & custFixations,
			 Customer_product_related_transports_collection & cpTransports,
			 const vector<int> & resourcesWithFixations,
			 list<int> & solution_additionalProcessesPlantFixations,
			 std::string &logfilename
			 );
//void solveit(const vector<string> & plants, 
//			 const vector<string> & customers, 
//			 const vector<string> & resources, 
//			 const vector<string> & products, 
//			 const vector<string> & periods, 
//			 const vector<production> & processes, 
//			 const vector<vector<double> > & capacity, 
//			 const vector<transports> & transport_plant, 
//			 const vector<transports> & transport_customer, 
//			 const vector<demand> & demands, 
//			 const vector<double> & nPallets_per_ton, 
//			 vector<production_result> & solution_production, 
//			 vector<single_transport_result_plant> & solution_single_transport_plant, 
//			 vector<single_transport_result_customer> & solution_single_transport_customer, 
//			 vector<dummy_result> & solution_dummy, 
//			 vector<inventory_result> & solution_inventory, 
//			 const vector<vector<double> > & fixations, 
//			 const vector<vector<int> > & baskets, 
//			 const vector<vector<int> > & basket_coherence, 
//			 const vector<int> & customer_for_basket, 
//			 const vector<vector<double> > & start_inventory, 
//			 const vector<vector<double> > & min_inventory_v2, 
//			 const vector<vector<vector<double> > > & min_inventory_v1, 
//			 const vector<int> & plant_capacities, 
//			 vector<vector<pair<double, double> > > & solution_external_warehouse_info, 
//			 const vector<fixed_transport> & fixed_transports, 
//			 const string & expansion_stage,
//			 vector <demand> & minInventoryUsageFinalPeriod,
//			 const double fixationThreshold,
//			 const vector<article_customer_fixations> & customer_fixations
//			 );

/*!
*	reading input data
*	\param plants ordered names of plants
*	\param customers ordered names of customers
*	\param resources ordered names of machines
*	\param products ordered article numbers
*	\param periods ordered names of periods
*	\param processes ordered processes consisting of product index, machine index, tons per shift, cost per shift, plant index
*	\param capacity capacities per machine and period
*	\param transport_plant ordered ic-transport informations consisting of index of start plant, index of end plant, transport cost per pallet
*	\param transport_customer ordered customer transport informations consisting of index of plant, index of customer, transport cost per pallet
*	\param demands demand list consisting of customer index, product index, period index and amount in pallets
*	\param nPallets_per_ton amount of pallets per ton for ordered products
*	\param fixations runtimes of processes per period from the sales plan
*	\param productions amount of produced pallets per product and period according to the sales plan
*	\param baskets_for_customer per customer set of baskets, each basket represented by a boolean vector, entry at product index is true if the product is contained in the basket
*	\param product_names orderd names of products
*	\param plant_capacities internal warehouse capacities of plants
*	\param start_inventory start inventory per product and plant
*	\param minimum_inventory_v1 minimum inventory per product, plant and period
*	\param minimum_inventory_v2	minimum inventory per product and period
*	\param fixed_transports fixed customer transports 
*/
void get_written_data(vector<string> & plants, 
					  vector<string> & customers, 
					  vector<string> & resources, 
					  vector<string> & products, 
					  vector<string> & periods, 
					  vector<production> & processes, 
					  vector<vector<double> > & capacity, 
					  vector<transports> & transport_plant, 
					  vector<transports> & transport_customer, 
					  vector<demand> & demands, 
					  vector<double> & nPallets_per_ton, 
					  vector<vector<double> > & fixations, 
					  vector<vector<double> > & productions, 
					  vector<vector<vector<bool> > > & baskets_for_customer, 
					  vector<string> & product_names, 
					  vector<int> & plant_capacities, 
					  vector<vector<double> > & start_inventory, 
					  vector<vector<vector<double> > > & minimum_inventory_v1, 
					  vector<vector<double> > & minimum_inventory_v2, 
					  vector<fixed_transport> & fixed_transports,
					  //vector<article_customer_fixations> & customer_fixations
					  Article_customer_fixation_collection & custFixations,
					  Customer_product_related_transports_collection & cpTransports,
					  vector<int> & resourcesWithFixations
					  );

int speicherverbrauch();

/*! 
*	writing output data
*	\param resources ordered names of machines
*	\param periods ordered names of periods
*	\param products ordered article numbers
*	\param plants ordered names of plants
*	\param customers ordered names of customers
*	\param solution_production production results consisting of product index, machine index, period index, runtime in shifts and process index
*	\param solution_single_transport_plant ic-transport results consisting of product index, start plant index, end plant index, period index, transported amount in pallets
*	\param solution_single_transport_customer customer transport results consisting of product index, plant index, customer index, period index, transported amount in pallets
*	\param solution_dummy results for usage of dummy capacities consisting of machine index, period index, amount of dummy usage
*	\param solution_inventory results for the inventory per product and plant
*	\param runfile output file name for processes
*	\param transsicfile	output file name for ic-transports
*	\param transscustomerfile output file name for customer transports
*	\param dummyfile output file name for dummy usage
*	\param processes ordered processes consisting of product index, machine index, tons per shift, cost per shift, plant index
*	\param transport_plant ordered ic-transport informations consisting of index of start plant, index of end plant, transport cost per pallet
*	\param transport_customer ordered customer transport informations consisting of index of plant, index of customer, transport cost per pallet
*	\param nPallets_per_ton amount of pallets per ton for ordered products
*	\param product_names orderd names of products
*	\param wepaoutputfile output file name for detailed production
*	\param capacityoutputfile output file name for detailed capacity usages
*	\param capacity capacities per machine and period
*	\param guifile output file name for overview for GUI
*	\param inventoryfile output file for inventory informations
*	\param warehousecostfile output file for external inventory informations
*	\param solution_external_warehouses: solution: external inventory informations
*/

//void output(const vector<string> & resources, 
//			const vector<string> & periods, 
//			const vector<string> & products, 
//			const vector<string> & plants, 
//			const vector<string> & customers, 
//			const vector<production_result> & solution_production, 
//			const vector<single_transport_result_plant> & solution_single_transport_plant,
//			const vector<single_customer_group_transport_result_plant> & solution_customer_group_single_transport_plant,
//			const vector<single_transport_result_customer> & solution_single_transport_customer, 
//			const vector<dummy_result> & solution_dummy, 
//			const vector<inventory_result> & solution_inventory, 
//			const string & runfile, 
//			const string & transsicfile, 
//			const string & transscustomerfile, 
//			const string & dummyfile, 
//			const vector<production> & processes, 
//			const vector<transports> & transport_plant, 
//			const vector<transports> & transport_customer, 
//			const vector<double> & nPallets_per_ton, 
//			const vector<string> & product_names, 
//			const string & wepaoutputfile, 
//			const string & capacityoutputfile, 
//			const vector<vector<double> > capacity, 
//			const string & guifile, 
//			const string & inventoryfile, 
//			const string & warehousecostfile, 
//			vector<vector<pair<double, double> > > & solution_external_warehouses, 
//			const vector<demand> & demands, 
//			const string & graphicfile, 
//			const string & namefile, 
//			const string & detailproductionfile, 
//			const string & monthlyproductionfile, 
//			const string & articleproductionfile, 
//			const string & graphicfile_ic
//			);
void output_db(std::string &optFolderName,
			std::string &userName,
			int optRunNr,
			const vector<string> & resources, 
			const vector<string> & periods, 
			const vector<string> & products, 
			const vector<string> & plants, 
			const vector<string> & customers, 
			const vector<production_result> & solution_production,
			const vector<customer_group_production_result> & customer_group_solution_production,
			const vector<single_transport_result_plant> & solution_single_transport_plant, 
			const vector<single_customer_group_transport_result_plant> & customer_group_solution_single_transport_plant,
			const vector<single_transport_result_customer> & solution_single_transport_customer, 
			const vector<dummy_result> & solution_dummy, 
			const vector<inventory_result> & solution_inventory,
			const vector<customer_group_inventory_result> & customer_group_solution_inventory,
			General_Data_Source *runDS,//const string & runfile, 
			General_Data_Source *transicDS,//const string & transsicfile, 
			General_Data_Source *transcustomerDS,//const string & transscustomerfile, 
			General_Data_Source *dummyDS,//const string & dummyfile, 
			const vector<production> & processes, 
			const vector<transports> & transport_plant, 
			const vector<transports> & transport_customer,  
			const vector<double> & nPallets_per_ton,  
			const vector<string> & product_names, 
			General_Data_Source *wepaDS,//const string & wepaoutputfile, 
			General_Data_Source *capacityDS,//const string & capacityoutputfile, 
			const vector<vector<double> > capacity, 
			General_Data_Source *guiDS,//const string & guifile, 
			General_Data_Source *inventoryDS,//const string & inventoryfile, 
			General_Data_Source *warehouseDS,//const string & warehousecostfile, 
			vector<vector<pair<double,double> > > & solution_external_warehouses, 
			const vector<demand> & demands, 
			General_Data_Source *graphicDS,//const string & graphicfile, 
			General_Data_Source *nameDS,//const string & namefile, 
			General_Data_Source *detailproductionpalletsDS,//const string & detailproductionfile_pallets, 
			General_Data_Source *detailproductiontonsDS,//const string & detailproductionfile_tons, 
			General_Data_Source *monthlyproductionDS,//const string & monthlyproductionfile, 
			General_Data_Source *articleproductionDS,//const string & articleproductionfile, 
			General_Data_Source *graphic_icDS,//const string & graphicfile_ic,
			const Article_customer_fixation_collection & custFixations,
			General_Data_Source *additionalProcessesPlantFixationsDS,//const string & additionalprocessesplantfixationsfile,
			list<int> &solution_additionalAllowedProcessesPlantFixations,
			General_Data_Source *comparisonDS,
			const string &expansion_stage,
			WEPA_Opt_Params & usedParams,
			vector<vector<double> > & transport_cost_plant,
			vector<vector<double> > & transport_cost_customer
			);

//void output(const vector<string> & resources, 
//			const vector<string> & periods, 
//			const vector<string> & products, 
//			const vector<string> & plants, 
//			const vector<string> & customers, 
//			const vector<production_result> & solution_production, 
//			const vector<single_transport_result_plant> & solution_single_transport_plant,
//			const vector<single_customer_group_transport_result_plant> & solution_customer_group_single_transport_plant,
//			const vector<single_transport_result_customer> & solution_single_transport_customer, 
//			const vector<dummy_result> & solution_dummy, 
//			const vector<inventory_result> & solution_inventory, 
//			const string & runfile, 
//			const string & transsicfile, 
//			const string & transscustomerfile, 
//			const string & dummyfile, 
//			const vector<production> & processes, 
//			const vector<transports> & transport_plant, 
//			const vector<transports> & transport_customer, 
//			const vector<double> & nPallets_per_ton, 
//			const vector<string> & product_names, 
//			const string & wepaoutputfile, 
//			const string & capacityoutputfile, 
//			const vector<vector<double> > capacity, 
//			const string & guifile, 
//			const string & inventoryfile, 
//			const string & warehousecostfile, 
//			vector<vector<pair<double, double> > > & solution_external_warehouses, 
//			const vector<demand> & demands, 
//			const string & graphicfile, 
//			const string & namefile, 
//			const string & detailproductionfile, 
//			const string & monthlyproductionfile, 
//			const string & articleproductionfile, 
//			const string & graphicfile_ic
//			);
void output_csv(const vector<string> & resources,
	const vector<string> & periods,
	const vector<string> & products,
	const vector<string> & plants,
	const vector<string> & customers,
	const vector<production_result> & solution_production,
	const vector<customer_group_production_result> & customer_group_solution_production,
	const vector<single_transport_result_plant> & solution_single_transport_plant,
	const vector<single_customer_group_transport_result_plant> & customer_group_solution_single_transport_plant,
	const vector<single_transport_result_customer> & solution_single_transport_customer,
	const vector<dummy_result> & solution_dummy,
	const vector<inventory_result> & solution_inventory,
	const vector<customer_group_inventory_result> & customer_group_solution_inventory_result,
	const string & runfile,
	const string & transsicfile,
	const string & transscustomerfile,
	const string & dummyfile,
	const vector<production> & processes,
	const vector<transports> & transport_plant,
	const vector<transports> & transport_customer,
	const vector<double> & nPallets_per_ton,
	const vector<string> & product_names,
	const string & wepaoutputfile,
	const string & capacityoutputfile,
	const vector<vector<double> > capacity,
	const string & guifile,
	const string & inventoryfile,
	const string & warehousecostfile,
	vector<vector<pair<double, double> > > & solution_external_warehouses,
	const vector<demand> & demands,
	const string & graphicfile,
	const string & namefile,
	const string & detailproductionfile_pallets,
	const string & detailproductionfile_tons,
	const string & monthlyproductionfile,
	const string & articleproductionfile,
	const string & graphicfile_ic,
	//vector<article_customer_fixations> & customer_fixations
	const Article_customer_fixation_collection & custFixations,
	const string & additionalprocessesplantfixationsfile,
	list<int> &solution_additionalAllowedProcessesPlantFixations,
	const string &comparisonFile,
	const string &expansion_stage,
	bool german,
	vector<vector<double> > & transport_cost_plant,
	vector<vector<double> > & transport_cost_customer
);
/*
void output(const vector<string> & resources, 
			const vector<string> & periods, 
			const vector<string> & products, 
			const vector<string> & plants, 
			const vector<string> & customers, 
			const vector<production_result> & solution_production,
			const vector<customer_group_production_result> & customer_group_solution_production,
			const vector<single_transport_result_plant> & solution_single_transport_plant, 
			const vector<single_customer_group_transport_result_plant> & customer_group_solution_single_transport_plant,
			const vector<single_transport_result_customer> & solution_single_transport_customer, 
			const vector<dummy_result> & solution_dummy, 
			const vector<inventory_result> & solution_inventory,
			const vector<customer_group_inventory_result> & customer_group_solution_inventory_result,
			const string & runfile, 
			const string & transsicfile, 
			const string & transscustomerfile, 
			const string & dummyfile, 
			const vector<production> & processes, 
			const vector<transports> & transport_plant, 
			const vector<transports> & transport_customer,  
			const vector<double> & nPallets_per_ton,  
			const vector<string> & product_names, 
			const string & wepaoutputfile, 
			const string & capacityoutputfile, 
			const vector<vector<double> > capacity, 
			const string & guifile, 
			const string & inventoryfile, 
			const string & warehousecostfile, 
			vector<vector<pair<double,double> > > & solution_external_warehouses, 
			const vector<demand> & demands, 
			const string & graphicfile, 
			const string & namefile, 
			const string & detailproductionfile_pallets,
			const string & detailproductionfile_tons, 
			const string & monthlyproductionfile, 
			const string & articleproductionfile, 
			const string & graphicfile_ic,
			//vector<article_customer_fixations> & customer_fixations
			const Article_customer_fixation_collection & custFixations,
			const string & additionalprocessesplantfixationsfile,
			list<int> &solution_additionalAllowedProcessesPlantFixations,
			const string &comparisonFile,
			const string &expansion_stage,
			WEPA_Opt_Params & usedParams,
			vector<vector<double> > & transport_cost_plant,
			vector<vector<double> > & transport_cost_customer
			);
*/
void set_plant_capacities(const vector<string> & plants, vector<int> & plant_capacities);

/*void set_minimum_inventory_v1(const vector<vector<int> > & productions, vector<vector<vector<double> > > & minimum_inventory_v1, const int & nPlants, const int & nProducts, const int & nPeriods, const vector<production> & processes, const double & factor);*/

/*! 
*	set the minimum inventory per product and period according to the given factor
*	\param demands customer demands
*	\param minimum_inventory_v2 output: minimum inventory per product and period
*	\param nProducts number of products
*	\param nPeriods number of periods
*	\param processes production processes
*/
void set_minimum_inventory_v2(vector<demand> demands, vector<vector<double> > & minimum_inventory_v2, const int & nProducts, const int & nPeriods, const vector<production> & processes,vector<demand> & finalPeriodMinInvUsages,WEPA_Opt_Params & usedParams);

/*! 
*	generates the subset of non-dominated baskets of all baskets
*	\param baskets_for_customer given baskets
*	\param baskets_for_customer_new set of non-dominated baskets
*	\param nProducts number of products
*	\param nCustomers number of customers
*/
void basket_domination(const vector<vector<vector<bool> > > & baskets_for_customer, vector<vector<vector<bool> > > & baskets_for_customer_new, const int & nProducts, const int & nCustomers);

/*!
*	generates sets of period-relevant baskets by excluding baskets without demand
*	\param baskets_for_customer given baskets
*	\param baskets_for_customer_and_period relevant baskets per period
*	\param nPeriods number of periods
*/
void basket_revision(const vector<vector<vector<bool> > > & baskets_for_customer, 
					 vector<vector<vector<vector<bool> > > > & baskets_for_customer_and_period, 
					  const int & nPeriods,vector<vector<vector<bool> > > &m_is_demand_for_customer_and_product_in_period
					  );

/*!
*	converts customer and product dependent baskets in demand dependent baskets
*	\param baskets_for_customer_and_period product dependent baskets per period
*	\param demands customer demands
*	\param nCustomers number of customers
*	\param nProducts number of products
*	\param nPeriods number of periods
*	\param new_baskets demand index dependent baskets
*	\param basket_coherence relevant basket indices per demand index
*	\param customer_for_basket customer index per basket index
*/
void basket_mapping(vector<vector<vector<vector<bool> > > >  & baskets_for_customer_and_period, const vector<demand> & demands, const int & nCustomers, const int & nProducts, const int & nPeriods, vector<vector<int> > & new_baskets, vector<vector<int> > & basket_coherence, vector<int> & customer_for_basket);

/*!
*	basket preprocessing
*	\param baskets_for_customer given baskets
*	\param nPeriods number of periods
*	\param nCustomers number of customers
*	\param nProducts number of products
*	\param demands customer demands
*	\param new_baskets adapted baskets
*	\param basket_coherence relevant basket indices per demand index
*	\param customer_for_basket relevant customer indices per basket index
*/
void reduce_baskets(const vector<vector<vector<bool> > > & baskets_for_customer, const int & nPeriods, const int & nCustomers, const int & nProducts, const vector<demand> & demands, vector<vector<int> > & new_baskets, vector<vector<int> > & basket_coherence, vector<int> & customer_for_basket,vector<vector<vector<bool> > > m_is_demand_for_customer_and_product_in_period);

/*! contains the given basket the given product?
*	\param basket basket
*	\param product product index
*/
bool basket_contains_product(vector<bool> & basket, const int & product);

/*!	is the basket dominated for the given product
*	\param basket basket index
*	\param product product index
*	\param ordered_baskets_for_customer ordered baskets according to size
*	\param nProducts number of products
*/
bool basket_is_dominated_for_product(const int & basket, const int & product, const vector<vector<bool> > & ordered_baskets_for_customer, const int & nProducts);

/*! writes the optimization logfile
*	\param logfile logfile path
*/
void write_logfile(const string & logfile,WEPA_Opt_Params & usedParams);


/*! \brief
* german (1) or english formatted output (0)
*/
//bool german;

/*! \brief 
* regarding (1) or not regarding transport cost
*/
//bool just_production_cost;

/*! \brief
* fix production to plants from manual planning
*/
//bool fix_plant_production;

/*! \brief
* is it allowed to injure the split constraints in optimization (1) if they are injured in fixation or not (0)
*/
//bool split_injure;

/*! \brief
* capacities of machines are multiplied with this factor
*/
//double capacity_factor;

/*! \brief
* factor for minimum inventory
*/
//double minimum_inventory_factor;

/*! \brief
* set production fixations?
*/
//bool set_fixations;

/*! \brief
* minimum runtime in shifts per process
*/
//double min_runtime;

/*! \brief
* minimum runtime in shifts for which splitting among different plants is allowed
*/
//double min_for_split;



/*! \brief
* customer transport costs
*/
//vector<vector<double> > transport_cost_customer;

/*! \brief
* take into account baskets?
*/
//bool use_baskets;

/*! \brief
* truck capacity
*/
//int lkw_capacity = 34;

/*! \brief
* external warehouse cost
*/
//double external_warehouse_cost = 10.0;

/*! \brief
* gap after solving
*/
//double gap;

/*! 
* \brief
* exists demand for the corresponding product?
*/
//vector<bool> m_is_demand;
/*!
* \brief
* exists demand for the corresponding customer and product?
*/
//vector<vector<bool> > m_is_demand_for_customer_and_product;
/*!
* \brief
* exists demand for the corresponding customer and product in the corresponding period?
*/
//vector<vector<vector<bool> > > m_is_demand_for_customer_and_product_in_period;
/*!
* \brief
* exists demand for the corresponding product in the corresponding period?
*/
//vector<vector<double > > demand_for_product_in_period;

/*!
* \brief
* number of plants
*/
int nPlants;
/*!
* \brief
* number of customers
*/
int nCustomers;
/*! 
* \brief
* number of resources
*/
int nResources;
/*!
* \brief 
* number of products
*/
int nProducts;
/*!
* \brief number of periods
*/
int nPeriods;
/*!
* \brief number of processes
*/
int nProcesses;
/*!
* \brief number of interplant transport processes
*/
int nTransportProcessesPlants;
/*!
* \brief number of customer transport processes
*/
int nTransportProcessesCustomer;

/*!
* Threshold for fixations
*/
//double fixationThreshold;
/*!
* \brief epGap: Relative gap used for the optimization runs
*/
//double ePGap;
/*!
* \brief usage of customer_fixations
*/
//bool customer_fixation_usage;
/*!
* \brief instance for time measurement
*/
TM tm;

/*
	std::string guiTabName="OUT_INTMED_CUSTOMER_RESULT_GUI";
	guiDT=new DataBase_Table(&usedDB);
	guiDT->TableName_s(guiTabName);
	
	std::string detailProdTabNamePallets="OUT_INTMED_CUSTOMER_PRODUCTION_PALLETS";
	detailproductionDT_pallets=new DataBase_Table(&usedDB);
	detailproductionDT_pallets->TableName_s(detailProdTabNamePallets);

	std::string monthlyProdTabName="OUT_INTMED_CUSTOMER_MONTHLY_CUSTOMER_ARTICLE_PRODUCTION";
	monthlyproductionDT=new DataBase_Table(&usedDB);
	monthlyproductionDT->TableName_s(monthlyProdTabName);

	std::string articleProdTabName="OUT_INTMED_CUSTOMER_ARTICLE_PRODUCTION";
	articleproductionDT=new DataBase_Table(&usedDB);
	articleproductionDT->TableName_s(articleProdTabName);

	std::string comparisonTabName="OUT_INTMED_COMPARISON";
	comparisonDT=new DataBase_Table(&usedDB);
	comparisonDT->TableName_s(comparisonTabName);
*/
std::string guiTabName="OUT_INTMED_CUSTOMER_RESULT_GUI";
std::string detailProdTabNamePallets="OUT_INTMED_CUSTOMER_PRODUCTION_PALLETS";
std::string monthlyProdTabName="OUT_INTMED_CUSTOMER_MONTHLY_CUSTOMER_ARTICLE_PRODUCTION";
std::string articleProdTabName="OUT_INTMED_CUSTOMER_ARTICLE_PRODUCTION";
std::string comparisonTabName="OUT_INTMED_COMPARISON";

void clearEntriesFromTable (
			std::string &tableName,
			std::string &optFolderName,
			std::string &userName,
			int optRunNr,
			DataBase &usedDb)
{
	std::string cmd="delete from "+tableName+" where "+
		" Optimization_Folder_Name='"+optFolderName+"' and"+
		" userid='"+userName+"' and"+
		" optimization_number="+std::to_string(optRunNr);

	usedDb.execSQLCmd(cmd);
}
void clearAllResultTables (
			std::string &optFolderName,
			std::string &userName,
			int optRunNr,
			DataBase &usedDb)
{
	clearEntriesFromTable(guiTabName,
						  optFolderName,
						  userName,
						  optRunNr,
						  usedDb
						  );
	clearEntriesFromTable(detailProdTabNamePallets,
						  optFolderName,
						  userName,
						  optRunNr,
						  usedDb
						  );
	clearEntriesFromTable(monthlyProdTabName,
						  optFolderName,
						  userName,
						  optRunNr,
						  usedDb
						  );
	clearEntriesFromTable(articleProdTabName,
						  optFolderName,
						  userName,
						  optRunNr,
						  usedDb
						  );
	clearEntriesFromTable(comparisonTabName,
						  optFolderName,
						  userName,
						  optRunNr,
						  usedDb
						  );
}

/*!
* main function
* \param argc number of arguments
* \param argv[1] optimization (0) or fixation (1)
* \param argv[2] minimum lotsize in shifts
* \param argv[3] minimum lotsize for which splitting production to different plants is allowed
* \param argv[4] minimum inventory
* \param argv[5] factor for resource capacities
* \param argv[6] allowed (1) to injure split constraint in optimization which are injured in fixation or not (0)
* \param argv[7] production and transport optimization (0) or just production optimization (1)
* \param argv[8] input path
* \param argv[9] output path
* \param argv[10] english (0) or german (1) formatted output
* \param argv[11] consider baskets (1) or not (0)
* \param argv[12] expansionstage
* \param argv[13] name of demandfile if exists
*/
int main(int argc, char* argv[])
{

	string usedDSN, usedUserName, usedPW;//="TestRemote24";

		/*! \brief
	* intercompany transport costs
	*/

	/*! \brief
	* customer transport costs
	*/
	vector<vector<double> > transport_cost_plant;
	vector<vector<double> > transport_cost_customer;
	string plantfile;
	string customerfile;
	string resourcefile;
	string productfile;
	string periodfile;
	string processfile;
	string capacityfile;
	string transportplantfile;
	string transportcustomerfile;
	string demandfile;
	string palletspertonfile;
	string basketsfile;
	string fixationfile;
	string artikelnamenfile;
	string plant_capacityfile;

	string start_inventoryfile;

	string fixed_transport_file;

	string logfilename;

	//string plant_capacity_file;

	string customer_fixation_group_file;

	string customer_prioritized_fixation_group_file;

	string customer_product_related_transport_file;

	string resources_with_fixations_file;

	string input_folder;
	//std::chrono::system_clock::time_point start_time,end_time;
	WEPA_Opt_Params usedParams;
	usedParams.lkw_capacity = 34;

	usedParams.external_warehouse_cost = 10.0;

	if (argc < 14)
	{
		cout << "Falsche Anzahl von Parametern!" << endl;
		cout << "Richtiger Aufruf lautet WEPA_Modellierung [fixations (0/1)] [min_runtime in shifts] [min_for_split in shifts] [factor for minimum inventory] [factor for resource capacities] [allow to injure split constraints (1/0)] [just production optimization (1/0)] [fix plants(1/0)][input path][output path][english / german(0 / 1)][use baskets(1 / 0)][expansion stage][name for demandfile]" << endl;
		exit(100);
	}

	int i;
	int custFixUsageSetting;
	string set_fixations_str = argv[1];
	string min_runtime_str = argv[2];
	string min_for_split_str = argv[3];
	string minimum_inventory_factor_str = argv[4];
	string capacity_factor_str = argv[5];
	string split_injure_str = argv[6];
	string just_production_cost_str = argv[7];
	string fix_plant_production_str = argv[8];
	string orig_input_folder = argv[9];
	string output_folder = argv[10];
	string german_str = argv[11];
	string basket_str = argv[12];
	string expansion_stage = argv[13];
	string demandfilename;
	string fixationThreshold_str;
	string customer_fixation_group_str;
	string epGap_str;
	string dataSourceStr;
	int usedDataSource;
	int optimizationRun;
	string userIdStr;
	string optimizationRunStr;
	//string customer_product_related_transports;

	if (argc >= 15)
	{
		demandfilename = argv[14];
	}
	else
	{
		demandfilename = "demands.csv";
	}

	if (argc >= 16)
	{
		fixationThreshold_str = argv[15];
	}
	else
	{
		fixationThreshold_str = "0";
	}

	if (argc >= 17)
	{
		customer_fixation_group_str = argv[16];
	}
	else
	{
		customer_fixation_group_str = "0";
	}

	if (argc >= 18)
	{
		epGap_str = argv[17];
	}
	else
		epGap_str = "0";

	if (argc >= 19)
		userIdStr = argv[18];
	else
		userIdStr = "";

	if (argc >= 20)
		optimizationRunStr = argv[19];
	else
		optimizationRunStr = "0";

	if (argc >= 21)
	{
		dataSourceStr = argv[20];
	}
	else
		dataSourceStr = "0";

	if (dataSourceStr == "0")
	{
		usedDataSource = 0;
	}
	else if (dataSourceStr == "1")
	{
		usedDataSource = 1;
	}
	else if (dataSourceStr == "2")
	{
		usedDataSource = 2;
	}
	else
	{
		usedDataSource = 0;
	}

	optimizationRun = stoi(optimizationRunStr);
	usedParams.german = atoi(german_str.c_str());
	usedParams.use_baskets = atoi(basket_str.c_str());

	input_folder = orig_input_folder + "\\";
	output_folder += "\\";

	plantfile = input_folder + "plants.csv";
	customerfile = input_folder + "customers.csv";
	resourcefile = input_folder + "resources.csv";
	productfile = input_folder + "products.csv";
	periodfile = input_folder + "periods.csv";
	processfile = input_folder + "processes.csv";
	capacityfile = input_folder + "capacities.csv";
	transportplantfile = input_folder + "transportsplant.csv";

	transportcustomerfile = input_folder + "transportscustomer.csv";
	demandfile = input_folder + demandfilename;
	palletspertonfile = input_folder + "pallets_per_ton.csv";
	basketsfile = input_folder + "baskets.csv";
	fixationfile = input_folder + "histProduction.csv";
	artikelnamenfile = input_folder + "artikelnamen.csv";
	start_inventoryfile = input_folder + "startbestand.csv";
	fixed_transport_file = input_folder + "detaillierteTransporte.csv";
	plant_capacityfile = input_folder + "lagerkapazitaeten.csv";
	customer_fixation_group_file = input_folder + "customer_fixation_groups.csv";
	customer_product_related_transport_file = input_folder + "customer_product_related_transports.csv";
	resources_with_fixations_file = input_folder + "resourcesWithFixations.csv";
	customer_prioritized_fixation_group_file = input_folder + "customer_prioritized_fixation_groups.csv";
	usedParams.set_fixations = atoi(set_fixations_str.c_str());
	usedParams.min_runtime = atof(min_runtime_str.c_str());
	usedParams.min_for_split = atof(min_for_split_str.c_str());
	usedParams.minimum_inventory_factor = atof(minimum_inventory_factor_str.c_str());
	//minimum_inventory_factor = 0.0;
	usedParams.just_production_cost = atoi(just_production_cost_str.c_str());
	usedParams.fix_plant_production = atoi(fix_plant_production_str.c_str());
	//fix_plant_production=false;
	usedParams.capacity_factor = atof(capacity_factor_str.c_str());
	usedParams.split_injure = atoi(split_injure_str.c_str());
	usedParams.fixationThreshold = atof(fixationThreshold_str.c_str());

	custFixUsageSetting = atoi(customer_fixation_group_str.c_str());
	usedParams.customer_prioritized_fixations_usage = usedParams.customer_fixation_usage = false;

	if (custFixUsageSetting == 1)
		usedParams.customer_fixation_usage = true;
	else if (custFixUsageSetting == 2)
	{
		usedParams.customer_fixation_usage = true;
		usedParams.customer_prioritized_fixations_usage = true;
	}
	usedParams.ePGap = atof(epGap_str.c_str()) / 100;

	if (usedParams.set_fixations < 0 || usedParams.set_fixations > 1)
	{
		cout << "Das erste Argument (Fixierung?) muss 0 oder 1 sein!" << endl;
		exit(2);
	}
	if (usedParams.min_runtime < 0)
	{
		cout << "Das zweite Argument (minimale Losgroesse) darf nicht negativ sein!" << endl;
		exit(3);
	}
	if (usedParams.min_for_split < 0)
	{
		cout << "Das dritte Argument (minimale Losgroesse bei Splittung) darf nicht negativ sein!" << endl;
		exit(4);
	}
	if (usedParams.minimum_inventory_factor < 0)
	{
		cout << "Das vierte Argument (Faktor fuer Mindestbestand) darf nicht negativ sein!" << endl;
		exit(5);
	}
	if (usedParams.capacity_factor < 0)
	{
		cout << "Das fuenfte Argument (Kapazitaetsfaktor fuer Maschinen) darf nicht negativ sein!" << endl;
		exit(6);
	}
	if (usedParams.split_injure < 0 || usedParams.split_injure > 1)
	{
		cout << "Das sechste Argument (Verletzung von in Fixierung verletzten Splitbedingungen erlauben?) muss 0 oder 1 sein!" << endl;
		exit(7);
	}
	if (usedParams.just_production_cost < 0 || usedParams.just_production_cost > 1)
	{
		cout << "Das siebte Argument (nur Produktionsoptimierung?) muss 0 oder 1 sein!" << endl;
		exit(8);
	}
	if (usedParams.german < 0 || usedParams.german > 1)
	{
		cout << "Das zehnte Argument (deutsche Ausgabe?) muss 0 oder 1 sein!" << endl;
		exit(9);
	}
	if (usedParams.use_baskets < 0 || usedParams.use_baskets > 1)
	{
		cout << "Das elfte Argument (Warenkoerbe verwenden?) muss 0 oder 1 sein!" << endl;
		exit(10);
	}

	if (usedParams.set_fixations)
	{
		usedParams.split_injure = true;
		usedParams.minimum_inventory_factor = 0.0;
	}

	if (expansion_stage != "1" && expansion_stage != "2a")
	{
		cout << "Die Ausbaustufe muss 1 oder 2a sein!" << endl;
		exit(1111);
	}

	if (usedParams.fixationThreshold < 0)
	{
		cout << "Fixierungsschwelle muss positiv oder gleich 0 sein!" << endl;
	}

	if (usedParams.ePGap < 0)
		cout << "Relativer Gap muss>=0 sein." << endl;

	if (usedDataSource == 0)
		logfilename = output_folder + "logfile_infeasibilities.log";
	else if (usedDataSource == 1)
		logfilename = output_folder + "logfile_infeasibilities_" + orig_input_folder + "_" + userIdStr + "_" + optimizationRunStr + ".log";

	if (usedParams.set_fixations)
	{
		output_folder += "fixierung_";
	}
	else
	{
		output_folder += "optimierung_";
	}

	vector<bool> m_is_demand;
	vector<vector<bool> > m_is_demand_for_customer_and_product;
	vector<vector<vector<bool> > > m_is_demand_for_customer_and_product_in_period;
	vector<vector<double > > demand_for_product_in_period;
	vector<string> plants;
	vector<string> customers;
	vector<string> resources;
	vector<string> products;
	vector<string> periods;
	vector<production> processes;
	vector<vector<double> >capacity;
	vector<transports> transport_plant;
	vector<transports> transport_customer;
	vector<basket> baskets;
	vector<int> product;
	vector<demand> demands;
	vector<demand> minInventoryUsageFinalPeriod;
	vector<double>nPallets_per_ton;
	vector<int> plant_capacities;
	vector<vector<double> > start_inventory;
	vector<vector<vector<double> > > minimum_inventory_v1;
	vector<vector<double> > minimum_inventory_v2;
	vector<vector<double> > fixations;
	vector<vector<double> > fixations_sales_plan;
	vector<vector<double> > productions;
	vector<vector<vector<bool> > > baskets_for_customer;
	vector<vector<int> > new_baskets;
	vector<vector<int> > basket_coherence;
	vector<int> customer_for_basket;
	vector<production_result> solution_production;
	vector<customer_group_production_result> customer_group_solution_production;
	vector<single_transport_result_plant> solution_single_transport_plant;
	vector<single_customer_group_transport_result_plant> customer_group_solution_single_transport_plant;
	vector<single_transport_result_customer> solution_single_transport_customer;
	vector<dummy_result> solution_dummy;
	vector<inventory_result> solution_inventory;
	vector<customer_group_inventory_result> customer_group_solution_inventory;
	vector<vector<pair<double, double> > > solution_external_warehouse_info;
	vector<fixed_transport> fixed_transports;
	vector<string> product_names;
	list<int> solution_additionalAllowedProcessesPlantFixations;
	Article_customer_fixation_collection custFixations;
	Customer_product_related_transports_collection cpTransports;
	vector<int> resourcesWithFixations;

	tm.init();

	cout << "reading data..." << endl;

	ODBC_DataBase usedDB;
	DataModule_CSV dMCSV;
	DataModule_DB dMDB;

	if (usedDataSource == 0)
	{
		dMCSV.setFileNames(
			plantfile,
			customerfile,
			resourcefile,
			productfile,
			periodfile,
			processfile,
			capacityfile,
			transportplantfile,
			transportcustomerfile,
			demandfile,
			palletspertonfile,
			basketsfile,
			fixationfile,
			artikelnamenfile,
			plant_capacityfile,
			start_inventoryfile,
			fixed_transport_file,
			logfilename,
			customer_fixation_group_file,
			customer_product_related_transport_file,
			resources_with_fixations_file,
			customer_prioritized_fixation_group_file
		);

		dMCSV.get_input_data(plants,
			customers,
			resources,
			products,
			periods,
			processes,
			capacity,
			transport_plant,
			transport_customer,
			demands,
			nPallets_per_ton,
			fixations_sales_plan,
			productions,
			baskets_for_customer,
			product_names,
			plant_capacities,
			start_inventory,
			minimum_inventory_v1,
			minimum_inventory_v2,
			fixed_transports,
			custFixations,
			cpTransports,
			resourcesWithFixations, m_is_demand,
			m_is_demand_for_customer_and_product,
			m_is_demand_for_customer_and_product_in_period,
			demand_for_product_in_period,
			usedParams
		);
	}
	else if (usedDataSource == 1)
	{
		if (argc >= 21)
			usedDSN = argv[20];
		else
			usedDSN = "";

		if (argc >= 22)
			usedUserName = argv[21];
		else
			usedUserName = "";

		if (argc >= 23)
			usedPW = argv[22];
		else
			usedPW = "";

		usedDB.DSN_s(usedDSN, usedUserName, usedPW);//(usedDSN,"RemoteUser25","remote2017");
		usedDB.connect();
		dMDB.setDB(&usedDB);

		dMDB.OptFolder_s(orig_input_folder);//("t01032017");
		dMDB.UserId_s(userIdStr);
		dMDB.OptimizationRun_s(optimizationRun);

		dMDB.get_input_data(plants,
			customers,
			resources,
			products,
			periods,
			processes,
			capacity,
			transport_plant,
			transport_customer,
			demands,
			nPallets_per_ton,
			fixations_sales_plan,
			productions,
			baskets_for_customer,
			product_names,
			plant_capacities,
			start_inventory,
			minimum_inventory_v1,
			minimum_inventory_v2,
			fixed_transports,
			custFixations,
			cpTransports,
			resourcesWithFixations, m_is_demand,
			m_is_demand_for_customer_and_product, m_is_demand_for_customer_and_product_in_period, demand_for_product_in_period,
			usedParams
		);
	}
	else
	{
	}

	/*
	dMCSV.setFileNames(plantfile,customerfile,resourcefile,productfile,periodfile,processfile,capacityfile,transportplantfile,transportcustomerfile,demandfile,palletspertonfile,basketsfile,
		fixationfile,artikelnamenfile,plant_capacity_file,start_inventoryfile,fixed_transport_file,logfilename,customer_fixation_group_file,customer_product_related_transport_file,resources_with_fixations_file
		);

	dMCSV.get_input_data(plants,
					 customers,
					 resources,
					 products,
					 periods,
					 processes,
					 capacity,
					 transport_plant,
					 transport_customer,
					 demands,
					 nPallets_per_ton,
					 fixations_sales_plan,
					 productions,
					 baskets_for_customer,
					 product_names,
					 plant_capacities,
					 start_inventory,
					 minimum_inventory_v1,
					 minimum_inventory_v2,
					 fixed_transports,
					 custFixations,
					 cpTransports,
					 resourcesWithFixations,m_is_demand,
					 m_is_demand_for_customer_and_product,m_is_demand_for_customer_and_product_in_period,demand_for_product_in_period,
					 usedParams
					 );
					 */
					 /*
				 get_written_data(plants,
								  customers,
								  resources,
								  products,
								  periods,
								  processes,
								  capacity,
								  transport_plant,
								  transport_customer,
								  demands,
								  nPallets_per_ton,
								  fixations_sales_plan,
								  productions,
								  baskets_for_customer,
								  product_names,
								  plant_capacities,
								  start_inventory,
								  minimum_inventory_v1,
								  minimum_inventory_v2,
								  fixed_transports,
								  //customer_fixations
								  custFixations,
								  cpTransports,
								  resourcesWithFixations
								  );
				 */
				 /*cout << "external data..." << endl;
				 string real_production_file = "Extern\\Tagesproduktion_Jan-Mar.csv";
				 get_real_production(real_production_file, products, resources, processes, fixations, nPallets_per_ton);*/

	nPlants = plants.size();
	nCustomers = customers.size();
	nResources = resources.size();
	nProducts = products.size();
	nPeriods = periods.size() + 1;
	nProcesses = processes.size();
	nTransportProcessesPlants = transport_plant.size();
	nTransportProcessesCustomer = transport_customer.size();
	vector<double> last_inventory;

	tm.add_event("Input");

	cout << "Vorbereiten der Warenkoerbe" << endl;

	if (usedParams.use_baskets)
	{
		reduce_baskets(baskets_for_customer, nPeriods, nCustomers, nProducts, demands, new_baskets, basket_coherence, customer_for_basket, m_is_demand_for_customer_and_product_in_period);
		int i, j, k;

		for (i = 0;i < new_baskets.at(53).size();i++)
		{
			std::cout << "Product:" << products.at(demands.at(new_baskets.at(53).at(i)).product) << " Customer:" << customers.at(demands.at(new_baskets.at(53).at(i)).customer) << " Period:" << periods.at(demands.at(new_baskets.at(53).at(i)).period) << std::endl;
		}
	}

	/*ofstream output3("new_baskets.csv");
	int j;
	for (i = 0; i < new_baskets.size(); ++i)
	{
		for (j = 0; j < new_baskets.at(i).size(); ++j)
		{
			output3 << new_baskets.at(i).at(j) << ";";
		}
		output3 << endl;
	}
	output3.close();*/

	/*ofstream output2("basket_coherence.csv");
	for (i = 0; i < basket_coherence.size(); ++i)
	{
		output2 << i << ";";
		for (j = 0; j < basket_coherence.at(i).size(); ++j)
		{
			output2 << basket_coherence.at(i).at(j) << ";";
		}
		output2 << endl;
	}
	output2.close();*/


	cout << "nach Dateneinlesen" << endl;
	minimum_inventory_v1.resize(nPlants, vector<vector<double> >(nProducts, vector<double>(nPeriods, 0.0)));

	transport_cost_plant.resize(plants.size(), vector<double>(plants.size(), 0.0));
	transport_cost_customer.resize(customers.size(), vector<double>(plants.size(), 0.0));

	for (i = 0; i < transport_plant.size(); ++i)
	{
		transport_cost_plant.at(transport_plant.at(i).start).at(transport_plant.at(i).end) = transport_plant.at(i).cost / (double)usedParams.lkw_capacity;
	}

	for (i = 0; i < transport_customer.size(); ++i)
	{
		transport_cost_customer.at(transport_customer.at(i).end).at(transport_customer.at(i).start) = transport_customer.at(i).cost / (double)usedParams.lkw_capacity;
	}

	//set_plant_capacities(plants, plant_capacities);

	minimum_inventory_v1.resize(nPlants, vector<vector<double> >(nProducts, vector<double>(nPeriods, 0)));
	set_minimum_inventory_v2(demands, minimum_inventory_v2, products.size(), periods.size(), processes, minInventoryUsageFinalPeriod, usedParams);

	solveit(plants,
		customers,
		resources,
		products,
		periods,
		processes,
		capacity,
		transport_plant,
		transport_customer,
		demands,
		nPallets_per_ton,
		solution_production,
		customer_group_solution_production,
		solution_single_transport_plant,
		customer_group_solution_single_transport_plant,
		solution_single_transport_customer,
		solution_dummy,
		solution_inventory,
		customer_group_solution_inventory,
		fixations_sales_plan,
		new_baskets,
		basket_coherence,
		customer_for_basket,
		start_inventory,
		minimum_inventory_v2,
		minimum_inventory_v1,
		plant_capacities,
		solution_external_warehouse_info,
		fixed_transports,
		expansion_stage,
		minInventoryUsageFinalPeriod,
		usedParams,
		custFixations,
		cpTransports,
		resourcesWithFixations,
		solution_additionalAllowedProcessesPlantFixations,
		logfilename
	);

	string logfile;

	if (usedDataSource == 0)
	{
		logfile = output_folder + "logfile.log";
		//std::cout<<"Number of additional processes in main procedure:"<<solution_additionalAllowedProcessesPlantFixations.size()<<std::endl;
		string runfile = output_folder + "output_produktion.csv";
		string transsicfile = output_folder + "output_interplant_einzel.csv";
		string transscustomerfile = output_folder + "output_kundentransport_einzel.csv";
		string transicfile = output_folder + "output_interplant_lkw.csv";
		string transcustomerfile = output_folder + "output_kundentransport_lkw.csv";
		string dummyfile = output_folder + "output_dummies.csv";
		string wepaoutputfile = output_folder + "output_uebersicht_gesamt_produktion_und_fracht.csv";
		string capacityoutputfile = output_folder + "output_uebersicht_kapazitaeten.csv";
		string guifile = output_folder + "output_gui.csv";
		string inventoryfile = output_folder + "output_bestand.csv";
		string warehousecostfile = output_folder + "output_externe_lager.csv";
		
		string graphicfile = output_folder + "output_graphics.csv";
		string graphicfile_ic = output_folder + "output_graphics_ic.csv";
		string namefile = output_folder + "output_artikelnamen.csv";
		string detailproductionfile_pallets = output_folder + "output_detail_produktion_pallets.csv";
		string detailproductionfile_tons = output_folder + "output_detail_produktion_tons.csv";
		string monthlyproductionfile = output_folder + "output_monats_produktion.csv";
		string articleproductionfile = output_folder + "output_artikel_produktion.csv";

		string comparisonFile = output_folder + "comparisonData.csv";

		string additionalprocessesplantfixationsfile = output_folder + "additionalprocessesplantfixations.csv";
		cout << "Writing files.." << endl;
		output_csv(resources,
			periods,
			products,
			plants,
			customers,
			solution_production,
			customer_group_solution_production,
			solution_single_transport_plant,
			customer_group_solution_single_transport_plant,
			solution_single_transport_customer,
			solution_dummy,
			solution_inventory,
			customer_group_solution_inventory,
			runfile,
			transsicfile,
			transscustomerfile,
			dummyfile,
			processes,
			transport_plant,
			transport_customer,
			nPallets_per_ton,
			product_names,
			wepaoutputfile,
			capacityoutputfile,
			capacity,
			guifile,
			inventoryfile,
			warehousecostfile,
			solution_external_warehouse_info,
			demands,
			graphicfile,
			namefile,
			detailproductionfile_pallets,
			detailproductionfile_tons,
			monthlyproductionfile,
			articleproductionfile,
			graphicfile_ic,
			custFixations,
			additionalprocessesplantfixationsfile,
			solution_additionalAllowedProcessesPlantFixations,
			comparisonFile,
			expansion_stage,usedParams.german,
			transport_cost_plant,
			transport_cost_customer
		);
		cout << "Done" << endl;
		//cin.get();
	}
	else if (usedDataSource == 1)
	{
		logfile = output_folder + "logfile_" + orig_input_folder + "_" + userIdStr + "_" + optimizationRunStr + ".log";

		DataBase_Table *runDT,
			*transicDT,
			*transcustDT,
			*dummyDT,
			*wepaoutputDT,
			*capacityoutputDT,
			*guiDT,
			*inventoryDT,
			*warehousecostDT,
			*graphicDT,
			*nameDT,
			*detailproductionDT_pallets,
			*detailproductionDT_tons,
			*monthlyproductionDT,
			*articleproductionDT,
			*graphicDT_ic,
			*additionalprocessesplantfixationsDT,
			*comparisonDT
			;

		additionalprocessesplantfixationsDT = graphicDT_ic = detailproductionDT_tons = nameDT = graphicDT = warehousecostDT = inventoryDT = capacityoutputDT = wepaoutputDT = dummyDT = transcustDT = transcustDT = transicDT = runDT = NULL;

		clearAllResultTables(orig_input_folder, userIdStr, optimizationRun, usedDB);

		//std::string guiTabName="OUT_INTMED_CUSTOMER_RESULT_GUI";
		guiDT = new DataBase_Table(&usedDB);
		guiDT->TableName_s(guiTabName);

		//std::string detailProdTabNamePallets="OUT_INTMED_CUSTOMER_PRODUCTION_PALLETS";
		detailproductionDT_pallets = new DataBase_Table(&usedDB);
		detailproductionDT_pallets->TableName_s(detailProdTabNamePallets);

		//std::string monthlyProdTabName="OUT_INTMED_CUSTOMER_MONTHLY_CUSTOMER_ARTICLE_PRODUCTION";
		monthlyproductionDT = new DataBase_Table(&usedDB);
		monthlyproductionDT->TableName_s(monthlyProdTabName);

		//std::string articleProdTabName="OUT_INTMED_CUSTOMER_ARTICLE_PRODUCTION";
		articleproductionDT = new DataBase_Table(&usedDB);
		articleproductionDT->TableName_s(articleProdTabName);

		//std::string comparisonTabName="OUT_INTMED_COMPARISON";
		comparisonDT = new DataBase_Table(&usedDB);
		comparisonDT->TableName_s(comparisonTabName);

		output_db(orig_input_folder,
			userIdStr,
			optimizationRun,
			resources,
			periods,
			products,
			plants,
			customers,
			solution_production,
			customer_group_solution_production,
			solution_single_transport_plant,
			customer_group_solution_single_transport_plant,
			solution_single_transport_customer,
			solution_dummy,
			solution_inventory,
			customer_group_solution_inventory,
			runDT,
			transicDT,
			transcustDT,
			dummyDT,
			processes,
			transport_plant,
			transport_customer,
			nPallets_per_ton,
			product_names,
			wepaoutputDT,
			capacityoutputDT,
			capacity,
			guiDT,
			inventoryDT,
			warehousecostDT,
			solution_external_warehouse_info,
			demands,
			graphicDT,
			nameDT,
			detailproductionDT_pallets,
			detailproductionDT_tons,
			monthlyproductionDT,
			articleproductionDT,
			graphicDT_ic,
			custFixations,
			additionalprocessesplantfixationsDT,
			solution_additionalAllowedProcessesPlantFixations,
			comparisonDT,
			expansion_stage,
			usedParams,
			transport_cost_plant,
			transport_cost_customer
		);
	}
	write_logfile(logfile,usedParams);
	tm.end();
	cout<<"Total run time:"<< tm.get_overallduration()<<" seconds."<<std::endl;
	//std::cin.get();
	return 0;
}

bool basket_contains_product(const vector<bool> & basket, const int & product)
{
	return basket.at(product); 
}

bool basket_is_dominated(const int & basket, const vector<vector<bool> > & ordered_baskets_for_customer, const int & nProducts)
{
	int l;
	for (l = 0; l < nProducts; ++l)
	{
		if (basket_contains_product(ordered_baskets_for_customer.at(basket), l) && !basket_is_dominated_for_product(basket, l, ordered_baskets_for_customer, nProducts))
		{
			return false;
		}
	}
	return true;
}

bool basket_is_dominated_for_product(const int & basket, const int & product, const vector<vector<bool> > & ordered_baskets_for_customer, const int & nProducts)
{
	int j, l;
	bool dominated;

	//Warenk�rbe mit weniger oder gleicher Anzahl an Elementen durchlaufen
	for (j = 0; j < basket; ++j)
	{
		dominated = false;
		if (basket_contains_product(ordered_baskets_for_customer.at(j), product))
		{
			dominated = true;
			for (l = 0; l < nProducts; ++l)
			{
				if (basket_contains_product(ordered_baskets_for_customer.at(j), l) && !basket_contains_product(ordered_baskets_for_customer.at(basket), l))
				{
					dominated = false;
				}
			}
		}
		if (dominated)
		{
			return true;
		}
	}
	return false;
}

void sort_baskets(const vector<vector<vector<bool> > > & baskets_for_customer, vector<vector<vector<bool> > > & new_baskets_for_customer, const int & nCustomers, const int & nProducts)
{
	int i, j, l;
	int number;

	new_baskets_for_customer.clear();
	new_baskets_for_customer.resize(nCustomers);

	for (i = 0; i < nCustomers; ++i)
	{
		vector<pair<int, int> > sizes;

		for (j = 0; j < baskets_for_customer.at(i).size(); ++j)
		{
			number = 0;
			for (l = 0; l < nProducts; ++l)
			{
				if (basket_contains_product(baskets_for_customer.at(i).at(j), l))
				{
					number++;
				}
			}

			sizes.push_back(pair<int, int>(number, j));
		}

		sort(sizes.begin(), sizes.end());

		for (j = 0; j < sizes.size(); ++j)
		{
			new_baskets_for_customer.at(i).push_back(baskets_for_customer.at(i).at(sizes.at(j).second));
		}

		/*for (j = 0; j < new_baskets_for_customer.at(i).size(); ++j)
		{
			for (l = 0; l < nProducts; ++l)
			{
				if (new_baskets_for_customer.at(i).at(j).at(l))
				{
					cout << l << " ";
				}
			}
			char oi;
			cin >> oi;
		}
		cout << endl << endl;*/
	
	}


}

void basket_domination(const vector<vector<vector<bool> > > & baskets_for_customer, vector<vector<vector<bool> > > & baskets_for_customer_new, const int & nProducts, const int & nCustomers)
{
	int i, j, l;

	baskets_for_customer_new.clear();
	baskets_for_customer_new.resize(nCustomers);

	for (i = 0; i < nCustomers; ++i)
	{
		for (j = 0; j < baskets_for_customer.at(i).size(); ++j)
		{
			if (!basket_is_dominated(j, baskets_for_customer.at(i), nProducts))
			{
				baskets_for_customer_new.at(i).push_back(baskets_for_customer.at(i).at(j));
			}
			else
			{
				/*cout << "dominiert f�r Kunde " << i << ": ";
				for (l = 0; l < nProducts; ++l)
				{
					if (baskets_for_customer.at(i).at(j).at(l))
					{
						cout << l << " ";
					}
				}
				char oi;
				cin >> oi;*/
			}
		}
	}
}


void reduce_baskets(const vector<vector<vector<bool> > > & baskets_for_customer, const int & nPeriods, const int & nCustomers, const int & nProducts, const vector<demand> & demands, vector<vector<int> > & new_baskets, vector<vector<int> > & basket_coherence, vector<int> & customer_for_basket,vector<vector<vector<bool> > > m_is_demand_for_customer_and_product_in_period)
{
	int i;

	vector<vector<vector<bool> > > baskets_for_customer_new;
	vector<vector<vector<bool> > > baskets_for_customer_new_new;
	vector<vector<vector<vector<bool> > > > baskets_for_customer_and_period;

	//sort_baskets(baskets_for_customer, baskets_for_customer_new, nCustomers, nProducts);
	//basket_domination(baskets_for_customer_new, baskets_for_customer_new_new, nProducts, nCustomers);
	//basket_revision(baskets_for_customer_new_new, baskets_for_customer_and_period, nPeriods);
	basket_revision(baskets_for_customer, baskets_for_customer_and_period, nPeriods,m_is_demand_for_customer_and_product_in_period);
	basket_mapping(baskets_for_customer_and_period, demands, nCustomers, nProducts, nPeriods, new_baskets, basket_coherence, customer_for_basket);

	
	/*cout << new_baskets.size() << endl;
	int nr = 0;
	unsigned long int nr2 = 0;
	for (i = 0; i < new_baskets.size(); ++i)
	{
		
		if (new_baskets.at(i).size() > 2)
		{
			nr += new_baskets.at(i).size();
			nr2 += fak(new_baskets.at(i).size()) / (fak(new_baskets.at(i).size() - 2) * 2);
		}
	}
	cout << nr << endl;
	cout << nr2 << endl;
	char op;
	cin >> op;*/
}



unsigned long int fak(int n)
{
	if (n == 1)
	{
		return 1;
	}
	return n*fak(n - 1);
}

void basket_mapping(vector<vector<vector<vector<bool> > > >  & baskets_for_customer_and_period, const vector<demand> & demands, const int & nCustomers, const int & nProducts, const int & nPeriods, vector<vector<int> > & new_baskets, vector<vector<int> > & basket_coherence, vector<int> & customer_for_basket)
{
	new_baskets.clear();
	basket_coherence.clear();
	basket_coherence.resize(demands.size());
	customer_for_basket.clear();

	int i, j, k, l;

	vector<vector<vector<int> > > demandindices(nCustomers, vector<vector<int> >(nProducts, vector<int>(nPeriods, -1)));

	for (i = 0; i < demands.size(); ++i)
	{
		demandindices.at(demands.at(i).customer).at(demands.at(i).product).at(demands.at(i).period) = i;
	}
	
	vector<int> dummy;

	for (k = 0; k < nPeriods-1; ++k)
	{
		for (i = 0; i < nCustomers; ++i)
		{
			for (j = 0; j < baskets_for_customer_and_period.at(k).at(i).size(); ++j)
			{
				dummy.clear();
				for (l = 0; l < nProducts; ++l)
				{
					if (baskets_for_customer_and_period.at(k).at(i).at(j).at(l))
					{
						dummy.push_back(demandindices.at(i).at(l).at(k));
						basket_coherence.at(demandindices.at(i).at(l).at(k)).push_back(new_baskets.size());
					}
				}
			
				new_baskets.push_back(dummy);
				customer_for_basket.push_back(i);
			}
		}
	}
}


void basket_revision(const vector<vector<vector<bool> > > & baskets_for_customer, vector<vector<vector<vector<bool> > > > & baskets_for_customer_and_period, const int & nPeriods,vector<vector<vector<bool> > > & m_is_demand_for_customer_and_product_in_period)
{
	int i, j, k, l;
	bool add_basket;

	baskets_for_customer_and_period.clear();
	baskets_for_customer_and_period.resize(nPeriods - 1, vector<vector<vector<bool> > >(baskets_for_customer.size()));

	for (i = 0; i < baskets_for_customer.size(); ++i)
	{
		//cout << "vorher: " << baskets_for_customer.at(i).size() << endl;
		for (j = 0; j < baskets_for_customer.at(i).size(); ++j)
		{
			for (k = 0; k < nPeriods - 1; ++k)
			{
				add_basket = true;
				for (l = 0; l < baskets_for_customer.at(i).at(j).size(); ++l)
				{
					if (baskets_for_customer.at(i).at(j).at(l) && !m_is_demand_for_customer_and_product_in_period.at(i).at(l).at(k))
					{
						add_basket = false;
						break;
					}
				}
				if (add_basket)
				{
					baskets_for_customer_and_period.at(k).at(i).push_back(baskets_for_customer.at(i).at(j));
				}

			

				/*cout << i << " ";
				for (l = 0; l < baskets_for_customer_and_period.at(k).at(i).at(j).size(); ++l)
				{
					if (baskets_for_customer_and_period.at(k).at(i).at(j).at(l))
					{
						cout << l << " ";
					}
				}
				cout << endl;
				char oi;
				cin >> oi;*/
			}
		}

		/*for (k = 0; k < nPeriods - 1; ++k)
		{
			cout << "Periode " << k << ": " << baskets_for_customer_and_period.at(k).at(i).size() << endl;
		}

		char oi;
		cin >> oi;*/
	}

	

	/*for (j = 0; j < baskets_for_customer_and_period.at(0).at(2061).size(); ++j)
	{
		for (l = 0; l < baskets_for_customer_and_period.at(0).at(2061).at(j).size(); ++l)
		{
			if (baskets_for_customer_and_period.at(0).at(2061).at(j).at(l))
			{
				cout << l << " ";
			}
		}
		cout << endl;

		char op;
		cin >> op;
	}*/
}


void solveit(const vector<string> & plants, 
			 const vector<string> & customers, 
			 const vector<string> & resources, 
			 const vector<string> & products, 
			 const vector<string> & periods, 
			 const vector<production> & processes, 
			 const vector<vector<double> > & capacity, 
			 const vector<transports> & transport_plant, 
			 const vector<transports> & transport_customer, 
			 const vector<demand> & demands, 
			 const vector<double> & nPallets_per_ton, 
			 vector<production_result> & solution_production, 
			 vector<customer_group_production_result> & customer_group_solution_production,
			 vector<single_transport_result_plant> & solution_single_transport_plant, 
			 vector<single_customer_group_transport_result_plant> & customer_group_solution_single_transport_plant,
			 vector<single_transport_result_customer> & solution_single_transport_customer, 
			 vector<dummy_result> & solution_dummy, 
			 vector<inventory_result> & solution_inventory, 
			 vector<customer_group_inventory_result> & customer_group_solution_inventory,
			 const vector<vector<double> > & fixations,
			 const vector<vector<int> > & baskets, 
			 const vector<vector<int> > & basket_coherence, 
			 const vector<int> & customer_for_basket, 
			 const vector<vector<double> > & start_inventory, 
			 const vector<vector<double> > & min_inventory_v2, 
			 const vector<vector<vector<double> > > & min_inventory_v1, 
			 const vector<int> & plant_capacities, 
			 vector<vector<pair<double, double> > > & solution_external_warehouse_info, 
			 const vector<fixed_transport> & fixed_transports, 
			 const string & expansion_stage,
			 vector<demand> & minInventoryUsageFinalPeriod,
			 //const double fixationThreshold,
			 WEPA_Opt_Params & usedParams,
			 //const vector<article_customer_fixations> & customer_fixations
			 Article_customer_fixation_collection & custFixations,
			 Customer_product_related_transports_collection & cpTransports,
			 const vector<int> & resourcesWithFixations,
			 list<int> & solution_additionalProcessesPlantFixations,
			 std::string &logfilename
			 )
{
	cout << endl;
	
	int nPlants = plants.size();
	int nCustomers = customers.size();
	int nResources = resources.size();
	int nProducts = products.size();
	int nPeriods = periods.size()+1;
	int nProcesses = processes.size();

	vector<vector<bool> > solutions;
	vector<vector<double> > dummies;
	solution_external_warehouse_info.resize(nPlants,vector<pair<double,double> >(nPeriods,pair<double,double>(0.0,0.0)));

	/*char oi;
	cin >> oi;*/

	cout << "constructor model..." << endl;

	Wepa* wepa = new Wepa(nPlants, 
							plants, 
							nCustomers, 
							customers, 
							nResources, 
							resources, 
							nProducts, 
							products, 
							nPeriods, 
							periods, 
							nProcesses, 
							processes, 
							capacity, 
							transport_plant, 
							transport_customer, 
							demands, 
							minInventoryUsageFinalPeriod,
							nPallets_per_ton, 
							usedParams.min_runtime, 
							usedParams.min_for_split, 
							usedParams.lkw_capacity, 
							baskets, 
							basket_coherence,
							customer_for_basket, 
							start_inventory, 
							min_inventory_v2, 
							min_inventory_v1, 
							plant_capacities,
							fixations,
							usedParams.set_fixations, 
							usedParams.external_warehouse_cost, 
							fixed_transports, 
							logfilename, 
							expansion_stage,
							usedParams.fixationThreshold,
							//customer_fixations
							custFixations,
							resourcesWithFixations,
							cpTransports
							);

	//cin >> oi;

	//double epgap_start = 0.01;//0.01;//0.005;
	//double epgap_start_fixations = 0.005;
	//double epgap_end = 0.01;//0.01;//0.005;
	//double used_epgap;
	bool changed = false;
	bool polish = false;
	int i;

	cout << "Preprocessing..." << endl;

	//if (set_fixations)
	//{
		wepa->adapt_start_inventory();
	//}

	wepa->preprocessing();

	cout << "Anpassen der Mindestlosgroessen..." << endl;
	wepa->adapt_minimum_production();
	if (usedParams.split_injure)
	{
		cout << "Anpassen der Mindestlosgroessen f�r Splittung" << endl;
		wepa->adapt_split_injuries();
	}

	tm.add_event("Preprocessing");
	
	/*
	cout << "init variables..." << endl;
	wepa->init_vars();
	cout << endl;

	cout << "init constraints..." << endl;
	wepa->init_cons();

	cout << endl;

	if (fix_plant_production)
	{
		cout << "fix plant production..." << endl;
		wepa->fix_plant_production();
		cout << endl;
	}
	
	if (set_fixations)
	{
		cout << "fixations..." << endl;
		wepa->set_production_fixations(fixations);
		used_epgap =epgap_start_fixations;
	}
	else
	{
		cout<<"fixations according to threshold"<<endl;
		wepa->set_production_fixations_on_threshold (fixations);
		used_epgap =epgap_start;
	}
	cout<<"Verwendeter Gap:"<<used_epgap;
	cout << endl;
	cout << "solving model..." << endl;
	

	tm.add_event("Modellierung");
	
//************************************************************SPEICHERBEDARF*********************************************************************************************************************************************
//#ifndef DEBUG
//	if (speicherverbrauch() > 1000000000)
//	{
//		cout << "Speicherverbrauch zu hoch!" << endl;
//		cout << "Pruefen Sie die Input-Daten!" << endl;
//		cout << "Abbruch!" << endl;
//		exit(1000);
//	}
//#endif
//************************************************************SPEICHERBEDARF*********************************************************************************************************************************************
	

	wepa->solve_model(used_epgap,false,gap);//(epgap_start, false, gap);
	cout << endl;

	tm.add_event("Loesung ohne Warenkoerbe");

	if (use_baskets && just_production_cost == false)
	{
		cout << "injured baskets..." << endl;
		wepa->separate_baskets_part1(solutions);
		cout << endl;

		int model = 0;

		changed = wepa->separate_baskets_part2(solutions);

		while (changed)
		{
			model++;
			changed = false;
			
//#ifndef DEBUG
			//************************************************************SPEICHERBEDARF*********************************************************************************************************************************************
//			if (speicherverbrauch() > 1000000000)
//			{
//				cout << "Speicherverbrauch zu hoch!" << endl;
//				cout << "Pruefen Sie die Input-Daten!" << endl;
//				cout << "Abbruch!" << endl;
//				exit(1000);
//			}
//			************************************************************SPEICHERBEDARF*********************************************************************************************************************************************
//#endif
			
			wepa->solve_model(used_epgap,polish,gap);//(epgap_start, polish, gap);
			wepa->separate_baskets_part1(solutions);
			changed = wepa->separate_baskets_part2(solutions);
	
			if (changed == false)
			{
				break;
			}
		}
	}
	*/
	/*
	

						 cout << endl;
	cout << "write results..." << endl;

 	tm.add_event("Separation der Warenkoerbe");

	for (i = 1; i < nPeriods; ++i)
	{ 
		

		wepa->write_results_for_period(solution_production,
										customer_group_solution_production,
									   solution_single_transport_plant,
									   customer_group_solution_single_transport_plant,
									   solution_single_transport_customer, 
									   solution_dummy, 
									   solution_inventory,
									   customer_group_solution_inventory,
									   i, 
									   solution_external_warehouse_info
									   );
	}

	tm.add_event("Output");

	tm.end();
*/
//usedParams.customer_prioritized_fixations_usage
	wepa->solve_model (usedParams.ePGap,polish,usedParams.gap,usedParams.fix_plant_production,usedParams.use_baskets,usedParams.set_fixations,usedParams.just_production_cost,tm,solution_production,customer_group_solution_production,
	solution_single_transport_plant,customer_group_solution_single_transport_plant,solution_single_transport_customer ,solution_dummy,solution_inventory,customer_group_solution_inventory,
	solution_external_warehouse_info,solution_additionalProcessesPlantFixations,usedParams.customer_prioritized_fixations_usage,false);

	//std::cout<<"Number of additional processes in solveit:"<<solution_additionalProcessesPlantFixations.size()<<std::endl;
	
	//delete wepa;
}

/*
void get_written_data(vector<string> & plants, 
					  vector<string> & customers, 
					  vector<string> & resources, 
					  vector<string> & products, 
					  vector<string> & periods, 
					  vector<production> & processes, 
					  vector<vector<double> > & capacity, 
					  vector<transports> & transport_plant, 
					  vector<transports> & transport_customer, 
					  vector<demand> & demands, 
					  vector<double> & nPallets_per_ton, 
					  vector<vector<double> > & fixations, 
					  vector<vector<double> > & productions, 
					  vector<vector<vector<bool> > > & baskets_for_customer, 
					  vector<string> & product_names, 
					  vector<int> & plant_capacities, 
					  vector<vector<double> > & start_inventory, 
					  vector<vector<vector<double> > > & minimum_inventory_v1, 
					  vector<vector<double> > & minimum_inventory_v2, 
					  vector<fixed_transport> & fixed_transports,
					  //vector<article_customer_fixations> & customer_fixations,
					  Article_customer_fixation_collection & custFixations,
					  Customer_product_related_transports_collection & cpTransports,
					  vector<int> & resourcesWithFixations
					  )
{
	plants.clear();
	customers.clear();
	resources.clear();
	products.clear();
	periods.clear();
	processes.clear();
	capacity.clear();
	transport_plant.clear();
	transport_customer.clear();
	demands.clear();
	nPallets_per_ton.clear();
	fixations.clear();
	baskets_for_customer.clear();
	product_names.clear();
	productions.clear();
	fixed_transports.clear();
	
	//customer_fixations.clear ();
	string line;
	string word;
	string product, machine, tonspershift, costspershift, plant;
	string start, end, cost;
	string customer, period, amount;

	
	cout << "Werke..." << endl;
	ifstream plantdata(plantfile.c_str());
	
	if (plantdata.fail())
	{
		cout << "Kann Datei " << plantfile << " nicht oeffnen!" << endl;
		exit(1);
	}
	while (!plantdata.eof())
	{
		getline(plantdata, line);
		if (line.size() > 0)
		{
			istringstream is(line);
			is >> word;
			plants.push_back(word);
		}
	}
	plantdata.close();

	cout << "Kunden..." << endl;
	ifstream customerdata(customerfile.c_str());
	if (customerdata.fail())
	{
		cout << "Kann Datei " << customerfile << " nicht oeffnen!" << endl;
		exit(1);
	}

	string name;
	while (!customerdata.eof())
	{
		getline(customerdata, line);
		if (line.size() > 0)
		{
			istringstream is(line);
			name = "";
			while (!is.eof())
			{
				is >> word;
				name = name + word;
			}
			customers.push_back(name);
		}
	}
	customerdata.close();

	cout << "Maschinen..." << endl;
	ifstream resourcedata(resourcefile.c_str());
	if (resourcedata.fail())
	{
		cout << "Kann Datei " << resourcefile << " nicht oeffnen!" << endl;
		exit(1);
	}
	while (!resourcedata.eof())
	{
		getline(resourcedata, line);
		if (line.size() > 0)
		{
			istringstream is(line);
			is >> word;
			resources.push_back(word);
		}
	}
	resourcedata.close();

	cout << "Produkte..." << endl;
	ifstream productdata(productfile.c_str());
	if (productdata.fail())
	{
		cout << "Kann Datei " << productfile << " nicht oeffnen!" << endl;
		exit(1);
	}
	while (!productdata.eof())
	{
		getline(productdata, line);
		if (line.size() > 0)
		{
			istringstream is(line);
			is >> word;
			products.push_back(word);
		}
	}
	productdata.close();

	cout << "Artikelnamen..." << endl;
	vector<pair<string, string> > artikelnamepair;
	ifstream artikelnamendata(artikelnamenfile.c_str());
	if (artikelnamendata.fail())
	{
		cout << "Kann Datei " << artikelnamenfile << " nicht oeffnen!" << endl;
		exit(1);
	}
	while (!artikelnamendata.eof())
	{
		getline(artikelnamendata, line);
		if (line.size() > 0)
		{
			vector<string> daten;
			istringstream is(line);
			string dummy1;
			string dummy2;
			is >> dummy1;
			while (!is.eof())
			{
				is >> dummy2;
				daten.push_back(dummy2);
			}
			string name;
			int i;
			for (i = 0; i < daten.size(); ++i)
			{
				name += daten.at(i) + " ";
			}
			//readin_explode(line, daten, ',');
			//if (daten.size() > 1)
			//{
				artikelnamepair.push_back(pair<string, string>(dummy1, name));
			//}
		}
	}
	artikelnamendata.close();

	int i, j;
	product_names.resize(products.size());
	for (i = 0; i < products.size(); ++i)
	{
		for (j = 0; j < artikelnamepair.size(); ++j)
		{
			if (products.at(i) == artikelnamepair.at(j).first)
			{
				product_names.at(i) = artikelnamepair.at(j).second;
			}
		}
	}

	cout << "Perioden..." << endl;
	ifstream perioddata(periodfile.c_str());
	if (perioddata.fail())
	{
		cout << "Kann Datei " << periodfile << " nicht oeffnen!" << endl;
		exit(1);
	}
	while (!perioddata.eof())
	{
		getline(perioddata, line);
		if (line.size() > 0)
		{
			istringstream is(line);
			is >> word;
			periods.push_back(word);
		}
	}
	perioddata.close();

	cout << "Produktionsprozesse..." << endl;
	ifstream processdata(processfile.c_str());
	if (processdata.fail())
	{
		cout << "Kann Datei " << processfile << " nicht oeffnen!" << endl;
		exit(1);
	}
	production dummy_production;
	while (!processdata.eof())
	{
		getline(processdata, line);
		if (line.size() > 0)
		{
			istringstream is(line);
			is >> dummy_production.product;
			is >> dummy_production.machine;
			is >> dummy_production.tonspershift;
			is >> dummy_production.costspershift;
			is >> dummy_production.product_plant_index;

			if (dummy_production.product < 0)
			{
				cout << "Negativer Produktindex " << dummy_production.product << " in " << processfile << endl;
				exit(44);
			}
			if (dummy_production.product >= products.size())
			{
				cout << "Produktindex " << dummy_production.product << " in " << processfile << " zu gross!" << endl;
				exit(44);
			}
			if (dummy_production.machine < 0)
			{
				cout << "Negativer Maschinenindex " << dummy_production.machine << " in " << processfile << endl;
				exit(45);
			}
			if (dummy_production.machine >= resources.size())
			{
				cout << "Maschinenindex " << dummy_production.machine << " in " << processfile << " zu gross!" << endl;
				exit(45);
			}
			if (dummy_production.tonspershift <= 0)
			{
				cout << "Nicht-positive Tonnen-pro-Schicht-Angabe " << dummy_production.tonspershift << " in " << processfile << endl;
				exit(46);
			}
			if (dummy_production.costspershift < 0)
			{
				cout << "Negative Kosten-pro-Schicht-Angabe " << dummy_production.costspershift << " in " << processfile << endl;
				exit(47);
			}
			if (dummy_production.product_plant_index < 0)
			{
				cout << "Negativer Werksindex " << dummy_production.product_plant_index << " in " << processfile << endl;
				exit(48);
			}
			if (dummy_production.product_plant_index >= plants.size())
			{
				cout << "Werksindex " << dummy_production.product_plant_index << " in " << processfile << " zu gross!" << endl;
				exit(48);
			}
			processes.push_back(dummy_production);
		}
	}
	processdata.close();

	cout << "Maschinenkapazit�ten..." << endl;
	ifstream capacitydata(capacityfile.c_str());
	if (capacitydata.fail())
	{
		cout << "Kann Datei " << capacityfile << " nicht oeffnen!" << endl;
		exit(1);
	}
	i = 0;
	capacity.resize(resources.size(), vector<double>(periods.size()));

	while (!capacitydata.eof())
	{
		getline(capacitydata, line);
		if (line.size() > 0)
		{
			istringstream is(line);
			j = 0;
			while (!is.eof())
			{
				word = "";
				is >> word;
				if (word.size() > 0)
				{
					if (i >= resources.size())
					{
						cout << "Zu viele Kapazitaeten in " << capacityfile << endl;
						exit(23);
					}
					if (j >= periods.size())
					{
						cout << "Zu viele Kapazitaeten fuer Maschine " << resources.at(i) << " in " << capacityfile << endl;
						exit(22);
					}
					capacity.at(i).at(j) = capacity_factor * atof(word.c_str());
					if (capacity.at(i).at(j) < 0)
					{
						cout << "negative Kapazitaet fuer Maschine " << resources.at(i) << " in Periode " << periods.at(j) << " in " << capacityfile << endl;
						exit(21);
					}
					j++;
				}

			}
			if (j < periods.size())
			{
				cout << "Zu wenige Kapazitaeten fuer Maschine " << resources.at(i) << " in " << capacityfile << endl;
				exit(24);
			}
			i++;
		}
	}

	if (i < resources.size())
	{
		cout << "Zu wenige Kapazitaeten in " << capacityfile << endl;
		exit(25);
	}
	capacitydata.close();

	cout << "Werkstransporte..." << endl;
	ifstream transportplantdata(transportplantfile.c_str());
	if (transportplantdata.fail())
	{
		cout << "Kann Datei " << transportplantfile << " nicht oeffnen!" << endl;
		exit(1);
	}
	transports dummy_transport;
	vector<vector<bool> > plant_transports(plants.size(), vector<bool>(plants.size(), false));
	while (!transportplantdata.eof())
	{
		getline(transportplantdata, line);
		{
			if (line.size() > 0)
			{
				istringstream is(line);
				is >> dummy_transport.start;
				is >> dummy_transport.end;
				is >> dummy_transport.cost;

				if (just_production_cost)
				{
					dummy_transport.cost = 0.0;
				}

				if (dummy_transport.start < 0)
				{
					cout << "Negativer IC-Startindex " << dummy_transport.start << " in " << transportplantfile << endl;
					exit(26);
				}
				if (dummy_transport.start >= plants.size())
				{
					cout << "IC-Startindex " << dummy_transport.start << " in " << transportplantfile << " zu gross!" << endl;
					exit(26);
				}
				if (dummy_transport.end < 0)
				{
					cout << "Negativer IC-Endindex " << dummy_transport.end << " in " << transportplantfile << endl;
					exit(27);
				}
				if (dummy_transport.end >= plants.size())
				{
					cout << "IC-Endindex " << dummy_transport.end << " in " << transportplantfile << " zu gross!" << endl;
					exit(27);
				}
				if (dummy_transport.cost < 0.0)
				{
					cout << "negative IC-Kosten" << " in " << transportplantfile << endl;
					exit(28);
				}

				plant_transports.at(dummy_transport.start).at(dummy_transport.end) = true;
				transport_plant.push_back(dummy_transport);

			}
		}
	}
	transportplantdata.close();

	

	for (i = 0; i < plants.size(); ++i)
	{
		for (j = 0; j < plants.size(); ++j)
		{
			if (!plant_transports.at(i).at(j) && i != j)
			{
				cout << "Keine Transportinformationen von " << plants.at(i) << " nach " << plants.at(j) << " vorhanden!" << endl;
				//exit(71);
			}
		}
	}

	std::cout << "Kundentransporte..." << endl;
	ifstream transportcustomerdata(transportcustomerfile.c_str());
	if (transportcustomerdata.fail())
	{
		cout << "Kann Datei " << transportcustomerfile << " nicht oeffnen!" << endl;
		exit(1);
	}
	while (!transportcustomerdata.eof())
	{
		getline(transportcustomerdata, line);
		if (line.size() > 0)
		{

			istringstream is(line);
			is >> dummy_transport.start;
			is >> dummy_transport.end;
			is >> dummy_transport.cost;

			if (just_production_cost)
			{
				dummy_transport.cost = 0.0;
			}

			if (dummy_transport.start < 0)
			{
				cout << "Negativer Kundentransport-Startindex " << dummy_transport.start << " in " << transportcustomerfile << endl;
				exit(29);
			}
			if (dummy_transport.start >= plants.size())
			{
				cout << "Kundentransport-Startindex " << dummy_transport.start << " in " << transportcustomerfile << " zu gross!" << endl;
				exit(29);
			}
			if (dummy_transport.end < 0)
			{
				cout << "Negativer Kundentransport-Endindex " << dummy_transport.end << " in " << transportcustomerfile << endl;
				exit(30);
			}
			if (dummy_transport.end >= customers.size())
			{
				cout << "Kundentransport-Endindex " << dummy_transport.end << " in " << transportcustomerfile << " zu gross!" << endl;
				exit(30);
			}
			if (dummy_transport.cost < 0.0)
			{
				cout << "negative Kundentransport-Kosten in " << transportcustomerfile << endl;
				exit(31);
			}

			transport_customer.push_back(dummy_transport);
		}
	}
	transportcustomerdata.close();

	ifstream customerProductRelatedTransportData (customer_product_related_transport_file );
	if (customerProductRelatedTransportData.fail())
	{
		cout << "Kann Datei " << transportcustomerfile << " nicht oeffnen!" << endl;
		exit(1);
	}

	int currProduct;
	int currTransProcess;
	int currCustomer;
	int currPlant;
	int currEntryNum;
	while (!customerProductRelatedTransportData.eof())
	{
		getline(customerProductRelatedTransportData, line);

		if (line.size() > 0)
		{
			istringstream is(line);

			is>> currEntryNum;
			is >> currProduct;
			is >> currTransProcess;

			currCustomer=transport_customer.at(currTransProcess).end;
			currPlant=transport_customer.at(currTransProcess).start;
			cpTransports.add (currCustomer,currProduct,currPlant);
		}
	}
	customerProductRelatedTransportData.close ();

	m_is_demand.resize(products.size());
	m_is_demand_for_customer_and_product.resize(customers.size(), vector<bool>(products.size()));
	m_is_demand_for_customer_and_product_in_period.resize(customers.size(), vector<vector<bool> >(products.size(), vector<bool>(periods.size())));
	demand_for_product_in_period.resize(products.size(), vector<double>(periods.size()));

	cout << "Bedarfe..." << endl;
	ifstream demanddata(demandfile.c_str());
	if (demanddata.fail())
	{
		cout << "Kann Datei " << demandfile << " nicht oeffnen!" << endl;
		exit(1);
	}
	demand dummy_demand;
	
	
	while (!demanddata.eof())
	{
		getline(demanddata, line);
		if (line.size() > 0)
		{
			istringstream is(line);
			is >> dummy_demand.customer;
			is >> dummy_demand.product;
			is >> dummy_demand.period;
			is >> dummy_demand.amount;
			//dummy_demand.amount = round(demand, 0);

			
			
			if (dummy_demand.customer < 0)
			{
				cout << "Negativer Kundenindex " << dummy_demand.customer << " in " << demandfile << endl;
				exit(32);
			}
			if (dummy_demand.customer >= customers.size())
			{
				cout << "Kundenindex " << dummy_demand.customer << " in " << demandfile << " zu gross!" << endl;
				exit(32);
			}

			if (dummy_demand.product < 0)
			{
				cout << "Negativer Produktindex " << dummy_demand.product << " in " << demandfile << endl;
				exit(33);
			}
			if (dummy_demand.product >= products.size())
			{
				cout << "Produktindex " << dummy_demand.product << " in " << demandfile << " zu gross!" << endl;
				exit(33);
			}

			if (dummy_demand.period < 0)
			{
				cout << "Negativer Periodenindex " << dummy_demand.period << " in " << demandfile << endl;
				exit(34);
			}
			if (dummy_demand.period >= periods.size())
			{
				cout << "Periodenindex " << dummy_demand.period << " in " << demandfile << " zu gross!" << endl;
				exit(34);
			}

			if (dummy_demand.amount < 0)
			{
				cout << " nicht positiver Bedarf " << dummy_demand.amount << " in " << demandfile << endl;
				exit(35);
			}

			if (dummy_demand.amount >= 1)
			{
				m_is_demand.at(dummy_demand.product) = true;
				m_is_demand_for_customer_and_product.at(dummy_demand.customer).at(dummy_demand.product) = true;
				m_is_demand_for_customer_and_product_in_period.at(dummy_demand.customer).at(dummy_demand.product).at(dummy_demand.period) = true;

				
			}
			demand_for_product_in_period.at(dummy_demand.product).at(dummy_demand.period) += dummy_demand.amount;

			demands.push_back(dummy_demand);
		}
	}
	demanddata.close();

	cout << "Paletten pro Tonne..." << endl;
	ifstream palletspertondata(palletspertonfile.c_str());
	if (palletspertondata.fail())
	{
		cout << "Kann Datei " << palletspertonfile << " nicht offnen!" << endl;
		exit(1);
	}
	nPallets_per_ton.resize(products.size());
	i = 0;
	while (!palletspertondata.eof())
	{
		getline(palletspertondata, line);
		if (line.size() > 0)
		{
			istringstream is(line);
			is >> word;

			if (i >= products.size())
			{
				cout << "Anzahl der Zeilen in " << palletspertonfile << " zu gross!" << endl;
				exit(36);
			}

			nPallets_per_ton.at(i) = atof(word.c_str());

			if (nPallets_per_ton.at(i) <= 0)
			{
				cout << "Nicht-positiver Paletten-pro-Tonne-Wert " << nPallets_per_ton.at(i) << " fuer Produkt " << products.at(i) << " in " << palletspertonfile << endl;
				exit(37);
			}

			i++;
		}
	}
	if (i < products.size())
	{
		cout << "Anzahl der Zeilen in " << palletspertonfile << " zu klein!" << endl;
		exit(38);
	}
	palletspertondata.close();

	cout << "Warenkoerbe..." << endl;
	
	int customerdummy;
	baskets_for_customer.clear();
	baskets_for_customer.resize(customers.size());

	ifstream basketsdata(basketsfile.c_str());
	if (basketsdata.fail())
	{
		cout << "Kann Datei " << basketsfile << " nicht oeffnen!" << endl;
		exit(1);
	}

	while (!basketsdata.eof())
	{
		getline(basketsdata, line);
		
		if (line.size() > 0)
		{
			istringstream is(line);
			is >> customerdummy;
			vector<bool> basket_products(products.size(), false);
			if (customerdummy < 0)
			{
				cout << "Negativer Kundenindex " << customerdummy << " in " << basketsfile << endl;
				exit(39);
			}
			if (customerdummy >= customers.size())
			{
				cout << "Kundenindex " << customerdummy << " in " << basketsfile << " zu gross!" << endl;
				exit(39);
			}
			while (!is.eof())
			{
				word = "";
				is >> word;
				if (word.size() > 0)
				{
					basket_products.at(atoi(word.c_str())) = true;
				}
			}

			baskets_for_customer.at(customerdummy).push_back(basket_products);
		}
	}
	basketsdata.close();

	cout << "historische Produktionen..." << endl;
	int processindex;
	int periodindex;
	double amount_in_pallets;
	fixations.resize(processes.size(), vector<double>(periods.size(), -1.0));
	productions.resize(processes.size(), vector<double>(periods.size(), 0));
	ifstream fixationdata(fixationfile.c_str());
	if (fixationdata.fail())
	{
		cout << "Kann Datei " << fixationfile << " nicht oeffnen!" << endl;
		exit(1);
	}
	else
	{
		while (!fixationdata.eof())
		{
			getline(fixationdata, line);
			if (line.size() > 0)
			{
				istringstream is(line);
				is >> processindex;
				is >> periodindex;
				is >> amount_in_pallets;

				if (processindex < 0)
				{
					cout << "Negativer Prozessindex " << processindex << " in " << fixationfile << endl;
					exit(41);
				}
				if (processindex >= processes.size())
				{
					cout << "Prozessindex " << processindex << " in " << fixationfile << " zu gross!" << endl;
					exit(41);
				}
				if (periodindex < 0)
				{
					cout << "Negativer Periodenindex " << periodindex << " in " << fixationfile << endl;
					exit(42);
				}
				if (periodindex >= periods.size())
				{
					cout << "Periodenindex " << periodindex << " in " << fixationfile << " zu gross!" << endl;
					exit(42);
				}
				if (amount_in_pallets <= 0)
				{
					cout << "Nicht positiver Bedarf " << amount_in_pallets << " in " << fixationfile << endl;
					exit(43);
				}

				if (m_is_demand.at(processes.at(processindex).product))
				{
					fixations.at(processindex).at(periodindex) = amount_in_pallets / (double)nPallets_per_ton.at(processes.at(processindex).product) / (double)processes.at(processindex).tonspershift;

					productions.at(processindex).at(periodindex) += amount_in_pallets;

				
				}
			}
		}
		fixationdata.close();
	}

	cout << "Werkskapazitaeten..." << endl;
	plant_capacities.clear();
	plant_capacities.resize(plants.size(),0);
	ifstream plant_capacitydata(plant_capacityfile.c_str());

	if (plant_capacitydata.fail())
	{
		cout << "Kann Datei " << plant_capacityfile << " nicht oeffnen!" << endl;
		exit(1);
	}

	int capacityint;
	int plantindex;
	while (!plant_capacitydata.eof())
	{
		getline(plant_capacitydata, line);

		if (line.size() == 0)
		{
			continue;
		}
		
		istringstream is(line);
		is >> plantindex;
		is >> capacityint;

		if (plantindex < 0)
		{
			cout << "negativer Werksindex in " << plant_capacityfile << endl;
			exit(61);
		}
		if (capacityint < 0)
		{
			cout << "negative Werkskapazitaet in " << plant_capacityfile << endl;
			exit(62);
		}

		plant_capacities.at(plantindex) = capacityint;
	}
	
	plant_capacitydata.close();

	cout << "Startbestaende..." << endl;
	start_inventory.resize(plants.size(), vector<double>(products.size(), 0.0));
	ifstream start_inventorydata(start_inventoryfile.c_str());
	int productindex;

	double amountint;
	if (start_inventorydata.fail())
	{
		cout << "Kann Datei " << start_inventoryfile << " nicht oeffnen!" << endl;
		exit(1);
	}
	while (!start_inventorydata.eof())
	{
		getline(start_inventorydata, line);
		if (line.size() == 0)
		{
			continue;
		}
		istringstream is(line);
	
		is >> plantindex;
		if (plantindex < 0 || plantindex >= plants.size())
		{
			cout << "Fehlerhafter Werksindex " << plantindex << " in Datei " << start_inventoryfile << endl;
			exit(14);
		}
		is >> productindex;
		if (productindex < 0 || productindex >= products.size())
		{
			cout << "Fehlerhafter Produktindex " << productindex << " in Datei " << start_inventoryfile << endl;
			exit(15);
		}
		is >> amountint;
		if (amountint < 0)
		{
			cout << "Fehlerhafter Startbestand " << amountint << " in Datei " << start_inventoryfile << endl;
			exit(16);
		}
		start_inventory.at(plantindex).at(productindex) = amountint;
	}
	start_inventorydata.close();

	if (customer_fixation_usage && !set_fixations)
	{
		cout << "Kundenfixierungen..." << endl;
		ifstream customer_fixation_data(customer_fixation_group_file .c_str ());
		if (customer_fixation_data.fail())
		{
			cout << "Kann Datei " << customer_fixation_data << " nicht oeffnen!" << endl;
			exit(1);
		}
		int currGroup,lastGroup;
		article_customer_fixations currACFs;
		//fixation_process currFProc;
		int currFProc;
		int prodI;
		int custLocI;
		int rsI;
		int plantI;
		currGroup=lastGroup =-1;
		while (!customer_fixation_data.eof())
		{	
			getline(customer_fixation_data, line);
			if (line.size ()==0)
				continue;

			istringstream is(line);
			is>> currGroup ;
			if (currGroup <0 || currGroup <lastGroup )
			{
				cout<<"Fehlerhafter Gruppenindex "<<currGroup<<" in Datei "<<customer_fixation_group_file <<endl;
				exit(17); //Fehlercode pr�fen !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			}
			is>>prodI ;
			is>>custLocI ;
			is>>currFProc;//currFProc.plant;
			//is>>currFProc .resource;
			
			if (prodI<0 || prodI>=products .size ())
			{
				cout << "Fehlerhafter Produktindex " << prodI << " in Datei " << customer_fixation_group_file << endl;
				exit(15);
			}

			if (custLocI<0 || custLocI>=customers .size ())
			{
				cout << "Fehlerhafter Kundenindex " << custLocI << " in Datei " << customer_fixation_group_file << endl;
				exit(15);
			}

			if (currFProc <0 || currFProc >=processes .size () )
			{
				cout << "Fehlerhafter Prozessindex " <<currFProc << " in Datei " << customer_fixation_group_file << endl;
				exit(15);
			}

			if (currGroup !=lastGroup )
			{
				if (lastGroup >=0)
				{
					//customer_fixations.push_back (currACFs);
					custFixations .add (currACFs);
				}

				currACFs.allowedProcesses .clear ();
				currACFs.customerLocations .clear ();
				currACFs .article =prodI;
				lastGroup =currGroup;
			}
			
			currACFs.addCustomerLocation (custLocI);
			currACFs.addAllowedProcess (currFProc);
			//currACFs .customerLocations.push_back (custLocI );
			//currACFs .allowedProcesses .push_back (currFProc);
		}

		customer_fixation_data.close();
		
		if (currGroup>=0)
		{
			//customer_fixations.push_back (currACFs);
			custFixations .add(currACFs);
		}

		ifstream resources_with_fixations_data(resources_with_fixations_file .c_str ());
		if (resources_with_fixations_data.fail())
		{
			cout << "Kann Datei " <<resources_with_fixations_data << " nicht oeffnen!" << endl;
			cout<<"Ignoring file for compatibility"<<std::endl;
			//exit(1);
		}
		else
		{
			while(!resources_with_fixations_data.eof())
			{
				int resourceI;
				getline(resources_with_fixations_data,line);
				if (line.size()==0)
					continue;
				istringstream is(line);
				is>>resourceI;
				if (resourceI<0 || resourceI>resources.size())
				{
					cout << "Fehlerhafter Ressourcenindex " <<resourceI << " in Datei " << resources_with_fixations_file << endl;
					exit(16);
				}
				resourcesWithFixations.push_back(resourceI);
			}

			resources_with_fixations_data.close();
		}
	}


}
*/


void set_minimum_inventory_v2(vector<demand> demands, vector<vector<double> > & minimum_inventory_v2, const int & nProducts, const int & nPeriods, const vector<production> & processes,vector<demand> & finalPeriodMinInvUsages,WEPA_Opt_Params & usedParams)
{
	int i,j;

	minimum_inventory_v2.clear();
	minimum_inventory_v2.resize(nProducts,vector<double>(nPeriods,0.0));

	j=0;
	for (i = 0; i < demands.size(); ++i)
	{
		if (demands.at(i).period > 0)
		{
			minimum_inventory_v2.at(demands.at(i).product).at(demands.at(i).period-1) += usedParams.minimum_inventory_factor * demands.at(i).amount;
		}
		if (demands.at(i).period == nPeriods-1)
		{
			minimum_inventory_v2.at(demands.at(i).product).at(demands.at(i).period) += usedParams.minimum_inventory_factor * demands.at(i).amount;

			finalPeriodMinInvUsages .push_back (demands.at(i));

			//finalPeriodMinInvUsages.at(i)=demands.at(i);
			finalPeriodMinInvUsages.at(j).amount =finalPeriodMinInvUsages.at(j).amount*usedParams.minimum_inventory_factor;
			j++;
		}
	}
}

void set_plant_capacities(const vector<string> & plants, vector<int> & plant_capacities)
{
	int i;
	plant_capacities.resize(plants.size(),0);

	for (i = 0; i < plants.size(); ++i)
	{
		if (plants.at(i) == "PL58573")
		{
			plant_capacities.at(i) = 24000;
		}
		else if (plants.at(i) == "DE59757"/*"M�schede"*/)
		{
			plant_capacities.at(i) = 33000;
		}
		else if (plants.at(i) == "DE34431"/*"Giershagen"*/)
		{
			plant_capacities.at(i) = 43000;
		}
		else if (plants.at(i) == "DE06237"/*"Leuna"*/)
		{
			plant_capacities.at(i) = 28000;
		}
		else if (plants.at(i) == "DE55120"/*"Mainz"*/)
		{
			plant_capacities.at(i)= 15820;
		}
		else if (plants.at(i) == "IT55100" /*Lucca bzw. Salanetti*/)
		{
			plant_capacities.at(i) = 32800;
		}
		else if (plants.at(i) == "IT03043" /*Cassino*/)
		{
			plant_capacities.at(i) = 38000;
		}
		else if (plants.at(i) == "DE09648"/*"Kriebstein"*/)
		{
			plant_capacities.at(i) = 15000;
		}
		else if (plants.at(i) == "FR59166"/*"Lille"*/)
		{
			plant_capacities.at(i) = 43333;
		}
		else if (plants.at(i) == "ES50600"/*"Ejea"*/)
		{
			plant_capacities.at(i) = 15000;
		}
		else if (plants.at(i) == "DE18147"/*"Rostock"*/)
		{
			plant_capacities.at(i) = 0;
		}
		else if (plants.at(i) == "FR10000")
		{
			plant_capacities.at(i) = 24000;
		}
		else
		{
			cout << "Keine Kapazitaetsinformationen fuer " << plants.at(i) << "vorhanden!" << endl;
			exit(200);
		}
	}
}



void output_db(std::string &optFolderName,
			std::string &userName,
			int optRunNr,
			const vector<string> & resources, 
			const vector<string> & periods, 
			const vector<string> & products, 
			const vector<string> & plants, 
			const vector<string> & customers, 
			const vector<production_result> & solution_production,
			const vector<customer_group_production_result> & customer_group_solution_production,
			const vector<single_transport_result_plant> & solution_single_transport_plant, 
			const vector<single_customer_group_transport_result_plant> & customer_group_solution_single_transport_plant,
			const vector<single_transport_result_customer> & solution_single_transport_customer, 
			const vector<dummy_result> & solution_dummy, 
			const vector<inventory_result> & solution_inventory,
			const vector<customer_group_inventory_result> & customer_group_solution_inventory,
			General_Data_Source *runDS,//const string & runfile, 
			General_Data_Source *transicDS,//const string & transsicfile, 
			General_Data_Source *transcustomerDS,//const string & transscustomerfile, 
			General_Data_Source *dummyDS,//const string & dummyfile, 
			const vector<production> & processes, 
			const vector<transports> & transport_plant, 
			const vector<transports> & transport_customer,  
			const vector<double> & nPallets_per_ton,  
			const vector<string> & product_names, 
			General_Data_Source *wepaDS,//const string & wepaoutputfile, 
			General_Data_Source *capacityDS,//const string & capacityoutputfile, 
			const vector<vector<double> > capacity, 
			General_Data_Source *guiDS,//const string & guifile, 
			General_Data_Source *inventoryDS,//const string & inventoryfile, 
			General_Data_Source *warehouseDS,//const string & warehousecostfile, 
			vector<vector<pair<double,double> > > & solution_external_warehouses, 
			const vector<demand> & demands, 
			General_Data_Source *graphicDS,//const string & graphicfile, 
			General_Data_Source *nameDS,//const string & namefile, 
			General_Data_Source *detailproductionpalletsDS,//const string & detailproductionfile_pallets, 
			General_Data_Source *detailproductiontonsDS,//const string & detailproductionfile_tons, 
			General_Data_Source *monthlyproductionDS,//const string & monthlyproductionfile, 
			General_Data_Source *articleproductionDS,//const string & articleproductionfile, 
			General_Data_Source *graphic_icDS,//const string & graphicfile_ic,
			const Article_customer_fixation_collection & custFixations,
			General_Data_Source *additionalProcessesPlantFixationsDS,//const string & additionalprocessesplantfixationsfile,
			list<int> &solution_additionalAllowedProcessesPlantFixations,
			General_Data_Source *comparisonDS,
			const string &expansion_stage,
			WEPA_Opt_Params & usedParams,
			vector<vector<double> > & transport_cost_plant,
			vector<vector<double> > & transport_cost_customer
			)
{
	/*
	string filename_pallets = detailproductionfile_pallets;
	string filename_tons = detailproductionfile_tons;
	string filename_monthly = monthlyproductionfile;
	string filename_weekly = articleproductionfile;
	*/
	int nMachines = resources.size();
	//DataBase_Table *palletsTab,*tonsTab,*monthlyTab,*articleTab,*graphicTab,*nameTab,*graphicICTab,*comparisonTab;

	
	//std::cout<<"Number of additional processes in output procedure:"<<solution_additionalAllowedProcessesPlantFixations.size()<<std::endl;
	
	/*
	Output(	General_Data_Source *palletsDS,//const string & filename_pallets, 
			General_Data_Source *tonsDS,//const string & filename_tons,
			General_Data_Source *monthlyDS,//const string & filename_monthly, 
			General_Data_Source *articleDS,//const string & filename_article, 
			const vector<string> & periods, 
			const vector<string> & products, 
			const vector<string> & product_names, 
			const vector<production_result> & solution_production,
			const vector<customer_group_production_result> & customer_group_solution_production,
			const vector<string> & plants, 
			const vector<string> & machines, 
			const vector<production> & processes, 
			const vector<double> & nPallets_per_ton, 
			const int & nMachines,
			const int & nProducts,
			const vector<inventory_result> & solution_inventory, 
			const vector<customer_group_inventory_result> & customer_group_solution_inventory,
			const vector<single_transport_result_plant> & solution_single_transport_plant, 
			const vector<single_customer_group_transport_result_plant> & customer_group_solution_single_transport_plant,
			const vector<single_transport_result_customer> & solution_single_transport_customer, 
			const vector<transports> & transport_plant, 
			const vector<transports> & transport_customer, 
			const vector<demand> demands, 
			const vector<string> & customers, 
			General_Data_Source *graphicDS,//const string & filename_graphic, 
			General_Data_Source *namesDS,//const string & filename_names, 
			General_Data_Source *graphicICDS,//const string & filename_graphic_ic,
			const vector<article_customer_fixations> & customer_fixations,
			const Article_customer_fixation_collection & custFixations,
			General_Data_Source *comparisonDS,//const string & comparisonFile, 
			const string & expansion_stage
			);
	*/


	Output_DB out = Output_DB(optFolderName,userName,optRunNr,
		detailproductionpalletsDS,detailproductiontonsDS,monthlyproductionDS,articleproductionDS,periods,products,product_names,
		solution_production,customer_group_solution_production,plants,resources,processes,nPallets_per_ton,nMachines,nProducts,
		solution_inventory,customer_group_solution_inventory,solution_single_transport_plant,
		customer_group_solution_single_transport_plant,solution_single_transport_customer,transport_plant,transport_customer,demands,
		customers,NULL,nameDS,graphic_icDS,custFixations,comparisonDS,expansion_stage);

/*
	Output out = Output(palletsTab, 
						tonsTab,
						monthlyTab, 
						articleTab, 
						periods, 
						products, 
						product_names, 
						solution_production,
						customer_group_solution_production,
						plants, 
						resources, 
						processes, 
						nPallets_per_ton, 
						nMachines, 
						nProducts, 
						solution_inventory,
						customer_group_solution_inventory,
						solution_single_transport_plant, 
						customer_group_solution_single_transport_plant,
						solution_single_transport_customer, 
						transport_plant, transport_customer, 
						demands, 
						customers, 
						graphicTab, 
						nameTab, 
						graphicICTab,
						//customer_fixations
						custFixations,
						comparisonTab,
						expansion_stage
						);
						*/
	out.output_wepa(usedParams.german);
	out.output_comparison(usedParams.german);
	out.monthly_to_merge();
	out.article_to_merge();
	out.output_for_graphic(usedParams.german);
	out.output_for_graphic_ic(usedParams.german);
	out.write_article_names();

	
	/*
	ofstream guidata(guifile.c_str());
	if (usedParams.german) guidata.imbue(locale("german"));
	if (guidata.fail())
	{
		cout << "Kann Datei " << guifile << " nicht anlegen!" << endl;
		exit(1);
	}
	guidata << "Produktionskosten;IC-Transportkosten;Kundentransportkosten;Lagerkosten;gesamt;mittlere Kapazitaetsauslastung;Median Kapazitaetsauslastung" << endl;
	guidata << fixed << setprecision(2) << round(runtimecost,2) << "" << ";" << round(transiccost,2) << "" << ";" << round(transcustomercost,2) << "" << ";" << round(externalwarehousecost,2) << ";" << round(runtimecost + transiccost + transcustomercost + externalwarehousecost,2) << "" << ";" << round(mean_workload,2) << ";" << round(median_workload,2) << endl;
	guidata.close();

	cout << endl << "gesamt: " << runtimecost + transiccost + transcustomercost + externalwarehousecost << endl;
	*/
	int i, j, k;
	
	list<int>::iterator it,itEnd;
	/*
	ofstream wepaadditionalprocessesplantfixations(additionalprocessesplantfixationsfile.c_str());
	if (usedParams.german) wepaadditionalprocessesplantfixations.imbue(locale("german"));
	
	//const string & additionalprocessesplantfixationsfile,
		//	list<int> &solution_additionalAllowedProcessesPlantFixations
	it=solution_additionalAllowedProcessesPlantFixations.begin();
	itEnd=solution_additionalAllowedProcessesPlantFixations.end();
	
	wepaadditionalprocessesplantfixations<<"Produkt;Maschine;Werk"<<endl;
	while (it!=itEnd)
	{
		wepaadditionalprocessesplantfixations<<products.at( processes.at(*it).product)<<";";
		wepaadditionalprocessesplantfixations<<resources.at(processes.at(*it).machine)<<";";
		wepaadditionalprocessesplantfixations<<plants.at(processes.at(*it).product_plant_index)<<";";
		wepaadditionalprocessesplantfixations<<endl;
		//resources.at( processes.at(i).r
		it++;
	}
	wepaadditionalprocessesplantfixations.close();
	*/

	//ofstream wepaoutputdata(wepaoutputfile.c_str());
	//if (usedParams.german) wepaoutputdata.imbue(locale("german"));
	vector<bool> used_plants(plants.size());
	vector<bool> used_resources(resources.size());
	vector<bool> transported_plants(plants.size());
	vector<bool> transported_customers(customers.size());
	vector<bool> used_si_plants(plants.size());
	vector<bool> used_ei_plants(plants.size());

	double runtime;
	double pallets;
	double tons;
	double runtime_cost;

	double inventory_start;
	double inventory_end;


	double transport_cost_plant_s;
	double transport_cost_customer_s;

	vector<vector<double> > used_capacity(resources.size(), vector<double>(periods.size(), 0.0));
	vector<vector<double> > production_ton(resources.size(), vector<double>(periods.size(), 0.0));
	vector<vector<double> > production_pallet(resources.size(), vector<double>(periods.size(), 0.0));
	/*
	if (wepaoutputdata.fail())
	{
		cout << "Kann Datei " << wepaoutputfile << " nicht anlegen!" << endl;
		exit(1);
	}
	*/
	//--Erg�nzung von Startbestand und Endbestand nach Produktionskosten, jeweilige Werke und Prozentzahl
	//--bei IC-Transporten und Kundentransporten in Prozentzahlen Startbest�nde mit ber�cksichtigen

	//umspeichern solution_inventory, in Zukunft gleich in Vektoren abspeichern
	vector<vector<vector<pair<double,double> > > > inventory(plants.size(), vector<vector<pair<double,double> > >(products.size(), vector<pair<double,double> >(periods.size())));
	for (i = 0; i < solution_inventory.size(); ++i)
	{
		inventory.at(solution_inventory.at(i).plant).at(solution_inventory.at(i).product).at(solution_inventory.at(i).period - 1).first = solution_inventory.at(i).amount_start;
		inventory.at(solution_inventory.at(i).plant).at(solution_inventory.at(i).product).at(solution_inventory.at(i).period - 1).second = solution_inventory.at(i).amount_end;
	}

	for (i=0;i<customer_group_solution_inventory .size ();i++)
	{
		inventory.at(customer_group_solution_inventory.at(i).plant).at(customer_group_solution_inventory.at(i).product).at(customer_group_solution_inventory.at(i).period - 1).first+=customer_group_solution_inventory.at(i).amount_start;
		inventory.at(customer_group_solution_inventory.at(i).plant).at(customer_group_solution_inventory.at(i).product).at(customer_group_solution_inventory.at(i).period - 1).second+=customer_group_solution_inventory.at(i).amount_end;
	}

	//wepaoutputdata << "Artikelnummer;Artikelbezeichnung;Periode;#produzierende Maschinen;produzierende Maschinen;produzierende Werke;Laufzeit in Schichten;Produktion in Tonnen;Produktion in Paletten;Produktionskosten;Startbestand;VerteilungStartbestand;Endbestand;VerteilungEndbestand;Werke IC-Transporte;IC-Transportkosten;Kunden Kundentransporte;Kunden-Transportkosten;" << endl;
	/*
	wepaoutputdata.setf(ios::fixed, ios::floatfield);
	wepaoutputdata.precision(2);
	*/
	vector<double> production_machine(resources.size());
	vector<double> production_plant(plants.size());
	vector<double> transport_ic(plants.size());
	vector<double> transport_cust(customers.size());
	vector<double> start_inventory_plant(plants.size());
	vector<double> end_inventory_plant(plants.size());

	double all_production;

	vector<vector<double> > production_cost(resources.size(), vector<double>(periods.size(), 0.0));

	for (k = 1; k < periods.size() + 1; ++k)
	{
		for (i = 0; i < products.size(); ++i)
		{
			production_machine.clear();
			production_machine.resize(resources.size(), 0.0);
			production_plant.clear();
			production_plant.resize(plants.size(), 0.0);
			transport_ic.clear();
			transport_ic.resize(plants.size(), 0.0);
			transport_cust.clear();
			transport_cust.resize(customers.size(), 0.0);
			start_inventory_plant.clear();
			start_inventory_plant.resize(plants.size(),0.0);
			end_inventory_plant.clear();
			end_inventory_plant.resize(plants.size(),0.0);
			all_production = 0.0;

			used_plants.clear();
			used_plants.resize(plants.size(), false);
			used_resources.clear();
			used_resources.resize(resources.size(), false);
			transported_plants.clear();
			transported_plants.resize(plants.size(), false);
			transported_customers.clear();
			transported_customers.resize(customers.size(), false);
			used_si_plants.clear();
			used_si_plants.resize(plants.size(),false);
			used_ei_plants.clear();
			used_ei_plants.resize(plants.size(),false);

			runtime = 0.0;
			pallets = 0.0;
			tons = 0.0;
			runtime_cost = 0.0;

			inventory_start = 0.0;
			inventory_end = 0.0;

			transport_cost_plant_s = 0.0;
			transport_cost_customer_s = 0.0;
			nMachines = 0;
			//std::cout<<"Produktion-Produkte"<<endl;
			for (j = 0; j < solution_production.size(); ++j)
			{
				if (solution_production.at(j).product == i && solution_production.at(j).period == k)
				{
					production_machine.at(solution_production.at(j).machine) += solution_production.at(j).runtime * processes.at(solution_production.at(j).process).tonspershift;

					production_plant.at(processes.at(solution_production.at(j).process).product_plant_index) += solution_production.at(j).runtime * processes.at(solution_production.at(j).process).tonspershift;

					all_production += solution_production.at(j).runtime * processes.at(solution_production.at(j).process).tonspershift;

					used_plants.at(processes.at(solution_production.at(j).process).product_plant_index) = true;
					used_resources.at(solution_production.at(j).machine) = true;
					runtime += solution_production.at(j).runtime;
					tons += solution_production.at(j).runtime * processes.at(solution_production.at(j).process).tonspershift;
					pallets += solution_production.at(j).runtime * processes.at(solution_production.at(j).process).tonspershift * nPallets_per_ton.at(i);
					runtime_cost += solution_production.at(j).runtime * processes.at(solution_production.at(j).process).costspershift;
					nMachines++;
					used_capacity.at(solution_production.at(j).machine).at(k - 1) += solution_production.at(j).runtime;
					production_ton.at(solution_production.at(j).machine).at(k - 1) += solution_production.at(j).runtime * processes.at(solution_production.at(j).process).tonspershift;
					production_pallet.at(solution_production.at(j).machine).at(k - 1) += 
						solution_production.at(j).runtime * processes.at(solution_production.at(j).process).tonspershift * nPallets_per_ton.at(i);

					production_cost.at(solution_production.at(j).machine).at(k - 1) += solution_production.at(j).runtime * processes.at(solution_production.at(j).process).costspershift;
				}
			}
			//std::cout<<"Produktion-Kundengruppe"<<endl;
			for (j=0;j<customer_group_solution_production .size ();j++)
			{
				if (customer_group_solution_production.at(j).product == i && customer_group_solution_production.at(j).period == k)
				{
					production_machine.at(customer_group_solution_production.at(j).machine) += customer_group_solution_production.at(j).runtime * processes.at(customer_group_solution_production.at(j).process).tonspershift;

					production_plant.at(processes.at(customer_group_solution_production.at(j).process).product_plant_index) += customer_group_solution_production.at(j).runtime * processes.at(customer_group_solution_production.at(j).process).tonspershift;

					all_production += customer_group_solution_production.at(j).runtime * processes.at(customer_group_solution_production.at(j).process).tonspershift;

					used_plants.at(processes.at(customer_group_solution_production.at(j).process).product_plant_index) = true;
					used_resources.at(customer_group_solution_production.at(j).machine) = true;
					runtime += customer_group_solution_production.at(j).runtime;
					tons += customer_group_solution_production.at(j).runtime * processes.at(customer_group_solution_production.at(j).process).tonspershift;
					pallets += customer_group_solution_production.at(j).runtime * processes.at(customer_group_solution_production.at(j).process).tonspershift * nPallets_per_ton.at(i);
					runtime_cost += customer_group_solution_production.at(j).runtime * processes.at(customer_group_solution_production.at(j).process).costspershift;
					nMachines++;
					used_capacity.at(customer_group_solution_production.at(j).machine).at(k - 1) += customer_group_solution_production.at(j).runtime;
					production_ton.at(customer_group_solution_production.at(j).machine).at(k - 1) += customer_group_solution_production.at(j).runtime * processes.at(customer_group_solution_production.at(j).process).tonspershift;
					production_pallet.at(customer_group_solution_production.at(j).machine).at(k - 1) += 
						customer_group_solution_production.at(j).runtime * processes.at(customer_group_solution_production.at(j).process).tonspershift * nPallets_per_ton.at(i);

					production_cost.at(customer_group_solution_production.at(j).machine).at(k - 1) += customer_group_solution_production.at(j).runtime * processes.at(customer_group_solution_production.at(j).process).costspershift;
				}
			}

			//summiere �ber alle Plants --> inventory Start und inventory end ;
			for (j = 0; j < plants.size(); ++j){
				inventory_start += inventory.at(j).at(i).at(k-1).first;
				inventory_end += inventory.at(j).at(i).at(k-1).second;

				if (inventory.at(j).at(i).at(k-1).first > 0)
				{
					used_si_plants.at(j) = true;
				}
				if (inventory.at(j).at(i).at(k-1).second > 0)
				{
					used_ei_plants.at(j) = true;
				}
			}
			

			for (j = 0; j < resources.size(); ++j)
			{
				production_machine.at(j) = production_machine.at(j) / all_production;
			}
			//std::cout<<"IC-Transporte-Produkte"<<endl;
			for (j = 0; j < solution_single_transport_plant.size(); ++j)
			{
				if (solution_single_transport_plant.at(j).product == i && solution_single_transport_plant.at(j).period == k)
				{
					transported_plants.at(solution_single_transport_plant.at(j).plant2) = true;
					transport_cost_plant_s += solution_single_transport_plant.at(j).amount * transport_cost_plant.at(solution_single_transport_plant.at(j).plant1).at(solution_single_transport_plant.at(j).plant2);

					transport_ic.at(solution_single_transport_plant.at(j).plant2) += solution_single_transport_plant.at(j).amount;
				}
			}
			//std::cout<<"IC-Transporte-Kundengruppe"<<endl;
			for (j=0;j<customer_group_solution_single_transport_plant.size ();j++)
			{
				if (customer_group_solution_single_transport_plant.at(j).product == i && customer_group_solution_single_transport_plant.at(j).period == k)
				{
					transported_plants.at(customer_group_solution_single_transport_plant.at(j).plant2) = true;
					transport_cost_plant_s += customer_group_solution_single_transport_plant.at(j).amount * transport_cost_plant.at(customer_group_solution_single_transport_plant.at(j).plant1).at(customer_group_solution_single_transport_plant.at(j).plant2);

					transport_ic.at(customer_group_solution_single_transport_plant.at(j).plant2) += 
						customer_group_solution_single_transport_plant.at(j).amount;
				}
			}

			for (j = 0; j < solution_single_transport_customer.size(); ++j)
			{
				if (solution_single_transport_customer.at(j).product == i && solution_single_transport_customer.at(j).period == k)
				{
					transported_customers.at(solution_single_transport_customer.at(j).customer) = true;
					transport_cost_customer_s += solution_single_transport_customer.at(j).amount * transport_cost_customer.at(solution_single_transport_customer.at(j).customer).at(solution_single_transport_customer.at(j).plant);

					transport_cust.at(solution_single_transport_customer.at(j).customer) += solution_single_transport_customer.at(j).amount;
				}
			}

			//std::cout<<"Anteile"<<endl;

			for (j = 0; j < plants.size(); ++j)
			{

				production_plant.at(j) = production_plant.at(j) / all_production;
				//1
				transport_ic.at(j) = transport_ic.at(j);// / (pallets + inventory_start);

				start_inventory_plant.at(j) = inventory.at(j).at(i).at(k-1).first / inventory_start;

				end_inventory_plant.at(j) = inventory.at(j).at(i).at(k-1).second / inventory_end;
			}
			for (j = 0; j < customers.size(); ++j)
			{
				//2
				transport_cust.at(j) = transport_cust.at(j); // / (pallets + inventory_start);
	
			}

			/*wepaoutputdata << "\"" << products.at(i) << "\"" << ";" << product_names.at(i) << ";" << periods.at(k - 1) << ";" << nMachines << ";";
			for (j = 0; j < resources.size(); ++j)
			{
				if (used_resources.at(j))
				{
					wepaoutputdata << resources.at(j) << "(" << production_machine.at(j) << ")" << " ";
				}
			}
			
			wepaoutputdata << ";";
			for (j = 0; j < plants.size(); ++j)
			{
				if (used_plants.at(j))
				{
					wepaoutputdata << plants.at(j) << "(" << production_plant.at(j) << ")" << " ";
				}
			}

			wepaoutputdata << ";" << runtime << "; " << tons << "; " << pallets << "; " << runtime_cost << "" << "; ";
			
			//Startbestand
			
			wepaoutputdata << inventory_start << ";";
			for (j = 0; j < plants.size(); ++j)
			{
				if (used_si_plants.at(j))
				{
					wepaoutputdata << plants.at(j) << "(" << start_inventory_plant.at(j) << ")" << " ";
				}
			}
			wepaoutputdata << ";";
			
			wepaoutputdata << inventory_end << ";";
			for (j = 0; j < plants.size(); ++j)
			{
				if (used_ei_plants.at(j))
				{
					wepaoutputdata << plants.at(j) << "(" << end_inventory_plant.at(j) << ")" << " ";
				}
			}
			wepaoutputdata << ";";

			for (j = 0; j < plants.size(); ++j)
			{
				if (transported_plants.at(j))
				{
					wepaoutputdata << plants.at(j) << "(" << transport_ic.at(j) << ")" << " ";
				}
			}

			wepaoutputdata << ";" << transport_cost_plant_s << "" << "; ";

			for (j = 0; j < customers.size(); ++j)
			{
				if (transported_customers.at(j))
				{
					wepaoutputdata << customers.at(j) << "(" << transport_cust.at(j) << ")" << " ";
				}
			}
			wepaoutputdata << ";" << transport_cost_customer_s << "" << endl;
			*/
		}
	}

	

	//wepaoutputdata.close();

	//--

	vector<double> workload(resources.size()*periods.size());
	double mean_workload = 0.0;
	/*
	ofstream capacityoutputdata(capacityoutputfile.c_str());
	if (usedParams.german) capacityoutputdata.imbue(locale("german"));
	if (capacityoutputdata.fail())
	{
		cout << "Kann Datei " << capacityoutputfile << " nicht anlegen!" << endl;
		exit(1);
	}
	*/
	//std::cout<<"Kapazit�tsauslastung"<<endl;
	//capacityoutputdata << "Maschine;Periode;genutzte Schichten;verf�gbare Schichten;Auslastung;Produktion in Tonnen;Produktion in Paletten;Produktionskosten" << endl;
	for (j = 0; j < periods.size(); ++j)
	{
		for (i = 0; i < resources.size(); ++i)
		{
			/*
			capacityoutputdata << resources.at(i) << ";" << periods.at(j) << ";" << used_capacity.at(i).at(j) << ";" << capacity.at(i).at(j) << ";";
			if (capacity.at(i).at(j) > 0)
			{
				capacityoutputdata << used_capacity.at(i).at(j) / capacity.at(i).at(j) << ";";
			}
			else
			{
				capacityoutputdata << 1000.0 << ";";//1.0 << ";";
			}
			capacityoutputdata << production_ton.at(i).at(j) << ";" << production_pallet.at(i).at(j) << ";" << production_cost.at(i).at(j) << "" << endl;
			*/
			if (capacity.at(i).at(j) > 0)
			{
				workload.at(i*periods.size() + j) = used_capacity.at(i).at(j) / capacity.at(i).at(j);
			}
			else
			{
				workload.at(i*periods.size() + j) = 1.0;
			}
			mean_workload += workload.at(i*periods.size() + j);
			
		}
	}
	//capacityoutputdata.close();

	mean_workload = mean_workload / (double)(resources.size()*periods.size());
	double median_workload = get_median(workload);

	/*ofstream rundata(runfile.c_str());
	if (usedParams.german) rundata.imbue(locale("german"));
	if (rundata.fail())
	{
		cout << "Kann Datei " << runfile << " nicht anlegen!" << endl;
		exit(1);
	}
	*/
	//std::cout<<"Aggregierte Produktionsmengen-Produkte"<<endl;
	double runtimecost = 0.0;
	//rundata << "Werk;Maschine;Produkt;Periode;Produktionsschichten;Produktion Tonnen;Produktion Paletten;Produktionskosten" << endl;
	unordered_map <string,production_result_per_plant_machine_product_period> cumProdQuantities;
	string key;
	string sep=";";
	production_result_per_plant_machine_product_period currProdResult;
	for (i = 0; i < solution_production.size(); ++i)
	{
		for (j = 0; j < processes.size(); ++j)
		{
			if (processes.at(j).machine == solution_production.at(i).machine && processes.at(j).product == solution_production.at(i).product)
			{
				
				key=toString<int>(processes.at(j).product_plant_index)+sep+toString<int>(solution_production.at(i).machine)+sep+toString<int>(solution_production.at(i).product)+sep+toString<int>(solution_production.at(i).period);
				
				if (cumProdQuantities.find (key)==cumProdQuantities .end ())
				{
					currProdResult.plant =processes.at(j).product_plant_index;
					currProdResult.machine =solution_production.at(i).machine;
					currProdResult.product =solution_production.at(i).product;
					currProdResult.period =solution_production.at(i).period;
					currProdResult.runtime =0;
					currProdResult.tons =0;
					currProdResult.nrPallets =0;
					currProdResult.cost=0;
					cumProdQuantities .insert(pair<string,production_result_per_plant_machine_product_period>(key,currProdResult));
				}
				
				cumProdQuantities .at(key).runtime +=solution_production.at(i).runtime;
				cumProdQuantities.at(key).tons+=processes.at(j).tonspershift * solution_production.at(i).runtime;
				cumProdQuantities.at(key).nrPallets+=nPallets_per_ton.at(processes.at(j).product) * processes.at(j).tonspershift * solution_production.at(i).runtime;
				cumProdQuantities.at(key).cost+=processes.at(j).costspershift * solution_production.at(i).runtime;
				//currResult.
					

					
				//if (cumProdQuantities. (key)==cumProdQuantities .end ().)
				//{
				//}

				//rundata << plants.at(processes.at(j).product_plant_index) << ";" << resources.at(solution_production.at(i).machine) << ";" << "\"" << products.at(solution_production.at(i).product) << "\"" << ";" << periods.at(solution_production.at(i).period - 1) << ";" << solution_production.at(i).runtime << "; " << processes.at(j).tonspershift * solution_production.at(i).runtime << "; " << nPallets_per_ton.at(processes.at(j).product) * processes.at(j).tonspershift * solution_production.at(i).runtime << "; " << processes.at(j).costspershift * solution_production.at(i).runtime << "" << endl;
				runtimecost += processes.at(j).costspershift*solution_production.at(i).runtime;
			}
		}	
	}
	
	currProdResult.runtime =0;
	currProdResult.tons =0;
	currProdResult.nrPallets =0;
	currProdResult.cost=0;
	//std::cout<<"Aggregierte Produktionsmengen-Kundengruppen"<<endl;
	for (i = 0; i < customer_group_solution_production.size(); ++i)
	{
		for (j = 0; j < processes.size(); ++j)
		{
			if (processes.at(j).machine == customer_group_solution_production.at(i).machine && processes.at(j).product == customer_group_solution_production.at(i).product)
			{
				key=toString<int>(processes.at(j).product_plant_index)+sep+toString<int>(customer_group_solution_production.at(i).machine)+sep+toString<int>(customer_group_solution_production.at(i).product)+sep+toString<int>(customer_group_solution_production.at(i).period);
				
				if (cumProdQuantities.find (key)==cumProdQuantities .end ())
				{
					currProdResult.plant =processes.at(j).product_plant_index;
					currProdResult.machine =customer_group_solution_production.at(i).machine;
					currProdResult.product =customer_group_solution_production.at(i).product;
					currProdResult.period =customer_group_solution_production.at(i).period;
					
					cumProdQuantities .insert(pair<string,production_result_per_plant_machine_product_period>(key,currProdResult));
				}
				
				cumProdQuantities.at(key).runtime +=customer_group_solution_production.at(i).runtime;
				cumProdQuantities.at(key).tons+=processes.at(j).tonspershift * customer_group_solution_production.at(i).runtime;
				cumProdQuantities.at(key).nrPallets+=nPallets_per_ton.at(processes.at(j).product) * processes.at(j).tonspershift * customer_group_solution_production.at(i).runtime;
				cumProdQuantities.at(key).cost+=processes.at(j).costspershift * customer_group_solution_production.at(i).runtime;
				//rundata << plants.at(processes.at(j).product_plant_index) << ";" << resources.at(customer_group_solution_production.at(i).machine) << ";" << "\"" << products.at(customer_group_solution_production.at(i).product) << "\"" << ";" << periods.at(customer_group_solution_production.at(i).period - 1) << ";" << customer_group_solution_production.at(i).runtime << "; " << processes.at(j).tonspershift * customer_group_solution_production.at(i).runtime << "; " << nPallets_per_ton.at(processes.at(j).product) * processes.at(j).tonspershift * customer_group_solution_production.at(i).runtime << "; " << processes.at(j).costspershift * customer_group_solution_production.at(i).runtime << "" << endl;
				runtimecost += processes.at(j).costspershift*customer_group_solution_production.at(i).runtime;
			}
		}	
	}

	unordered_map <string,production_result_per_plant_machine_product_period>::iterator itProd;
/*
	itProd=cumProdQuantities.begin ();
	while (itProd!=cumProdQuantities.end ())
	{	
		rundata << plants.at((*itProd).second.plant) << ";" << resources.at((*itProd).second.machine) << ";" << "\"" << products.at((*itProd).second.product) << "\"" << ";" << periods.at((*itProd).second.period - 1) << ";" << (*itProd).second.runtime << "; " << (*itProd).second.tons << "; " << (*itProd).second.nrPallets  << "; " << (*itProd).second.cost << "" << endl;
		itProd++;
	}

	rundata.close();
	*/
	cout << endl;
	cout << "Laufkosten: " << runtimecost << endl;

	double transiccost = 0.0;
	/*
	ofstream transsicdata(transsicfile.c_str());
	if (usedParams.german) transsicdata.imbue(locale("german"));
	if (transsicdata.fail())
	{
		cout << "Kann Datei " << transsicfile << " nicht anlegen!" << endl;
		exit(1);
	}
	transsicdata << "Produkt;Start;Ziel;Periode;Palettenanzahl;Kosten" << endl;
	*/
	unordered_map <string,transport_result_per_product_start_dest_period> cumTranspPlantQuantities;
	unordered_map <string,transport_result_per_product_start_dest_period>::iterator itTransp;

	transport_result_per_product_start_dest_period currTranspResult;

	//std::cout<<"Aggregierte Transportmengen-Produkte"<<endl;
	for (i = 0; i < solution_single_transport_plant.size(); ++i)
	{
		key=toString<int>(solution_single_transport_plant.at(i).product)+sep+toString<int>(solution_single_transport_plant.at(i).plant1)+sep+toString<int>(solution_single_transport_plant.at(i).plant2)+sep+toString<int>(solution_single_transport_plant.at(i).period);
		if (cumTranspPlantQuantities.find (key)==cumTranspPlantQuantities.end())
		{
			currTranspResult.product =solution_single_transport_plant.at(i).product;
			currTranspResult.start =solution_single_transport_plant.at(i).plant1;
			currTranspResult.dest =solution_single_transport_plant.at(i).plant2;
			currTranspResult.period=solution_single_transport_plant.at(i).period;
			currTranspResult.amount=0;
			currTranspResult.cost=0;

			cumTranspPlantQuantities .insert(pair<string,transport_result_per_product_start_dest_period>(key,currTranspResult ));
		}

		cumTranspPlantQuantities .at(key).amount +=solution_single_transport_plant.at(i).amount;
		cumTranspPlantQuantities .at(key).cost +=solution_single_transport_plant.at(i).amount * transport_cost_plant.at(solution_single_transport_plant.at(i).plant1).at(solution_single_transport_plant.at(i).plant2);
		//transsicdata << "\"" << products.at(solution_single_transport_plant.at(i).product) << "\"" << ";" << plants.at(solution_single_transport_plant.at(i).plant1) << ";" << plants.at(solution_single_transport_plant.at(i).plant2) << ";" << periods.at(solution_single_transport_plant.at(i).period - 1) << ";" << solution_single_transport_plant.at(i).amount << ";" << solution_single_transport_plant.at(i).amount * transport_cost_plant.at(solution_single_transport_plant.at(i).plant1).at(solution_single_transport_plant.at(i).plant2) << "" << endl;
		transiccost += solution_single_transport_plant.at(i).amount * transport_cost_plant.at(solution_single_transport_plant.at(i).plant1).at(solution_single_transport_plant.at(i).plant2);
	}
	//std::cout<<"Aggregierte Transportmengen-Kundengruppen"<<endl;
	currTranspResult.amount=0;
	currTranspResult.cost=0;
	for (i=0;i<customer_group_solution_single_transport_plant.size();i++)
	{
		key=toString<int>(customer_group_solution_single_transport_plant.at(i).product)+sep+toString<int>(customer_group_solution_single_transport_plant.at(i).plant1)+sep+toString<int>(customer_group_solution_single_transport_plant.at(i).plant2)+sep+toString<int>(customer_group_solution_single_transport_plant.at(i).period);

		if (cumTranspPlantQuantities.find (key)==cumTranspPlantQuantities.end())
		{
			currTranspResult.product =customer_group_solution_single_transport_plant.at(i).product;
			currTranspResult.start =customer_group_solution_single_transport_plant.at(i).plant1;
			currTranspResult.dest =customer_group_solution_single_transport_plant.at(i).plant2;
			currTranspResult.period=customer_group_solution_single_transport_plant.at(i).period;
			
			cumTranspPlantQuantities.insert(pair<string,transport_result_per_product_start_dest_period>(key,currTranspResult));
		}

		cumTranspPlantQuantities .at(key).amount +=customer_group_solution_single_transport_plant.at(i).amount;
		cumTranspPlantQuantities .at(key).cost +=customer_group_solution_single_transport_plant.at(i).amount * transport_cost_plant.at(customer_group_solution_single_transport_plant.at(i).plant1).at(customer_group_solution_single_transport_plant.at(i).plant2);
		transiccost += customer_group_solution_single_transport_plant.at(i).amount * transport_cost_plant.at(customer_group_solution_single_transport_plant.at(i).plant1).at(customer_group_solution_single_transport_plant.at(i).plant2);
	}

	/*
	//itProd=cumProdQuantities.begin ();
	itTransp=cumTranspPlantQuantities.begin();
	while (itTransp!=cumTranspPlantQuantities.end ())
	{	
		transsicdata << "\"" << products.at((*itTransp).second.product) << "\"" << ";" << plants.at((*itTransp).second.start) << ";" << plants.at((*itTransp).second.dest) << ";" << periods.at((*itTransp).second.period - 1) << ";" << (*itTransp).second.amount << ";" << (*itTransp).second.cost << "" << endl;
		//rundata << plants.at((*itProd).second.plant) << ";" << resources.at((*itProd).second.machine) << ";" << "\"" << products.at((*itProd).second.product) << "\"" << ";" << periods.at((*itProd).second.period - 1) << ";" << (*itProd).second.runtime << "; " << (*itProd).second.tons << "; " << (*itProd).second.nrPallets  << "; " << (*itProd).second.cost << "" << endl;
		itTransp++;
	}

	transsicdata.close();
	*/

	double transcustomercost = 0.0;
	/*
	ofstream transscustomerdata(transscustomerfile.c_str());
	if (usedParams.german) transscustomerdata.imbue(locale("german"));
	if (transscustomerdata.fail())
	{
		cout << "Kann Datei " << transscustomerfile << " nicht anlegen!" << endl;
		exit(1);
	}
	transscustomerdata << "Produkt;Werk;Kunde;Periode;Kundentransporte Paletten;Kundentransportkosten" << endl;
	*/
	for (i = 0; i < solution_single_transport_customer.size(); ++i)
	{
		//transscustomerdata << "\"" << products.at(solution_single_transport_customer.at(i).product) << "\"" << ";" << plants.at(solution_single_transport_customer.at(i).plant) << ";" << customers.at(solution_single_transport_customer.at(i).customer) << ";" << periods.at(solution_single_transport_customer.at(i).period - 1) << ";" << solution_single_transport_customer.at(i).amount << ";" << solution_single_transport_customer.at(i).amount * transport_cost_customer.at(solution_single_transport_customer.at(i).customer).at(solution_single_transport_customer.at(i).plant) << "" << endl;
		transcustomercost += solution_single_transport_customer.at(i).amount * transport_cost_customer.at(solution_single_transport_customer.at(i).customer).at(solution_single_transport_customer.at(i).plant);
	}

	
	//transscustomerdata.close();

	cout << "IC-Transportkosten: " << transiccost << endl; 
	cout << "Kunden-Transportkosten: " << transcustomercost << endl;

	double externalwarehousecost = 0.0;
	/*
	ofstream warehousedata(warehousecostfile.c_str());
	if (usedParams.german) warehousedata.imbue(locale("german"));
	if (warehousedata.fail())
	{
		cout << "Kann Datei " << warehousecostfile << " nicht anlegen!" << endl;
		exit(1);
	}
	warehousedata << "Werk;Periode;ExterneLagermenge;ExterneLagerkosten" << endl;
	*/
	for (i = 0; i < solution_external_warehouses.size(); ++i)
	{
		for (j = 1; j < solution_external_warehouses.at(i).size(); ++j)
		{
			//warehousedata << plants.at(i) << ";" << periods.at(j-1) << ";" << solution_external_warehouses.at(i).at(j).first << ";" << solution_external_warehouses.at(i).at(j).second << endl;
			externalwarehousecost += solution_external_warehouses.at(i).at(j).second;
		}
	}

	cout << "externe Lagerkosten: " << externalwarehousecost << endl;
	/*
	ofstream dummydata(dummyfile.c_str());
	if (usedParams.german) dummydata.imbue(locale("german"));
	if (dummydata.fail())
	{
		cout << "Kann Datei " << dummyfile << " nicht anlegen!" << endl;
		exit(1);
	}
	dummydata << "Maschine;Periode;fehlendeSchichten;Kosten" << endl;
	for (i = 0; i < solution_dummy.size(); ++i)
	{
		dummydata << resources.at(solution_dummy.at(i).resource) << ";" << periods.at(solution_dummy.at(i).period - 1) << ";" << solution_dummy.at(i).value << ";" << solution_dummy.at(i).cost  << "" << endl;
	}
	dummydata.close();
	*/
	/*ofstream inventorydata(inventoryfile.c_str());
	if (usedParams.german) inventorydata.imbue(locale("german"));
	if (inventorydata.fail())
	{
		cout << "Kann Datei " << inventoryfile << " nicht anlegen!" << endl;
		exit(1);
	}
	inventorydata << "Werk;Produkt;Periode;Startbestand;Endbestand" << endl;
	*/
	unordered_map <string,inventory_result_per_location_product_period> cumInvQuantities;
	unordered_map <string,inventory_result_per_location_product_period>::iterator itInv;
	inventory_result_per_location_product_period currInvResult;
	currInvResult.amount_start =currInvResult .amount_end =0;
	//std::cout<<"Aggregierte Lagermengen-Produkte"<<endl;
	for (i = 0; i < solution_inventory.size(); ++i)
	{
		key=toString<int>(solution_inventory.at(i).plant)+sep+toString<int>(solution_inventory.at(i).product)+sep+toString<int>(solution_inventory.at(i).period);
		
		if (cumInvQuantities.find (key)==cumInvQuantities.end ())
		{
			currInvResult.location =solution_inventory.at(i).plant;
			currInvResult.product =solution_inventory.at(i).product;
			currInvResult.period=solution_inventory.at(i).period;
			cumInvQuantities.insert(pair<string,inventory_result_per_location_product_period>(key,currInvResult));
		}

		cumInvQuantities .at(key).amount_start +=solution_inventory.at(i).amount_start;
		cumInvQuantities .at(key).amount_end +=solution_inventory.at(i).amount_end;

		//inventorydata << plants.at(solution_inventory.at(i).plant) << ";" << "\"" << products.at(solution_inventory.at(i).product) << "\"" << ";" << periods.at(solution_inventory.at(i).period - 1) << ";" << solution_inventory.at(i).amount_start << ";" << solution_inventory.at(i).amount_end << endl;
	}
	//std::cout<<"Aggregierte Lagermengen-Kundengruppen"<<endl;
	for (i=0;i<customer_group_solution_inventory .size();i++)
	{
		key=toString<int>(customer_group_solution_inventory.at(i).plant)+sep+toString<int>(customer_group_solution_inventory.at(i).product)+sep+toString<int>(customer_group_solution_inventory.at(i).period);
		
		if (cumInvQuantities.find (key)==cumInvQuantities.end ())
		{
			currInvResult.location =customer_group_solution_inventory.at(i).plant;
			currInvResult.product =customer_group_solution_inventory.at(i).product;
			currInvResult.period=customer_group_solution_inventory.at(i).period;
			cumInvQuantities.insert(pair<string,inventory_result_per_location_product_period>(key,currInvResult));
		}

		cumInvQuantities .at(key).amount_start +=customer_group_solution_inventory.at(i).amount_start;
		cumInvQuantities .at(key).amount_end +=customer_group_solution_inventory.at(i).amount_end;
	}
	/*
	itInv=cumInvQuantities .begin ();

	while(itInv!=cumInvQuantities .end ())
	{
		//inventorydata << plants.at(solution_inventory.at(i).plant) << ";" << "\"" << products.at(solution_inventory.at(i).product) << "\"" << ";" << periods.at(solution_inventory.at(i).period - 1) << ";" << solution_inventory.at(i).amount_start << ";" << solution_inventory.at(i).amount_end << endl;
		inventorydata <<plants.at((*itInv).second.location)<<";"<<"\""<< "\"" << products.at((*itInv).second.product) << "\"" << ";" << periods.at((*itInv).second .period - 1) << ";" << (*itInv).second .amount_start << ";" << (*itInv).second .amount_end << endl;
		itInv++;
	}
	inventorydata.close();
	*/
	/*
	ofstream guidata(guifile.c_str());
	if (usedParams.german) guidata.imbue(locale("german"));
	if (guidata.fail())
	{
		cout << "Kann Datei " << guifile << " nicht anlegen!" << endl;
		exit(1);
	}
	guidata << "Produktionskosten;IC-Transportkosten;Kundentransportkosten;Lagerkosten;gesamt;mittlere Kapazitaetsauslastung;Median Kapazitaetsauslastung" << endl;
	guidata << fixed << setprecision(2) << round(runtimecost,2) << "" << ";" << round(transiccost,2) << "" << ";" << round(transcustomercost,2) << "" << ";" << round(externalwarehousecost,2) << ";" << round(runtimecost + transiccost + transcustomercost + externalwarehousecost,2) << "" << ";" << round(mean_workload,2) << ";" << round(median_workload,2) << endl;
	guidata.close();
	*/
	std::string currLine [11];

	currLine[0]="'"+optFolderName+"'";
	currLine[1]="'"+userName+"'";
	currLine[2]=std::to_string(optRunNr);
	currLine[3]=std::to_string(usedParams.set_fixations);
	currLine[4]=std::to_string(runtimecost);
	currLine[5]=std::to_string(transiccost);
	currLine[6]=std::to_string(transcustomercost);
	currLine[7]=std::to_string(externalwarehousecost);
	currLine[8]=std::to_string(runtimecost + transiccost + transcustomercost + externalwarehousecost);
	currLine[9]=std::to_string(mean_workload);
	currLine[10]=std::to_string(median_workload);
	guiDS->writeData(currLine,11);
	cout << endl << "gesamt: " << runtimecost + transiccost + transcustomercost + externalwarehousecost << endl;
}


void output_csv(const vector<string> & resources,
	const vector<string> & periods,
	const vector<string> & products,
	const vector<string> & plants,
	const vector<string> & customers,
	const vector<production_result> & solution_production,
	const vector<customer_group_production_result> & customer_group_solution_production,
	const vector<single_transport_result_plant> & solution_single_transport_plant,
	const vector<single_customer_group_transport_result_plant> & customer_group_solution_single_transport_plant,
	const vector<single_transport_result_customer> & solution_single_transport_customer,
	const vector<dummy_result> & solution_dummy,
	const vector<inventory_result> & solution_inventory,
	const vector<customer_group_inventory_result> & customer_group_solution_inventory,
	const string & runfile,
	const string & transsicfile,
	const string & transscustomerfile,
	const string & dummyfile,
	const vector<production> & processes,
	const vector<transports> & transport_plant,
	const vector<transports> & transport_customer,
	const vector<double> & nPallets_per_ton,
	const vector<string> & product_names,
	const string & wepaoutputfile,
	const string & capacityoutputfile,
	const vector<vector<double> > capacity,
	const string & guifile,
	const string & inventoryfile,
	const string & warehousecostfile,
	vector<vector<pair<double, double> > > & solution_external_warehouses,
	const vector<demand> & demands,
	const string & graphicfile,
	const string & namefile,
	const string & detailproductionfile_pallets,
	const string & detailproductionfile_tons,
	const string & monthlyproductionfile,
	const string & articleproductionfile,
	const string & graphicfile_ic,
	const Article_customer_fixation_collection & custFixations,
	const string & additionalprocessesplantfixationsfile,
	list<int> & solution_additionalAllowedProcessesPlantFixations,
	const string &comparisonFile,
	const string &expansion_stage,
	bool german,
	vector<vector<double> > & transport_cost_plant,
	vector<vector<double> > & transport_cost_customer
)
{
	string filename_pallets = detailproductionfile_pallets;
	string filename_tons = detailproductionfile_tons;
	string filename_monthly = monthlyproductionfile;
	string filename_weekly = articleproductionfile;
	int nMachines = resources.size();

	//std::cout<<"Number of additional processes in output procedure:"<<solution_additionalAllowedProcessesPlantFixations.size()<<std::endl;
	Output_CSV out = Output_CSV(filename_pallets,
		filename_tons,
		filename_monthly,
		filename_weekly,
		periods,
		products,
		product_names,
		solution_production,
		customer_group_solution_production,
		plants,
		resources,
		processes,
		nPallets_per_ton,
		nMachines,
		nProducts,
		solution_inventory,
		customer_group_solution_inventory,
		solution_single_transport_plant,
		customer_group_solution_single_transport_plant,
		solution_single_transport_customer,
		transport_plant, transport_customer,
		demands,
		customers,
		graphicfile,
		namefile,
		graphicfile_ic,
		//customer_fixations
		custFixations,
		comparisonFile,
		expansion_stage
	);

	out.output_wepa(german);
	out.output_comparison(german);
	out.monthly_to_merge();
	out.article_to_merge();
	out.output_for_graphic(german);
	out.output_for_graphic_ic(german);
	out.write_article_names();

	int i, j, k;

	list<int>::iterator it, itEnd;
	ofstream wepaadditionalprocessesplantfixations(additionalprocessesplantfixationsfile.c_str());
	if (german) wepaadditionalprocessesplantfixations.imbue(locale("german"));
	//const string & additionalprocessesplantfixationsfile,
	//	list<int> &solution_additionalAllowedProcessesPlantFixations
	it = solution_additionalAllowedProcessesPlantFixations.begin();
	itEnd = solution_additionalAllowedProcessesPlantFixations.end();
	wepaadditionalprocessesplantfixations << "Produkt;Maschine;Werk" << endl;
	while (it != itEnd)
	{
		wepaadditionalprocessesplantfixations << products.at(processes.at(*it).product) << ";";
		wepaadditionalprocessesplantfixations << resources.at(processes.at(*it).machine) << ";";
		wepaadditionalprocessesplantfixations << plants.at(processes.at(*it).product_plant_index) << ";";
		wepaadditionalprocessesplantfixations << endl;
		//resources.at( processes.at(i).r
		it++;
	}
	wepaadditionalprocessesplantfixations.close();


	ofstream wepaoutputdata(wepaoutputfile.c_str());
	if (german) wepaoutputdata.imbue(locale("german"));
	vector<bool> used_plants(plants.size());
	vector<bool> used_resources(resources.size());
	vector<bool> transported_plants(plants.size());
	vector<bool> transported_customers(customers.size());
	vector<bool> used_si_plants(plants.size());
	vector<bool> used_ei_plants(plants.size());

	double runtime;
	double pallets;
	double tons;
	double runtime_cost;

	double inventory_start;
	double inventory_end;


	double transport_cost_plant_s;
	double transport_cost_customer_s;

	vector<vector<double> > used_capacity(resources.size(), vector<double>(periods.size(), 0.0));
	vector<vector<double> > production_ton(resources.size(), vector<double>(periods.size(), 0.0));
	vector<vector<double> > production_pallet(resources.size(), vector<double>(periods.size(), 0.0));

	if (wepaoutputdata.fail())
	{
		cout << "Kann Datei " << wepaoutputfile << " nicht anlegen!" << endl;
		exit(1);
	}

	//--Erg�nzung von Startbestand und Endbestand nach Produktionskosten, jeweilige Werke und Prozentzahl
	//--bei IC-Transporten und Kundentransporten in Prozentzahlen Startbest�nde mit ber�cksichtigen

	//umspeichern solution_inventory, in Zukunft gleich in Vektoren abspeichern
	vector<vector<vector<pair<double, double> > > > inventory(plants.size(), vector<vector<pair<double, double> > >(products.size(), vector<pair<double, double> >(periods.size())));
	for (i = 0; i < solution_inventory.size(); ++i)
	{
		inventory.at(solution_inventory.at(i).plant).at(solution_inventory.at(i).product).at(solution_inventory.at(i).period - 1).first = solution_inventory.at(i).amount_start;
		inventory.at(solution_inventory.at(i).plant).at(solution_inventory.at(i).product).at(solution_inventory.at(i).period - 1).second = solution_inventory.at(i).amount_end;
	}

	for (i = 0; i<customer_group_solution_inventory.size(); i++)
	{
		inventory.at(customer_group_solution_inventory.at(i).plant).at(customer_group_solution_inventory.at(i).product).at(customer_group_solution_inventory.at(i).period - 1).first += customer_group_solution_inventory.at(i).amount_start;
		inventory.at(customer_group_solution_inventory.at(i).plant).at(customer_group_solution_inventory.at(i).product).at(customer_group_solution_inventory.at(i).period - 1).second += customer_group_solution_inventory.at(i).amount_end;
	}

	wepaoutputdata << "Artikelnummer;Artikelbezeichnung;Periode;#produzierende Maschinen;produzierende Maschinen;produzierende Werke;Laufzeit in Schichten;Produktion in Tonnen;Produktion in Paletten;Produktionskosten;Startbestand;VerteilungStartbestand;Endbestand;VerteilungEndbestand;Werke IC-Transporte;IC-Transportkosten;Kunden Kundentransporte;Kunden-Transportkosten;" << endl;

	wepaoutputdata.setf(ios::fixed, ios::floatfield);
	wepaoutputdata.precision(2);

	vector<double> production_machine(resources.size());
	vector<double> production_plant(plants.size());
	vector<double> transport_ic(plants.size());
	vector<double> transport_cust(customers.size());
	vector<double> start_inventory_plant(plants.size());
	vector<double> end_inventory_plant(plants.size());

	double all_production;

	vector<vector<double> > production_cost(resources.size(), vector<double>(periods.size(), 0.0));

	for (k = 1; k < periods.size() + 1; ++k)
	{
		for (i = 0; i < products.size(); ++i)
		{
			production_machine.clear();
			production_machine.resize(resources.size(), 0.0);
			production_plant.clear();
			production_plant.resize(plants.size(), 0.0);
			transport_ic.clear();
			transport_ic.resize(plants.size(), 0.0);
			transport_cust.clear();
			transport_cust.resize(customers.size(), 0.0);
			start_inventory_plant.clear();
			start_inventory_plant.resize(plants.size(), 0.0);
			end_inventory_plant.clear();
			end_inventory_plant.resize(plants.size(), 0.0);
			all_production = 0.0;

			used_plants.clear();
			used_plants.resize(plants.size(), false);
			used_resources.clear();
			used_resources.resize(resources.size(), false);
			transported_plants.clear();
			transported_plants.resize(plants.size(), false);
			transported_customers.clear();
			transported_customers.resize(customers.size(), false);
			used_si_plants.clear();
			used_si_plants.resize(plants.size(), false);
			used_ei_plants.clear();
			used_ei_plants.resize(plants.size(), false);

			runtime = 0.0;
			pallets = 0.0;
			tons = 0.0;
			runtime_cost = 0.0;

			inventory_start = 0.0;
			inventory_end = 0.0;

			transport_cost_plant_s = 0.0;
			transport_cost_customer_s = 0.0;
			nMachines = 0;
			//std::cout<<"Produktion-Produkte"<<endl;
			for (j = 0; j < solution_production.size(); ++j)
			{
				if (solution_production.at(j).product == i && solution_production.at(j).period == k)
				{
					production_machine.at(solution_production.at(j).machine) += solution_production.at(j).runtime * processes.at(solution_production.at(j).process).tonspershift;

					production_plant.at(processes.at(solution_production.at(j).process).product_plant_index) += solution_production.at(j).runtime * processes.at(solution_production.at(j).process).tonspershift;

					all_production += solution_production.at(j).runtime * processes.at(solution_production.at(j).process).tonspershift;

					used_plants.at(processes.at(solution_production.at(j).process).product_plant_index) = true;
					used_resources.at(solution_production.at(j).machine) = true;
					runtime += solution_production.at(j).runtime;
					tons += solution_production.at(j).runtime * processes.at(solution_production.at(j).process).tonspershift;
					pallets += solution_production.at(j).runtime * processes.at(solution_production.at(j).process).tonspershift * nPallets_per_ton.at(i);
					runtime_cost += solution_production.at(j).runtime * processes.at(solution_production.at(j).process).costspershift;
					nMachines++;
					used_capacity.at(solution_production.at(j).machine).at(k - 1) += solution_production.at(j).runtime;
					production_ton.at(solution_production.at(j).machine).at(k - 1) += solution_production.at(j).runtime * processes.at(solution_production.at(j).process).tonspershift;
					production_pallet.at(solution_production.at(j).machine).at(k - 1) += solution_production.at(j).runtime * processes.at(solution_production.at(j).process).tonspershift * nPallets_per_ton.at(i);

					production_cost.at(solution_production.at(j).machine).at(k - 1) += solution_production.at(j).runtime * processes.at(solution_production.at(j).process).costspershift;
				}
			}
			//std::cout<<"Produktion-Kundengruppe"<<endl;
			for (j = 0; j<customer_group_solution_production.size(); j++)
			{
				if (customer_group_solution_production.at(j).product == i && customer_group_solution_production.at(j).period == k)
				{
					production_machine.at(customer_group_solution_production.at(j).machine) += customer_group_solution_production.at(j).runtime * processes.at(customer_group_solution_production.at(j).process).tonspershift;

					production_plant.at(processes.at(customer_group_solution_production.at(j).process).product_plant_index) += customer_group_solution_production.at(j).runtime * processes.at(customer_group_solution_production.at(j).process).tonspershift;

					all_production += customer_group_solution_production.at(j).runtime * processes.at(customer_group_solution_production.at(j).process).tonspershift;

					used_plants.at(processes.at(customer_group_solution_production.at(j).process).product_plant_index) = true;
					used_resources.at(customer_group_solution_production.at(j).machine) = true;
					runtime += customer_group_solution_production.at(j).runtime;
					tons += customer_group_solution_production.at(j).runtime * processes.at(customer_group_solution_production.at(j).process).tonspershift;
					pallets += customer_group_solution_production.at(j).runtime * processes.at(customer_group_solution_production.at(j).process).tonspershift * nPallets_per_ton.at(i);
					runtime_cost += customer_group_solution_production.at(j).runtime * processes.at(customer_group_solution_production.at(j).process).costspershift;
					nMachines++;
					used_capacity.at(customer_group_solution_production.at(j).machine).at(k - 1) += customer_group_solution_production.at(j).runtime;
					production_ton.at(customer_group_solution_production.at(j).machine).at(k - 1) += customer_group_solution_production.at(j).runtime * processes.at(customer_group_solution_production.at(j).process).tonspershift;
					production_pallet.at(customer_group_solution_production.at(j).machine).at(k - 1) += customer_group_solution_production.at(j).runtime * processes.at(customer_group_solution_production.at(j).process).tonspershift * nPallets_per_ton.at(i);

					production_cost.at(customer_group_solution_production.at(j).machine).at(k - 1) += customer_group_solution_production.at(j).runtime * processes.at(customer_group_solution_production.at(j).process).costspershift;
				}
			}

			//summiere �ber alle Plants --> inventory Start und inventory end ;
			for (j = 0; j < plants.size(); ++j) {
				inventory_start += inventory.at(j).at(i).at(k - 1).first;
				inventory_end += inventory.at(j).at(i).at(k - 1).second;

				if (inventory.at(j).at(i).at(k - 1).first > 0)
				{
					used_si_plants.at(j) = true;
				}
				if (inventory.at(j).at(i).at(k - 1).second > 0)
				{
					used_ei_plants.at(j) = true;
				}
			}


			for (j = 0; j < resources.size(); ++j)
			{
				production_machine.at(j) = production_machine.at(j) / all_production;
			}
			//std::cout<<"IC-Transporte-Produkte"<<endl;
			for (j = 0; j < solution_single_transport_plant.size(); ++j)
			{
				if (solution_single_transport_plant.at(j).product == i && solution_single_transport_plant.at(j).period == k)
				{
					transported_plants.at(solution_single_transport_plant.at(j).plant2) = true;
					transport_cost_plant_s += solution_single_transport_plant.at(j).amount * transport_cost_plant.at(solution_single_transport_plant.at(j).plant1).at(solution_single_transport_plant.at(j).plant2);

					transport_ic.at(solution_single_transport_plant.at(j).plant2) += solution_single_transport_plant.at(j).amount;
				}
			}
			//std::cout<<"IC-Transporte-Kundengruppe"<<endl;
			for (j = 0; j<customer_group_solution_single_transport_plant.size(); j++)
			{
				if (customer_group_solution_single_transport_plant.at(j).product == i && customer_group_solution_single_transport_plant.at(j).period == k)
				{
					transported_plants.at(customer_group_solution_single_transport_plant.at(j).plant2) = true;
					transport_cost_plant_s += customer_group_solution_single_transport_plant.at(j).amount * transport_cost_plant.at(customer_group_solution_single_transport_plant.at(j).plant1).at(customer_group_solution_single_transport_plant.at(j).plant2);

					transport_ic.at(customer_group_solution_single_transport_plant.at(j).plant2) += customer_group_solution_single_transport_plant.at(j).amount;
				}
			}

			for (j = 0; j < solution_single_transport_customer.size(); ++j)
			{
				if (solution_single_transport_customer.at(j).product == i && solution_single_transport_customer.at(j).period == k)
				{
					transported_customers.at(solution_single_transport_customer.at(j).customer) = true;
					transport_cost_customer_s += solution_single_transport_customer.at(j).amount * transport_cost_customer.at(solution_single_transport_customer.at(j).customer).at(solution_single_transport_customer.at(j).plant);

					transport_cust.at(solution_single_transport_customer.at(j).customer) += solution_single_transport_customer.at(j).amount;
				}
			}

			//std::cout<<"Anteile"<<endl;

			for (j = 0; j < plants.size(); ++j)
			{

				production_plant.at(j) = production_plant.at(j) / all_production;
				//1
				transport_ic.at(j) = transport_ic.at(j); // / (pallets + inventory_start);

				start_inventory_plant.at(j) = inventory.at(j).at(i).at(k - 1).first / inventory_start;

				end_inventory_plant.at(j) = inventory.at(j).at(i).at(k - 1).second / inventory_end;
			}
			for (j = 0; j < customers.size(); ++j)
			{
				//2
				transport_cust.at(j) = transport_cust.at(j) / (pallets + inventory_start);
	
			}

			wepaoutputdata << "\"" << products.at(i) << "\"" << ";" << product_names.at(i) << ";" << periods.at(k - 1) << ";" << nMachines << ";";
			for (j = 0; j < resources.size(); ++j)
			{
				if (used_resources.at(j))
				{
					wepaoutputdata << resources.at(j) << "(" << production_machine.at(j) << ")" << " ";
				}
			}

			wepaoutputdata << ";";
			for (j = 0; j < plants.size(); ++j)
			{
				if (used_plants.at(j))
				{
					wepaoutputdata << plants.at(j) << "(" << production_plant.at(j) << ")" << " ";
				}
			}

			wepaoutputdata << ";" << runtime << "; " << tons << "; " << pallets << "; " << runtime_cost << "" << "; ";

			//Startbestand

			wepaoutputdata << inventory_start << ";";
			for (j = 0; j < plants.size(); ++j)
			{
				if (used_si_plants.at(j))
				{
					wepaoutputdata << plants.at(j) << "(" << start_inventory_plant.at(j) << ")" << " ";
				}
			}
			wepaoutputdata << ";";

			wepaoutputdata << inventory_end << ";";
			for (j = 0; j < plants.size(); ++j)
			{
				if (used_ei_plants.at(j))
				{
					wepaoutputdata << plants.at(j) << "(" << end_inventory_plant.at(j) << ")" << " ";
				}
			}
			wepaoutputdata << ";";

			for (j = 0; j < plants.size(); ++j)
			{
				if (transported_plants.at(j))
				{
					wepaoutputdata << plants.at(j) << "(" << transport_ic.at(j) << ")" << " ";
				}
			}

			wepaoutputdata << ";" << transport_cost_plant_s << "" << "; ";


			for (j = 0; j < customers.size(); ++j)
			{
				if (transported_customers.at(j))
				{
					wepaoutputdata << customers.at(j) << "(" << transport_cust.at(j) << ")" << " ";
				}
			}
			wepaoutputdata << ";" << transport_cost_customer_s << "" << endl;

		}
	}



	wepaoutputdata.close();

	//--

	vector<double> workload(resources.size()*periods.size());
	double mean_workload = 0.0;

	ofstream capacityoutputdata(capacityoutputfile.c_str());
	if (german) capacityoutputdata.imbue(locale("german"));
	if (capacityoutputdata.fail())
	{
		cout << "Kann Datei " << capacityoutputfile << " nicht anlegen!" << endl;
		exit(1);
	}
	//std::cout<<"Kapazit�tsauslastung"<<endl;
	capacityoutputdata << "Maschine;Periode;genutzte Schichten;verf�gbare Schichten;Auslastung;Produktion in Tonnen;Produktion in Paletten;Produktionskosten" << endl;
	for (j = 0; j < periods.size(); ++j)
	{
		for (i = 0; i < resources.size(); ++i)
		{
			capacityoutputdata << resources.at(i) << ";" << periods.at(j) << ";" << used_capacity.at(i).at(j) << ";" << capacity.at(i).at(j) << ";";
			if (capacity.at(i).at(j) > 0)
			{
				capacityoutputdata << used_capacity.at(i).at(j) / capacity.at(i).at(j) << ";";
			}
			else
			{
				capacityoutputdata << 1000.0 << ";";//1.0 << ";";
			}
			capacityoutputdata << production_ton.at(i).at(j) << ";" << production_pallet.at(i).at(j) << ";" << production_cost.at(i).at(j) << "" << endl;

			if (capacity.at(i).at(j) > 0)
			{
				workload.at(i*periods.size() + j) = used_capacity.at(i).at(j) / capacity.at(i).at(j);
			}
			else
			{
				workload.at(i*periods.size() + j) = 1.0;
			}
			mean_workload += workload.at(i*periods.size() + j);

		}
	}
	capacityoutputdata.close();

	mean_workload = mean_workload / (double)(resources.size()*periods.size());
	double median_workload = get_median(workload);

	ofstream rundata(runfile.c_str());
	if (german) rundata.imbue(locale("german"));
	if (rundata.fail())
	{
		cout << "Kann Datei " << runfile << " nicht anlegen!" << endl;
		exit(1);
	}
	//std::cout<<"Aggregierte Produktionsmengen-Produkte"<<endl;
	double runtimecost = 0.0;
	rundata << "Werk;Maschine;Produkt;Periode;Produktionsschichten;Produktion Tonnen;Produktion Paletten;Produktionskosten" << endl;
	unordered_map <string, production_result_per_plant_machine_product_period> cumProdQuantities;
	string key;
	string sep = ";";
	production_result_per_plant_machine_product_period currProdResult;
	for (i = 0; i < solution_production.size(); ++i)
	{
		for (j = 0; j < processes.size(); ++j)
		{
			if (processes.at(j).machine == solution_production.at(i).machine && processes.at(j).product == solution_production.at(i).product)
			{

				key = toString<int>(processes.at(j).product_plant_index) + sep + toString<int>(solution_production.at(i).machine) + sep + toString<int>(solution_production.at(i).product) + sep + toString<int>(solution_production.at(i).period);

				if (cumProdQuantities.find(key) == cumProdQuantities.end())
				{
					currProdResult.plant = processes.at(j).product_plant_index;
					currProdResult.machine = solution_production.at(i).machine;
					currProdResult.product = solution_production.at(i).product;
					currProdResult.period = solution_production.at(i).period;
					currProdResult.runtime = 0;
					currProdResult.tons = 0;
					currProdResult.nrPallets = 0;
					currProdResult.cost = 0;
					cumProdQuantities.insert(pair<string, production_result_per_plant_machine_product_period>(key, currProdResult));
				}

				cumProdQuantities.at(key).runtime += solution_production.at(i).runtime;
				cumProdQuantities.at(key).tons += processes.at(j).tonspershift * solution_production.at(i).runtime;
				cumProdQuantities.at(key).nrPallets += nPallets_per_ton.at(processes.at(j).product) * processes.at(j).tonspershift * solution_production.at(i).runtime;
				cumProdQuantities.at(key).cost += processes.at(j).costspershift * solution_production.at(i).runtime;
				//currResult.



				//if (cumProdQuantities. (key)==cumProdQuantities .end ().)
				//{
				//}

				//rundata << plants.at(processes.at(j).product_plant_index) << ";" << resources.at(solution_production.at(i).machine) << ";" << "\"" << products.at(solution_production.at(i).product) << "\"" << ";" << periods.at(solution_production.at(i).period - 1) << ";" << solution_production.at(i).runtime << "; " << processes.at(j).tonspershift * solution_production.at(i).runtime << "; " << nPallets_per_ton.at(processes.at(j).product) * processes.at(j).tonspershift * solution_production.at(i).runtime << "; " << processes.at(j).costspershift * solution_production.at(i).runtime << "" << endl;
				runtimecost += processes.at(j).costspershift*solution_production.at(i).runtime;
			}
		}
	}

	currProdResult.runtime = 0;
	currProdResult.tons = 0;
	currProdResult.nrPallets = 0;
	currProdResult.cost = 0;
	//std::cout<<"Aggregierte Produktionsmengen-Kundengruppen"<<endl;
	for (i = 0; i < customer_group_solution_production.size(); ++i)
	{
		for (j = 0; j < processes.size(); ++j)
		{
			if (processes.at(j).machine == customer_group_solution_production.at(i).machine && processes.at(j).product == customer_group_solution_production.at(i).product)
			{
				key = toString<int>(processes.at(j).product_plant_index) + sep + toString<int>(customer_group_solution_production.at(i).machine) + sep + toString<int>(customer_group_solution_production.at(i).product) + sep + toString<int>(customer_group_solution_production.at(i).period);

				if (cumProdQuantities.find(key) == cumProdQuantities.end())
				{
					currProdResult.plant = processes.at(j).product_plant_index;
					currProdResult.machine = customer_group_solution_production.at(i).machine;
					currProdResult.product = customer_group_solution_production.at(i).product;
					currProdResult.period = customer_group_solution_production.at(i).period;

					cumProdQuantities.insert(pair<string, production_result_per_plant_machine_product_period>(key, currProdResult));
				}

				cumProdQuantities.at(key).runtime += customer_group_solution_production.at(i).runtime;
				cumProdQuantities.at(key).tons += processes.at(j).tonspershift * customer_group_solution_production.at(i).runtime;
				cumProdQuantities.at(key).nrPallets += nPallets_per_ton.at(processes.at(j).product) * processes.at(j).tonspershift * customer_group_solution_production.at(i).runtime;
				cumProdQuantities.at(key).cost += processes.at(j).costspershift * customer_group_solution_production.at(i).runtime;
				//rundata << plants.at(processes.at(j).product_plant_index) << ";" << resources.at(customer_group_solution_production.at(i).machine) << ";" << "\"" << products.at(customer_group_solution_production.at(i).product) << "\"" << ";" << periods.at(customer_group_solution_production.at(i).period - 1) << ";" << customer_group_solution_production.at(i).runtime << "; " << processes.at(j).tonspershift * customer_group_solution_production.at(i).runtime << "; " << nPallets_per_ton.at(processes.at(j).product) * processes.at(j).tonspershift * customer_group_solution_production.at(i).runtime << "; " << processes.at(j).costspershift * customer_group_solution_production.at(i).runtime << "" << endl;
				runtimecost += processes.at(j).costspershift*customer_group_solution_production.at(i).runtime;
			}
		}
	}

	unordered_map <string, production_result_per_plant_machine_product_period>::iterator itProd;

	itProd = cumProdQuantities.begin();
	while (itProd != cumProdQuantities.end())
	{
		rundata << plants.at((*itProd).second.plant) << ";" << resources.at((*itProd).second.machine) << ";" << "\"" << products.at((*itProd).second.product) << "\"" << ";" << periods.at((*itProd).second.period - 1) << ";" << (*itProd).second.runtime << "; " << (*itProd).second.tons << "; " << (*itProd).second.nrPallets << "; " << (*itProd).second.cost << "" << endl;
		itProd++;
	}

	rundata.close();

	cout << endl;
	cout << "Laufkosten: " << runtimecost << endl;

	double transiccost = 0.0;

	ofstream transsicdata(transsicfile.c_str());
	if (german) transsicdata.imbue(locale("german"));
	if (transsicdata.fail())
	{
		cout << "Kann Datei " << transsicfile << " nicht anlegen!" << endl;
		exit(1);
	}
	transsicdata << "Produkt;Start;Ziel;Periode;Palettenanzahl;Kosten" << endl;

	unordered_map <string, transport_result_per_product_start_dest_period> cumTranspPlantQuantities;
	unordered_map <string, transport_result_per_product_start_dest_period>::iterator itTransp;

	transport_result_per_product_start_dest_period currTranspResult;

	//std::cout<<"Aggregierte Transportmengen-Produkte"<<endl;
	for (i = 0; i < solution_single_transport_plant.size(); ++i)
	{
		key = toString<int>(solution_single_transport_plant.at(i).product) + sep + toString<int>(solution_single_transport_plant.at(i).plant1) + sep + toString<int>(solution_single_transport_plant.at(i).plant2) + sep + toString<int>(solution_single_transport_plant.at(i).period);
		if (cumTranspPlantQuantities.find(key) == cumTranspPlantQuantities.end())
		{
			currTranspResult.product = solution_single_transport_plant.at(i).product;
			currTranspResult.start = solution_single_transport_plant.at(i).plant1;
			currTranspResult.dest = solution_single_transport_plant.at(i).plant2;
			currTranspResult.period = solution_single_transport_plant.at(i).period;
			currTranspResult.amount = 0;
			currTranspResult.cost = 0;

			cumTranspPlantQuantities.insert(pair<string, transport_result_per_product_start_dest_period>(key, currTranspResult));
		}

		cumTranspPlantQuantities.at(key).amount += solution_single_transport_plant.at(i).amount;
		cumTranspPlantQuantities.at(key).cost += solution_single_transport_plant.at(i).amount * transport_cost_plant.at(solution_single_transport_plant.at(i).plant1).at(solution_single_transport_plant.at(i).plant2);
		//transsicdata << "\"" << products.at(solution_single_transport_plant.at(i).product) << "\"" << ";" << plants.at(solution_single_transport_plant.at(i).plant1) << ";" << plants.at(solution_single_transport_plant.at(i).plant2) << ";" << periods.at(solution_single_transport_plant.at(i).period - 1) << ";" << solution_single_transport_plant.at(i).amount << ";" << solution_single_transport_plant.at(i).amount * transport_cost_plant.at(solution_single_transport_plant.at(i).plant1).at(solution_single_transport_plant.at(i).plant2) << "" << endl;
		transiccost += solution_single_transport_plant.at(i).amount * transport_cost_plant.at(solution_single_transport_plant.at(i).plant1).at(solution_single_transport_plant.at(i).plant2);
	}
	//std::cout<<"Aggregierte Transportmengen-Kundengruppen"<<endl;
	currTranspResult.amount = 0;
	currTranspResult.cost = 0;
	for (i = 0; i<customer_group_solution_single_transport_plant.size(); i++)
	{
		key = toString<int>(customer_group_solution_single_transport_plant.at(i).product) + sep + toString<int>(customer_group_solution_single_transport_plant.at(i).plant1) + sep + toString<int>(customer_group_solution_single_transport_plant.at(i).plant2) + sep + toString<int>(customer_group_solution_single_transport_plant.at(i).period);

		if (cumTranspPlantQuantities.find(key) == cumTranspPlantQuantities.end())
		{
			currTranspResult.product = customer_group_solution_single_transport_plant.at(i).product;
			currTranspResult.start = customer_group_solution_single_transport_plant.at(i).plant1;
			currTranspResult.dest = customer_group_solution_single_transport_plant.at(i).plant2;
			currTranspResult.period = customer_group_solution_single_transport_plant.at(i).period;

			cumTranspPlantQuantities.insert(pair<string, transport_result_per_product_start_dest_period>(key, currTranspResult));
		}

		cumTranspPlantQuantities.at(key).amount += customer_group_solution_single_transport_plant.at(i).amount;
		cumTranspPlantQuantities.at(key).cost += customer_group_solution_single_transport_plant.at(i).amount * transport_cost_plant.at(customer_group_solution_single_transport_plant.at(i).plant1).at(customer_group_solution_single_transport_plant.at(i).plant2);
		transiccost += customer_group_solution_single_transport_plant.at(i).amount * transport_cost_plant.at(customer_group_solution_single_transport_plant.at(i).plant1).at(customer_group_solution_single_transport_plant.at(i).plant2);
	}


	//itProd=cumProdQuantities.begin ();
	itTransp = cumTranspPlantQuantities.begin();
	while (itTransp != cumTranspPlantQuantities.end())
	{
		transsicdata << "\"" << products.at((*itTransp).second.product) << "\"" << ";" << plants.at((*itTransp).second.start) << ";" << plants.at((*itTransp).second.dest) << ";" << periods.at((*itTransp).second.period - 1) << ";" << (*itTransp).second.amount << ";" << (*itTransp).second.cost << "" << endl;
		//rundata << plants.at((*itProd).second.plant) << ";" << resources.at((*itProd).second.machine) << ";" << "\"" << products.at((*itProd).second.product) << "\"" << ";" << periods.at((*itProd).second.period - 1) << ";" << (*itProd).second.runtime << "; " << (*itProd).second.tons << "; " << (*itProd).second.nrPallets  << "; " << (*itProd).second.cost << "" << endl;
		itTransp++;
	}

	transsicdata.close();


	double transcustomercost = 0.0;

	ofstream transscustomerdata(transscustomerfile.c_str());
	if (german) transscustomerdata.imbue(locale("german"));
	if (transscustomerdata.fail())
	{
		cout << "Kann Datei " << transscustomerfile << " nicht anlegen!" << endl;
		exit(1);
	}
	transscustomerdata << "Produkt;Werk;Kunde;Periode;Kundentransporte Paletten;Kundentransportkosten" << endl;
	for (i = 0; i < solution_single_transport_customer.size(); ++i)
	{
		transscustomerdata << "\"" << products.at(solution_single_transport_customer.at(i).product) << "\"" << ";" << plants.at(solution_single_transport_customer.at(i).plant) << ";" << customers.at(solution_single_transport_customer.at(i).customer) << ";" << periods.at(solution_single_transport_customer.at(i).period - 1) << ";" << solution_single_transport_customer.at(i).amount << ";" << solution_single_transport_customer.at(i).amount * transport_cost_customer.at(solution_single_transport_customer.at(i).customer).at(solution_single_transport_customer.at(i).plant) << "" << endl;
		transcustomercost += solution_single_transport_customer.at(i).amount * transport_cost_customer.at(solution_single_transport_customer.at(i).customer).at(solution_single_transport_customer.at(i).plant);
	}


	transscustomerdata.close();

	cout << "IC-Transportkosten: " << transiccost << endl;
	cout << "Kunden-Transportkosten: " << transcustomercost << endl;

	double externalwarehousecost = 0.0;
	ofstream warehousedata(warehousecostfile.c_str());
	if (german) warehousedata.imbue(locale("german"));
	if (warehousedata.fail())
	{
		cout << "Kann Datei " << warehousecostfile << " nicht anlegen!" << endl;
		exit(1);
	}
	warehousedata << "Werk;Periode;ExterneLagermenge;ExterneLagerkosten" << endl;
	for (i = 0; i < solution_external_warehouses.size(); ++i)
	{
		for (j = 1; j < solution_external_warehouses.at(i).size(); ++j)
		{
			warehousedata << plants.at(i) << ";" << periods.at(j - 1) << ";" << solution_external_warehouses.at(i).at(j).first << ";" << solution_external_warehouses.at(i).at(j).second << endl;
			externalwarehousecost += solution_external_warehouses.at(i).at(j).second;
		}
	}

	cout << "externe Lagerkosten: " << externalwarehousecost << endl;

	ofstream dummydata(dummyfile.c_str());
	if (german) dummydata.imbue(locale("german"));
	if (dummydata.fail())
	{
		cout << "Kann Datei " << dummyfile << " nicht anlegen!" << endl;
		exit(1);
	}
	dummydata << "Maschine;Periode;fehlendeSchichten;Kosten" << endl;
	for (i = 0; i < solution_dummy.size(); ++i)
	{
		dummydata << resources.at(solution_dummy.at(i).resource) << ";" << periods.at(solution_dummy.at(i).period - 1) << ";" << solution_dummy.at(i).value << ";" << solution_dummy.at(i).cost << "" << endl;
	}
	dummydata.close();

	ofstream inventorydata(inventoryfile.c_str());
	if (german) inventorydata.imbue(locale("german"));
	if (inventorydata.fail())
	{
		cout << "Kann Datei " << inventoryfile << " nicht anlegen!" << endl;
		exit(1);
	}
	inventorydata << "Werk;Produkt;Periode;Startbestand;Endbestand" << endl;

	unordered_map <string, inventory_result_per_location_product_period> cumInvQuantities;
	unordered_map <string, inventory_result_per_location_product_period>::iterator itInv;
	inventory_result_per_location_product_period currInvResult;
	currInvResult.amount_start = currInvResult.amount_end = 0;
	//std::cout<<"Aggregierte Lagermengen-Produkte"<<endl;
	for (i = 0; i < solution_inventory.size(); ++i)
	{
		key = toString<int>(solution_inventory.at(i).plant) + sep + toString<int>(solution_inventory.at(i).product) + sep + toString<int>(solution_inventory.at(i).period);

		if (cumInvQuantities.find(key) == cumInvQuantities.end())
		{
			currInvResult.location = solution_inventory.at(i).plant;
			currInvResult.product = solution_inventory.at(i).product;
			currInvResult.period = solution_inventory.at(i).period;
			cumInvQuantities.insert(pair<string, inventory_result_per_location_product_period>(key, currInvResult));
		}

		cumInvQuantities.at(key).amount_start += solution_inventory.at(i).amount_start;
		cumInvQuantities.at(key).amount_end += solution_inventory.at(i).amount_end;

		//inventorydata << plants.at(solution_inventory.at(i).plant) << ";" << "\"" << products.at(solution_inventory.at(i).product) << "\"" << ";" << periods.at(solution_inventory.at(i).period - 1) << ";" << solution_inventory.at(i).amount_start << ";" << solution_inventory.at(i).amount_end << endl;
	}
	//std::cout<<"Aggregierte Lagermengen-Kundengruppen"<<endl;
	for (i = 0; i<customer_group_solution_inventory.size(); i++)
	{
		key = toString<int>(customer_group_solution_inventory.at(i).plant) + sep + toString<int>(customer_group_solution_inventory.at(i).product) + sep + toString<int>(customer_group_solution_inventory.at(i).period);

		if (cumInvQuantities.find(key) == cumInvQuantities.end())
		{
			currInvResult.location = customer_group_solution_inventory.at(i).plant;
			currInvResult.product = customer_group_solution_inventory.at(i).product;
			currInvResult.period = customer_group_solution_inventory.at(i).period;
			cumInvQuantities.insert(pair<string, inventory_result_per_location_product_period>(key, currInvResult));
		}

		cumInvQuantities.at(key).amount_start += customer_group_solution_inventory.at(i).amount_start;
		cumInvQuantities.at(key).amount_end += customer_group_solution_inventory.at(i).amount_end;
	}

	itInv = cumInvQuantities.begin();

	while (itInv != cumInvQuantities.end())
	{
		//inventorydata << plants.at(solution_inventory.at(i).plant) << ";" << "\"" << products.at(solution_inventory.at(i).product) << "\"" << ";" << periods.at(solution_inventory.at(i).period - 1) << ";" << solution_inventory.at(i).amount_start << ";" << solution_inventory.at(i).amount_end << endl;
		inventorydata << plants.at((*itInv).second.location) << ";" << "\"" << "\"" << products.at((*itInv).second.product) << "\"" << ";" << periods.at((*itInv).second.period - 1) << ";" << (*itInv).second.amount_start << ";" << (*itInv).second.amount_end << endl;
		itInv++;
	}
	inventorydata.close();


	ofstream guidata(guifile.c_str());
	if (german) guidata.imbue(locale("german"));
	if (guidata.fail())
	{
		cout << "Kann Datei " << guifile << " nicht anlegen!" << endl;
		exit(1);
	}
	guidata << "Produktionskosten;IC-Transportkosten;Kundentransportkosten;Lagerkosten;gesamt;mittlere Kapazitaetsauslastung;Median Kapazitaetsauslastung" << endl;
	guidata << fixed << setprecision(2) << round(runtimecost, 2) << "" << ";" << round(transiccost, 2) << "" << ";" << round(transcustomercost, 2) << "" << ";" << round(externalwarehousecost, 2) << ";" << round(runtimecost + transiccost + transcustomercost + externalwarehousecost, 2) << "" << ";" << round(mean_workload, 2) << ";" << round(median_workload, 2) << endl;
	guidata.close();

	cout << endl << "gesamt: " << runtimecost + transiccost + transcustomercost + externalwarehousecost << endl;

}

void write_logfile(const string & logfile,WEPA_Opt_Params & usedParams)
{
	ofstream output(logfile.c_str());
	if (output.fail())
	{
		cout << "Kann Datei " << logfile << " nicht anlegen!" << endl;
		exit(1);
	}

	output << "Input-Parameter" << endl;
	output << "minimale Losgroesse: " << usedParams.min_runtime << " Schichten" << endl;
	output << "minimale Logroesse fuer Splittung: " << usedParams.min_for_split << " Schichten" << endl;
	output << "Faktor fuer Mindestbestand: " << usedParams.minimum_inventory_factor << endl;
	output << "Faktor fuer verfuegbare Schichten: " << usedParams.capacity_factor << endl;
	if (usedParams.use_baskets)
	{
		output << "Beruecksichtigung von Warenkoerben" << endl;
	}
	else
	{
		output << "Keine Beruecksichtigung von Warenkoerben" << endl;
	}
	output << endl;

	output << "Eckdaten" << endl;
	output << nPlants << " Werke" << endl;
	output << nResources << " Maschinen" << endl;
	output << nProducts << " Artikel" << endl;
	output << nProcesses << " Produktionsprozesse" << endl;
	output << nCustomers << " Kundenstandorte" << endl;
	output << nTransportProcessesPlants << " IC-Transportprozesse" << endl;
	output << nTransportProcessesCustomer << " Kunden-Transportprozesse" << endl;
	output << endl;

	output << "Rechenzeit" << endl;
	int i;
	for (i = 0; i < tm.get_event_number(); ++i)
	{
		output << tm.get_eventname(i) << ": " << tm.get_eventduration(i) << " Sekunden" << endl;
	}
	output << "gesamt: " << tm.get_overallduration() << " Sekunden" << endl;
	output << endl;
	output << "maximale L�cke zur Optimall�sung: " << usedParams.gap * 100.0 << " Prozent" << endl;


	output.close();
}

int speicherverbrauch()
{
	PROCESS_MEMORY_COUNTERS pmc;
	GetProcessMemoryInfo(GetCurrentProcess(), &pmc, sizeof(pmc));
	return pmc.PagefileUsage;
}

