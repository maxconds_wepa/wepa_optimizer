#include "stringFunctions.h"
#include<iostream>
#include <list>
#include <boost/algorithm/string/replace.hpp>
//using msclr::interop::marshal_as;

int numberDigits=4;
int oc=0;
std::string *charDelete (std::string s,char c)
{
	const char *sc;
	char *nsc;
	std::string *nstr;
	int length,i,j;

	sc= s.c_str();

	length=0;
	while (sc[length]!=0)
		length++;

	nsc=new char [length];

	j=0;
	for (i=0;i<length;i++)
	{
		if (sc[i]!=c)
		{
			nsc[j]=sc[i];
			j++;
		}
	}
	nsc[j]=0;
	
	nstr=new std::string;
	nstr->append(nsc);
	return nstr;
}

std::string *charReplace (std::string s,char oldChar,char newChar)
{
	const char *sc;
	char *nsc;
	std::string *nstr;
	int length,i;

	sc= s.c_str();

	length=0;
	while (sc[length]!=0)
		length++;

	nsc=new char [length+1];

	for (i=0;i<length;i++)
	{
		if (sc[i]!=oldChar)
		{
			nsc[i]=sc[i];
		}
		else
		{
			nsc[i]=newChar;
		}
	}

	nsc[i]=0;
	
	nstr=new std::string;
	nstr->append(nsc);
	return nstr;
}

//char *fromSystemStringToCChar(String ^s)
//{
//   pin_ptr<const wchar_t> wch = PtrToStringChars(s);
//   //printf_s("%S\n", wch);
//
//   // Conversion to char* :
//   // Can just convert wchar_t* to char* using one of the 
//   // conversion functions such as: 
//   // WideCharToMultiByte()
//   // wcstombs_s()
//   // ... etc
//   size_t convertedChars = 0;
//   size_t  sizeInBytes = ((s->Length + 1) * 2);
//   errno_t err = 0;
//   char    *ch = (char *)malloc(sizeInBytes);
//
//   err = wcstombs_s(&convertedChars, 
//                    ch, sizeInBytes,
//                    wch, sizeInBytes);
//   if (err != 0)
//      printf_s("wcstombs_s  failed!\n");
//   return ch;
//}
//
//void fromSystemStringToStlString(String ^s, string & stlStr)
//{
//	string stdStr;
//
//	stdStr=marshal_as<string>(s);
//	stlStr.append(stdStr);
//}

void setNumberDigits(int n)
{
	numberDigits=n;
}
/*
template <> std::string toString<double> (const double &t)
{
	  std::ostringstream os;
	    
	  os.precision(numberDigits);
	
		os <<std::fixed<< t;
	    return os.str();
}*/

std::string xFormatStr(std::string & source,std::list<std::string> & replacements)
{
	std::string placeHolderBase="%";
	std::list<std::string>::iterator it,itEnd;
	std::string currentStr;
	std::string currentPlaceHolder;
	int i;
	currentStr=source;

	it=replacements.begin();
	itEnd=replacements.end();
	i=1;

	while (it!=itEnd)
	{
		currentPlaceHolder=placeHolderBase+std::to_string(i);
		boost::replace_all(currentStr,currentPlaceHolder,*it);
		i++;
		it++;
	}

	return currentStr;
}

std::string digitStrs []={"0","1","2","3","4","5","6","7","8","9"};
/*
template <> string toString<double> (const double &t)
{
		string nStr;
		int c,d,commaPos,th;
		int vih,vil;
		double v1,v,absT;
		//bool isNegative;
		list<int> l;

		if (oc==36)
			oc+=0;
		//cout<<"OC:<<"<<oc<<" V:"<<t<<endl;//os.str()<<endl;
		//nStr=new string();
		//cout<<t<<endl;
		
		//if (oc==94)
		//	oc=oc;

	
		absT=abs(t);

		th=pow(10,numberDigits);

		vih=floor(absT);

		v1=abs((absT-vih)*th);

		v=floor(v1);

		if (v1-v>=0.5)
			v++;
		vil=v;
		
		do {	
			d=vil%10;
			l.push_back(d);
			vil/=10;
		}
		while (vil>0);
		
		while (l.size()<numberDigits)
			l.push_back(0);//F�hrende 0 hinzuf�gen
		
		do {	
			d=vih%10;
			l.push_back(d);
			vih/=10;
		}
		while (vih>0);

		if (t<0)
			//os<<"-";
				nStr.append("-");

		do
		{
			d=l.back();
			l.pop_back();
			c=l.size();
			//os<<d;
			//if (d==0)
				//nStr.append("0");
			if (d<0 && d>=10)
			{
				cout<<"Ung�ltiges d"<<endl;
				return nStr;
			}
			nStr.append(digitStrs[d]);
			//else if (d==1)

				//nStr.append("0");//toString<int>(d));

			if (c==numberDigits) //Bei numberDigits verbleibenden Stellen setze einen .
				//os<<".";
					nStr.append(".");
		}
		while (c>0);
	    
		//cout<<"OC:<<"<<oc<<"OSSTR:"<<nStr<<endl;//os.str()<<endl;
		oc++;
		return nStr;//os.str();
}
*/
/*
template <> string toString<int> (const int &t)
{
		string nStr;
		int c,d;
		int absT;
		list<int> l;
		
		absT=abs(t);

		do {	
			d=absT%10;
			l.push_back(d);
			absT/=10;
		}
		while (absT>0);

		if (t<0)
			//os<<"-";
				nStr.append("-");

		do
		{
			d=l.back();
			l.pop_back();
			c=l.size();
			
			nStr.append(digitStrs[d]);
		}
		while (c>0);
	    
		//cout<<"OC:<<"<<oc<<"OSSTR:"<<nStr<<endl;//os.str()<<endl;
		//oc++;
		return nStr;//os.str();
}*/

/*
string doubleToString(const double t)
{
		string nStr;
		int c,d,commaPos,th,absT;
		int vih,vil;
		double v1,v;
		bool isNegative;
		list<int> l;
		char *p;

		cout<<t<<endl;
		
		//if (oc==94)
		//	oc=oc;

		if (t<0)
			isNegative=true;
		else
			isNegative=false;

		absT=abs(t);

		th=pow(10,numberDigits);

		vih=floor(absT);

		v1=abs((absT-vih)*th);

		v=floor(v1);

		if (v1-v>=0.5)
			v++;
		vil=v;
		
		do {	
			d=vil%10;
			l.push_back(d);
			vil/=10;
		}
		while (vil>0);
		
		while (l.size()<numberDigits)
			l.push_back(0);//F�hrende 0 hinzuf�gen
		
		do {	
			d=vih%10;
			l.push_back(d);
			vih/=10;
		}
		while (vih>0);

		if (isNegative)
			//os<<"-";
				nStr->append("-");

		do
		{
			d=l.back();
			l.pop_back();
			c=l.size();
			//os<<d;
			//if (d==0)
				//nStr.append("0");
			nStr->append(digitStrs[d]);
			//else if (d==1)

				//nStr.append("0");//toString<int>(d));

			if (c==numberDigits) //Bei numberDigits verbleibenden Stellen setze einen .
				//os<<".";
					nStr->append(".");
		}
		while (c>0);
		
		cout<<"OC:<<"<<oc<<"OSSTR:"<<*nStr<<endl;//os.str()<<endl;
		//oc++;
		return nStr;//os.str();
}
*/