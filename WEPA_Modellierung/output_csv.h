#pragma once
#ifndef OUTPUT_CSV_H
#define OUTPUT_CSV_H

#include <vector>
#include <fstream>
#include <iostream>
#include <set>
#include <string>
#include <map>

#include "data.h"
#include "bib.h"
#include "data_customer_backtracking.h"
#include "customer_backtracking_model.h"


using namespace std;

class Output_CSV
{
public:

	Output_CSV(const string & filename_pallets,
		const string & filename_tons,
		const string & filename_monthly,
		const string & filename_article,
		const vector<string> & periods,
		const vector<string> & products,
		const vector<string> & product_names,
		const vector<production_result> & solution_production,
		const vector<customer_group_production_result> & customer_group_solution_production,
		const vector<string> & plants,
		const vector<string> & machines,
		const vector<production> & processes,
		const vector<double> & nPallets_per_ton,
		const int & nMachines,
		const int & nProducts,
		const vector<inventory_result> & solution_inventory,
		const vector<customer_group_inventory_result> & customer_group_solution_inventory,
		const vector<single_transport_result_plant> & solution_single_transport_plant,
		const vector<single_customer_group_transport_result_plant> & customer_group_solution_single_transport_plant,
		const vector<single_transport_result_customer> & solution_single_transport_customer,
		const vector<transports> & transport_plant,
		const vector<transports> & transport_customer,
		const vector<demand> demands,
		const vector<string> & customers,
		const string & filename_graphic,
		const string & filename_names,
		const string & filename_graphic_ic,
		//const vector<article_customer_fixations> & customer_fixations
		const Article_customer_fixation_collection & custFixations,
		const string & comparisonFile,
		const string & expansion_stage
	);


	void output_wepa(const bool & german);
	void output_wepa_pallets(const bool & german);
	void output_wepa_ton(const bool & german);

	void output_comparison(const bool & german);

	void get_machine_locations();

	void get_overall_productions();

	double get_production_pallets(const int & solution_production_index);
	double get_production_tons(const int & solution_production_index);
	//double get_production_pallets(const int & solution_production_index);
	//double get_production_tons(const int & solution_production_index);

	double get_customer_group_production_pallets(const int & customer_group_solution_production_index);
	double get_customer_group_production_tons(const int & customer_group_solution_production_index);
	//double get_customer_group_production_pallets(const int & customer_group_solution_production_index);
	//double get_customer_group_production_tons(const int & customer_group_solution_production_index);


	double get_relation_production_pallets(const int & solution_production_index);
	double get_relation_production_tons(const int & solution_production_index);
	//double get_relation_production_pallets(const int & solution_production_index);
	//double get_relation_production_tons(const int & solution_production_index);


	double get_production_cost(const int & solution_production_index);

	double get_production_cost_pallet(const int & solution_production_index);
	double get_production_cost_ton(const int & solution_production_index);
	//double get_production_cost_pallet(const int & solution_production_index);
	//double get_production_cost_ton(const int & solution_production_index);

	//pair<double, double> get_inventory(const int & solution_production_index);
	pair<double, double> get_inventory_pallets(const int & solution_production_index);
	pair<double, double> get_inventory_tons(const int & solution_production_index);


	transport_information get_customer_transports_pallets(int product, int period, int machine);//(const int & solution_production_index);
	transport_information get_customer_transports_tons(int product, int period, int machine);//(const int & solution_production_index);

																							 //pair<double, double> get_ic_transports(const int & solution_production_index);
	pair<double, double> get_ic_transports_pallets(const int & solution_production_index);
	pair<double, double> get_ic_transports_tons(const int & solution_production_index);

	//pair<double,double> get_customer_group_ic_transports(const int & customer_group_solution_production_index);

	double get_customer_group_relation_production_pallets(const int & customer_group_solution_production_index);
	double get_customer_group_relation_production_tons(const int & customer_group_solution_production_index);
	//double get_customer_group_relation_production_pallets(const int & customer_group_solution_production_index);
	//double get_customer_group_relation_production_tons(const int & customer_group_solution_production_index);

	double get_customer_group_production_cost(const int & customer_group_solution_production_index);

	double get_customer_group_production_cost_pallet(const int & customer_group_solution_production_index);
	double get_customer_group_production_cost_ton(const int & customer_group_solution_production_index);
	//double get_customer_group_production_cost_pallet(const int & customer_group_solution_production_index);
	//double get_customer_group_production_cost_ton(const int & customer_group_solution_production_index);

	pair<double, double> get_customer_group_inventory_pallets(const int & customer_group_solution_production_index);
	pair<double, double> get_customer_group_inventory_tons(const int & customer_group_solution_production_index);

	//transport_information get_customer_group_customer_transports(const int & customer_group_solution_production_index);

	//pair<double, double> get_customer_group_ic_transports(const int & customer_group_solution_production_index);
	pair<double, double> get_customer_group_ic_transports_pallets(const int & customer_group_solution_production_index);
	pair<double, double> get_customer_group_ic_transports_tons(const int & customer_group_solution_production_index);



	void set_transport_indices_plant();

	void set_transport_indices_customer();

	int get_transport_index_plant(const int & plant1, const int & plant2);

	int get_transport_index_customer(const int & plant, const int & customer);

	void monthly_to_merge();

	void article_to_merge();

	void get_customers_for_products();

	void get_production_plants();
	//void get_production_plants_pallets();
	//void get_production_plants_tons();

	void get_inventories();

	void get_all_customer_transports();

	void get_all_ic_transports();

	void get_3month_production();

	void output_for_graphic(const bool & german);

	void output_for_graphic_ic(const bool & german);

	void get_production_per_plant();

	void get_customer_transports_per_plant();

	void get_ic_transports_per_plant();

	void get_overall_production_per_plant();

	void get_overall_customer_transports_per_plant();

	void get_overall_ic_transports_per_plant();

	void write_article_names();

	void prepare_model_data();

	void get_ic_transports();


private:
	const string & m_filename_pallets;
	const string & m_filename_tons;
	const string & m_filename_monthly;
	const string & m_filename_article;
	const vector<string> & m_periods;
	const vector<string> & m_products;
	const vector<string> & m_product_names;
	const vector<production_result> & m_solution_production;
	const vector<customer_group_production_result> & m_customer_group_solution_production;
	const vector<string> & m_plants;
	const vector<string> & m_machines;
	const vector<production> & m_processes;
	const vector<double> & m_nPallets_per_ton;
	const int & m_nMachines;
	const int & m_nProducts;
	const vector<inventory_result> & m_solution_inventory;
	const vector<customer_group_inventory_result> & m_customer_group_solution_inventory;
	const vector<single_transport_result_plant> & m_solution_single_transport_plant;
	const vector<single_customer_group_transport_result_plant> & m_customer_group_solution_single_transport_plant;
	const vector<single_transport_result_customer> & m_solution_single_transport_customer;
	const vector<transports> & m_transport_plant;
	const vector<transports> & m_transport_customer;
	const vector<demand> & m_demands;
	const vector<string> & m_customers;
	const string & m_filename_graphic;
	const string & m_filename_names;
	const string & m_filename_graphic_ic;

	const string & m_comparisonFile;
	const string & m_expansion_stage;
	//const vector<article_customer_fixations> m_customer_fixations;
	Article_customer_fixation_collection & m_custFixations;

	vector<int> machine_locations;
	vector<double> overall_production;
	vector<vector<int> > transport_indices_plant;
	vector<vector<int> > transport_indices_customer;
	vector<set<int> > customers_for_products;

	vector<vector<vector<pair<int, double> > > > production_per_plant;
	//vector<vector<vector<pair<int, double> > > > production_per_plant_pallets;
	//vector<vector<vector<pair<int, double> > > > production_per_plant_tons;
	vector<vector<production_information> > production_per_article;
	//vector<vector<production_information> > production_per_article_pallets;
	//vector<vector<production_information> > production_per_article_tons;

	vector<vector<vector<pair<int, double> > > > production_per_machine;
	//vector<vector<vector<pair<int, double> > > > production_per_machine_pallets;
	//vector<vector<vector<pair<int, double> > > > production_per_machine_tons;

	vector<vector<pair<double, double> > > inventories;
	vector<vector<transport_information> > customer_transports;
	vector<vector<transport_information> > ic_transports;

	vector<transport_information> threemonth_customer_transports;
	vector<transport_information> threemonth_ic_transports;

	vector<production_information> threemonth_production;
	vector<vector<production_information> > productions_per_plant;
	vector<vector<transport_information> > customer_transports_per_plant;
	vector<vector<transport_information> > ic_transports_per_plant;
	vector<vector<transport_information> > ic_in_transports_per_plant;
	vector<vector<transport_information> > ic_out_transports_per_plant;
	vector<production_information> overall_productions_per_plant;
	vector<transport_information> overall_customer_transports_per_plant;
	vector<transport_information> overall_ic_transports_per_plant;
	vector<transport_information> overall_ic_in_transports_per_plant;
	vector<transport_information> overall_ic_out_transports_per_plant;

	vector<vector<vector<transport_information> > > ic_in_transports;
	vector<vector<vector<transport_information> > > ic_out_transports;
	vector<vector<vector<transport_information> > > ic_transport;

	vector<vector<map<int, double> > > pallet_production_model;
	vector<vector<map<int, double> > > customer_group_pallet_production_model;
	vector<vector<map<int, double> > > transport_per_customer_model;
	vector<vector<map<int, double> > > customer_group_transport_per_customer_model;
	vector<vector<vector<pair<double, double> > > > inventory_model;
	vector<vector<vector<pair<double, double> > > > customer_group_inventory_model;
	vector<vector<vector<transport_information2> > > detailled_transports_customer_model;
	vector<vector<vector<transport_information2> > > customer_group_detailled_transports_customer_model;
	vector<vector<vector<transport_information2> > > detailled_transports_plants_model;
	vector<vector<vector<transport_information2> > > customer_group_detailled_transports_plants_model;

	vector<vector<vector<map<int, double> > > > backtrackings;

	vector<vector<vector<map<int, double> > > > customer_group_backtrackings;

	/*struct {
	int plant;
	int machine;
	int period;
	double quantity;
	} ProductionEntry;

	struct {
	int plant;
	int period;
	double quantity;
	} InventoryEntry*/
};

#endif