/*! \file
* data structures
*/

#ifndef DATA_H
#define DATA_H

#include <vector>
#include<unordered_map>
#include<unordered_set>
using namespace std;

//struct fixation_process
//{
//	int resource;
//	int plant;
//};
template<class T> T fromString(const string& s)
{
     istringstream stream (s);
     T t;
     stream >> t;
     return t;
}

template <typename T> string toString (const T &t) {
	    ostringstream os;
	    os << t;
	    return os.str();
}

struct CustFixationProcessInfo
{
	int processNumber;
	bool isPrioritized;
};

class article_customer_fixations
{
public:
	int article;
	vector <int> customerLocations;
	//vector<fixation_process> allowedProcesses;
	vector<CustFixationProcessInfo> allowedProcesses;

	void addCustomerLocation(int cLoc);
	void addAllowedProcess(CustFixationProcessInfo procInfo);
};

//class customer_product_related_transport
//{
//public:
//	int article;
//	int transportation_process;
//};

class Customer_product_related_transports_collection
{
protected:
	unordered_map<int,unordered_map<int,unordered_set<int>>> crTranspProcesses;
public:
	Customer_product_related_transports_collection();
	void add(int customer,int product,int plant);
	bool isPlantValidForCustomerArticle (int customer,int product,int plant);
	list<int> getPlantsForCustomerProduct (int customer,int product);
};


class Article_customer_fixation_collection {

protected:
	vector<article_customer_fixations> acFixations;
	//unordered_map<int,unordered_set<int>> customersOfProduct;
	unordered_map<int,unordered_map<int,int>> customerFixationIndices;
public:
	Article_customer_fixation_collection();
	int numberACFixations();
	bool isCustomerLocationInFixationsForArticle (int customer,int product);
	int getCustomerFixationIndex(int customer,int product);
	article_customer_fixations & get(int i);
	void add(article_customer_fixations acf);
};


/*!
* transports
* \param start plant index or customer index
* \param end customer index
* \param cost cost
*/
struct transports
{
	int start;
	int end;
	double cost;
};

/*!
* production processes
* \param product product index
* \param machine machine index
* \param tonspershift produced tons per shift
* \param costspershift process costs per shift
* \param product_plant_index index of the corresponding plant
*/
struct production
{
	int product;
	int machine;
	double tonspershift;
	double costspershift;
	int product_plant_index;
};

/*!
* demands
* \param customer customer
* \param product index
* \param period period index
* \param amount demand
*/
struct demand
{
	int customer;
	int product;
	int period;
	double amount;
};

/*!
* production results
* \param product product index
* \param machine machine index
* \param period period index
* \param runtime run time of the machine
* \param process process index
*/
struct production_result
{
	int product;
	int machine;
	int period;
	double runtime;
	int process;
};

/*!
* customer group production results
* \param product product index
* \param machine machine index
* \param period period index
* \param runtime run time of the machine
* \param process process index
*/
struct customer_group_production_result
{
	int product;
	int machine;
	int period;
	int cGroup;
	double runtime;
	int process;
};


/*!
* ic transport results
* \param product product index
* \param plant1 index of start plant
* \param plant2 index of end plant
* \param period period index
* \param amount number of transported pallets
*/
struct single_transport_result_plant
{
	int product;
	int plant1;
	int plant2;
	int period;
	double amount;

};

/*!
* ic customer group transport results
* \param product product index
* \param plant1 index of start plant
* \param plant2 index of end plant
* \param period period index
* \param amount number of transported pallets
*/
struct single_customer_group_transport_result_plant
{
	int product;
	int plant1;
	int plant2;
	int cGroup;
	int period;
	double amount;

};

/*!
* customer transport results
* \param product product index
* \param plant plant index
* \param customer customer index
* \param period period index
* \param amount number of transported pallets
*/
struct single_transport_result_customer
{
	int product;
	int plant;
	int customer;
	int period;
	double amount;
	
};

/*struct transport_result_plant
{
	int plant1;
	int plant2;
	int period;
	int number;
};

struct transport_result_customer
{
	int plant;
	int customer;
	int period;
	int number;
};
*/

/*!
* exceeding capacity results
* \param resource index of machine
* \param period period index
* \param value exceeded capacity
*/
struct dummy_result
{
	int resource;
	int period;
	double value;
    double cost;
};

/*!
* customer basket
* \param customer customer index
* \param productindices indices of products in basket
*/
struct basket
{
	int customer;
	vector<int> productindices;
};

/*!
*	inventory results
* \param plant plant index
* \param product product index
* \param period period index
* \param amount_start start inventory
* \param amount_end end inventory
*/
struct inventory_result
{
	int plant;
	int product;
	int period;
	double amount_start;
	double amount_end;
};


/*!
* customer group inventory results
* \param plant plant index
* \param product product index
* \param period period index
* \param amount_start start inventory
* \param amount_end end inventory
*/
struct customer_group_inventory_result
{
	int plant;
	int cGroup;
	int product;
	int period;
	double amount_start;
	double amount_end;
};

struct production_result_per_plant_machine_product_period {
	int plant;
	int machine;
	int product;
	int period;
	double runtime;
	double tons;
	double nrPallets;
	double cost;

};


struct transport_result_per_product_start_dest_period {
	int start;
	int dest;
	int product;
	int period;
	
	double amount;
	double cost;
};


struct inventory_result_per_location_product_period
{
	int location;
	int product;
	int period;

	double amount_start;
	double amount_end;
};
/*struct min_inventory_information
{
	int resource;
	int product;
	int cycle;
	int amount;
};*/

/*!
* fixed transports
* \param plant plant index
* \param customer customer index
* \param product product index
* \param period period index
* \param amount transported amount
* \param cost transport cost
*/
struct fixed_transport
{
	int plant;
	int customer;
	int product;
	int period;
	double amount;
	double cost;
};

struct customer_transport_variable_info
{
	int customer;
	int product;
	int index;
};

struct transport_information
{
	double amount;
	double costsum;
	double costpallet;
	double costtons;
};

struct production_information
{
	double costsum;
	double costpallet;
	double costton;
	double shifts;
	double pallets;
	double tons;
};

struct production_information2
{
	int machine_index;
	double production_in_shifts;
	double production_in_tons;
	double production_in_pallets;
	double production_cost;
};

struct transport_information2
{
	int start_index;
	int end_index;
	double amount;
	double cost;
};

struct backtracking_information
{
	int machine_index;
	int customer_index;
	int period_index;
	double amount;
};


#endif