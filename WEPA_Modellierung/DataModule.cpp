#include "DataModule.h"
#include "stringfunctions.h"


const char *DataModule_DB::articleInfoTabBaseCmd=
		"select ART_Nr,"
			   "short_art_name,"
			   "long_art_name,"
			   "pallets_per_ton "
			   "FROM IN_INTMED_ARTICLE_INFO "
			   "where Optimization_Folder_Name='%1'";
const char *DataModule_DB::basketTabBaseCmd=
	"SELECT [Customer_Location_Nr],"
			"[Basket_Nr],"
			"[ART_Nr] "
			"FROM IN_INTMED_BASKETS "
			"where Optimization_Folder_Name='%1'";


/*
select tab1.Production_Resource_Nr as Production_Resource_Nr,
			tab1.Period_Nr as Period_Nr,
			isNull(tab2.capacity,tab1.capacity) as capacity
from IN_INTMED_CAPACITIES tab1
left join (
	SELECT [Production_Resource_Nr]
      ,[Period_Nr]
      ,[capacity]
	FROM [Demo_SNP].[dbo].[USER_CAPACITIES]
	where userid='58c02594d9d62' and 
		  optimization_number=2 and 
		  Optimization_Folder_Name='t01032017'
 ) tab2 on tab1.Production_Resource_Nr=tab2.Production_Resource_Nr and 
		   tab1.Period_Nr=tab2.Period_Nr
where tab1.Optimization_Folder_Name='t01032017'
*/

const char *DataModule_DB::capacitiesTabBaseCmd=
	"select tab1.Production_Resource_Nr as Production_Resource_Nr,"
		   "tab1.Period_Nr as Period_Nr,"
		   "isNull(tab2.capacity,tab1.capacity) as capacity "
		   "from IN_INTMED_CAPACITIES tab1 "
		   "left join ( "
						"SELECT Production_Resource_Nr"
						",Period_Nr"
						",capacity "
						"FROM USER_CAPACITIES "
						"where userid='%2' and "
						"optimization_number=%3 and "
						"Optimization_Folder_Name='%1'"
					") tab2 on tab1.Production_Resource_Nr=tab2.Production_Resource_Nr and "
							  "tab1.Period_Nr=tab2.Period_Nr "
"where tab1.Optimization_Folder_Name='%1'"
;
	/*
			"SELECT [Production_Resource_Nr],"
				   "[Period_Nr],"
				   "[capacity] "
				   "FROM [Demo_SNP].[dbo].[IN_INTMED_CAPACITIES] "
			"where Optimization_Folder_Name='%1'";
			*/
const char *DataModule_DB::customerFixationGroupsTabBaseCmd=
			"SELECT [Group_Nr],"
					"[ART_Nr],"
					"[Customer_Location_Nr],"
					"[Process_Nr] "
					"FROM IN_INTMED_CUSTOMER_FIXATION_GROUPS "
					"where Optimization_Folder_Name='%1'";
const char *DataModule_DB::customerProductRelatedTransportsTabBaseCmd=
			"SELECT [Transport_Nr],"
			"[ART_Nr],"
			"[TransportationProcess_Nr] "
			"FROM IN_INTMED_CUSTOMER_PRODUCT_RELATED_TRANSPORTS "
			"where Optimization_Folder_Name='%1'";
const char *DataModule_DB::customerLocationsTabBaseCmd=
			"SELECT [Customer_Location_Nr],"
			"[location_name] "
			"FROM IN_INTMED_CUSTOMER_LOCATIONS "
			"where Optimization_Folder_Name='%1'";

/*
select tab1.Customer_Location_Nr as Customer_Location_Nr,
	   tab1.ART_Nr as ART_Nr,
	   tab1.Period_Nr as Period_Nr,
	   isNULL(tab2.demand,tab1.demand) as demand
from IN_INTMED_DEMANDS tab1
left join (
	SELECT [userid]
		  ,[optimization_number]
		  ,[timestmp]
		  ,[Optimization_Folder_Name]
		  ,[Customer_Location_Nr]
		  ,[ART_Nr]
		  ,[Period_Nr]
		  ,[demand]
	  FROM [Demo_SNP].[dbo].[USER_DEMANDS]
	  where userid='58bebda4b6c91' and 
			optimization_number=2 and 
			Optimization_Folder_Name='t01032017'
 ) tab2 on tab1.ART_Nr=tab2.ART_Nr and 
		   tab1.Optimization_Folder_Name=tab2.Optimization_Folder_Name and
		   tab1.Period_Nr=tab2.Period_Nr and
		   tab1.Customer_Location_Nr=tab2.Customer_Location_Nr
where tab1.Optimization_Folder_Name='t01032017'
*/
/*
const char *DataModule_DB::demandsTabBaseCmd="SELECT [Customer_Location_Nr],"
											 "[ART_Nr],[Period_Nr],[demand] FROM [Demo_SNP].[dbo].[IN_INTMED_DEMANDS] where Optimization_Folder_Name='%s'"
											 ;
*/
const char *DataModule_DB::demandsTabBaseCmd=
	"select tab1.Customer_Location_Nr as Customer_Location_Nr,"
	"tab1.ART_Nr as ART_Nr,"
	"tab1.Period_Nr as Period_Nr,"
	"isNULL(tab2.demand,tab1.demand) as demand "
	"from IN_INTMED_DEMANDS tab1 "
	"left join ("
	"SELECT userid "
	",optimization_number"
	",timestmp"
	",Optimization_Folder_Name"
	",Customer_Location_Nr"
	",ART_Nr"
	",Period_Nr"
	",demand"
	" FROM USER_DEMANDS "
	"where userid='%2' and "
	"optimization_number=%3 and "
	"Optimization_Folder_Name='%1'"
	") tab2 on tab1.ART_Nr=tab2.ART_Nr and "
			  "tab1.Optimization_Folder_Name=tab2.Optimization_Folder_Name and "
			  "tab1.Period_Nr=tab2.Period_Nr and "
			  "tab1.Customer_Location_Nr=tab2.Customer_Location_Nr "
			  "where tab1.Optimization_Folder_Name='%1'"
	;
const char *DataModule_DB::missingCustomerTransportsTabBaseCmd=
				"SELECT Optimization_Folder_Name,"
				"Plant_Location_Nr,"
				"Customer_Location_Nr"
				" FROM IN_INTMED_MISSING_CUSTOMER_TRANSPORTS "
				" where Optimization_Folder_Name='%1'";
const char *DataModule_DB::scheduledProductionTabBaseCmd=
			"SELECT Process_Nr,"
			"Period_Nr,"
			"quantity "
			"FROM IN_INTMED_SCHEDULED_PRODUCTION "
			" where Optimization_Folder_Name='%1'";
const char *DataModule_DB::plantLocationsTabBaseCmd=
			"SELECT Plant_Location_Nr,"
			"plant_location_name,"
			"warehouse_capacity "
			"FROM IN_INTMED_PLANT_LOCATIONS "
			"where Optimization_Folder_Name='%1'";
const char *DataModule_DB::fixationsTabBaseCmd=
			"SELECT Group_Nr,"
			"ART_Nr,"
			"Customer_Location_Nr,"
			"Process_Nr "
			"FROM IN_INTMED_CUSTOMER_FIXATION_GROUPS "
			"where Optimization_Folder_Name='%1'";
const char *DataModule_DB::periodsInfoTabBaseCmd=
			"SELECT Period_Nr,"
			"period_name "
			"FROM IN_INTMED_PERIOD_INFO "
			"where Optimization_Folder_Name='%1'";
const char *DataModule_DB::productionProcessesTabBaseCmd=
			"SELECT ART_Nr,"
			"Production_Resource_Nr,"
			"Plant_Location_Nr,"
			"ton_per_shift,"
			"cost_per_shift "
			"FROM IN_INTMED_PRODUCTION_PROCESSES "
			"where Optimization_Folder_Name='%1'";
const char *DataModule_DB::productionResourcesTabBaseCmd=
			"SELECT Production_Resource_Nr,"
			"production_resource_name "
			"FROM IN_INTMED_PRODUCTION_RESOURCES "
			"where Optimization_Folder_Name='%1'";
const char *DataModule_DB::customerTransportsTabBaseCmd=
			"SELECT Plant_Location_Nr,"
			"Customer_Location_Nr,"
			"transport_cost "
			"FROM IN_INTMED_CUSTOMER_TRANSPORTS "
			"where Optimization_Folder_Name='%1'";
const char *DataModule_DB::interplantTabBaseCmd=
			"SELECT Source_Plant_Location_Nr,"
			"Destination_Plant_Location_Nr,"
			"transport_cost "
			"FROM IN_INTMED_INTERPLANT_TRANSPORTS "
			"where Optimization_Folder_Name='%1'";

/*
dMCSV.get_input_data(plants, 
					 customers, 
					 resources, 
					 products, 
					 periods, 
					 processes, 
					 capacity, 
					 transport_plant, 
					 transport_customer, 
					 demands, 
					 nPallets_per_ton, 
					 baskets_for_customer,
					 fixations_sales_plan, 
					 productions, 
					 product_names, 
					 plant_capacities, 
					 start_inventory, 
					 minimum_inventory_v1, 
					 minimum_inventory_v2,
					 fixed_transports,
					 custFixations,
					 cpTransports,
					 resourcesWithFixations,m_is_demand,
					 m_is_demand_for_customer_and_product,m_is_demand_for_customer_and_product_in_period,demand_for_product_in_period,
					 usedParams
					 );

*/


void DataModule_CSV::setFileNames (
			string plantFN,
			string customerFN,
			string resourceFN,
			string productFN,
			string periodFN,
			string processFN,
			string capacityFN,
			string transportplantFN,
			string transportcustomerFN,
			string demandFN,
			string palletspertonFN,
			string basketsFN,
			string fixationFN,
			string artikelnamenFN,
			string plant_capacityFN,
			string start_inventoryFN,
			string fixed_transportFN,
			string logFN,
			string customer_fixation_groupFN,
			string customer_product_related_transportFN,
			string resources_with_fixationsFN,
			string customer_prioritized_fixation_groupFN
		)
{
	this->plantfile=plantFN;
	this->customerfile=customerFN;
	this->resourcefile=resourceFN;
	this->productfile=productFN;
	this->periodfile=periodFN;
	this->processfile=processFN;
	this->capacityfile=capacityFN;
	this->transportplantfile=transportplantFN;
	this->transportcustomerfile=transportcustomerFN;
	this->demandfile=demandFN;
	this->palletspertonfile=palletspertonFN;
	this->basketsfile=basketsFN;
	this->fixationfile=fixationFN;
	this->artikelnamenfile=artikelnamenFN;
	this->plant_capacityfile=plant_capacityFN;
	this->start_inventoryfile=start_inventoryFN;
	this->fixed_transport_file=fixed_transportFN;
	this->logfilename=logFN;
	this->customer_fixation_group_file=customer_fixation_groupFN;
	this->customer_fixation_group_file=customer_fixation_groupFN;
	this->customer_product_related_transport_file=customer_product_related_transportFN;
	this->resources_with_fixations_file=resources_with_fixationsFN;
	this->customer_prioritized_fixation_group_file=customer_prioritized_fixation_groupFN;
}

std::string & replaceChar(string s,char oldChar, char newChar)
{
	int pos;
	char *sCh;
	unsigned i,l;
	std::string nStr;

	l = s.length();
	sCh = (char *)s.c_str();
	
	for (i = 0;i < l;i++)
	{
		if (sCh[i] == oldChar)
			sCh[i] = newChar;
	}

	nStr = sCh;

	return nStr;
}

std::string  deleteChar(string s, char oldChar)
{
	int pos;
	char *sCh;
	char *nCh;
	unsigned i, j,l;
	std::string nStr;

	l = s.length();
	sCh = (char *)s.c_str();
	nCh = new char[l+1];
	for (j=i = 0;i < l;i++)
	{
		if (sCh[i] != oldChar)
		{
			nCh[j] = sCh[i];
			j++;
		}
	}

	nCh[j] = 0;
	nStr = nCh;

	delete[] nCh;
	return nStr;
}

void DataModule_CSV::get_input_data(std::vector<std::string> & plants, 
					  std::vector<std::string> & customers, 
					  std::vector<std::string> & resources, 
					  std::vector<std::string> & products, 
					  std::vector<std::string> & periods, 
					  std::vector<production> & processes, 
					  std::vector<std::vector<double> > & capacity, 
					  std::vector<transports> & transport_plant, 
					  std::vector<transports> & transport_customer, 
					  std::vector<demand> & demands, 
					  std::vector<double> & nPallets_per_ton, 
					  std::vector<std::vector<double> > & fixations, 
					  std::vector<std::vector<double> > & productions, 
					  std::vector<std::vector<std::vector<bool> > > & baskets_for_customer, 
					  std::vector<std::string> & product_names, 
					  std::vector<int> & plant_capacities, 
					  std::vector<std::vector<double> > & start_inventory, 
					  std::vector<std::vector<std::vector<double> > > & minimum_inventory_v1, 
					  std::vector<std::vector<double> > & minimum_inventory_v2, 
					  std::vector<fixed_transport> & fixed_transports,
					  Article_customer_fixation_collection & custFixations,
					  Customer_product_related_transports_collection & cpTransports,
					  std::vector<int> & resourcesWithFixations,
					  vector<bool> & m_is_demand,
					  vector<vector<bool> > & m_is_demand_for_customer_and_product,
					  vector<vector<vector<bool> > > & m_is_demand_for_customer_and_product_in_period,
					  vector<vector<double > > & demand_for_product_in_period,
					  WEPA_Opt_Params &params
					  )
{
	plants.clear();
	customers.clear();
	resources.clear();
	products.clear();
	periods.clear();
	processes.clear();
	capacity.clear();
	transport_plant.clear();
	transport_customer.clear();
	demands.clear();
	nPallets_per_ton.clear();
	fixations.clear();
	baskets_for_customer.clear();
	product_names.clear();
	productions.clear();
	fixed_transports.clear();
	
	//customer_fixations.clear ();
	string line;
	string word;
	string product, machine, tonspershift, costspershift, plant;
	string start, end, cost;
	string customer, period, amount;

	
	cout << "Werke..." << endl;
	ifstream plantdata(plantfile.c_str());
	
	if (plantdata.fail())
	{
		cout << "Kann Datei " << plantfile << " nicht oeffnen!" << endl;
		exit(1);
	}
	while (!plantdata.eof())
	{
		getline(plantdata, line);
		if (line.size() > 0)
		{
			istringstream is(line);
			is >> word;
			plants.push_back(word);
		}
	}
	plantdata.close();

	cout << "Kunden..." << endl;
	ifstream customerdata(customerfile.c_str());
	if (customerdata.fail())
	{
		cout << "Kann Datei " << customerfile << " nicht oeffnen!" << endl;
		exit(1);
	}

	string name;
	while (!customerdata.eof())
	{
		getline(customerdata, line);
		if (line.size() > 0)
		{
			istringstream is(line);
			name = "";
			while (!is.eof())
			{
				is >> word;
				
				//To avoid incompatibilities in merge we eliminate all brackets from customer names
				std::string r = deleteChar(word, '('); 
				r = deleteChar(r, ')');
				name = name + r;
			}
			customers.push_back(name);
		}
	}
	customerdata.close();

	cout << "Maschinen..." << endl;
	ifstream resourcedata(resourcefile.c_str());
	if (resourcedata.fail())
	{
		cout << "Kann Datei " << resourcefile << " nicht oeffnen!" << endl;
		exit(1);
	}
	while (!resourcedata.eof())
	{
		getline(resourcedata, line);
		if (line.size() > 0)
		{
			istringstream is(line);
			is >> word;
			resources.push_back(word);
		}
	}
	resourcedata.close();

	cout << "Produkte..." << endl;
	ifstream productdata(productfile.c_str());
	if (productdata.fail())
	{
		cout << "Kann Datei " << productfile << " nicht oeffnen!" << endl;
		exit(1);
	}
	while (!productdata.eof())
	{
		getline(productdata, line);
		if (line.size() > 0)
		{
			istringstream is(line);
			is >> word;
			products.push_back(word);
		}
	}
	productdata.close();

	cout << "Artikelnamen..." << endl;
	vector<pair<string, string> > artikelnamepair;
	ifstream artikelnamendata(artikelnamenfile.c_str());
	if (artikelnamendata.fail())
	{
		cout << "Kann Datei " << artikelnamenfile << " nicht oeffnen!" << endl;
		exit(1);
	}
	while (!artikelnamendata.eof())
	{
		getline(artikelnamendata, line);
		if (line.size() > 0)
		{
			vector<string> daten;
			istringstream is(line);
			string dummy1;
			string dummy2;
			is >> dummy1;
			while (!is.eof())
			{
				is >> dummy2;
				daten.push_back(dummy2);
			}
			string name;
			int i;
			for (i = 0; i < daten.size(); ++i)
			{
				name += daten.at(i) + " ";
			}
			//readin_explode(line, daten, ',');
			//if (daten.size() > 1)
			//{
				artikelnamepair.push_back(pair<string, string>(dummy1, name));
			//}
		}
	}
	artikelnamendata.close();

	int i, j;
	product_names.resize(products.size());
	for (i = 0; i < products.size(); ++i)
	{
		for (j = 0; j < artikelnamepair.size(); ++j)
		{
			if (products.at(i) == artikelnamepair.at(j).first)
			{
				product_names.at(i) = artikelnamepair.at(j).second;
			}
		}
	}

	cout << "Perioden..." << endl;
	ifstream perioddata(periodfile.c_str());
	if (perioddata.fail())
	{
		cout << "Kann Datei " << periodfile << " nicht oeffnen!" << endl;
		exit(1);
	}
	while (!perioddata.eof())
	{
		getline(perioddata, line);
		if (line.size() > 0)
		{
			istringstream is(line);
			is >> word;
			periods.push_back(word);
		}
	}
	perioddata.close();

	cout << "Produktionsprozesse..." << endl;
	ifstream processdata(processfile.c_str());
	if (processdata.fail())
	{
		cout << "Kann Datei " << processfile << " nicht oeffnen!" << endl;
		exit(1);
	}
	production dummy_production;
	while (!processdata.eof())
	{
		getline(processdata, line);
		if (line.size() > 0)
		{
			istringstream is(line);
			is >> dummy_production.product;
			is >> dummy_production.machine;
			is >> dummy_production.tonspershift;
			is >> dummy_production.costspershift;
			is >> dummy_production.product_plant_index;

			if (dummy_production.product < 0)
			{
				cout << "Negativer Produktindex " << dummy_production.product << " in " << processfile << endl;
				exit(44);
			}
			if (dummy_production.product >= products.size())
			{
				cout << "Produktindex " << dummy_production.product << " in " << processfile << " zu gross!" << endl;
				exit(44);
			}
			if (dummy_production.machine < 0)
			{
				cout << "Negativer Maschinenindex " << dummy_production.machine << " in " << processfile << endl;
				exit(45);
			}
			if (dummy_production.machine >= resources.size())
			{
				cout << "Maschinenindex " << dummy_production.machine << " in " << processfile << " zu gross!" << endl;
				exit(45);
			}
			if (dummy_production.tonspershift <= 0)
			{
				cout << "Nicht-positive Tonnen-pro-Schicht-Angabe " << dummy_production.tonspershift << " in " << processfile << endl;
				exit(46);
			}
			if (dummy_production.costspershift < 0)
			{
				cout << "Negative Kosten-pro-Schicht-Angabe " << dummy_production.costspershift << " in " << processfile << endl;
				exit(47);
			}
			if (dummy_production.product_plant_index < 0)
			{
				cout << "Negativer Werksindex " << dummy_production.product_plant_index << " in " << processfile << endl;
				exit(48);
			}
			if (dummy_production.product_plant_index >= plants.size())
			{
				cout << "Werksindex " << dummy_production.product_plant_index << " in " << processfile << " zu gross!" << endl;
				exit(48);
			}
			processes.push_back(dummy_production);
		}
	}
	processdata.close();

	cout << "Maschinenkapazit�ten..." << endl;
	ifstream capacitydata(capacityfile.c_str());
	if (capacitydata.fail())
	{
		cout << "Kann Datei " << capacityfile << " nicht oeffnen!" << endl;
		exit(1);
	}
	i = 0;
	capacity.resize(resources.size(), vector<double>(periods.size()));

	while (!capacitydata.eof())
	{
		getline(capacitydata, line);
		if (line.size() > 0)
		{
			istringstream is(line);
			j = 0;
			while (!is.eof())
			{
				word = "";
				is >> word;
				if (word.size() > 0)
				{
					if (i >= resources.size())
					{
						cout << "Zu viele Kapazitaeten in " << capacityfile << endl;
						exit(23);
					}
					if (j >= periods.size())
					{
						cout << "Zu viele Kapazitaeten fuer Maschine " << resources.at(i) << " in " << capacityfile << endl;
						exit(22);
					}
					capacity.at(i).at(j) = params.capacity_factor * atof(word.c_str());
					if (capacity.at(i).at(j) < 0)
					{
						cout << "negative Kapazitaet fuer Maschine " << resources.at(i) << " in Periode " << periods.at(j) << " in " << capacityfile << endl;
						exit(21);
					}
					j++;
				}

			}
			if (j < periods.size())
			{
				cout << "Zu wenige Kapazitaeten fuer Maschine " << resources.at(i) << " in " << capacityfile << endl;
				exit(24);
			}
			i++;
		}
	}

	if (i < resources.size())
	{
		cout << "Zu wenige Kapazitaeten in " << capacityfile << endl;
		exit(25);
	}
	capacitydata.close();

	cout << "Werkstransporte..." << endl;
	ifstream transportplantdata(transportplantfile.c_str());
	if (transportplantdata.fail())
	{
		cout << "Kann Datei " << transportplantfile << " nicht oeffnen!" << endl;
		exit(1);
	}
	transports dummy_transport;
	vector<vector<bool> > plant_transports(plants.size(), vector<bool>(plants.size(), false));
	while (!transportplantdata.eof())
	{
		getline(transportplantdata, line);
		{
			if (line.size() > 0)
			{
				istringstream is(line);
				is >> dummy_transport.start;
				is >> dummy_transport.end;
				is >> dummy_transport.cost;

				if (params.just_production_cost)
				{
					dummy_transport.cost = 0.0;
				}

				if (dummy_transport.start < 0)
				{
					cout << "Negativer IC-Startindex " << dummy_transport.start << " in " << transportplantfile << endl;
					exit(26);
				}
				if (dummy_transport.start >= plants.size())
				{
					cout << "IC-Startindex " << dummy_transport.start << " in " << transportplantfile << " zu gross!" << endl;
					exit(26);
				}
				if (dummy_transport.end < 0)
				{
					cout << "Negativer IC-Endindex " << dummy_transport.end << " in " << transportplantfile << endl;
					exit(27);
				}
				if (dummy_transport.end >= plants.size())
				{
					cout << "IC-Endindex " << dummy_transport.end << " in " << transportplantfile << " zu gross!" << endl;
					exit(27);
				}
				if (dummy_transport.cost < 0.0)
				{
					cout << "negative IC-Kosten" << " in " << transportplantfile << endl;
					exit(28);
				}

				plant_transports.at(dummy_transport.start).at(dummy_transport.end) = true;
				transport_plant.push_back(dummy_transport);

			}
		}
	}
	transportplantdata.close();

	for (i = 0; i < plants.size(); ++i)
	{
		for (j = 0; j < plants.size(); ++j)
		{
			if (!plant_transports.at(i).at(j) && i != j)
			{
				cout << "Keine Transportinformationen von " << plants.at(i) << " nach " << plants.at(j) << " vorhanden!" << endl;
				//exit(71);
			}
		}
	}

	std::cout << "Kundentransporte..." << endl;
	ifstream transportcustomerdata(transportcustomerfile.c_str());
	if (transportcustomerdata.fail())
	{
		cout << "Kann Datei " << transportcustomerfile << " nicht oeffnen!" << endl;
		exit(1);
	}
	while (!transportcustomerdata.eof())
	{
		getline(transportcustomerdata, line);
		if (line.size() > 0)
		{

			istringstream is(line);
			is >> dummy_transport.start;
			is >> dummy_transport.end;
			is >> dummy_transport.cost;

			if (params.just_production_cost)
			{
				dummy_transport.cost = 0.0;
			}

			if (dummy_transport.start < 0)
			{
				cout << "Negativer Kundentransport-Startindex " << dummy_transport.start << " in " << transportcustomerfile << endl;
				exit(29);
			}
			if (dummy_transport.start >= plants.size())
			{
				cout << "Kundentransport-Startindex " << dummy_transport.start << " in " << transportcustomerfile << " zu gross!" << endl;
				exit(29);
			}
			if (dummy_transport.end < 0)
			{
				cout << "Negativer Kundentransport-Endindex " << dummy_transport.end << " in " << transportcustomerfile << endl;
				exit(30);
			}
			if (dummy_transport.end >= customers.size())
			{
				cout << "Kundentransport-Endindex " << dummy_transport.end << " in " << transportcustomerfile << " zu gross!" << endl;
				exit(30);
			}
			if (dummy_transport.cost < 0.0)
			{
				cout << "negative Kundentransport-Kosten in " << transportcustomerfile << endl;
				exit(31);
			}

			transport_customer.push_back(dummy_transport);
		}
	}
	transportcustomerdata.close();

	ifstream customerProductRelatedTransportData (customer_product_related_transport_file );
	if (customerProductRelatedTransportData.fail())
	{
		cout << "Kann Datei " << transportcustomerfile << " nicht oeffnen!" << endl;
		exit(1);
	}

	int currProduct;
	int currTransProcess;
	int currCustomer;
	int currPlant;
	int currEntryNum;

	if (params.customer_fixation_usage && !params.set_fixations)
	{
		if (params.customer_fixation_usage)
		{

			while (!customerProductRelatedTransportData.eof())
			{
				getline(customerProductRelatedTransportData, line);

				if (line.size() > 0)
				{
					istringstream is(line);

					is >> currEntryNum;
					is >> currProduct;
					is >> currTransProcess;

					currCustomer = transport_customer.at(currTransProcess).end;
					currPlant = transport_customer.at(currTransProcess).start;
					cpTransports.add(currCustomer, currProduct, currPlant);
				}
			}
			customerProductRelatedTransportData.close();
		}
	}
	m_is_demand.resize(products.size());
	m_is_demand_for_customer_and_product.resize(customers.size(), vector<bool>(products.size()));
	m_is_demand_for_customer_and_product_in_period.resize(customers.size(), vector<vector<bool> >(products.size(), vector<bool>(periods.size())));
	demand_for_product_in_period.resize(products.size(), vector<double>(periods.size()));

	cout << "Bedarfe..." << endl;
	ifstream demanddata(demandfile.c_str());
	if (demanddata.fail())
	{
		cout << "Kann Datei " << demandfile << " nicht oeffnen!" << endl;
		exit(1);
	}
	demand dummy_demand;
	
	
	while (!demanddata.eof())
	{
		getline(demanddata, line);
		if (line.size() > 0)
		{
			istringstream is(line);
			is >> dummy_demand.customer;
			is >> dummy_demand.product;
			is >> dummy_demand.period;
			is >> dummy_demand.amount;
			//dummy_demand.amount = round(demand, 0);

			/*if (dummy_demand.amount == 0)
			{
				continue;
			}*/
			
			if (dummy_demand.customer < 0)
			{
				cout << "Negativer Kundenindex " << dummy_demand.customer << " in " << demandfile << endl;
				exit(32);
			}
			if (dummy_demand.customer >= customers.size())
			{
				cout << "Kundenindex " << dummy_demand.customer << " in " << demandfile << " zu gross!" << endl;
				exit(32);
			}

			if (dummy_demand.product < 0)
			{
				cout << "Negativer Produktindex " << dummy_demand.product << " in " << demandfile << endl;
				exit(33);
			}
			if (dummy_demand.product >= products.size())
			{
				cout << "Produktindex " << dummy_demand.product << " in " << demandfile << " zu gross!" << endl;
				exit(33);
			}

			if (dummy_demand.period < 0)
			{
				cout << "Negativer Periodenindex " << dummy_demand.period << " in " << demandfile << endl;
				exit(34);
			}
			if (dummy_demand.period >= periods.size())
			{
				cout << "Periodenindex " << dummy_demand.period << " in " << demandfile << " zu gross!" << endl;
				exit(34);
			}

			if (dummy_demand.amount < 0)
			{
				cout << " nicht positiver Bedarf " << dummy_demand.amount << " in " << demandfile << endl;
				exit(35);
			}

			if (dummy_demand.amount >= 1)
			{
				m_is_demand.at(dummy_demand.product) = true;
				m_is_demand_for_customer_and_product.at(dummy_demand.customer).at(dummy_demand.product) = true;
				m_is_demand_for_customer_and_product_in_period.at(dummy_demand.customer).at(dummy_demand.product).at(dummy_demand.period) = true;

				
			}
			demand_for_product_in_period.at(dummy_demand.product).at(dummy_demand.period) += dummy_demand.amount;

			demands.push_back(dummy_demand);
		}
	}
	demanddata.close();

	cout << "Paletten pro Tonne..." << endl;
	ifstream palletspertondata(palletspertonfile.c_str());
	if (palletspertondata.fail())
	{
		cout << "Kann Datei " << palletspertonfile << " nicht offnen!" << endl;
		exit(1);
	}
	nPallets_per_ton.resize(products.size());
	i = 0;
	while (!palletspertondata.eof())
	{
		getline(palletspertondata, line);
		if (line.size() > 0)
		{
			istringstream is(line);
			is >> word;

			if (i >= products.size())
			{
				cout << "Anzahl der Zeilen in " << palletspertonfile << " zu gross!" << endl;
				exit(36);
			}

			nPallets_per_ton.at(i) = atof(word.c_str());

			if (nPallets_per_ton.at(i) <= 0)
			{
				cout << "Nicht-positiver Paletten-pro-Tonne-Wert " << nPallets_per_ton.at(i) << " fuer Produkt " << products.at(i) << " in " << palletspertonfile << endl;
				exit(37);
			}

			i++;
		}
	}
	if (i < products.size())
	{
		cout << "Anzahl der Zeilen in " << palletspertonfile << " zu klein!" << endl;
		exit(38);
	}
	palletspertondata.close();

	cout << "Warenkoerbe..." << endl;
	
	int customerdummy;
	baskets_for_customer.clear();
	baskets_for_customer.resize(customers.size());

	ifstream basketsdata(basketsfile.c_str());
	if (basketsdata.fail())
	{
		cout << "Kann Datei " << basketsfile << " nicht oeffnen!" << endl;
		exit(1);
	}

	while (!basketsdata.eof())
	{
		getline(basketsdata, line);
		
		if (line.size() > 0)
		{
			istringstream is(line);
			is >> customerdummy;
			vector<bool> basket_products(products.size(), false);
			if (customerdummy < 0)
			{
				cout << "Negativer Kundenindex " << customerdummy << " in " << basketsfile << endl;
				exit(39);
			}
			if (customerdummy >= customers.size())
			{
				cout << "Kundenindex " << customerdummy << " in " << basketsfile << " zu gross!" << endl;
				exit(39);
			}
			while (!is.eof())
			{
				word = "";
				is >> word;
				if (word.size() > 0)
				{
					basket_products.at(atoi(word.c_str())) = true;
				}
			}

			baskets_for_customer.at(customerdummy).push_back(basket_products);
		}
	}
	basketsdata.close();

	/*baskets_for_customer.resize(customers.size());
	vector<bool> product_in_basket(products.size());

	cout << "Warenkoerbe..." << endl;
	ifstream basketsdata(basketsfile.c_str());
	if (basketsdata.fail())
	{
		cout << "Kann Datei " << basketsfile << " nicht oeffnen!" << endl;
		exit(1);
	}
	int nr;
	vector < vector<pair<int, vector<bool> > > > sorted_baskets_for_customer(customers.size());
	bool add_basket;
	while (!basketsdata.eof())
	{
		getline(basketsdata, line);
		if (line.size() > 0)
		{
			basket dummy;
			product_in_basket.clear();
			product_in_basket.resize(products.size(), false);
			basket_products.clear();
			istringstream is(line);
			is >> dummy.customer;

			if (dummy.customer < 0)
			{
				cout << "Negativer Kundenindex " << dummy.customer << " in " << basketsfile << endl;
				exit(39);
			}
			if (dummy.customer >= customers.size())
			{
				cout << "Kundenindex " << dummy.customer << " in " << basketsfile << " zu gross!" << endl;
				exit(39);
			}


			while (!is.eof())
			{
				word = "";
				is >> word;
				if (word.size() > 0)
				{
					basket_products.push_back(atoi(word.c_str()));
				}
			}

			dummy.productindices.resize(basket_products.size());
			nr = 0;
			add_basket = true;
			for (i = 0; i < basket_products.size(); ++i)
			{
				if (basket_products.at(i) < 0)
				{
					cout << "Negativer Produktindex " << basket_products.at(i) << " in " << basketsfile << endl;
					exit(40);
				}
				if (basket_products.at(i) >= products.size())
				{
					cout << "Produktindex " << basket_products.at(i) << " in " << basketsfile << " zu gross!" << endl;
					exit(40);
				}
				//if (m_is_demand_for_customer_and_product.at(dummy.customer).at(basket_products.at(i)))
				if (m_is_demand_for_customer_and_product_in_period.at(dummy.customer).at(basket_products.at(i)).at(0) && m_is_demand_for_customer_and_product_in_period.at(dummy.customer).at(basket_products.at(i)).at(1) && m_is_demand_for_customer_and_product_in_period.at(dummy.customer).at(basket_products.at(i)).at(2))
				{
					nr++;
					dummy.productindices.at(i) = basket_products.at(i);

					product_in_basket.at(basket_products.at(i)) = true;
				}
				
			}

			if (add_basket)
			{
				sorted_baskets_for_customer.at(dummy.customer).push_back(pair<int, vector<bool> >(nr, product_in_basket));
			}
		}
	}
	basketsdata.close();
	


	int k;

	for (i = 0; i < customers.size(); ++i)
	{
		sort(sorted_baskets_for_customer.at(i).begin(), sorted_baskets_for_customer.at(i).end());
		for (j = 0; j < sorted_baskets_for_customer.at(i).size(); ++j)
		{
			nr = 0;
			for (k = 0; k < sorted_baskets_for_customer.at(i).at(j).second.size(); ++k)
			{
				if (sorted_baskets_for_customer.at(i).at(j).second.at(k))
				{
					nr++;
				}
			}
			if (nr > 0)
			{
				baskets_for_customer.at(i).push_back(sorted_baskets_for_customer.at(i).at(j).second);
			}

		}
	}*/

	cout << "historische Produktionen..." << endl;
	int processindex;
	int periodindex;
	double amount_in_pallets;
	fixations.resize(processes.size(), vector<double>(periods.size(), -1.0));
	productions.resize(processes.size(), vector<double>(periods.size(), 0));
	ifstream fixationdata(fixationfile.c_str());
	if (fixationdata.fail())
	{
		cout << "Kann Datei " << fixationfile << " nicht oeffnen!" << endl;
		exit(1);
	}
	else
	{
		while (!fixationdata.eof())
		{
			getline(fixationdata, line);
			if (line.size() > 0)
			{
				istringstream is(line);
				is >> processindex;
				is >> periodindex;
				is >> amount_in_pallets;

				if (processindex < 0)
				{
					cout << "Negativer Prozessindex " << processindex << " in " << fixationfile << endl;
					exit(41);
				}
				if (processindex >= processes.size())
				{
					cout << "Prozessindex " << processindex << " in " << fixationfile << " zu gross!" << endl;
					exit(41);
				}
				if (periodindex < 0)
				{
					cout << "Negativer Periodenindex " << periodindex << " in " << fixationfile << endl;
					exit(42);
				}
				if (periodindex >= periods.size())
				{
					cout << "Periodenindex " << periodindex << " in " << fixationfile << " zu gross!" << endl;
					exit(42);
				}
				if (amount_in_pallets <= 0)
				{
					cout << "Nicht positiver Bedarf " << amount_in_pallets << " in " << fixationfile << endl;
					exit(43);
				}

				if (m_is_demand.at(processes.at(processindex).product))
				{
					fixations.at(processindex).at(periodindex) = amount_in_pallets / (double)nPallets_per_ton.at(processes.at(processindex).product) / (double)processes.at(processindex).tonspershift;

					productions.at(processindex).at(periodindex) += amount_in_pallets;

					/*if (processindex == 2 && periodindex == 0)
					{
						cout << fixations.at(processindex).at(periodindex) << endl;
						cout << productions.at(processindex).at(periodindex) << endl;

						char op;
						cin >> op;
					}*/
				}
			}
		}
		fixationdata.close();
	}

	cout << "Werkskapazitaeten..." << endl;
	plant_capacities.clear();
	plant_capacities.resize(plants.size(),0);
	ifstream plant_capacitydata(plant_capacityfile.c_str());

	if (plant_capacitydata.fail())
	{
		cout << "Kann Datei " << plant_capacityfile << " nicht oeffnen!" << endl;
		exit(1);
	}

	int capacityint;
	int plantindex;
	while (!plant_capacitydata.eof())
	{
		getline(plant_capacitydata, line);

		if (line.size() == 0)
		{
			continue;
		}
		
		istringstream is(line);
		is >> plantindex;
		is >> capacityint;

		if (plantindex < 0)
		{
			cout << "negativer Werksindex in " << plant_capacityfile << endl;
			exit(61);
		}
		if (capacityint < 0)
		{
			cout << "negative Werkskapazitaet in " << plant_capacityfile << endl;
			exit(62);
		}

		plant_capacities.at(plantindex) = capacityint;
	}
	
	plant_capacitydata.close();

	cout << "Startbestaende..." << endl;
	start_inventory.resize(plants.size(), vector<double>(products.size(), 0.0));
	ifstream start_inventorydata(start_inventoryfile.c_str());
	int productindex;

	double amountint;
	if (start_inventorydata.fail())
	{
		cout << "Kann Datei " << start_inventoryfile << " nicht oeffnen!" << endl;
		exit(1);
	}
	while (!start_inventorydata.eof())
	{
		getline(start_inventorydata, line);
		if (line.size() == 0)
		{
			continue;
		}
		istringstream is(line);
	
		is >> plantindex;
		if (plantindex < 0 || plantindex >= plants.size())
		{
			cout << "Fehlerhafter Werksindex " << plantindex << " in Datei " << start_inventoryfile << endl;
			exit(14);
		}
		is >> productindex;
		if (productindex < 0 || productindex >= products.size())
		{
			cout << "Fehlerhafter Produktindex " << productindex << " in Datei " << start_inventoryfile << endl;
			exit(15);
		}
		is >> amountint;
		if (amountint < 0)
		{
			cout << "Fehlerhafter Startbestand " << amountint << " in Datei " << start_inventoryfile << endl;
			exit(16);
		}
		start_inventory.at(plantindex).at(productindex) = amountint;
	}
	start_inventorydata.close();

	if (params.customer_fixation_usage && !params.set_fixations)
	{
		if (params.customer_fixation_usage)
		{
			cout << "Kundenfixierungen..." << endl;
			ifstream customer_fixation_data(customer_fixation_group_file .c_str ());
			if (customer_fixation_data.fail())
			{
				std::cout << "Kann Datei " << customer_fixation_group_file << " nicht oeffnen!" << endl;
				exit(1);
			}
			int currGroup,lastGroup;
			article_customer_fixations currACFs;
			//fixation_process currFProc;
			int currFProc;
			int prodI;
			int custLocI;
			int rsI;
			int plantI;
			int customerPrioritizationI;
			CustFixationProcessInfo currProcInfo;
			currProcInfo.isPrioritized=false;
			currGroup=lastGroup =-1;
			while (!customer_fixation_data.eof())
			{	
				getline(customer_fixation_data, line);
				if (line.size ()==0)
					continue;

				istringstream is(line);
				is>> currGroup ;
				if (currGroup <0 || currGroup <lastGroup )
				{
					cout<<"Fehlerhafter Gruppenindex "<<currGroup<<" in Datei "<<customer_fixation_group_file <<endl;
					exit(17); //Fehlercode pr�fen !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				}
				is>>prodI ;
				is>>custLocI ;
				is>>currFProc;//currFProc.plant;
				//is>>currFProc .resource;
				if (params.customer_prioritized_fixations_usage)
					is>>customerPrioritizationI;
				else
					customerPrioritizationI=0;

				if (prodI<0 || prodI>=products .size ())
				{
					cout << "Fehlerhafter Produktindex " << prodI << " in Datei " << customer_fixation_group_file << endl;
					exit(15);
				}

				if (custLocI<0 || custLocI>=customers .size ())
				{
					cout << "Fehlerhafter Kundenindex " << custLocI << " in Datei " << customer_fixation_group_file << endl;
					exit(15);
				}

				if (currFProc <0 || currFProc >=processes .size () )
				{
					cout << "Fehlerhafter Prozessindex " <<currFProc << " in Datei " << customer_fixation_group_file << endl;
					exit(15);
				}

				if (customerPrioritizationI<0)
				{
					cout << "Fehlerhafte Kundenproduktions-Priorisierung " <<customerPrioritizationI << " in Datei " << customer_fixation_group_file << endl;
					exit(15);
				}
				/*if (currFProc.plant<0 || currFProc.plant>=plants .size ())
				{
					cout << "Fehlerhafter Werksindex " <<currFProc.plant << " in Datei " << customer_fixation_group_file << endl;
					exit(15);
				}

				if (currFProc .resource<0 || currFProc .resource>=resources .size ())
				{
					cout << "Fehlerhafter Maschinenindex " << currFProc .resource << " in Datei " << customer_fixation_group_file << endl;
					exit(15);
				}*/
				if (currGroup !=lastGroup )
				{
					if (lastGroup >=0)
					{
						//customer_fixations.push_back (currACFs);
						custFixations .add (currACFs);
					}

					currACFs.allowedProcesses .clear ();
					currACFs.customerLocations .clear ();
					currACFs .article =prodI;
					lastGroup =currGroup;
				}
			
				currACFs.addCustomerLocation (custLocI);

				currProcInfo.processNumber=currFProc;
				
				if (customerPrioritizationI>0)
					currProcInfo.isPrioritized=true;
				else
					currProcInfo.isPrioritized=false;

				currACFs.addAllowedProcess (currProcInfo);//(currFProc);
				//currACFs .customerLocations.push_back (custLocI );
				//currACFs .allowedProcesses .push_back (currFProc);
			}

			customer_fixation_data.close();
		
			if (currGroup>=0)
			{
				//customer_fixations.push_back (currACFs);
				custFixations .add(currACFs);
			}
		}
		
		/*
		if (params.customer_prioritized_fixations_usage)
		{
			cout << "Kundenfixierungen..." << endl;
			ifstream customer_fixation_data(customer_prioritized_fixation_group_file.c_str ());
			if (customer_fixation_data.fail())
			{
				cout << "Kann Datei " << customer_prioritized_fixation_group_file << " nicht oeffnen!" << endl;
				exit(1);
			}
			int currGroup,lastGroup;
			article_customer_fixations currACFs;
			
			int currFProc;
			int prodI;
			int custLocI;
			int rsI;
			int plantI;
			int prioritizedFlag;
			CustFixationProcessInfo currProcInfo;
			currProcInfo.isPrioritized=false;
			currGroup=lastGroup =-1;
			while (!customer_fixation_data.eof())
			{	
				getline(customer_fixation_data, line);
				if (line.size ()==0)
					continue;
				
				istringstream is(line);
				is>> currGroup ;
				if (currGroup <0 || currGroup <lastGroup )
				{
					cout<<"Fehlerhafter Gruppenindex "<<currGroup<<" in Datei "<<customer_fixation_group_file <<endl;
					exit(17); //Fehlercode pr�fen !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				}
				is>>prodI;
				is>>custLocI;
				is>>currFProc;
				is>>prioritizedFlag;
			
				if (prodI<0 || prodI>=products .size ())
				{
					cout << "Fehlerhafter Produktindex " << prodI << " in Datei " << customer_fixation_group_file << endl;
					exit(15);
				}

				if (custLocI<0 || custLocI>=customers .size ())
				{
					cout << "Fehlerhafter Kundenindex " << custLocI << " in Datei " << customer_fixation_group_file << endl;
					exit(15);
				}

				if (currFProc <0 || currFProc >=processes .size () )
				{
					cout << "Fehlerhafter Prozessindex " <<currFProc << " in Datei " << customer_fixation_group_file << endl;
					exit(15);
				}
			
				if (currGroup !=lastGroup )
				{
					if (lastGroup >=0)
					{
						custFixations .add (currACFs);
					}

					currACFs.allowedProcesses .clear ();
					currACFs.customerLocations .clear ();
					currACFs .article =prodI;
					lastGroup =currGroup;
				}
			
				currACFs.addCustomerLocation (custLocI);

				currProcInfo.processNumber=currFProc;
				if (prioritizedFlag==1)
					currProcInfo.isPrioritized=true;
				else
					currProcInfo.isPrioritized=false;
				currACFs.addAllowedProcess (currProcInfo);
			}

			customer_fixation_data.close();
		
			if (currGroup>=0)
			{
				custFixations .add(currACFs);
			}
		}*/

		ifstream resources_with_fixations_data(resources_with_fixations_file.c_str ());
		if (resources_with_fixations_data.fail())
		{
			cout << "Kann Datei " << resources_with_fixations_file << " nicht oeffnen!" << endl;
			cout<<"Ignoring file for compatibility"<<std::endl;
			//exit(1);
		}
		else
		{
			while(!resources_with_fixations_data.eof())
			{
				int resourceI;
				getline(resources_with_fixations_data,line);
				if (line.size()==0)
					continue;
				istringstream is(line);
				is>>resourceI;
				if (resourceI<0 || resourceI>resources.size())
				{
					cout << "Fehlerhafter Ressourcenindex " <<resourceI << " in Datei " << resources_with_fixations_file << endl;
					exit(16);
				}
				resourcesWithFixations.push_back(resourceI);
			}

			resources_with_fixations_data.close();
		}
	}

	
	/*cout << "fixierte Transporte: " << endl;

	fixed_transport dummyft;
	ifstream ftdata(fixed_transport_file.c_str());
	if (ftdata.fail())
	{
		cout << "Kann Datei " << fixed_transport_file << " nicht oeffnen!" << endl;
		exit(1);
	}

	while (!ftdata.eof())
	{
		getline(ftdata,line);
		if (line.size() == 0)
		{
			continue;
		}
		istringstream is(line);
		is >> dummyft.plant;
		is >> dummyft.customer;
		is >> dummyft.product;
		is >> dummyft.period;
		is >> dummyft.amount;
		is >> dummyft.cost;

		if (dummyft.plant < 0)
		{
			cout << "negativer Werksindex in " << fixed_transport_file << endl;
			exit(301);
		}
		if (dummyft.plant >= plants.size())
		{
			cout << "Werksindex " << dummyft.plant << " in " << fixed_transport_file << " zu gross!" << endl;
			exit(302);
		}
		if (dummyft.customer < 0)
		{
			cout << "negativer Kundenindex in " << fixed_transport_file << endl;
			exit(303);
		}
		if (dummyft.customer >= customers.size())
		{
			cout << "Kundenindex " << dummyft.customer << " in " << fixed_transport_file << " zu gross!" << endl;
			exit(304);
		}
		if (dummyft.period < 0)
		{
			cout << "negativer Periodenindex in " << fixed_transport_file << endl;
			exit(305);
		}
		if (dummyft.period >= periods.size())
		{
			cout << "Periodenindex " << dummyft.period << " in " << fixed_transport_file << " zu gross!" << endl;
			exit(306);
		}
		if (dummyft.amount < 0)
		{
			cout << "negative Menge " << dummyft.amount << " in " << fixed_transport_file << endl;
			exit(307);
		}

		fixed_transports.push_back(dummyft);
	}

	ftdata.close();*/


	/*
	ifstream minimum_inventoryv1data(minimum_inventoryv1file.c_str());
	minimum_inventory_v1.resize(plants.size(), vector<vector<int> >(products.size(), vector<int>(periods.size())));
	if (minimum_inventoryv1data.fail())
	{
		cout << "Kann Datei " << minimum_inventoryv1file << " nicht oeffnen!" << endl;
		exit(1);
	}
	getline(minimum_inventoryv1data, line);
	while (!minimum_inventoryv1data.eof())
	{
		getline(minimum_inventoryv1data, line);
		if (line.size() == 0)
		{
			continue;
		}
		vector<string> daten;
		readin_explode(line, daten, ';');
		if (daten.size() == 0)
		{
			continue;
		}
		if (daten.size() < 4 || daten.size() > 4)
		{
			cout << "Fehlerhafte Zeile " << line << " in Datei " << minimum_inventoryv1file << endl;
			exit(16);
		}
		plantindex = atoi(daten.at(0).c_str());
		if (plantindex < 0 || plantindex >= plants.size())
		{
			cout << "Fehlerhafter Werksindex " << plantindex << " in Datei " << minimum_inventoryv1file << endl;
			exit(17);
		}
		productindex = atoi(daten.at(1).c_str());
		if (productindex < 0 || productindex >= products.size())
		{
			cout << "Fehlerhafter Produktindex " << productindex << " in Datei " << minimum_inventoryv1file << endl;
			exit(18);
		}
		periodindex = atoi(daten.at(2).c_str());
		if (periodindex < 0 || periodindex >= periods.size())
		{
			cout << "Fehlerhafter Periodenindex " << periodindex << " in Datei " << minimum_inventoryv1file << endl;
			exit(19);
		}
		amountint = atoi(daten.at(3).c_str());
		if (amountint < 0)
		{
			cout << "Negativer Mindestbestand " << amountint << " in Datei " << minimum_inventoryv1file << endl;
			exit(20);
		}

		minimum_inventory_v1.at(plantindex).at(productindex).at(periodindex) = amountint;
	}
	minimum_inventoryv1data.close();

	ifstream minimum_inventoryv2data(minimum_inventoryv2file.c_str());
	if (minimum_inventoryv2data.fail())
	{
		cout << "Kann Datei " << minimum_inventoryv2file << " nicht oeffnen!" << endl;
		exit(1);
	}
	getline(minimum_inventoryv2data, line);
	while (!minimum_inventoryv2data.eof())
	{
		getline(minimum_inventoryv2data, line);
		if (line.size() == 0)
		{
			continue;
		}
		vector<string> daten;
		readin_explode(line, daten, ';');
		if (daten.size() == 0)
		{
			continue;
		}
		if (daten.size() < 3 || daten.size() > 3)
		{
			cout << "Fehlerhafte Zeile " << line << " in Datei " << minimum_inventoryv2file << endl;
			exit(61);
		}
		productindex = atoi(daten.at(0).c_str());
		if (productindex < 0 || productindex >= products.size())
		{
			cout << "Fehlerhafter Artikelindex " << productindex << " in Datei " << minimum_inventoryv2file << endl;
			exit(62);
		}
		periodindex = atoi(daten.at(1).c_str());
		if (periodindex < 0 || periodindex >= periods.size())
		{
			cout << "Fehlerhafter Periodenindex " << periodindex << " in Datei " << minimum_inventoryv2file << endl;
			exit(63);
		}
		amountint = atoi(daten.at(2).c_str());
		if (amountint < 0)
		{
			cout << "Negativer Mindestbestand " << amountint << " in Datei " << minimum_inventoryv2file << endl;
		}

		minimum_inventory_v2.at(productindex).at(periodindex) = amountint;
	}
	minimum_inventoryv2data.close();
	*/
}

struct BasketInfo 
{
	int customer;
	list<int>products;
};

void DataModule_NewFormat::get_input_data
						(
							std::vector<std::string> & plants, 
							std::vector<std::string> & customers, 
							std::vector<std::string> & resources, 
							std::vector<std::string> & products, 
							std::vector<std::string> & periods, 
							std::vector<production> & processes, 
							std::vector<std::vector<double> > & capacity, 
							std::vector<transports> & transport_plant, 
							std::vector<transports> & transport_customer, 
							std::vector<demand> & demands, 
							std::vector<double> & nPallets_per_ton, 
							std::vector<std::vector<double> > & fixations, 
							std::vector<std::vector<double> > & productions, 
							std::vector<std::vector<std::vector<bool> > > & baskets_for_customer, 
							std::vector<std::string> & product_names, 
							std::vector<int> & plant_capacities, 
							std::vector<std::vector<double> > & start_inventory, 
							std::vector<std::vector<std::vector<double> > > & minimum_inventory_v1, 
							std::vector<std::vector<double> > & minimum_inventory_v2, 
							std::vector<fixed_transport> & fixed_transports,
							Article_customer_fixation_collection & custFixations,
							Customer_product_related_transports_collection & cpTransports,
							std::vector<int> & resourcesWithFixations,
							vector<bool> & m_is_demand,
							vector<vector<bool> > & m_is_demand_for_customer_and_product,
							vector<vector<vector<bool> > > & m_is_demand_for_customer_and_product_in_period,
							vector<vector<double > > & demand_for_product_in_period,
							WEPA_Opt_Params &params
						  )
{	
	SimpleDataTable *articleInfoTab;
	SimpleDataTable *basketTab;
	SimpleDataTable *capacitiesTab;
	SimpleDataTable *customerFixationGroupsTab;
	SimpleDataTable *customerProductRelatedTransportsTab;
	SimpleDataTable *customerLocationsTab;
	SimpleDataTable *demandsTab;
	//DataTable *missingCustomerTransportsTab;
	SimpleDataTable *scheduledProductionTab;
	SimpleDataTable *plantLocationsTab;
	SimpleDataTable *periodsInfoTab;
	SimpleDataTable *productionProcessesTab;
	SimpleDataTable *customerTransportsTab;
	SimpleDataTable *interplantTab;
	SimpleDataTable *productionResourcesTab;

	//missingCustomerTransportsTab=missingCustomerTransportsDS->getData();
	
	plants.clear();
	customers.clear();
	resources.clear();
	products.clear();
	periods.clear();
	processes.clear();
	capacity.clear();
	transport_plant.clear();
	transport_customer.clear();
	demands.clear();
	nPallets_per_ton.clear();
	fixations.clear();
	baskets_for_customer.clear();
	product_names.clear();
	productions.clear();
	fixed_transports.clear();
	
	//customer_fixations.clear ();
	string line;
	string word;
	string product, machine, tonspershift, costspershift, plant;
	string start, end, cost;
	string customer, period, amount;

	
	cout << "Werke und Lagerkapazit�ten..." << endl;
	plantLocationsTab=plantLocationsDS->getData();
	
	int i,j;
	std::string numStr;
	std::string name;
	std::string warehouseCapacityStr;
	int num,wareHouseCapacity;
	vector<string>plantV;
	plant_capacities.clear();
	plant_capacities.resize(plantLocationsTab->NumRows(),0);
	plantV.resize(plantLocationsTab->NumRows(),"");
	for (i=0;i<plantLocationsTab->NumRows();i++)
	{
		numStr=plantLocationsTab->tab[i][0];
		name=plantLocationsTab->tab[i][1];
		warehouseCapacityStr=plantLocationsTab->tab[i][2];
		num=stoi(numStr);
		wareHouseCapacity=stoi(warehouseCapacityStr);

		//plants.push_back(name);
		plantV.at(num)=name;
		plant_capacities.at(num)=wareHouseCapacity;
	}
	for (i=0;i<plantLocationsTab->NumRows();i++)
	{
		plants.push_back(plantV.at(i));
	}


	std::cout << "Kunden..." << endl;
	
	customerLocationsTab=customerLocationsDS->getData();
	vector<string>customerV;
	customerV.resize(customerLocationsTab->NumRows(),"");
	for (i=0;i<customerLocationsTab->NumRows();i++)
	{
		numStr=customerLocationsTab->tab[i][0];
		name=customerLocationsTab->tab[i][1];
		num=stoi(numStr);
		customerV.at(num)=name;
	}
	for (i=0;i<customerLocationsTab->NumRows();i++)
	{
		customers.push_back(customerV.at(i));
	}

	cout << "Maschinen..." << endl;
	productionResourcesTab=productionResourcesDS->getData();
	vector<string>productionResourcesV;
	
	productionResourcesV.resize(productionResourcesTab->NumRows(),"");
	for (i=0;i<productionResourcesTab->NumRows();i++)
	{
		numStr=productionResourcesTab->tab[i][0];
		name=productionResourcesTab->tab[i][1];
		num=stoi(numStr);
		productionResourcesV.at(num)=name;
	}
	for (i=0;i<productionResourcesTab->NumRows();i++)
	{
		resources.push_back(productionResourcesV.at(i));
	}

	cout << "Artikel und Artikelnamen..." << endl;
	articleInfoTab=articleInfoDS->getData();
	vector<pair<string, string> > artikelnamepair;
	vector<string>articleV;
	std::string longName;
	std::string palletsPerTonStr;
	double palletsPerTonV;
	articleV.resize(articleInfoTab->NumRows(),"");
	nPallets_per_ton.resize(articleInfoTab->NumRows(),0);
	for (i=0;i<articleInfoTab->NumRows();i++)
	{
		numStr=articleInfoTab->tab[i][0];
		name=articleInfoTab->tab[i][1];
		longName=articleInfoTab->tab[i][2];
		palletsPerTonStr=articleInfoTab->tab[i][3];
		
		num=stoi(numStr);
		palletsPerTonV=stod(palletsPerTonStr);

		articleV.at(num)=name;
		artikelnamepair.push_back(pair<string, string>(name, longName));
		nPallets_per_ton.at(num)=palletsPerTonV;
	}
	for (i=0;i<articleInfoTab->NumRows();i++)
	{	
		products.push_back(articleV.at(i));
	}

	//int i, j;
	product_names.resize(products.size(),"");
	for (i = 0; i < products.size(); ++i)
	{
		for (j = 0; j < artikelnamepair.size(); ++j)
		{
			if (products.at(i) == artikelnamepair.at(j).first)
			{
				product_names.at(i) = artikelnamepair.at(j).second;
			}
		}
	}


	cout << "Perioden..." << endl;
	periodsInfoTab=periodsInfoDS->getData();
	vector<string>periodsV;
	
	periodsV.resize(periodsInfoTab->NumRows(),"");
	for (i=0;i<periodsInfoTab->NumRows();i++)
	{
		numStr=periodsInfoTab->tab[i][0];
		name=periodsInfoTab->tab[i][1];
		num=stoi(numStr);
		periodsV.at(num)=name;
	}
	for (i=0;i<periodsInfoTab->NumRows();i++)
	{
		//resources.push_back(productionResourcesV.at(i));
		periods.push_back(periodsV.at(i));
	}
	cout << "Produktionsprozesse..." << endl;

	productionProcessesTab=productionProcessesDS->getData();
	production dummy_production;
	for (i=0;i<productionProcessesTab->NumRows();i++)
	{
		dummy_production.product=stoi(productionProcessesTab->tab[i][0]);
		dummy_production.machine=stoi(productionProcessesTab->tab[i][1]);
		dummy_production.tonspershift=stod(productionProcessesTab->tab[i][3]);
		dummy_production.costspershift=stod(productionProcessesTab->tab[i][4]);
		dummy_production.product_plant_index=stod(productionProcessesTab->tab[i][2]);

		processes.push_back(dummy_production);
	}

	cout << "Maschinenkapazit�ten..." << endl;
	capacitiesTab=capacitiesDS->getData();
	capacity.resize(resources.size(), vector<double>(periods.size()));
	//std::string resourceStr;
	//std::string periodStr;
	int resource,periodI;
	for (i=0;i<capacitiesTab->NumRows();i++)
	{
		resource=stoi(capacitiesTab->tab[i][0]);
		periodI=stoi(capacitiesTab->tab[i][1]);
		capacity.at(resource).at(periodI)=stod(capacitiesTab->tab[i][2]);
	}
	
	cout << "Werkstransporte..." << endl;
	interplantTab=interplantDS->getData();
	transports dummy_transport;
	vector<vector<bool> > plant_transports(plants.size(), vector<bool>(plants.size(), false));

	for (i=0;i<interplantTab->NumRows();i++)
	{
		dummy_transport.start=stoi(interplantTab->tab[i][0]);
		dummy_transport.end=stoi(interplantTab->tab[i][1]);
		dummy_transport.cost=stoi(interplantTab->tab[i][2]);

		if (params.just_production_cost)
		{
			dummy_transport.cost = 0.0;
		}

		plant_transports.at(dummy_transport.start).at(dummy_transport.end) = true;
		transport_plant.push_back(dummy_transport);
	}
	
	for (i = 0; i < plants.size(); ++i)
	{
		for (j = 0; j < plants.size(); ++j)
		{
			if (!plant_transports.at(i).at(j) && i != j)
			{
				cout << "Keine Transportinformationen von " << plants.at(i) << " nach " << plants.at(j) << " vorhanden!" << endl;
				//exit(71);
			}
		}
	}
	
	std::cout << "Kundentransporte..." << endl;
	customerTransportsTab=customerTransportsDS->getData();
	for (i=0;i<customerTransportsTab->NumRows();i++)
	{
		dummy_transport.start=stoi(customerTransportsTab->tab[i][0]);
		dummy_transport.end=stoi(customerTransportsTab->tab[i][1]);
		dummy_transport.cost=stod(customerTransportsTab->tab[i][2]);

		if (params.just_production_cost)
		{
			dummy_transport.cost = 0.0;
		}

		//plant_transports.at(dummy_transport.start).at(dummy_transport.end) = true;
		transport_customer.push_back(dummy_transport);
	}

	std::cout<<"Kundenprodukttransporte..."<<endl;
	customerProductRelatedTransportsTab=customerProductRelatedTransportsDS->getData();
	int currProduct;
	int currTransProcess;
	int currCustomer;
	int currPlant;
	int currEntryNum;

	for (i=0;i<customerProductRelatedTransportsTab->NumRows();i++)
	{
		currEntryNum=stoi(customerProductRelatedTransportsTab->tab[i][0]);
		currProduct=stoi(customerProductRelatedTransportsTab->tab[i][1]);
		currTransProcess=stoi(customerProductRelatedTransportsTab->tab[i][2]);

		currCustomer=transport_customer.at(currTransProcess).end;
		currPlant=transport_customer.at(currTransProcess).start;
		cpTransports.add (currCustomer,currProduct,currPlant);
	}

	cout << "Bedarfe..." << endl;
	demandsTab=demandsDS->getData();
	m_is_demand.resize(products.size());
	m_is_demand_for_customer_and_product.resize(customers.size(), vector<bool>(products.size()));
	m_is_demand_for_customer_and_product_in_period.resize(customers.size(), vector<vector<bool> >(products.size(), vector<bool>(periods.size())));
	demand_for_product_in_period.resize(products.size(), vector<double>(periods.size()));

	demand dummy_demand;
	for (i=0;i<demandsTab->NumRows();i++)
	{
		dummy_demand.customer=stoi(demandsTab->tab[i][0]);
		dummy_demand.product=stoi(demandsTab->tab[i][1]);
		dummy_demand.period=stoi(demandsTab->tab[i][2]);
		dummy_demand.amount=stod(demandsTab->tab[i][3]);

		if (dummy_demand.amount >= 1)
		{
			m_is_demand.at(dummy_demand.product) = true;
			m_is_demand_for_customer_and_product.at(dummy_demand.customer).at(dummy_demand.product) = true;
			m_is_demand_for_customer_and_product_in_period.at(dummy_demand.customer).at(dummy_demand.product).at(dummy_demand.period) = true;		
		}
		demand_for_product_in_period.at(dummy_demand.product).at(dummy_demand.period) += dummy_demand.amount;

		demands.push_back(dummy_demand);
	}

	cout << "Warenkoerbe..." << endl;
	basketTab=basketDS->getData();

	int customerdummy;
	int basketNum;
	int productNum;
	baskets_for_customer.clear();
	baskets_for_customer.resize(customers.size());
	unordered_map<int,BasketInfo> productsInBasket;
	unordered_map<int,list<int>> basketIndicesForCustomer;
	list<int>basketNums;
	list<int>::iterator itB,itBEnd,itP,itPEnd;
	for (i=0;i<basketTab->NumRows();i++)
	{
		customerdummy=stoi(basketTab->tab[i][0]);
		basketNum=stoi(basketTab->tab[i][1]);
		productNum=stoi(basketTab->tab[i][2]);

		if (productsInBasket.find(basketNum)==productsInBasket.end())
		{
			productsInBasket.insert(pair<int,BasketInfo>(basketNum,BasketInfo()));
			basketNums.push_back(basketNum);
			productsInBasket.at(basketNum).customer=customerdummy;
		}

		productsInBasket.at(basketNum).products.push_back(productNum);
		//cout<<"NProducts:"<<productsInBasket.at(basketNum).products.size()<<endl;
	}

	itB=basketNums.begin();
	itBEnd=basketNums.end();

	while (itB!=itBEnd)
	{
		vector<bool> basket_products(products.size(), false);

		itP=productsInBasket.at(*itB).products.begin();
		itPEnd=productsInBasket.at(*itB).products.end();

		while (itP!=itPEnd)
		{
			basket_products.at(*itP)=true;
			itP++;
		}

		baskets_for_customer.at(productsInBasket.at(*itB).customer).push_back(basket_products);
		itB++;
	}

	cout << "historische Produktionen..." << endl;
	scheduledProductionTab=scheduledProductionDS->getData();
	int processindex;
	int periodindex;
	double amount_in_pallets;
	
	fixations.resize(processes.size(), vector<double>(periods.size(), -1.0));
	productions.resize(processes.size(), vector<double>(periods.size(), 0));

	for (i=0;i<scheduledProductionTab->NumRows();i++)
	{
		processindex=stoi(scheduledProductionTab->tab[i][0]);
		periodindex=stoi(scheduledProductionTab->tab[i][1]);
		amount_in_pallets=stod(scheduledProductionTab->tab[i][2]);

		if (m_is_demand.at(processes.at(processindex).product))
		{
			fixations.at(processindex).at(periodindex) = amount_in_pallets / 
				(double)nPallets_per_ton.at(processes.at(processindex).product) / (double)processes.at(processindex).tonspershift;
			productions.at(processindex).at(periodindex) += amount_in_pallets;
		}
	}
	
	cout << "Startbestaende..." << endl;
	start_inventory.resize(plants.size(), vector<double>(products.size(), 0.0));

	/*
	ifstream start_inventorydata(start_inventoryfile.c_str());
	int productindex;

	double amountint;
	if (start_inventorydata.fail())
	{
		cout << "Kann Datei " << start_inventoryfile << " nicht oeffnen!" << endl;
		exit(1);
	}
	while (!start_inventorydata.eof())
	{
		getline(start_inventorydata, line);
		if (line.size() == 0)
		{
			continue;
		}
		istringstream is(line);
	
		is >> plantindex;
		if (plantindex < 0 || plantindex >= plants.size())
		{
			cout << "Fehlerhafter Werksindex " << plantindex << " in Datei " << start_inventoryfile << endl;
			exit(14);
		}
		is >> productindex;
		if (productindex < 0 || productindex >= products.size())
		{
			cout << "Fehlerhafter Produktindex " << productindex << " in Datei " << start_inventoryfile << endl;
			exit(15);
		}
		is >> amountint;
		if (amountint < 0)
		{
			cout << "Fehlerhafter Startbestand " << amountint << " in Datei " << start_inventoryfile << endl;
			exit(16);
		}
		start_inventory.at(plantindex).at(productindex) = amountint;
	}
	start_inventorydata.close();
	*/

	if (params.customer_fixation_usage && !params.set_fixations)
	{
		if (params.customer_fixation_usage)
		{
			cout << "Kundenfixierungen..." << endl;
			customerFixationGroupsTab=customerFixationGroupsDS->getData();
			int currGroup,lastGroup;
			article_customer_fixations currACFs;
			int currFProc;
			int prodI;
			int custLocI;
			int rsI;
			int plantI;

			unordered_map<int,article_customer_fixations>fGroups;
			list<int>fGroupNums;
			list<int>::iterator itFG,itFGEnd;
			CustFixationProcessInfo currProcInfo;
			currProcInfo.isPrioritized=false;

			for (i=0;i<customerFixationGroupsTab->NumRows();i++)
			{
				currGroup=stoi(customerFixationGroupsTab->tab[i][0]);
				prodI=stoi(customerFixationGroupsTab->tab[i][1]);
				custLocI=stoi(customerFixationGroupsTab->tab[i][2]);
				currFProc=stoi(customerFixationGroupsTab->tab[i][3]);

				if (fGroups.find(currGroup)==fGroups.end())
				{
					fGroups.insert(pair<int,article_customer_fixations>(currGroup,article_customer_fixations()));
					fGroups.at(currGroup).article=prodI;
					fGroupNums.push_back(currGroup);
				}

				fGroups.at(currGroup).addCustomerLocation(custLocI);
			
				currProcInfo.processNumber=currFProc;
				fGroups.at(currGroup).addAllowedProcess(currProcInfo);//(currFProc);
			}
		
			itFG=fGroupNums.begin();
			itFGEnd=fGroupNums.end();

			while (itFG!=itFGEnd)
			{
				custFixations.add(fGroups.at(*itFG));
				itFG++;
			}
		}
		else if (params.customer_prioritized_fixations_usage)
		{
			cout << "Priorisierte Kundenfixierungen..." << endl;
			customerFixationGroupsTab=customerFixationGroupsDS->getData();
			int currGroup,lastGroup;
			article_customer_fixations currACFs;
			int currFProc;
			int prodI;
			int custLocI;
			int rsI;
			int plantI;

			unordered_map<int,article_customer_fixations>fGroups;
			list<int>fGroupNums;
			list<int>::iterator itFG,itFGEnd;
			CustFixationProcessInfo currProcInfo;
			currProcInfo.isPrioritized=false;

			for (i=0;i<customerFixationGroupsTab->NumRows();i++)
			{
				currGroup=stoi(customerFixationGroupsTab->tab[i][0]);
				prodI=stoi(customerFixationGroupsTab->tab[i][1]);
				custLocI=stoi(customerFixationGroupsTab->tab[i][2]);
				currFProc=stoi(customerFixationGroupsTab->tab[i][3]);

				if (fGroups.find(currGroup)==fGroups.end())
				{
					fGroups.insert(pair<int,article_customer_fixations>(currGroup,article_customer_fixations()));
					fGroups.at(currGroup).article=prodI;
					fGroupNums.push_back(currGroup);
				}

				fGroups.at(currGroup).addCustomerLocation(custLocI);
			
				currProcInfo.processNumber=currFProc;
				fGroups.at(currGroup).addAllowedProcess(currProcInfo);//(currFProc);
			}
		
			itFG=fGroupNums.begin();
			itFGEnd=fGroupNums.end();

			while (itFG!=itFGEnd)
			{
				custFixations.add(fGroups.at(*itFG));
				itFG++;
			}
		}
		/*
		ifstream customer_fixation_data(customer_fixation_group_file .c_str ());
		if (customer_fixation_data.fail())
		{
			cout << "Kann Datei " << customer_fixation_data << " nicht oeffnen!" << endl;
			exit(1);
		}
		
		currGroup=lastGroup =-1;
		while (!customer_fixation_data.eof())
		{	
			getline(customer_fixation_data, line);
			if (line.size ()==0)
				continue;

			istringstream is(line);
			is>> currGroup ;
			if (currGroup <0 || currGroup <lastGroup )
			{
				cout<<"Fehlerhafter Gruppenindex "<<currGroup<<" in Datei "<<customer_fixation_group_file <<endl;
				exit(17); //Fehlercode pr�fen !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			}
			is>>prodI ;
			is>>custLocI ;
			is>>currFProc;//currFProc.plant;
			//is>>currFProc .resource;
			
			if (prodI<0 || prodI>=products .size ())
			{
				cout << "Fehlerhafter Produktindex " << prodI << " in Datei " << customer_fixation_group_file << endl;
				exit(15);
			}

			if (custLocI<0 || custLocI>=customers .size ())
			{
				cout << "Fehlerhafter Kundenindex " << custLocI << " in Datei " << customer_fixation_group_file << endl;
				exit(15);
			}

			if (currFProc <0 || currFProc >=processes .size () )
			{
				cout << "Fehlerhafter Prozessindex " <<currFProc << " in Datei " << customer_fixation_group_file << endl;
				exit(15);
			}
			
			if (currGroup !=lastGroup )
			{
				if (lastGroup >=0)
				{
					//customer_fixations.push_back (currACFs);
					custFixations .add (currACFs);
				}

				currACFs.allowedProcesses .clear ();
				currACFs.customerLocations .clear ();
				currACFs .article =prodI;
				lastGroup =currGroup;
			}
			
			currACFs.addCustomerLocation (custLocI);
			currACFs.addAllowedProcess (currFProc);
			//currACFs .customerLocations.push_back (custLocI );
			//currACFs .allowedProcesses .push_back (currFProc);
		}

		customer_fixation_data.close();
		
		if (currGroup>=0)
		{
			//customer_fixations.push_back (currACFs);
			custFixations .add(currACFs);
		}
		*/
		
		/*
		ifstream resources_with_fixations_data(resources_with_fixations_file .c_str ());
		if (resources_with_fixations_data.fail())
		{
			cout << "Kann Datei " <<resources_with_fixations_data << " nicht oeffnen!" << endl;
			cout<<"Ignoring file for compatibility"<<std::endl;
			//exit(1);
		}
		else
		{
			while(!resources_with_fixations_data.eof())
			{
				int resourceI;
				getline(resources_with_fixations_data,line);
				if (line.size()==0)
					continue;
				istringstream is(line);
				is>>resourceI;
				if (resourceI<0 || resourceI>resources.size())
				{
					cout << "Fehlerhafter Ressourcenindex " <<resourceI << " in Datei " << resources_with_fixations_file << endl;
					exit(16);
				}
				resourcesWithFixations.push_back(resourceI);
			}

			resources_with_fixations_data.close();
		}
		*/
	}
}

/*
void DataModule_DB::finalizeCmd (string &finalizedCmd,const char *baseCmd,std::list<std::string> & fillEls)
{
	char* nStr;
	
	nStr=new char[1000];

	//sprintf(nStr,baseCmd,usedOptFolderName.c_str());
	//std::string newString(nStr);
	std::string newString;
	newString=xFormatStr(baseCmd,fillEls);

	finalizedCmd=xFormatStr(baseCmd,fillEls);//newString;
}
*/
DataModule_DB::DataModule_DB()
{
	articleInfoDS=new DataBase_Table();
	basketDS=new DataBase_Table();
	capacitiesDS=new DataBase_Table();
	customerFixationGroupsDS=new DataBase_Table();
	customerProductRelatedTransportsDS=new DataBase_Table();
	customerLocationsDS=new DataBase_Table();
	demandsDS=new DataBase_Table();
	missingCustomerTransportsDS=new DataBase_Table();
	scheduledProductionDS=new DataBase_Table();
	plantLocationsDS=new DataBase_Table();
	periodsInfoDS=new DataBase_Table();
	productionProcessesDS=new DataBase_Table();
	productionResourcesDS=new DataBase_Table();
	customerTransportsDS=new DataBase_Table();
	interplantDS=new DataBase_Table();
	/*
	((DataBase_Table*)articleInfoDS)->Cmd_s(articleInfoTabCmd);
	((DataBase_Table*)basketDS)->Cmd_s(basketTabCmd);
	((DataBase_Table*)capacitiesDS)->Cmd_s(capacitiesTabCmd);
	((DataBase_Table*)customerFixationGroupsDS)->Cmd_s(customerFixationGroupsTabCmd);
	((DataBase_Table*)customerProductRelatedTransportsDS)->Cmd_s(customerProductRelatedTransportsTabCmd);
	*/
	/*
	((DataBase_Table*)customerLocationsDS)->Cmd_s(customerProductRelatedTransportsTabCmd);
	((DataBase_Table*)demandsDS)->Cmd_s(customerProductRelatedTransportsTabCmd);
	((DataBase_Table*)missingCustomerTransportsDS)->Cmd_s(customerProductRelatedTransportsTabCmd);
	((DataBase_Table*)scheduledProductionDS)->Cmd_s(customerProductRelatedTransportsTabCmd);
	((DataBase_Table*)plantLocationsDS)->Cmd_s(customerProductRelatedTransportsTabCmd);
	((DataBase_Table*)periodsInfoDS)->Cmd_s(dB);
	((DataBase_Table*)productionProcessesDS)->Cmd_s(dB);
	((DataBase_Table*)productionResourcesDS)->Cmd_s(dB);
	((DataBase_Table*)customerTransportsDS)->Cmd_s(dB);
	((DataBase_Table*)interplantDS)->Cmd_s(dB);
	*/
}

void DataModule_DB::updateSqlCmds()
{
	std::list<std::string> fillElements;

	fillElements.push_back(usedOptFolderName);
	fillElements.push_back(usedUserId);
	fillElements.push_back(std::to_string(usedOptimizationRun));

	articleInfoTabFinalizedCmd=xFormatStr(std::string(articleInfoTabBaseCmd),fillElements);
	basketTabFinalizedCmd=xFormatStr(std::string(basketTabBaseCmd),fillElements);
	capacitiesTabFinalizedCmd=xFormatStr(std::string(capacitiesTabBaseCmd),fillElements);
	customerFixationGroupsTabFinalizedCmd=xFormatStr(std::string(customerFixationGroupsTabBaseCmd),fillElements);
	customerProductRelatedTransportsTabFinalizedCmd=xFormatStr(std::string(customerProductRelatedTransportsTabBaseCmd),fillElements);
	customerLocationsTabFinalizedCmd=xFormatStr(std::string(customerLocationsTabBaseCmd),fillElements);
	demandsTabFinalizedCmd=xFormatStr(std::string(demandsTabBaseCmd),fillElements);
	missingCustomerTransportsTabFinalizedCmd=xFormatStr(std::string(missingCustomerTransportsTabBaseCmd),fillElements);
	scheduledProductionTabFinalizedCmd=xFormatStr(std::string(scheduledProductionTabBaseCmd),fillElements);
	plantLocationsTabFinalizedCmd=xFormatStr(std::string(plantLocationsTabBaseCmd),fillElements);
	periodsInfoTabFinalizedCmd=xFormatStr(std::string(periodsInfoTabBaseCmd),fillElements);
	productionProcessesTabFinalizedCmd=xFormatStr(std::string(productionProcessesTabBaseCmd),fillElements);
	productionResourcesTabFinalizedCmd=xFormatStr(std::string(productionResourcesTabBaseCmd),fillElements);
	customerTransportsTabFinalizedCmd=xFormatStr(std::string(customerTransportsTabBaseCmd),fillElements);
	interplantTabFinalizedCmd=xFormatStr(std::string(interplantTabBaseCmd),fillElements);

	/*
	finalizeCmd(articleInfoTabFinalizedCmd,articleInfoTabBaseCmd);
	finalizeCmd(basketTabFinalizedCmd,basketTabBaseCmd);
	finalizeCmd(capacitiesTabFinalizedCmd,capacitiesTabBaseCmd);
	finalizeCmd(customerFixationGroupsTabFinalizedCmd,customerFixationGroupsTabBaseCmd);
	finalizeCmd(customerProductRelatedTransportsTabFinalizedCmd,customerProductRelatedTransportsTabBaseCmd);
	finalizeCmd(customerLocationsTabFinalizedCmd,customerLocationsTabBaseCmd);
	finalizeCmd(demandsTabFinalizedCmd,demandsTabBaseCmd);
	finalizeCmd(missingCustomerTransportsTabFinalizedCmd,missingCustomerTransportsTabBaseCmd);
	finalizeCmd(scheduledProductionTabFinalizedCmd,scheduledProductionTabBaseCmd);
	finalizeCmd(plantLocationsTabFinalizedCmd,plantLocationsTabBaseCmd);
	finalizeCmd(periodsInfoTabFinalizedCmd,periodsInfoTabBaseCmd);
	finalizeCmd(productionProcessesTabFinalizedCmd,productionProcessesTabBaseCmd);
	finalizeCmd(productionResourcesTabFinalizedCmd,productionResourcesTabBaseCmd);
	finalizeCmd(customerTransportsTabFinalizedCmd,customerTransportsTabBaseCmd);
	finalizeCmd(interplantTabFinalizedCmd,interplantTabBaseCmd);
	*/
	((DataBase_Table*)articleInfoDS)->Cmd_s(articleInfoTabFinalizedCmd);
	((DataBase_Table*)basketDS)->Cmd_s(basketTabFinalizedCmd);
	((DataBase_Table*)capacitiesDS)->Cmd_s(capacitiesTabFinalizedCmd);
	((DataBase_Table*)customerFixationGroupsDS)->Cmd_s(customerFixationGroupsTabFinalizedCmd);
	((DataBase_Table*)customerProductRelatedTransportsDS)->Cmd_s(customerProductRelatedTransportsTabFinalizedCmd);
	((DataBase_Table*)customerLocationsDS)->Cmd_s(customerLocationsTabFinalizedCmd);
	((DataBase_Table*)demandsDS)->Cmd_s(demandsTabFinalizedCmd);
	((DataBase_Table*)missingCustomerTransportsDS)->Cmd_s(missingCustomerTransportsTabFinalizedCmd);
	((DataBase_Table*)scheduledProductionDS)->Cmd_s(scheduledProductionTabFinalizedCmd);
	((DataBase_Table*)plantLocationsDS)->Cmd_s(plantLocationsTabFinalizedCmd);
	((DataBase_Table*)periodsInfoDS)->Cmd_s(periodsInfoTabFinalizedCmd);
	((DataBase_Table*)productionProcessesDS)->Cmd_s(productionProcessesTabFinalizedCmd);
	((DataBase_Table*)productionResourcesDS)->Cmd_s(productionResourcesTabFinalizedCmd);
	((DataBase_Table*)customerTransportsDS)->Cmd_s(customerTransportsTabFinalizedCmd);
	((DataBase_Table*)interplantDS)->Cmd_s(interplantTabFinalizedCmd);
}

void DataModule_DB::OptFolder_s (std::string optFolderName)
{
	usedOptFolderName=optFolderName;

	updateSqlCmds();
}


DataModule_DB::~DataModule_DB ()
{
	delete articleInfoDS;
	delete basketDS;
	delete capacitiesDS;
	delete customerFixationGroupsDS;
	delete customerProductRelatedTransportsDS;
	delete customerLocationsDS;
	delete demandsDS;
	delete missingCustomerTransportsDS;
	delete scheduledProductionDS;
	delete plantLocationsDS;
	delete periodsInfoDS;
	delete productionProcessesDS;
	delete productionResourcesDS;
	delete customerTransportsDS;
	delete interplantDS;

}
void DataModule_DB::setDB (DataBase *dB)
{
	((DataBase_Table*)articleInfoDS)->Db_s(dB);
	((DataBase_Table*)basketDS)->Db_s(dB);
	((DataBase_Table*)capacitiesDS)->Db_s(dB);
	((DataBase_Table*)customerFixationGroupsDS)->Db_s(dB);
	((DataBase_Table*)customerProductRelatedTransportsDS)->Db_s(dB);
	((DataBase_Table*)customerLocationsDS)->Db_s(dB);
	((DataBase_Table*)demandsDS)->Db_s(dB);
	((DataBase_Table*)missingCustomerTransportsDS)->Db_s(dB);
	((DataBase_Table*)scheduledProductionDS)->Db_s(dB);
	((DataBase_Table*)plantLocationsDS)->Db_s(dB);
	((DataBase_Table*)periodsInfoDS)->Db_s(dB);
	((DataBase_Table*)productionProcessesDS)->Db_s(dB);
	((DataBase_Table*)productionResourcesDS)->Db_s(dB);
	((DataBase_Table*)customerTransportsDS)->Db_s(dB);
	((DataBase_Table*)interplantDS)->Db_s(dB);
}

void DataModule_DB::OptimizationRun_s(int oR)
{
	usedOptimizationRun=oR;
	updateSqlCmds();
}
void DataModule_DB::UserId_s(std::string userId)
{
	usedUserId=userId;
	updateSqlCmds();
}